<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Michelson Exploration

While we were familiarizing with Michelson we were making some experiments to better understand how it works.
Here we present results of these experiments.

## `contract` vs `address`

Values of `contract t` and `address` types have identical representations (optimized and readable).
According to the official Michelson documentation (and our experiments):
1. The `address` type merely gives the guarantee that the value has the form of a Mavryk address.
It can be a `KT1`-address of a non-existing contract.
It can contain an entrypoint name, but this name is not checked.
2. The `contract t` type, on the other hand, guarantees that the value is indeed a valid, existing (previously originated) account whose parameter type is `t`.
If a `contract t` contains an entrypoint name, this entrypoint must have type `t`, otherwise the value is ill-typed.

Note that value of the `contract t` type can be produced in two cases:
1. It can be passed as a parameter.
Hence in order to typecheck parameter value we should know whether a contract with given address exists and what its type is.
2. By calling `CONTRACT p` instruction.
Hence we should know whether a contract with given address exists and what its type is during interpretation.

## `client run script` command

`client run script` command is a bit obscure, because normally in order to run a contract you need to originate it first and then send a transaction to it.
This command seems to do both operations at once. We don't specify its environment (e. g. contract's balance), but it works nonetheless.
Probably it uses some hardcoded values.
I ran a contract which returns its own address.
Then I passed this address to a script whose storage has contract type.
It was passed successfully, even though the contract hasn't been actually originated on alphanet (I checked it in block explorer).
So apparently it was stored somewhere locally.
I don't know how exactly it works.

## On contract's parameter type

Note: this experiment was made on the `alphanet` version/protocol of Mavryk blockchain.
There is a small chance that something has changed in the latest version.

Contract's parameter type can be easily tricked and implicitly casted when making a call (making unexpected annotation mismatch).
Let's originate contract `test.mv`:

```
storage unit;
parameter (pair (int :p) (int %q));
code { FAILWITH;
     }
```

```
mavkit-client originate contract test transferring 0 from alice running test.mv --init 0 --burn-cap 1
```

Let's say its address is `KT1RPkhRdVRc5kYPUTuRGAGde1seLtfpdyxk`.
Then let's originate another contract `test1.mv`:
```
parameter address;
storage unit;
code { CAR;
       CONTRACT (pair int int);
       ASSERT_SOME;
       AMOUNT;
       PUSH (pair (int :t) (int %s)) (Pair -1 1);
       TRANSFER_TOKENS;
       NIL operation;
       SWAP; CONS;
       UNIT; SWAP; PAIR;
     }
```

```
mavkit-client originate contract test1 transferring 0 from alice running test1.mv --init Unit --burn-cap 1
```

And finally call it passing `KT1RPkhRdVRc5kYPUTuRGAGde1seLtfpdyxk` as parameter.

```
mavkit-client transfer 0 from alice to test1 --arg '"KT1RPkhRdVRc5kYPUTuRGAGde1seLtfpdyxk"'
```

Everything works like a charm:
```
script reached FAILWITH instruction
with (Pair (Pair -1 1) Unit)
```

We effectively launched contract with parameter type `pair (int :p) (int %q)` passing `pair (int :t) (int %s)` as parameter to it.

Just for the reference: if we change `CONTRACT (pair int int)` to `CONTRACT (pair (int :t) (int %s))`, the contracts will still be well-typed, but the `CONTRACT` instruction will return `None`.
