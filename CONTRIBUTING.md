<!--
SPDX-FileCopyrightText: 2021 Oxhead Alpha
SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# Contribution Guidelines

## Reporting Issues

Please <!-- xrefcheck: ignore link --> [open an issue](https://gitlab.com/morley-framework/morley/-/issues/new)
if you find a bug or have a feature request.
Before submitting a bug report or feature request, check to make sure it hasn't already been submitted.

The more detailed your report is, the faster it can be resolved.
Please use issue templates to create an issue.

## Code

If you would like to contribute code to fix a bug, add a new feature, or
otherwise improve our project, merge requests are most welcome.

Our merge request template contains a [checklist](/.gitlab/merge_request_templates/default.md#white_check_mark-checklist-for-your-merge-request) of acceptance criteria for your merge request.
Please read it before you start contributing and make sure your contributions adhere to this checklist.

### Prelude

All Haskell code uses
[Universum](https://hackage.haskell.org/package/universum) as a
replacement for the default prelude.

### Tests

We use [`tasty`](https://hackage.haskell.org/package/tasty) as our primary top-level testing framework.
Some old code may use `hspec` instead, but all new code must use `tasty`.
We use [`tasty-discover`](https://hackage.haskell.org/package/tasty-discover) to automatically find all tests.
We still require explicit exports to ensure that we don't accidentally miss some test.
If we accidentally name some test in a way which will be ignored by `tasty-discover`, `weeder` will detect a useless export.

Some hints regarding `tasty` and our test-suite:
1. You can use `--hide-successes` to see only failing tests.
It's useful because otherwise if test suite fails you need to find the cause of failure manually.
2. However, beware of [this issue](https://github.com/feuerbach/tasty/issues/152) with `--hide-successes`.
In short, this option is somewhat broken when `tasty` thinks that it outputs to console.
A workaround is to set `TERM=dumb`.
3. You can run tests using our `Makefile`, see below.

## Cabal and Stack

We use [`hpack`](https://hackage.haskell.org/package/hpack) and `stack.yaml` to maintain the project
and its dependencies, but we also provide `.cabal` files in the repository due to
[stack issue](https://github.com/commercialhaskell/stack/issues/4906) which makes it impossible
to use `morley` as a dependency with stack version > 2 without `.cabal` files. Also we provide
`cabal.project` and `cabal.project.freeze` files in order to provide an ability to build the
project using `cabal`. If you want to update dependencies in one of these cabal related files you
should transfer changes to `package.yaml` or `stack.yaml` and run [`scripts/generate-cabal-files.sh`](scripts/generate-cabal-files.sh),
this script will update these files.

## Makefile

We have a [Makefile](/Makefile) which provides shortcuts for the most
common developers' activities, like building with flags suitable for
development, testing, applying `stylish-haskell` and `hlint`, building
Haddock documentation. Mentioned `Makefile` can build all the packages we have.
Each extra package, like [`cleveland`](/code/cleveland/Makefile),
has its own `Makefile`.

If you want to run test suite with additional options, set `TEST_ARGUMENTS` variable.
Example: `TEST_ARGUMENTS="--pattern Parser" make test`.
If you want to enable `--hide-successes` option, you can use `make test-hide-successes`.
It will automatically set `TERM=dumb` which is a workaround for the issue mentioned earlier.

## BATS tests

We use [BATS](https://github.com/bats-core/bats-core) to test the CLI interface
of our binaries. BATS scripts are in `/scripts/*.bats`. Those should be updated
when the CLI changes.

The root [`Makefile`](/Makefile) provides targets for running specific BATS
tests. Bear in mind you do need to build the binaries before running those.

Some tests will create a few temporary files. Those should be cleaned up
automatically. If you want to avoid that, set `BATS_KEEP_TEMP_FILES` make variable to something non-empty, f.ex.

```bash
make test-morley-client-bats BATS_KEEP_TEMP_FILES=y
```

You can run `make clean-bats-tmp` to remove the temp files manually.

## Branching policy

Our branching policy is described [here](/docs/branching.md).

## Protocol updates

On updates of the protocol you should:

Update Mavkit binaries (e.g. grab those from
[here](https://github.com/serokell/tezos-packaging/releases)).

Update the protocol hash in some supporting scripts:

* [`scripts/ci/typecheck-mavkit-client.sh`](scripts/ci/typecheck-mavkit-client.sh)
* [`scripts/get-micheline-exprs.sh`](scripts/get-micheline-exprs.sh)

Update the `proto` variable at the top of those script files. Protocol hashes
can be found by doing an HTTP GET request against
`/chains/main/blocks/head/protocols` on a node, e.g.

```bash
curl https://<node-base-url>/chains/main/blocks/head/protocols
```

Find public node addresses [here](https://teztnets.xyz/).

Run `get-micheline-exprs.sh` and paste its output into appropriate place in
[`code/morley/src/Morley/Micheline/Expression.hs`](code/morley/src/Morley/Micheline/Expression.hs).

Update to a new version of
[morley-infra](https://gitlab.com/morley-framework/morley-infra) that supports
the new protocol:

```bash
nix flake lock --update-input morley-infra
```

The `gitlab-ci.yml` file might also need updates to run network tests:

* Basically replace (case insensitive) all names of the old protocol with the
  name of the new protocol (f.e. kathmandu -> lima)
* In the `test-<project>-local-chain-<nnn>` stages the `nnn` should be updated
  to the new protocol version (f.ex. kathmandu -> 014, lima -> 015 etc)
* Update the port number in the `TASTY_CLEVELAND_NODE_ENDPOINT` variable.

When adding new instructions/types/etc, remember to add de-/serialization tests
to `code/tests/morley-test/Test/Serialization/Untyped/Michelson.hs`.

Deserialization code is in `code/morley/src/Morley/Micheline/Class.hs`,
`FromExp` instances, and completeness is not checked by the compiler, be mindful
of that.

## Legal

We want to make sure that our projects come with correct licensing information
and that this information is machine-readable, thus we are following the
[REUSE Practices][reuse] – feel free to click the link and read about them,
but, basically, it all boils down to the following:

  * Add the following header at the very top (but below the shebang, if there
    is one) of each source file in the repository (yes, each and every source
    file – it is not as hard as it might sound):

    ```haskell
    -- SPDX-FileCopyrightText: 2022 Oxhead Alpha
    -- SPDX-License-Identifier: LicenseRef-MIT-OA
    ```

    (This is an example for Haskell; adapt it as needed for other languages.)

    The license identifier should be the same as the one in the `LICENSE` file.

  * If you are copying any source files from some other project, and they do not
    contain a header with a copyright and a machine-readable license identifier,
    add it, but be extra careful and make sure that information you are recording
    is correct.

    If the license of the file is different from the one used in the project and
    you do not plan to relicense it, use the appropriate license identifier and
    make sure the license text exists in the `LICENSES` directory.

    If the file contains the entire license in its header, it is best to move the
    text to a separate file in the `LICENSES` directory and leave a reference.

  * If you are copying pieces of code from some other project, leave a note in the
    comments, stating where you copied it from, who is the copyright owner, and
    what license applies.

  * All the same rules apply to documentation that is stored in the repository.

These simple rules should cover most of situation you are likely to encounter.
In case of doubt, consult the [REUSE Practices][reuse] document.

[reuse]: https://reuse.software/spec/
