<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Mavryk example contracts

The contracts in this directory are taken from the [mavryk repository][mv:contracts].

Note: we modify some contracts; check the list below.

## Update instructions

These contracts are occasionally updated in the [mavryk repository][mv:contracts] and we synchronize them from time to time.
Here are some things you should keep in mind:
1. Update the link to the contracts in this file.
2. List of our modifications that should persist unless they're obviated:
  + Whitespace is regularized according to CI
  + `ticket_wallet_fungible.mv` contains a workaround and a to-do
  + `view_op_toplevel_inconsistent_input_type.mv`: we modify the input type to nat, for the sake of uniformity
  + Several contracts testing sapling transactions have a note about typechecking but not implementing sapling functionality:
    + `ill_typed/pack_sapling_state.mv`
    + `ill_typed/sapling_build_empty_state_with_int_parameter.mv`
    + `ill_typed/unpack_sapling_state.mv`
    + `opcodes/sapling_empty_state.mv`
3. Carefully check the [Contracts.hs](/code/tests/test-common/Test/Util/Contracts.hs) module:
  + `wellTypedContractDirs` should list all folders with well-typed contracts; if folder structure of the [mavryk repository][mv:contracts] changes, update the list there.
  + We have to distinguish ill-typed and unparseable contracts, and our parser is not the same as in Mavryk, so there are ill-typed contracts that are not parseable by our code. `illTypedContractDirs` contains a list of directories with parseable ill-typed contracts; `unparsableContracts`, a list of unparseable contracts outside of `/contracts/unparsable` directory; `unsupportedContracts`, a list of unsupported contracts from the paragraph (3) above.
  + If you observe new `/ill-typed` of `/legacy` contracts that `morley` does not parse, feel free to simply add them to the `unparsableContracts` list. If some reference `/ill-typed` or `/legacy` contracts do typecheck by `morley`, or some well-typed reference contracts fail typechecking, the easiest workaround is putting it into `unsupportedContracts`, but it may warrant a fix to `morley` or a separate issue; please also document it here.

## License

[MIT][mv:license] © 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>

[mv:contracts]: https://gitlab.com/mavryk-network/mavryk-protocol/-/tree/3b555f29ca0bb32165d2501429b0f34364f387a9/michelson_test_scripts
[mv:license]: ../../LICENSES/LicenseRef-MIT-Tezos.txt
