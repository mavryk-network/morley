# SPDX-FileCopyrightText: 2021-2022 Tezos Commons
# SPDX-License-Identifier: LicenseRef-MIT-TC

# These are approximate types of one of the https://github.com/tezos-commons/baseDAO contracts

parameter
  (or (or (or (or (or (unit %accept_ownership) (unit %default))
                  (or (pair %propose (address %from) (nat %frozen_token) (bytes %proposal_metadata))
                      (pair %transfer_contract_tokens
                         (address %contract_address)
                         (list %params
                            (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))))))
              (address %transfer_ownership))
          (unit %custom_entrypoints))
      (or (or (or (bytes %drop_proposal) (nat %flush)) (or (nat %freeze) (nat %unfreeze)))
          (or (or (list %unstake_vote bytes)
                  (list %update_delegate (pair (bool %enable) (address %delegate))))
              (list %vote
                 (pair (pair %argument
                          (pair (address %from) (bytes %proposal_key))
                          (nat %vote_amount)
                          (bool %vote_type))
                       (option %permit (pair (key %key) (signature %signature)))))))) ;
storage
  (pair (pair (pair (pair (pair (address %admin)
                                (pair %config
                                   (pair (pair (pair (nat %fixed_proposal_fee_in_token) (nat %governance_total_supply))
                                               (int %max_quorum_change)
                                               (int %max_quorum_threshold))
                                         (pair (int %min_quorum_threshold) (nat %period))
                                         (nat %proposal_expired_level)
                                         (nat %proposal_flush_level))
                                   (int %quorum_change)))
                          (big_map %delegates (pair (address %owner) (address %delegate)) unit)
                          (pair %extra
                             (map %handler_storage string bytes)
                             (big_map %lambdas
                                string
                                (pair (pair (lambda %code
                                               (pair (pair (map %handler_storage string bytes) (bytes %packed_argument))
                                                     (pair %proposal_info
                                                        (address %from)
                                                        (nat %frozen_token)
                                                        (bytes %proposal_metadata)))
                                               (pair (pair (option %guardian address) (map %handler_storage string bytes))
                                                     (list %operations operation)))
                                            (lambda %handler_check (pair bytes (map string bytes)) unit))
                                      (bool %is_active)))))
                    (pair (big_map %freeze_history
                             address
                             (pair (pair (nat %current_stage_num) (nat %current_unstaked))
                                   (nat %past_unstaked)
                                   (nat %staked)))
                          (nat %frozen_token_id))
                    (nat %frozen_total_supply)
                    (pair %governance_token (address %address) (nat %token_id)))
              (pair (pair (address %guardian) (big_map %metadata string bytes))
                    (option %ongoing_proposals_dlist
                       (pair (bytes %first) (bytes %last) (big_map %map (pair bytes bool) bytes)))
                    (address %pending_owner))
              (pair (nat %permits_counter)
                    (big_map %proposals
                       bytes
                       (pair (pair (pair (nat %downvotes) (bytes %metadata))
                                   (address %proposer)
                                   (nat %proposer_frozen_token))
                             (pair (nat %quorum_threshold) (nat %start_level))
                             (nat %upvotes)
                             (nat %voting_stage_num))))
              (pair %quorum_threshold_at_cycle
                 (pair (nat %last_updated_cycle) (nat %quorum_threshold))
                 (nat %staked))
              (big_map %staked_votes (pair address bytes) nat))
        (nat %start_level)) ;
code { CDR; NIL operation; PAIR; };
