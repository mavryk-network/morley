#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script checks that all contracts in `contracts/` directory
# are correctly classified as well/ill-typed using mavkit-client.
# As a first  argument it accepts path to `morley` executable.
# This script expects 'mavkit-client' to be in PATH.
set -euo pipefail

morley=$1

# NOTE: When a new protocol is released and this variable is updated,
# please update `scripts/get-micheline-exprs.sh` as well.
#
# Protocol hashes can be found in: https://gitlab.com/mavryk-network/mavryk-protocol/-/tree/
# In the file `src/proto_<version>/lib_protocol/MAVRYK_PROTOCOL`
proto=${TYPECHECK_MAVKIT_CLIENT_PROTO:-"PtNairobiyssHuh87hEhfVBGCVrK3WnS8Z2FT4ymB5tAa4r1nQf"}

mavryk_client_args=(--mode mockup --protocol "$proto")

well_typed_by_extension () {
    well_typed_contracts=()
    while IFS= read -r -d $'\0'; do
        well_typed_contracts+=("$REPLY")
    done < <(find ./ -path ./examples -prune -o \
      -path ./contracts/ill-typed -prune -o \
      -path ./contracts/unparsable -prune -o \
      -path ./contracts/mavryk_examples/ill_typed -prune -o \
      -path ./contracts/mavryk_examples/legacy -prune -o \
      -path './contracts/verbose-typecheck/*-fail.*' -prune -o \
      -path ./contracts/mavryk_examples/unsupported -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/999_constant.mv -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/constant_unit.mv -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/constant_entrypoints.mv -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/tx_rollup_deposit_013_014.mv -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/tx_rollup_deposit_015_015.mv -prune -o \
      -path ./contracts/mavryk_examples/mini_scenarios/add_clear_tickets.mv -prune -o \
      -name "$1" -print0)
}

ill_typed_by_extension () {
    ill_typed_contracts=()
    while IFS= read -r -d $'\0'; do
        ill_typed_contracts+=("$REPLY")
    done < <(find ./contracts/ill-typed ./contracts/unparsable \
                  ./contracts/mavryk_examples/ill_typed \
                  ./contracts/mavryk_examples/legacy \
                  -path './contracts/verbose-typecheck/*-fail.*' -o \
                  -path ./contracts/mavryk_examples/unsupported -prune -o \
                  -name "$1" -print0)
}

export MAVRYK_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y
set -e
well_typed_by_extension "*.mv"
for f in "${well_typed_contracts[@]}"; do
    echo "$f"
    mavkit-client "${mavryk_client_args[@]}" typecheck script "$f"
    "$morley" print --contract "$f" -o tmp.mv
    mavkit-client "${mavryk_client_args[@]}" typecheck script tmp.mv
done
ill_typed_by_extension "*.mv"
set +e
well_typed_bug_contract="./contracts/ill-typed/annotation_mismatch_iter.mv"
well_typed_bug_contract_2="./contracts/mavryk_examples/ill_typed/badly_indented.mv"  # we're more permissive about whitespace
for f in "${ill_typed_contracts[@]}"; do
    echo "$f"
      if [[ $f != "$well_typed_bug_contract" ]]; then
          if mavkit-client "${mavryk_client_args[@]}" typecheck script "$f"; then
              echo "$f treated as well-typed by mavkit-client"
              exit 1
          fi
          if [[ $f != "$well_typed_bug_contract_2" ]]; then
              "$morley" print --contract "$f" -o tmp.mv
              if mavkit-client "${mavryk_client_args[@]}" typecheck script tmp.mv; then
                  echo "$f treated as well-typed by mavkit-client after \"morley print --contract $f\""
                  exit 1
              fi
          fi
      fi
done
