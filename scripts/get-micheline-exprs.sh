#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script uses `mavkit-client` to get a list of all the micheline primitives,
# in the correct order.
# We use it to update our micheline serializer/deserializer in `Morley.Micheline.Expression`.

# NOTE: When a new protocol is released and this variable is updated,
# please update `scripts/ci/typecheck-mavkit-client.sh` as well.
#
# Protocol hashes can be found in: https://gitlab.com/mavryk-network/mavryk-protocol/-/tree/
# In the file `src/proto_<version>/lib_protocol/MAVRYK_PROTOCOL`
proto="PtNairobiyssHuh87hEhfVBGCVrK3WnS8Z2FT4ymB5tAa4r1nQf"

export MAVRYK_CLIENT_UNSAFE_DISABLE_DISCLAIMER=YES

buf=""

for n in {0..999}; do
  hex=$(printf "%02x" "$n");

  # Run mavkit-client to get the primitive that corresponds to the binary code $hex.
  # We use `--mode mockup` to avoid needing a live node.
  # However, using `--mode mockup` might emit warnings like `Base directory /<dir>/.mavryk-client is non empty.`,
  # so we pipe stderr to /dev/null.
  primitive=$(mavkit-client --mode mockup --protocol $proto convert data "0x03$hex" from binary to michelson 2> /dev/null);

  # If `mavkit-client` did not print a primitive to stdout, run the command again, this time printing
  # stderr to the console.
  if [ -z "$primitive" ] ; then
    echo "  $buf"
    set -euo pipefail
    mavkit-client --mode mockup --protocol $proto convert data "0x03$hex" from binary to michelson
  fi

  if [ "$n" -eq 0 ]; then
    sep="="
  else
    sep="|"
  fi

  cur="$sep Prim_$primitive "

  oldbuf="$buf"
  buf="$buf$cur";

  if [ ${#buf} -gt 80 ] ; then
    echo "  $oldbuf"
    buf="$cur"
  fi
done;
