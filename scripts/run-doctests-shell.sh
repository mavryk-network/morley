#!/bin/bash

# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# A hacky alternative to `./run-doctests.hs`
#
# Use WITH_GHC environment variable to set custom path to GHC; otherwise the
# value from `cabal.project` is used.

# NB: Default extensions are pulled from `code/hpack/definitions.yaml`; if some
# project needs a different set of extensions, the script would have to be
# amended to work on per-project basis, same as `./run-doctests.hs`.

dir="$(dirname "$0")"
root="$dir/../"

cd "$root" || exit 1

WITH_GHC="${WITH_GHC:-"$(sed -rn '/with-compiler:/ { s/^.*:\s*//; p;}' 'cabal.project')"}"
readarray -t exts < \
  <(sed -rn '/&default-extensions/,/^$/ { /&default-extensions/ d; /^$/ d; s/^\s*-\s*/-X/; p;}' \
    'code/hpack/definitions.yaml')

cabal-docspec -w "$WITH_GHC" "${exts[@]}" "$@"
