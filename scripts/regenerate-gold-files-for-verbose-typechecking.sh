#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

go() {
tests_dir="$1"
for test_file in "$tests_dir"/*.mv; do
    base_file="${test_file%.*}"
    gold_file="${base_file}.gold"
    spdx="SPDX" # otherwise reuse gets confused
    copyright="# ${spdx}-FileCopyrightText: 2023 Oxhead Alpha
# ${spdx}-License-Identifier: LicenseRef-MIT-OA"
    if [ -f "${gold_file}" ]; then
      copyright="$(grep -E '^# *SPDX-' "${gold_file}")"
    fi
    cat > "$gold_file" <<EOF
${copyright}
EOF
    morley typecheck --contract "$test_file" --verbose 2>&1 \
        | sed 's/[[:space:]]*$//' \
        | sed ':loop /^$/{$d;N;bloop}' >> "$gold_file"  \
        || true
done
}

go "contracts/verbose-typecheck"
go "contracts/ill-typed"
