#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  bin_dir="tmp"
  contract="contracts/mavryk_examples/attic/add1.mv"
  contract_with_ep="contracts/entrypoints/contract1.mv"
  db="bats_db.json"
  if [[ $USE_LATEST == "True" ]]; then
    morley="./scripts/morley.sh --latest"
  else
    morley="./scripts/morley.sh"
  fi
  genesisAddress="mv1LZtp8QfCCHL5NqVQSdrHNRAENk9M4hrHW"
  HOME="$(mktemp -d --tmpdir="$PWD")"
  alias="bob"
}

@test "invoking ./morley.sh without arguments" {
  $morley
}

@test "invoking ./morley.sh --help" {
  $morley --help
}

@test "invoking ./morley.sh --version" {
  $morley --version
}

@test "invoking ./morley.sh typecheck with correct arguments" {
  $morley typecheck --contract "$contract" --verbose
}

@test "invoking ./morley.sh emulate run with correct arguments" {
  $morley emulate run --contract "$contract" \
    --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --verbose \
    --now 0 --max-steps 1000 --balance 100 --write
  $morley emulate run --contract "$contract_with_ep" \
    --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --entrypoint a \
    --verbose --now 0 --max-steps 1000 --balance 100 --write
  rm "$db"
}

@test "invoking ./morley.sh emulate run typechecking can be laxed or not" {
  $morley emulate run --contract contracts/mavryk_examples/opcodes/ticket_read.mv \
    --storage '"mv1G7PrKfppT1yVcQ9dPZ3XhhGqSBPGgBpR9"' \
    --parameter 'Pair "mv1G7PrKfppT1yVcQ9dPZ3XhhGqSBPGgBpR9" 42 1' \
    --typecheck-lax
  run $morley emulate run --contract contracts/mavryk_examples/opcodes/ticket_read.mv \
    --storage '"mv1G7PrKfppT1yVcQ9dPZ3XhhGqSBPGgBpR9"' \
    --parameter 'Pair "mv1G7PrKfppT1yVcQ9dPZ3XhhGqSBPGgBpR9" 42 1' \
  [ "$status" -ne 0 ]
}

@test "invoking ./morley.sh emulate originate with correct arguments" {
  $morley emulate originate --contract "$contract" \
    --db "$db" \
    --originator $genesisAddress \
    --delegate $genesisAddress \
    --alias "$alias" \
    --storage 1 --balance 1 --verbose
  rm "$db"
}

@test "invoking ./morley.sh emulate originate without db argument" {
  $morley emulate originate --contract "$contract" \
    --originator $genesisAddress \
    --delegate $genesisAddress \
    --storage 1 --balance 1 --verbose
}

@test "invoking ./morley.sh emulate transfer with correct arguments" {
  $morley emulate transfer --db "$db" \
    --to $genesisAddress \
    --sender $genesisAddress \
    --parameter Unit --amount 1 --now 0 --max-steps 1000 \
    --verbose --dry-run
}

@test "invoking ./morley.sh emulate transfer (by address alias) with correct arguments" {
  $morley emulate originate --contract "$contract" \
    --db "$db" \
    --originator $genesisAddress \
    --delegate $genesisAddress \
    --alias "$alias" \
    --storage 1 --balance 1 --verbose
  $morley emulate transfer --db "$db" \
    --to "$alias" \
    --sender $genesisAddress \
    --parameter 1 --amount 1 --now 0 --max-steps 1000 \
    --verbose --dry-run
  rm "$db"
}

@test "invoking ./morley.sh print with correct arguments" {
  $morley print --contract "$contract"
}

@test "run contract with big_map passed in storage" {
  $morley emulate run --contract \
    contracts/big_map_in_storage.mv  --storage 'Pair {} 0' --parameter 1
  $morley emulate run --contract \
    contracts/big_map_in_storage.mv  --storage 'Pair {Elt 3 5; Elt 4 6} 0' --parameter 1
  run $morley emulate run --contract \
    contracts/big_map_in_storage.mv  --storage 'Pair {Elt 4 5; Elt 3 6} 0' --parameter 1
  [ "$status" -ne 0 ]
}

@test "passing negative numeric value inappropriately" {
  run $morley emulate run --max-steps=-1
  [ "$status" -ne 0 ]
  [[ "$output" == *"failed to parse command-line numeric argument due to overflow/underflow"* ]]
}

@test "invoking morley to typecheck contract with cyrillic comments from stdin" {
  cat contracts/add1_with_cyrillic_comments.mv | $morley typecheck
}

@test "invoking morley to print contract with cyrillic comments" {
  $morley print --contract contracts/add1_with_cyrillic_comments.mv
}

@test "invoking morley expecting it to fail to parse an invalid contract with non-ascii characters in error message" {
  $morley typecheck --contract contracts/unparsable/non_ascii_error.mv 2>&1 \
    | grep "expecting \"%%\", '%', '@', or end of input"
}

@test "invoking ./morley.sh print -o" {
  $morley print --contract contracts/mavryk_examples/opcodes/first.mv -o output.mv
  rm output.mv
}

@test "invoking ./morley.sh optimize -o" {
  $morley print --contract contracts/mavryk_examples/opcodes/first.mv -o output.mv
  rm output.mv
}

@test "invoking morley to analyze mavryk_examples/attic/forward.mv" {
  $morley analyze --contract contracts/mavryk_examples/attic/forward.mv | grep '"buyer": 2'
}

@test "invoking morley repl" {
  $morley repl --map-dir stacks --latest <<EOF
PUSH nat 5
:dumpstack stacks/stack.json
:help
:stack
:loadstack stacks/stack.json
:clear
:quit
EOF
  grep "nat" stacks/stack.json
}
