#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ
#
# Test whether morley utilities work correctly independent of a system
# locale. For more detailed description of the problem (and also
# chosen approach to solve it) see
# https://serokell.io/blog/haskell-with-utf8.
#
# Tests expect morley executables and mavkit-client to be in PATH.

export LC_ALL=C # to discover encoding issues

contract="contracts/mavryk_examples/attic/add1.mv"
contract_with_ep="contracts/entrypoints/contract1.mv"
db="bats_db.json"
genesisAddress="mv1LZtp8QfCCHL5NqVQSdrHNRAENk9M4hrHW"
alias="bob"

@test "invoking morley emulate run" {
    morley emulate run --contract "$contract" \
           --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --verbose \
           --now 0 --max-steps 1000 --balance 100 --write
    morley emulate run --contract "$contract_with_ep" \
           --db "$db" --storage 1 --parameter 1 --amount 1 --level 1 --entrypoint a \
           --verbose --now 0 --max-steps 1000 --balance 100 --write
    rm "$db"
}

@test "invoking morley emulate originate" {
    morley emulate originate --contract "$contract" \
           --alias "$alias" \
           --db "$db" \
           --originator $genesisAddress \
           --delegate $genesisAddress \
           --storage 1 --balance 1 --verbose
    rm "$db"
}

@test "invoking morley emulate transfer" {
    morley emulate transfer --db "$db" \
           --to $genesisAddress \
           --sender $genesisAddress \
           --parameter Unit --amount 1 --now 0 --max-steps 1000 \
           --verbose --dry-run
}

@test "invoking morley emulate transfer (by address alias)" {
    morley emulate originate --contract "$contract" \
           --alias "$alias" \
           --db "$db" \
           --storage 1 --balance 1 --verbose
    morley emulate transfer --db "$db" \
           --to "$alias" \
           --sender $genesisAddress \
           --parameter 1 --amount 1 --now 0 --max-steps 1000 \
           --verbose --dry-run
}

@test "invoking morley to typecheck contract with cyrillic comments from stdin" {
    cat contracts/add1_with_cyrillic_comments.mv | morley typecheck
}

@test "invoking morley to print contract with cyrillic comments" {
    morley print --contract contracts/add1_with_cyrillic_comments.mv
}

@test "invoking morley expecting it to fail to parse an invalid contract with non-ascii characters in error message" {
    morley typecheck --contract contracts/unparsable/non_ascii_error.mv 2>&1 | grep "expecting \"%%\", '%', '@', or end of input"
}

@test "originating a contract from cyrillic alias with morley client" {
    tempdir="$(mktemp -d --tmpdir="$PWD")"
    mavryk_client_args=(-E "$TASTY_CLEVELAND_NODE_ENDPOINT" -d "$tempdir")

    alias="кириллический-псевдоним"
    mavkit-client "${mavryk_client_args[@]}" \
                 import secret key $alias \
                 "$TASTY_CLEVELAND_MONEYBAG_SECRET_KEY" --force

    mavkit-client "${mavryk_client_args[@]}" list known addresses

    morley-client "${mavryk_client_args[@]}" get-balance --addr $alias

    morley-client "${mavryk_client_args[@]}" originate \
                  --contract ./contracts/add1_with_cyrillic_comments.mv \
                  --initial-storage 1 \
                  --from $alias -VV # double verbose to print alias

    rm -r "$tempdir"
}
