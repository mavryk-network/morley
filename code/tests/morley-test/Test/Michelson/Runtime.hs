-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for "Morley.Michelson.Runtime".

module Test.Michelson.Runtime
  ( test_executorPure
  ) where

import Control.Lens (at)
import Data.Default (def)
import Fmt (pretty)
import Test.Hspec.Expectations (Expectation, expectationFailure)
import Test.HUnit (Assertion, assertFailure, (@?), (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Michelson.Interpret
import Morley.Michelson.Runtime hiding (transfer)
import Morley.Michelson.Runtime.Dummy
  (dummyBigMapCounter, dummyContractEnv, dummyGlobalCounter, dummyLevel, dummyMaxSteps,
  dummyMinBlockTime, dummyNow, dummyOrigination)
import Morley.Michelson.Runtime.GState
  (BigMapCounter, gsContractAddressAliasesL, gsContractAddressesL, initGState)
import Morley.Michelson.Text (MText)
import Morley.Michelson.Typed
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Operation (OriginationOperation(..), TransferOperation(..))
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias (Alias(..))
import Test.Cleveland.Instances ()

test_executorPure :: IO [TestTree]
test_executorPure = do
  pure
    [ testGroup "Updates storage value of executed contract" $
      [ testCase "contract1" $ updatesStorageValue contractAux1
      , testCase "contract2" $ updatesStorageValue contractAux2
      ]
    , testCase "Succeeds to originate the same contract twice, with different addresses"
        succeedsToOriginateTwice
    , testCase "Associates newly originated contract with an alias" addsAddressAlias
    , testCase "Overwrites address for existing alias" overwritesAddressForExistingAlias
    ]

----------------------------------------------------------------------------
-- Test code
----------------------------------------------------------------------------

-- | Data type, that containts contract and its auxiliary data.
data ContractAux cp st = ContractAux
  { caContract :: T.Contract cp st
  , caBigMapCounter :: BigMapCounter
  , caEnv :: ContractEnv
  , caStorage :: T.Value st
  , caParameter :: T.Value cp
  }

addsAddressAlias :: Expectation
addsAddressAlias = either (assertFailure . pretty) handleResult $
  runExecutorM dummyNow dummyLevel dummyMinBlockTime dummyMaxSteps def initGState $
    executeGlobalOrigination origination
  where
    origination = (contractAuxToOrigination contractAux1) { ooAlias = Just alias }
    alias = ContractAlias "alice"

    handleResult :: (ExecutorRes, ContractAddress) -> Expectation
    handleResult (res, addr) =
      case res ^. erGState . gsContractAddressAliasesL . at alias of
        Nothing -> expectationFailure $ "Alias " <> pretty alias <> " doesn't exist"
        Just addr' -> addr' @?= addr

overwritesAddressForExistingAlias :: Expectation
overwritesAddressForExistingAlias =
  either (assertFailure . pretty) handleResult $
    runExecutorM dummyNow dummyLevel dummyMinBlockTime dummyMaxSteps def initGState $
      executeGlobalOrigination origination1 *>
      executeGlobalOrigination origination2
  where
    contract = caContract contractAux1
    origination1 = (dummyOrigination (caStorage contractAux1) contract 0) { ooAlias = Just alias }
    origination2 = (dummyOrigination (caStorage contractAux1) contract 1) { ooAlias = Just alias }
    alias = ContractAlias "alice"
    handleResult :: (ExecutorRes, ContractAddress) -> Expectation
    handleResult (res, addr) =
      case res ^. erGState . gsContractAddressAliasesL . at alias of
        Nothing -> expectationFailure $ "Alias " <> pretty alias <> " doesn't exist"
        Just addr' -> addr' @?= addr

updatesStorageValue
  :: forall cp st. (ParameterScope cp, StorageScope st)
  => ContractAux cp st -> Assertion
updatesStorageValue ca = either (assertFailure . pretty) handleResult $ do
  let
    ce = caEnv ca
    origination = contractAuxToOrigination ca
    txData = TxData
      { tdSenderAddress = ceSender ce
      , tdParameter = TxTypedParam $ caParameter ca
      , tdEntrypoint = DefEpName
      , tdAmount = 100
      }

  runExecutorM dummyNow dummyLevel dummyMinBlockTime dummyMaxSteps def initGState $ do
    addr <- executeGlobalOrigination origination
    executeGlobalOperations [TransferOp $ TransferOperation (MkAddress addr) txData 1]
    return addr
  where
    toNewStorage :: ContractResult st -> SomeValue
    toNewStorage = SomeValue . snd . extractValOps . rslResult

    handleResult :: (ExecutorRes, ContractAddress) -> Assertion
    handleResult (ir, addr) = do
      expectedValue <-
        either (assertFailure . pretty) (pure . toNewStorage) $
        handleReturn $
        interpret
          (caContract ca)
          unsafeEpcCallRoot
          (caParameter ca)
          (caStorage ca)
          dummyGlobalCounter
          (caBigMapCounter ca)
          (caEnv ca)
      case ir ^. erGState . gsContractAddressesL . at addr of
        Nothing -> expectationFailure $ "Address not found: " <> pretty addr
        Just ContractState{..} -> SomeValue csStorage @?= expectedValue

succeedsToOriginateTwice :: Expectation
succeedsToOriginateTwice = either (assertFailure . pretty) handleResult $ do
  runExecutorM dummyNow dummyLevel dummyMinBlockTime dummyMaxSteps def initGState $ do
    addr1 <- executeGlobalOrigination origination1
    addr2 <- executeGlobalOrigination origination2
    return (addr1, addr2)
  where
    contract = caContract contractAux1
    origination1 = dummyOrigination (caStorage contractAux1) contract 0
    origination2 = dummyOrigination (caStorage contractAux1) contract 1

    handleResult :: (ExecutorRes, (ContractAddress, ContractAddress)) -> Assertion
    handleResult (_, (addr1, addr2)) =
      addr1 /= addr2 @? "Two originated addresses are not different"

----------------------------------------------------------------------------
-- Data
----------------------------------------------------------------------------

contractAux1 :: ContractAux 'TString 'TBool
contractAux1 = ContractAux
  { caContract = contract
  , caBigMapCounter = dummyBigMapCounter
  , caEnv = dummyContractEnv
  , caStorage = toVal True
  , caParameter = toVal ("aaa" :: MText)
  }
  where
    contract :: Contract 'TString 'TBool
    contract = Contract
      { cParamNotes = starParamNotes
      , cStoreNotes = starNotes
      , cCode = mkContractCode $
          CDR `Seq` NIL `Seq` PAIR
      , cEntriesOrder = def
      , cViews = def
      }

contractAux2 :: ContractAux 'TString 'TBool
contractAux2 = contractAux1
  { caContract = (caContract contractAux1)
    { cCode = mkContractCode $
        CDR `Seq` NOT `Seq` NIL `Seq` PAIR
    }
  }

contractAuxToOrigination
  :: (ParameterScope cp , StorageScope st)
  => ContractAux cp st -> OriginationOperation
contractAuxToOrigination ca =
  let contract = caContract ca
      ce = caEnv ca
      originationOp = dummyOrigination (caStorage ca) contract dummyGlobalCounter
   in originationOp {ooBalance = ceBalance ce}
