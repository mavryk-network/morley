-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Typecheck.Annotation
  ( test_variableAnnotations
  ) where

import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Michelson.ErrorPos (ErrorSrcPos(..), srcPos)
import Morley.Michelson.Typed
import Morley.Michelson.Untyped (noAnn)
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson.Import (importContract)
import Test.Util.Contracts (inContractsDir, (</>))

test_variableAnnotations :: [TestTree]
test_variableAnnotations =
  [ testCase "Annotations are preserved in cadr_annotation.mv" $ do
      let file = inContractsDir ("mavryk_examples" </> "attic" </> "cadr_annotation.mv")
          param = "param"
          no_name = "no_name"
          name = "name"
      contract <-
        importContract @('TPair ('TPair 'TUnit 'TString) 'TBool) @'TUnit file
      unContractCode (cCode contract) @?= loc 2 5 (Nested $
          loc 2 7 (AnnCAR (Anns2 param noAnn))
          `Seq` loc 2 19 (Nested (
            CAR
            `Seq` AnnCDR (Anns2 name no_name)))
          `Seq` loc 2 40 DROP
          `Seq` loc 2 46 UNIT
          `Seq` loc 2 52 NIL
          `Seq` loc 2 67 PAIR)

  , testCase "Annotations are preserved in pexec_2.mv" $ do
      let file = inContractsDir ("mavryk_examples" </> "opcodes" </> "pexec_2.mv")
      contract <-
        importContract @'TInt @('TList 'TInt) file
      unContractCode (cCode contract) @?= loc 2 5 (Nested $
          AnnUNPAIR (Anns4 "p" "s" "" "")
          `Seq` loc 4 6 (LAMBDA (RfNormal (
            UNPAIR
            `Seq` loc 5 24 (DIP UNPAIR)
            `Seq` loc 5 41 ADD
            `Seq` loc 5 47 MUL)))
          `Seq` loc 6 6 SWAP
          `Seq` loc 6 13 APPLY
          `Seq` loc 7 6 (PUSH (VInt 3))
          `Seq` loc 7 19 APPLY
          `Seq` loc 8 6 SWAP
          `Seq` loc 8 13 (MAP (loc 8 19 (DIP (loc 8 25 DUP))
            `Seq` loc 8 33 EXEC))
          `Seq` loc 9 6 (DIP (loc 9 12 DROP))
          `Seq` loc 10 6 NIL
          `Seq` loc 10 21 PAIR)

  -- Regression test:
  , testCase "UNPAPAIR macro generates UNPAIR instructions without annotations" $ do
      let file = inContractsDir "unpair_macro_simple.mv"
      contract <- importContract @'TUnit @'TUnit file
      unContractCode (cCode contract) @?= loc 6 5 (Nested $
        loc 6 7 DROP
        `Seq` loc 7 7 (AnnUNIT (Anns2 "u3" "a3"))
        `Seq` loc 7 21 (AnnUNIT (Anns2 "u2" "a2"))
        `Seq` loc 7 35 (AnnUNIT (Anns2 "u1" "a1"))
        `Seq` loc 8 7 (Nested (
          DIP (AnnPAIR (Anns4 "" "" "y2" ""))
          `Seq` (AnnPAIR (Anns4 "" "q" "y1" "") )))
        `Seq` loc 8 26 (Nested (
          UNPAIR
          `Seq` (DIP UNPAIR)))
        `Seq` loc 9 7 (DIP ((loc 9 13 DROP) `Seq` (loc 9 19 DROP)))
        `Seq` loc 9 27 NIL
        `Seq` loc 9 42 PAIR)
  ]
  where
    loc :: Word -> Word -> Instr a b -> Instr a b
    loc row col = WithLoc (ErrorSrcPos (srcPos row col))
