-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Serialization.Aeson
  ( test_Roundtrip
  ) where

import Data.Aeson (FromJSON, ToJSON, eitherDecode, encode)
import Data.Default (def)
import Hedgehog (Gen)
import Test.Tasty (TestTree)

import Hedgehog.Gen.Michelson.Untyped
  (genAnnotation, genContract, genElt, genExpandedOp, genInstrAbstract, genValue)
import Hedgehog.Gen.Mavryk.Core (genMumav, genTimestamp)
import Morley.Michelson.Untyped
import Morley.Mavryk.Core (Mumav, Timestamp)

import Test.Util.Hedgehog (roundtripTree)


-- Note: if we want to enforce a particular JSON format, we can extend
-- these test with golden tests (it's easy with `hspec-golden-aeson`).

test
  :: forall a. (Eq a, Show a, ToJSON a, FromJSON a, Typeable a)
  => Gen a -> TestTree
test genA = roundtripTree @a genA encode eitherDecode

test_Roundtrip :: [TestTree]
test_Roundtrip =
  [ -- Core Mavryk types
    test @Timestamp (genTimestamp def)
  , test @Mumav (genMumav def)

  -- Michelson types
  , test @ExpandedOp genExpandedOp

  -- these are actually all the same thing (Annotation a),
  -- where a is a phantom type,
  -- but let's test them in case they
  -- ever change for some reason
  , test @TypeAnn genAnnotation
  , test @FieldAnn genAnnotation
  , test @VarAnn genAnnotation

  , test @Contract genContract
  , test @ExpandedInstr (genInstrAbstract genExpandedOp)
  , test @Value genValue
  , test @(Elt [] ExpandedOp) (genElt genExpandedOp)
  ]
