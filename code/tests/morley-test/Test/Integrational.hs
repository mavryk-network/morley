-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for integrational testing machinery.
module Test.Integrational
  ( test_Chain_id
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Runtime.GState (gsChainId, initGState)
import Morley.Michelson.Typed
import Morley.Mavryk.Core
import Test.Cleveland

type Param = ()
type Storage = Maybe ChainId

test_Chain_id :: TestTree
test_Chain_id = testScenarioOnEmulator "Chain id can be set" $ scenario do
  handle <- originate "get_chain_id" Nothing contract
  transfer handle
  getStorage handle @@== Just expectedChainId
  where
    contract = TypedContract @Param @Storage @() $ defaultContract @(ToT Param) @(ToT Storage) $
      DROP `Seq` CHAIN_ID `Seq` SOME `Seq` NIL `Seq` PAIR
    expectedChainId = gsChainId initGState
