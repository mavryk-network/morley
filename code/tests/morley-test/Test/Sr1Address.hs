-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for sr1 addresses
module Test.Sr1Address
  ( test_sr1keyhash
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Morley.Mavryk.Address
import Test.Cleveland

test_sr1keyhash :: TestTree
test_sr1keyhash = testScenario "sr1 address is passed properly" $ scenario do
  handle <- originate "sr1_key_hash" Nothing $ storeContract @Address
  let addr = toAddress [ta|sr1RYurGZtN8KNSpkMcCt9CgWeUaNkzsAfXf|]
  transfer handle $ calling def addr
  getStorage handle @@== Just addr

storeContract :: StorageScope (ToT s) => TypedContract s (Maybe s) ()
storeContract = TypedContract $ defaultContract $ CAR :# SOME :# NIL :# PAIR
