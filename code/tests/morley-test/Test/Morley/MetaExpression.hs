-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Test on 'Expression' extended with meta.
--
-- This is a use case similar to what we need in LIGO debugger.
module Test.Morley.MetaExpression
  ( test_ExpressionTraversal
  , test_ParsingExp
  , test_TypeCheckingExp
  ) where

import Control.Lens (devoid, unsafePartsOf)
import Data.Aeson qualified as Aeson
import Data.Aeson.QQ (aesonQQ)
import Data.Data (Data)
import Data.Default (def)
import Data.Typeable (cast)
import Fmt (Buildable(..))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Micheline
import Morley.Micheline.Expression.WithMeta
import Morley.Michelson.Parser (uparamTypeQ, utypeQ)
import Morley.Michelson.TypeCheck qualified as Tc
import Morley.Michelson.TypeCheck.Helpers qualified as Tc
import Morley.Michelson.TypeCheck.Instr qualified as Tc
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U

data OpWithMeta meta
  = MPrimEx (InstrWithMeta meta)
  | MSeqEx [OpWithMeta meta]
  | MMetaEx meta (OpWithMeta meta)
  deriving stock (Eq, Show, Data)

type InstrWithMeta meta = U.InstrAbstract [] (OpWithMeta meta)
type ContractWithMeta meta = U.Contract' (OpWithMeta meta)

instance (meta ~ meta') => FromExp (WithMeta meta) (OpWithMeta meta') where
  fromExp e = MMetaEx (view expMetaL e) <$> case e of
    ExpSeq _ exps -> MSeqEx <$> traverse fromExp exps
    other -> MPrimEx <$> fromExp other

typeCheckOpWithMeta
  :: (Show meta, NFData meta, Data meta)
  => Tc.TcInstrBase (OpWithMeta meta)
typeCheckOpWithMeta instr hst = case instr of
  MMetaEx m i ->
    typeCheckOpWithMeta i hst <&> Tc.mapSeq (Tc.mapSomeInstr $ T.Meta (T.SomeMeta m))
  MPrimEx i ->
    Tc.typeCheckInstr typeCheckOpWithMeta i hst
  MSeqEx is ->
    Tc.typeCheckImpl typeCheckOpWithMeta is hst <&> Tc.mapSeq (Tc.mapSomeInstr T.Nested)

instance Data meta => Tc.IsInstrOp (OpWithMeta meta) where
  liftInstr = MPrimEx
  pickErrorSrcPos _ = Nothing
  tryOpToVal = \case
    MMetaEx _ i -> Tc.tryOpToVal i
    MSeqEx is -> case nonEmpty is of
      Nothing -> pure $ U.ValueNil
      Just is' -> U.ValueSeq <$> traverse Tc.tryOpToVal is'
    MPrimEx _ -> mzero
  tryValToOp = \case
    U.ValueNil -> Just $ MSeqEx []
    U.ValueSeq xs -> MSeqEx . toList <$> traverse Tc.tryValToOp xs
    _ -> Nothing

data SomeInstr = forall i o. SomeInstr (T.Instr i o)
deriving stock instance Show SomeInstr

instance Eq SomeInstr where
  SomeInstr i1 == SomeInstr i2 = T.instrToOps i1 == T.instrToOps i2

instance Buildable SomeInstr where
  build (SomeInstr i) = build i

collectInstrWithMetas :: Typeable meta => T.Instr i o -> [(meta, SomeInstr)]
collectInstrWithMetas = T.dfsFoldInstr def \case
  T.Meta (T.SomeMeta (cast -> Just meta)) i -> one (meta, SomeInstr (stripMetas i))
  _ -> mempty
  where
    stripMetas = T.dfsModifyInstr def \case
      T.Meta _ i -> i
      i -> i

-- Tests
----------------------------------------------------------------------------

-- | In our use case with debugger, metadata is given as a list, entries of which
-- are listed in DFS traversal order over Micheline tree.
--
-- Let's pretend the metadata list is this one. Usually it has a respective
-- length, but in our test this does not matter, extra elements will be stripped.
metadataFromAbove :: [Word]
metadataFromAbove = [0..]

expressionSample1 :: Expression
expressionSample1 = expressionSeq
  [ expressionInt 777
  , expressionString "s"
  , expressionSeq
      [ expressionInt 50
      , expressionInt 77
      ]
  , expressionPrim' Prim_Unit [expressionBytes "aaa"] []
  ]

{- | From the contract:

@
parameter (pair bool nat);
storage int;
code { UNPAIR;
       UNPAIR;
       DIP { INT };
       IF { ADD } { SWAP; SUB };
       {};
       { PUSH int 5; DROP };
       NIL operation; PAIR
       }
@

using the command
> mavkit-client convert script <file> from michelson to json

-}
expressionSample2 :: Expression
expressionSample2 = \case{ Aeson.Success r -> r; Aeson.Error e -> error (toText e) } $
  Aeson.fromJSON
  [aesonQQ|
    [ { "prim": "parameter",
        "args":
          [ { "prim": "pair", "args": [ { "prim": "bool" }, { "prim": "nat" } ] } ] },
      { "prim": "storage", "args": [ { "prim": "int" } ] },
      { "prim": "code",
        "args":
          [ [ { "prim": "UNPAIR" }, { "prim": "UNPAIR" },
              { "prim": "DIP", "args": [ [ { "prim": "INT" } ] ] },
              { "prim": "IF",
                "args":
                  [ [ { "prim": "ADD" } ],
                    [ { "prim": "SWAP" }, { "prim": "SUB" } ] ] }, [],
              [ { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "5" } ] },
                { "prim": "DROP" } ],
              { "prim": "NIL", "args": [ { "prim": "operation" } ] },
              { "prim": "PAIR" } ] ] } ]

  |]

pattern PrimExp' :: XExpPrim x -> MichelinePrimitive -> [Exp x] -> Exp x
pattern PrimExp' x prim args = ExpPrim x (MichelinePrimAp prim args [])

test_ExpressionTraversal :: TestTree
test_ExpressionTraversal = testGroup "Expression traversal"
  [ testCase "Enumerate simple expression" $
      ( expressionSample1
        & unsafePartsOf (expAllExtraL devoid) .~ metadataFromAbove
        :: ExpressionWithMeta Word
      ) @?=
      ExpSeq 0
        [ ExpInt 1 777
        , ExpString 2 "s"
        , ExpSeq 3
            [ ExpInt 4 50
            , ExpInt 5 77
            ]
        , PrimExp' 6 Prim_Unit [ ExpBytes 7 "aaa" ]
        ]

  , testCase "Enumerate real contract" $
      ( expressionSample2
        & unsafePartsOf (expAllExtraL devoid) .~ metadataFromAbove
        :: ExpressionWithMeta Word
      ) @?=
      ExpSeq 0
        [ PrimExp' 1 Prim_parameter
            [ PrimExp' 2 Prim_pair
              [ PrimExp' 3 Prim_bool []
              , PrimExp' 4 Prim_nat []
              ]
            ]
        , PrimExp' 5 Prim_storage
            [ PrimExp' 6 Prim_int []
            ]
        , PrimExp' 7 Prim_code
            [ ExpSeq 8
              [ PrimExp' 9 Prim_UNPAIR []
              , PrimExp' 10 Prim_UNPAIR []
              , PrimExp' 11 Prim_DIP
                [ ExpSeq 12
                  [ PrimExp' 13 Prim_INT []
                  ]
                ]
              , PrimExp' 14 Prim_IF
                [ ExpSeq 15
                   [ PrimExp' 16 Prim_ADD []
                   ]
                , ExpSeq 17
                   [ PrimExp' 18 Prim_SWAP []
                   , PrimExp' 19 Prim_SUB []
                   ]
                ]
              , ExpSeq 20 []
              , ExpSeq 21
                [ PrimExp' 22 Prim_PUSH
                  [ PrimExp' 23 Prim_int []
                  , ExpInt 24 5
                  ]
                , PrimExp' 25 Prim_DROP []
                ]
              , PrimExp' 26 Prim_NIL
                [ PrimExp' 27 Prim_operation []
                ]
              , PrimExp' 28 Prim_PAIR []
              ]
            ]

        ]

  ]

test_ParsingExp :: TestTree
test_ParsingExp = testGroup "Parsing exp"
  [ testCase "Real contract" $
      ( expressionSample2
        & unsafePartsOf (expAllExtraL devoid) .~ metadataFromAbove
        & fromExp @(WithMeta Word) @(ContractWithMeta _)
      ) @?= Right U.Contract
        { contractParameter = [uparamTypeQ|pair bool nat|]
        , contractStorage = [utypeQ|int|]
        , entriesOrder = def
        , contractViews = def
        , contractCode = MMetaEx 8 $ MSeqEx
          [ MMetaEx 9 $ MPrimEx (U.UNPAIR U.noAnn U.noAnn U.noAnn U.noAnn)
          , MMetaEx 10 $ MPrimEx (U.UNPAIR U.noAnn U.noAnn U.noAnn U.noAnn)
          , MMetaEx 11 $ MPrimEx $ U.DIP
            [ MMetaEx 13 $ MPrimEx (U.INT U.noAnn)
            ]
          , MMetaEx 14 $ MPrimEx $ U.IF
            [ MMetaEx 16 $ MPrimEx (U.ADD U.noAnn)
            ]
            [ MMetaEx 18 $ MPrimEx U.SWAP
            , MMetaEx 19 $ MPrimEx (U.SUB U.noAnn)
            ]
          , MMetaEx 20 $ MSeqEx []
          , MMetaEx 21 $ MSeqEx
            [ MMetaEx 22 $ MPrimEx (U.PUSH U.noAnn [utypeQ|int|] (U.ValueInt 5))
            , MMetaEx 25 $ MPrimEx U.DROP
            ]
          , MMetaEx 26 $ MPrimEx (U.NIL U.noAnn U.noAnn [utypeQ|operation|])
          , MMetaEx 28 $ MPrimEx (U.PAIR U.noAnn U.noAnn U.noAnn U.noAnn)
          ]
        }
  ]

test_TypeCheckingExp :: TestTree
test_TypeCheckingExp = testGroup "Typechecking exp"
  [ testCase "Real contract" do
      let expWithMeta =
            expressionSample2
            & unsafePartsOf (expAllExtraL devoid) .~ metadataFromAbove
      Right untypedContract <-
            pure $ fromExp @(WithMeta Word) @(ContractWithMeta _) expWithMeta
      Right (T.SomeContract typedContract) <-
            pure $ Tc.typeCheckingWith def $
            Tc.typeCheckContract' typeCheckOpWithMeta untypedContract
      -- the first element is 'Nested' everything, skip it to simplify.
      drop 1 (sortWith fst
        (collectInstrWithMetas @Word $ T.unContractCode $ T.cCode typedContract))
        @?=
        [ (9, SomeInstr $ T.UNPAIR)
        , (10, SomeInstr $ T.UNPAIR)
        , (11, SomeInstr $ T.DIP $ T.INT @'T.TNat)
        , (13, SomeInstr $ T.INT @'T.TNat)
        , (14, SomeInstr $ T.IF
            (T.ADD @'T.TInt @'T.TInt)
            (T.SWAP `T.Seq` T.SUB @'T.TInt @'T.TInt)
          )
        , (16, SomeInstr $ T.ADD @'T.TInt @'T.TInt)
        , (18, SomeInstr $ T.SWAP)
        , (19, SomeInstr $ T.SUB @'T.TInt @'T.TInt)
        , (20, SomeInstr $ T.Nested T.Nop)
        , (21, SomeInstr $ T.Nested $ T.PUSH (T.VInt 5) `T.Seq` T.DROP)
        , (22, SomeInstr $ T.PUSH (T.VInt 5))
        , (25, SomeInstr $ T.DROP)
        , (26, SomeInstr $ T.NIL @'T.TOperation)
        , (28, SomeInstr $ T.PAIR)
        ]

  ]
