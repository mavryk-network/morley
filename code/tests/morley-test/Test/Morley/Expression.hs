-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'Morley.Micheline.Expression'.

module Test.Morley.Expression
  ( test_Roundtrip_binary
  , test_Roundtrip_expression
  , test_Roundtrip_JSON
  , test_ToJSON_omits_empty_lists
  , test_fromExpression
  , test_toExpression
  ) where

import Unsafe qualified (fromJust)

import Data.Aeson
  (FromJSON, Result(Error, Success), ToJSON(toJSON), Value(String), eitherDecode, encode, fromJSON,
  object, (.=))
import Data.Aeson.QQ (aesonQQ)
import Data.Default (def)
import Data.Map qualified as Map
import Data.Set qualified as Set
import Data.Singletons (demote)
import Data.Text qualified as T
import Fmt (pretty)
import Hedgehog (Gen)
import Test.Hspec.Expectations (shouldBe)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (Assertion, assertFailure, testCase, (@?=))

import Hedgehog.Gen.Michelson.Typed (genSimpleInstr, genValueInt, genValueMumav, genValueUnit)
import Hedgehog.Gen.Michelson.Untyped (genType)
import Hedgehog.Gen.Morley.Micheline qualified as M
import Morley.Micheline qualified as M
import Morley.Michelson.Parser (notes)
import Morley.Michelson.Text (MText)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Mavryk.Core (Mumav)
import Test.Cleveland.Instances ()
import Test.Cleveland.Util (fromHex)
import Test.Util.Hedgehog (roundtripTree)

testJSON
  :: forall a. (Eq a, Show a, ToJSON a, FromJSON a, Typeable a)
  => Gen a -> TestTree
testJSON genA = roundtripTree @a genA encode eitherDecode

-- | Test that 'M.fromExpression' is inverse of 'M.toExpression'
roundtripExpression
  :: forall a. (Eq a, Show a, M.ToExpression a, M.FromExpression a, Typeable a)
  => Gen a -> TestTree
roundtripExpression genA = roundtripTree @a genA M.toExpression (M.fromExpression)

-- | Test that converting something to 'M.Expression', then to JSON,
-- then decoding this JSON and converting back to the original type
-- returns the original value.
roundtripExpressionJSON
  :: forall a. (Eq a, Show a, M.ToExpression a, M.FromExpression a, Typeable a)
  => Gen a -> TestTree
roundtripExpressionJSON genA = roundtripTree @a genA
  (encode . M.toExpression)
  (first displayException . M.fromExpression <=< eitherDecode)

testBinary :: Gen M.Expression -> TestTree
testBinary genA = roundtripTree genA M.encodeExpression' M.eitherDecodeExpression

test_Roundtrip_JSON :: [TestTree]
test_Roundtrip_JSON =
  [ testJSON M.genExprAnnotation
  , testJSON M.genExpression
  , testJSON (M.genMichelinePrimAp M.genExpression)
  , roundtripExpressionJSON @(T.Value $ T.ToT Integer) $ genValueInt def
  , roundtripExpressionJSON @(T.Value $ T.ToT Mumav) $ genValueMumav def
  , roundtripExpressionJSON @(T.Value $ T.ToT ()) genValueUnit
  , roundtripExpressionJSON @(T.Instr '[T.ToT Mumav] '[T.ToT Mumav]) genSimpleInstr
  , roundtripExpressionJSON (T.fromUType <$> genType)
  , roundtripExpressionJSON genType
  ]

test_ToJSON_omits_empty_lists :: TestTree
test_ToJSON_omits_empty_lists =
  testCase "ToJSON omits empty lists" $ do
    let actual = toJSON . M.toExpression $ T.toVal (Nothing :: Maybe Integer)
    let expected = object ["prim" .= String "None"]

    actual @?= expected

test_Roundtrip_binary :: [TestTree]
test_Roundtrip_binary =
  [ testBinary M.genExpression
  ]

test_Roundtrip_expression :: [TestTree]
test_Roundtrip_expression =
  [ roundtripExpression @(T.Value $ T.ToT Integer) $ genValueInt def
  , roundtripExpression @(T.Value $ T.ToT Mumav) $ genValueMumav def
  , roundtripExpression @(T.Value $ T.ToT ()) genValueUnit
  , roundtripExpression @(T.Instr '[T.ToT Mumav] '[T.ToT Mumav]) genSimpleInstr
  , roundtripExpression @T.T (T.fromUType <$> genType)
  , roundtripExpression genType
  , testCase "#692 regression" do
      let testValue = pairAnn [U.annQ|_|] unit $ pair unit unit
          ty = flip U.Ty U.noAnn
          unit = ty U.TUnit
          pair = pairAnn U.noAnn
          pairAnn ann l r = ty $ U.TPair U.noAnn U.noAnn U.noAnn ann l r
      Right testValue @?= M.fromExpression (M.toExpression testValue)
  ]

test_fromExpression :: [TestTree]
test_fromExpression =
  -- These michelson expressions are generated using commands like the following:
  --
  -- > mavkit-client convert data 'Pair 1 2 "Hi"' from michelson to json
  -- > mavkit-client convert data 'pair int nat string' from michelson to json
  [ testGroup "Value t"
    [ testCase "Converting 'Pair' expression with >2 args into right-combed pair" $ do
        fromExpressionTest
          (T.toVal @(Integer, (Natural, MText)) (1, (2, "Hi")))
          [aesonQQ|
            { "prim": "Pair", "args": [ { "int": "1" }, { "int": "2" }, { "string": "Hi" } ] }
          |]
    , testCase "Converting nested 'Pair' expression into right-combed pair" $ do
        fromExpressionTest
          (T.toVal @(Integer, (Natural, MText)) (1, (2, "Hi")))
          [aesonQQ|
            { "prim": "Pair",
              "args":
                [ { "int": "1" },
                  { "prim": "Pair", "args": [ { "int": "2" }, { "string": "Hi" } ] } ]
            }
          |]
    , testCase "Converting list expression into right-combed pair" $ do
        fromExpressionTest
          (T.toVal @(Integer, (Natural, MText)) (1, (2, "Hi")))
          [aesonQQ|
            [ { "int": "1" }, { "int": "2" }, { "string": "Hi" } ]
          |]
    ]
  , testGroup "Type"
      [ testCase "Converting 'pair' type expression with >2 args into right-combed pair type" $ do
          fromExpressionTest
            (T.toUType $ demote @(T.ToT (Integer, (Natural, MText))))
            [aesonQQ|
              { "prim": "pair",
                "args": [ { "prim": "int" }, { "prim": "nat" }, { "prim": "string" } ] }
            |]
      , testCase "Converting nested 'pair' type expression into right-combed pair type" $ do
          fromExpressionTest
            (T.toUType $ demote @(T.ToT (Integer, (Natural, MText))))
            [aesonQQ|
              { "prim": "pair",
                "args":
                  [ { "prim": "int" },
                    { "prim": "pair", "args": [ { "prim": "nat" }, { "prim": "string" } ] } ] }
            |]

      ]
    , testGroup "Contract"
        [ testCase "Conversion of contract with duplicated 'storage' block fails" $ do
            fromExpressionFailWithPredicateTest @U.Contract
              (T.isInfixOf "Duplicate contract field: storage\nMissing contract field: code")
              [aesonQQ|
                [ { "prim": "storage", "args": [ { "prim": "unit" } ] },
                  { "prim": "parameter", "args": [ { "prim": "unit" } ] },
                  { "prim": "storage", "args": [ { "prim": "unit" } ] } ]
              |]
        , testCase "Conversion of contract with absence one of blocks fails" $ do
            fromExpressionFailWithPredicateTest @U.Contract
              (T.isInfixOf "Duplicate contract field: storage\nMissing contract field: parameter")
              [aesonQQ|
                [ { "prim": "storage", "args": [ { "prim": "unit" } ] },
                  { "prim": "storage", "args": [ { "prim": "unit" } ] },
                  { "prim": "code",
                    "args":
                      [ [ { "prim": "DROP" }, { "prim": "UNIT" },
                          { "prim": "NIL", "args": [ { "prim": "operation" } ] },
                          { "prim": "PAIR" } ] ] } ]
              |]
        , testCase "Conversion of contract with invalid block arguments count fails" $ do
          fromExpressionFailWithPredicateTest @U.Contract
            (T.isInfixOf "Expected exactly 1 arguments, but got 2")
              [aesonQQ|
                [   { "prim": "storage",
                      "args": [ { "prim": "unit"}, {"prim": "int"} ] },
                    { "prim": "parameter", "args": [ { "prim": "unit" } ] },
                    { "prim": "code",
                      "args":
                        [ [ { "prim": "DROP" }, { "prim": "UNIT" },
                            { "prim": "NIL", "args": [ { "prim": "operation" } ] },
                            { "prim": "PAIR" } ] ] } ]
              |]
        , testCase "Conversion of contract with error inside block fails" $ do
          fromExpressionFailWithPredicateTest @U.Contract
            (T.isInfixOf "Expected exactly 0 arguments, but got 1")
              [aesonQQ|
                [   { "prim": "storage",
                      "args": [ { "prim": "unit", "args": [ { "prim": "int" } ] } ] },
                    { "prim": "parameter", "args": [ { "prim": "unit" } ] },
                    { "prim": "code",
                      "args":
                        [ [ { "prim": "DROP" }, { "prim": "UNIT" },
                            { "prim": "NIL", "args": [ { "prim": "operation" } ] },
                            { "prim": "PAIR" } ] ] } ]
              |]
        , testCase "Conversion of contract with parameter which contains more than 1 root ann" $ do
          fromExpressionFailWithPredicateTest @U.Contract
            (T.isInfixOf "Expected at most: 1 field annotations\nbut found: 2 field annotations")
              [aesonQQ|
                  [   { "prim": "storage", "args": [ { "prim": "unit" } ] },
                      { "prim": "parameter",
                        "args": [ { "prim": "unit", "annots": [ "%root1", "%root2" ] } ]},
                      { "prim": "code",
                        "args":
                          [ [ { "prim": "DROP" }, { "prim": "UNIT" },
                              { "prim": "NIL", "args": [ { "prim": "operation" } ] },
                              { "prim": "PAIR" } ] ] } ]
                |]
        ]
    , testGroup "Annotations"
        [ testGroup "Instructions"
          [ testCase "Conversion of instruction with extra annotations fails" $ do
              fromExpressionFailTest @U.ExpandedInstr
                [aesonQQ|
                  { "prim": "PAIR",
                    "annots": [ ":ta", "@va1", "@va2", "%fa1", "%fa2", "%fa3" ] }
                |]
          , testCase "Conversion of instruction with annotation of unexpected type fails" $ do
              fromExpressionFailTest @U.ExpandedInstr
                [aesonQQ|
                  { "prim": "DUP",
                    "annots": [ "@va", "%fa" ] }
                |]
          ]

        , testGroup "Types"
            [ testCase "Conversion of type with extra annotations fails" $ do
                fromExpressionFailTest @U.Ty
                  [aesonQQ| { "prim": "int", "annots": [ ":ta", "%fa" ] } |]
            ]
        ]
  ]
  where
    fromExpressionTest :: forall t. (Eq t, Show t, M.FromExpression t) => t -> Value -> Assertion
    fromExpressionTest expected exprJSON =
      case fromJSON @M.Expression exprJSON of
        Error err -> assertFailure err
        Success expr -> do
          actual <- either (assertFailure . displayException) pure $ M.fromExpression @t expr
          actual @?= expected

    fromExpressionFailTest :: forall t. (M.FromExpression t) => Value -> Assertion
    fromExpressionFailTest = fromExpressionFailWithPredicateTest @t (const True)

    fromExpressionFailWithPredicateTest
      :: forall t.
         (HasCallStack, M.FromExpression t)
      => (Text -> Bool)
      -> Value
      -> Assertion
    fromExpressionFailWithPredicateTest p exprJSON =
      case fromJSON @M.Expression exprJSON of
        Error err -> assertFailure err
        Success expr -> case M.fromExpression @t expr of
          Right _ -> assertFailure "Test was expected to fail, but it passed"
          Left err@(M.FromExpError _ msg) ->
            unless (p $ pretty msg) $
              assertFailure $ "Test failed with an unexpected error type:\n  " <> pretty err

test_toExpression :: [TestTree]
test_toExpression =
  [ testCase "Converting TInt to Expression" $
      M.toExpression T.TInt `shouldBe`
        M.expressionPrim (M.MichelinePrimAp (M.Prim_int) [] [])

  , testCase "Converting (TOption TString) to Expression" $
      M.toExpression (T.TOption T.TString) `shouldBe`
        M.expressionPrim
          ( M.MichelinePrimAp
            { mpaPrim = M.Prim_option
            , mpaArgs = [M.expressionPrim
                ( M.MichelinePrimAp {mpaPrim = M.Prim_string
                , mpaArgs = [], mpaAnnots = [] }
                )]
            , mpaAnnots = []
            }
          )

  , testCase "Converting Instr (DIP SIZE) to Expression" $
      M.toExpression (T.DIP (T.SIZE @'T.TString)) `shouldBe`
        M.expressionSeq
          [ M.expressionPrim (M.MichelinePrimAp
              { mpaPrim = M.Prim_DIP
              , mpaArgs = [M.expressionSeq [M.expressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.Prim_SIZE
                  , mpaArgs = []
                  , mpaAnnots = []
                  })]]
              , mpaAnnots = []
              })]
  , testCase "Converting VInt to Expression" $
      M.toExpression @(T.Value $ T.ToT Integer) (T.VInt 12) `shouldBe` M.expressionInt 12

  , testCase "Converting VPair to Expression" $
      M.toExpression @(T.Value $ T.ToT (Integer, Integer))
        (T.VPair (T.VInt 12, T.VInt 12)) `shouldBe`
          M.expressionSeq [M.expressionInt 12, M.expressionInt 12]

  , testCase "Converting VOption (Some) to Expression" $
      M.toExpression @(T.Value $ T.ToT (Maybe Integer)) (T.VOption $ Just $ T.VInt 42) `shouldBe`
        M.expressionPrim
          (M.MichelinePrimAp
            { mpaPrim = M.Prim_Some
            , mpaArgs = [M.expressionInt 42]
            , mpaAnnots = []
            })

  , testCase "Converting VOption (None) to Expression" $
      M.toExpression @(T.Value $ T.ToT (Maybe Integer)) (T.VOption Nothing) `shouldBe`
        M.expressionPrim
          (M.MichelinePrimAp
            { mpaPrim = M.Prim_None
            , mpaArgs = []
            , mpaAnnots = []
            })

  , testCase "Converting VList to Expression" $
      M.toExpression @(T.Value $ T.ToT [Integer]) (T.VList [T.VInt 12, T.VInt 12]) `shouldBe`
        M.expressionSeq [M.expressionInt 12, M.expressionInt 12]

  , testCase "Converting VMap to Expression" $
      M.toExpression @(T.Value $ T.ToT (Map Integer Integer))
        (T.VMap (Map.fromList [(T.VInt 12, T.VInt 12), (T.VInt 13, T.VInt 13)])) `shouldBe`
          M.expressionSeq
            [ (M.expressionPrim
                M.MichelinePrimAp
                  { mpaPrim = M.Prim_Elt
                  , mpaArgs = [M.expressionInt 12, M.expressionInt 12]
                  , mpaAnnots = []
                  }
              )
            , (M.expressionPrim
                M.MichelinePrimAp
                  { mpaPrim = M.Prim_Elt
                  , mpaArgs = [M.expressionInt 13, M.expressionInt 13]
                  , mpaAnnots = []
                  }
              )
           ]

  , testCase "Converting VOr (Right) to Expression" $
      M.toExpression @(T.Value $ T.ToT (Either Integer Integer)) (T.VOr $ Right $ T.VInt 12) `shouldBe`
        M.expressionPrim
          M.MichelinePrimAp
            { mpaPrim = M.Prim_Right
            , mpaArgs = [M.expressionInt 12]
            , mpaAnnots = []
            }

  , testCase "Converting VOr (Left) to Expression" $
      M.toExpression @(T.Value $ T.ToT (Either Integer Integer)) (T.VOr $ Left $ T.VInt 12) `shouldBe`
        M.expressionPrim
          M.MichelinePrimAp
            { mpaPrim = M.Prim_Left
            , mpaArgs = [M.expressionInt 12]
            , mpaAnnots = []
            }

  , testCase "Converting VSet to Expression" $
      M.toExpression @(T.Value $ T.ToT (Set Integer)) (T.VSet $ Set.fromList [T.VInt 1, T.VInt 2]) `shouldBe`
        M.expressionSeq [M.expressionInt 1, M.expressionInt 2]

  , testCase "Converting VMumav to Expression" $
      M.toExpression @(T.Value $ T.ToT Mumav) (T.VMumav $ 12) `shouldBe`
        M.expressionInt 12

  , testCase "Converting VBool to Expression" $
      M.toExpression @(T.Value $ T.ToT Bool) (T.VBool True) `shouldBe`
        M.expressionPrim
          (M.MichelinePrimAp
            { mpaPrim = M.Prim_True
            , mpaArgs = []
            , mpaAnnots = []
            })

  , testCase "Converting VContract to Expression" $
      M.toExpression @(T.Value $ 'T.TContract 'T.TAddress)
        (getSampleValue @('T.TContract 'T.TAddress)) `shouldBe`
          M.expressionBytes (unsafe $ fromHex "01122d038abd69be91b4b6803f2f098a088e259e7200")

  , testCase "Converting VBigMap to Expression" $
      M.toExpression @(T.Value $ 'T.TBigMap (T.ToT Integer) (T.ToT Integer))
        (T.VBigMap Nothing $ Map.fromList [(T.VInt 1, T.VInt 2), (T.VInt 3, T.VInt 4)]) `shouldBe`
          M.expressionSeq
            [ (M.expressionPrim
                M.MichelinePrimAp
                  { mpaPrim = M.Prim_Elt
                  , mpaArgs = [M.expressionInt 1, M.expressionInt 2]
                  , mpaAnnots = []
                  }
              )
            , (M.expressionPrim
                 M.MichelinePrimAp
                 { mpaPrim = M.Prim_Elt
                 , mpaArgs = [M.expressionInt 3, M.expressionInt 4]
                 , mpaAnnots = []
                 }
              )
            ]

  , testCase "Converting VUnit to Expression" $
      M.toExpression @(T.Value 'T.TUnit) T.VUnit `shouldBe`
        M.expressionPrim (M.MichelinePrimAp
          { mpaPrim = M.Prim_Unit
          , mpaArgs = []
          , mpaAnnots = []
          })

  , testCase "Converting VKey to Expression" $
      M.toExpression @(T.Value 'T.TKey) (getSampleValue @('T.TKey)) `shouldBe`
       M.expressionBytes (unsafe $ fromHex "00aad3f16293766169f7db278c5e0e9db4fb82ffe1cbcc35258059617dc0fec082")

  , testCase "Converting VTimestamp to Expression" $
      M.toExpression @(T.Value 'T.TTimestamp) (getSampleValue @('T.TTimestamp)) `shouldBe`
        M.expressionInt 1564142952

  , testCase "Converting VAddress to Expression" $
      M.toExpression @(T.Value 'T.TAddress) (getSampleValue @('T.TAddress)) `shouldBe`
        M.expressionBytes (unsafe $ fromHex "01122d038abd69be91b4b6803f2f098a088e259e7200")

  , testCase "Converting VChainId to Expression" $
      M.toExpression @(T.Value 'T.TChainId) (getSampleValue @('T.TChainId)) `shouldBe`
        M.expressionBytes (unsafe $ fromHex "458aa837")

  , testCase "Converting VSignature to Expression" $
      M.toExpression @(T.Value 'T.TSignature) (getSampleValue @('T.TSignature)) `shouldBe`
        (M.expressionBytes $ unsafe $ fromHex
          "91ac1e7fd668854fc7a40feec4034e42c06c068cce10622c607fda232db34c8cf5d8da83098dd89\
          \1cd4cb4299b3fa0352ae323ad99b24541e54b91888fdc8201")

  , testCase "Converting VKeyHash to Expression" $
      M.toExpression @(T.Value 'T.TKeyHash) (getSampleValue @('T.TKeyHash)) `shouldBe`
        M.expressionBytes (unsafe $ fromHex "0092629ed0afa9cd42835ce09ee2623c1efa0b590d")

  , testCase "Converting VBls12381Fr to Expression" $
      M.toExpression @(T.Value 'T.TBls12381Fr) (getSampleValue @('T.TBls12381Fr)) `shouldBe`
          M.expressionBytes (unsafe $ fromHex "0100000000000000000000000000000000000000000000000000000000000000")

  , testCase "Converting VLam to Expression" $
      M.toExpression @(T.Value $ 'T.TLambda 'T.TUnit 'T.TUnit)
        (getSampleValue @('T.TLambda 'T.TUnit 'T.TUnit)) `shouldBe`
          M.expressionSeq
            [ M.expressionPrim
              (M.MichelinePrimAp
                { mpaPrim = M.Prim_DROP
                , mpaArgs = []
                , mpaAnnots = []
                }
              )
            , M.expressionPrim
              (M.MichelinePrimAp
                { mpaPrim = M.Prim_PUSH
                , mpaArgs =
                  [ M.expressionPrim
                      (M.MichelinePrimAp
                        { mpaPrim = M.Prim_unit
                        , mpaArgs = []
                        , mpaAnnots = []
                        })
                  , M.expressionPrim
                      (M.MichelinePrimAp
                        { mpaPrim = M.Prim_Unit
                        , mpaArgs = []
                        , mpaAnnots = []
                        })
                  ]
                , mpaAnnots = []
                })
            ]

  , testCase "Converting NTPair to Expression" $
      M.toExpression [notes|pair :pair (string %a) (int %b :val)|] `shouldBe`
        M.expressionPrim
          (M.MichelinePrimAp
              { mpaPrim = M.Prim_pair
              , mpaArgs =
                [ M.expressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.Prim_string
                  , mpaArgs = []
                  , mpaAnnots = [M.AnnotationField "a"]
                  })
                , M.expressionPrim (M.MichelinePrimAp
                  { mpaPrim = M.Prim_int
                  , mpaArgs = []
                  , mpaAnnots = [ M.AnnotationType "val", M.AnnotationField "b"]
                  })
                ]
              , mpaAnnots = [ M.AnnotationType "pair" ]
              }
          )

  , testCase "Converting \"CAR @bar %foo\" to Expression" $
      M.toExpression (T.AnnCAR (T.Anns2 "bar" "foo")) `shouldBe`
        M.expressionSeq
          [ M.expressionPrim $ M.MichelinePrimAp
            { mpaPrim = M.Prim_CAR
            , mpaArgs = []
            , mpaAnnots =
              [ M.AnnotationField "foo"
              , M.AnnotationVariable "bar"
              ]
            }
          ]

  , testCase "Converting sample contract to Expression" $
      M.toExpression sampleContract `shouldBe`
        M.expressionSeq
          [ M.expressionPrim (M.MichelinePrimAp
              { mpaPrim = M.Prim_storage
              , mpaArgs = [M.expressionPrim (M.MichelinePrimAp {mpaPrim = M.Prim_chain_id, mpaArgs = [], mpaAnnots = []})]
              , mpaAnnots = []
              })
          , M.expressionPrim (M.MichelinePrimAp
              { mpaPrim = M.Prim_parameter
              , mpaArgs = [M.expressionPrim (M.MichelinePrimAp {mpaPrim = M.Prim_unit, mpaArgs = [], mpaAnnots = []})]
              , mpaAnnots = []
              })
          , M.expressionPrim (M.MichelinePrimAp
              { mpaPrim = M.Prim_code
              , mpaArgs = [M.expressionSeq
                [ M.expressionPrim (M.MichelinePrimAp {mpaPrim = M.Prim_DROP, mpaArgs = [], mpaAnnots = []})
                , M.expressionPrim (M.MichelinePrimAp {mpaPrim = M.Prim_CHAIN_ID, mpaArgs = [], mpaAnnots = []})
                , M.expressionPrim (M.MichelinePrimAp
                    { mpaPrim = M.Prim_NIL
                    , mpaArgs =
                      [ M.expressionPrim (M.MichelinePrimAp
                        { mpaPrim = M.Prim_operation
                        , mpaArgs = []
                        , mpaAnnots = []
                        })
                      ]
                    , mpaAnnots = []
                    })
                , M.expressionPrim (M.MichelinePrimAp {mpaPrim = M.Prim_PAIR, mpaArgs = [], mpaAnnots = []})
                ]]
              , mpaAnnots = []
              })
          ]
  ]
  where
    sampleContract :: T.Contract 'T.TUnit 'T.TChainId
    sampleContract =
      let code = T.DROP `T.Seq` T.CHAIN_ID `T.Seq` T.NIL `T.Seq` T.PAIR
      in T.Contract
            { cCode = T.mkContractCode code
            , cParamNotes = T.starParamNotes @'T.TUnit
            , cStoreNotes = T.starNotes
            , cEntriesOrder = U.mkEntriesOrder [U.EntryStorage, U.EntryParameter, U.EntryCode]
            , cViews = def
            }

    getSampleValue :: forall t. T.WellTyped t => T.Value t
    getSampleValue = Unsafe.fromJust $ T.sampleTypedValue $ T.sing @t
