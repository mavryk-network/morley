-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'create_n_transfer.mv' contract. See [#643]
module Test.Interpreter.CreateAndTransfer
  ( test_createAndTranfser
  ) where

import Test.Tasty (TestTree)

import Morley.Mavryk.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_createAndTranfser :: IO TestTree
test_createAndTranfser =
  pure $ testScenario "'create_n_transfer.mv' performs transfer after origination" $ scenario do
    contract <- importContract @() @() @() (contractsDir </> "create_n_transfer.mv")
    createAndTransfer <- originate "createAndTransfer" () contract
    oldBalance <- getBalance constAddr
    transfer createAndTransfer [mv|10u|]
    newBalance <- getBalance constAddr
    newBalance - oldBalance @== 1

-- Address hardcoded in 'create_n_transfer.mv'.
constAddr :: ImplicitAddress
constAddr = [ta|mv1VRYAgy8jErk7wK85goZLhtWAVLficKksU|]
