-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for delegate functions
module Test.Interpreter.SetDelegate
  ( test_setDelegate
  , test_registerDelegate
  ) where

import Fmt (pretty)
import Test.Tasty (TestTree)

import Morley.Client.RPC.Error (ClientRpcError(..))
import Morley.Michelson.Runtime (ExecutorErrorPrim(..))
import Morley.Michelson.Runtime.GState (GStateUpdateError(..))
import Morley.Michelson.Typed
import Morley.Mavryk.Address
import Morley.Mavryk.Crypto
import Test.Cleveland
import Test.Cleveland.Internal.Abstract (AddressAndAlias)
import Test.Cleveland.Internal.Exceptions.Annotated (fromPossiblyAnnotatedException)

contract :: TypedContract (Maybe KeyHash) () ()
contract = TypedContract $ defaultContract $
  CAR `Seq` SET_DELEGATE `Seq` DIP NIL `Seq` CONS `Seq` DIP UNIT `Seq` PAIR

test_setDelegate :: IO TestTree
test_setDelegate =
  pure $ testScenario "setDelegate works" $ scenario do
    handle <- originate "set_delegate" () contract
    getDelegate handle @@== Nothing
    addr <- newAddress "delegate"
    registerDelegate addr
    let kh = unImplicitAddress $ toImplicitAddress addr
    transfer handle $ calling def (Just kh)
    getDelegate handle @@== Just kh
    transfer handle $ calling def Nothing
    getDelegate handle @@== Nothing

test_registerDelegate :: TestTree
test_registerDelegate =
  testScenario "setDelegate fails on address not registered as delegate" $ scenario do
    handle <- originate "set_delegate" () contract
    getDelegate handle @@== Nothing
    addr <- newFreshAddress "delegate"
    transfer addr [mv|100 u|]
    let kh = unImplicitAddress $ toImplicitAddress addr
    (mbException :: Either SomeException ()) <- attempt $ transfer handle $ calling def (Just kh)
    ifEmulation
      case mbException of
        Left (fromPossiblyAnnotatedException @(ExecutorErrorPrim AddressAndAlias) ->
            Just (EEFailedToApplyUpdates (GStateNotDelegate errAddr)))
          -> toAddress errAddr @== toAddress addr
        Left e ->
          failure $ "Expected failure with 'unregistered delegate', but got "
            <> pretty (displayException e)
        Right _ -> failure "Expected failure, but the call succeeded"
      case mbException of
        Left (fromPossiblyAnnotatedException -> Just (DelegateNotRegistered errAddr))
          -> errAddr @== toImplicitAddress addr
        Left e -> failure $
          "Expected failure with 'unregistered delegate', but got "
            <> pretty (displayException e)
        Right _ -> failure "Expected failure, but the call succeeded"
