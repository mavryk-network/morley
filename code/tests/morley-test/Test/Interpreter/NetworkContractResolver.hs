-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Interpreter.NetworkContractResolver
  ( test_network_contract_resolver
  ) where

import Data.Vinyl (Rec(..))
import Fmt (pretty)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure)

import Morley.Client.OnlyRPC
import Morley.Client.RPC
import Morley.Michelson.Interpret
import Morley.Michelson.Runtime.Dummy (dummyBigMapCounter, dummyContractEnv, dummyGlobalCounter)
import Morley.Michelson.Typed (Instr(..), T(..), Value'(..), pattern (:#), toVal)
import Morley.Michelson.Typed qualified as T
import Morley.Mavryk.Address

import Test.Cleveland

type TestEvalM = EvalOpT IO

type TStorage t = 'TOption ('TContract t)

runTestEvalOp
  :: forall t. (T.ForbidNestedBigMaps t, T.ForbidOp t, T.WellTyped t)
  => Address
  -> MorleyOnlyRpcEnv
  -> IO (Either (MichelsonFailureWithStack Void) (T.Value (TStorage t)))
runTestEvalOp addr more = do
  let act = runInstr instr =<< mapToStkEl @NoStkElMeta initStack
      instr :: T.Instr (T.ContractInp 'TAddress (TStorage t)) (T.ContractOut (TStorage t))
      instr = CAR :# CONTRACT T.DefEpName :# NIL :# PAIR
      initStack = mkInitStack (toVal addr) (VOption Nothing)
      ce = (dummyContractEnv @TestEvalM)
        { ceContracts = liftIO . runMorleyOnlyRpcM more . contractStateResolver HeadId }
      initSt = InterpreterState (ceMaxSteps ce) dummyGlobalCounter dummyBigMapCounter
  res' <- rslResult <$> runEvalOpT act ce initSt
  pure $ res' <&> \(StkEl (VPair (_, res)) :& RNil) -> res

test_network_contract_resolver :: TestTree
test_network_contract_resolver = testScenarioOnNetwork "Contract resolver" $ scenarioNetwork do
  contractAddr <- originate "dummy" () saveInStorageContract

  more <- getOnlyRpcEnv []
  runIO do
    -- correct contract type
    runTestEvalOp @'TUnit (toAddress contractAddr) more >>= \case
      Right (VOption opt)
        | Just (VContract addr _) <- opt
        , toAddress contractAddr == addr -> pass
        | otherwise -> assertFailure "Contract address doesn't match"
      Left err -> assertFailure $ pretty err

    -- incorrect type
    runTestEvalOp @'TNat (toAddress contractAddr) more >>= \case
      Right (VOption opt)
        | Nothing <- opt -> pass
        | otherwise -> assertFailure "Found contract when shouldn't have"
      Left err -> assertFailure $ pretty err

    -- (hopefully) no such contract
    runTestEvalOp @'TNat (toAddress [ta|KT1LZwEZqbqtLYhwzaidBp6So9LgYDpkpEv7|]) more >>= \case
      Right (VOption opt)
        | Nothing <- opt -> pass
        | otherwise -> assertFailure "Found contract that was assumed nonexistent"
      Left err -> assertFailure $ pretty err

saveInStorageContract :: TypedContract () () ()
saveInStorageContract = TypedContract $ T.defaultContract $ CAR :# NIL :# PAIR
