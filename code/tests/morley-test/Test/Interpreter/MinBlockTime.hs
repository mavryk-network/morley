-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for @MIN_BLOCK_TIME@ instruction
module Test.Interpreter.MinBlockTime
  ( test_minBlockTime
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Test.Cleveland

contract :: TypedContract () Natural ()
contract = TypedContract $ defaultContract $
  DROP `Seq` MIN_BLOCK_TIME `Seq` NIL `Seq` PAIR

test_minBlockTime :: IO TestTree
test_minBlockTime =
  pure $ testScenario "MIN_BLOCK_TIME works" $ scenario do
    handle <- originate "MIN_BLOCK_TIME" 0 contract
    transfer handle $ calling def ()
    delay <- getMinBlockTime
    getStorage handle @@== delay
