-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on on-chain views.
module Test.Interpreter.Views
  ( test_runtime
  , test_step_constants
  , test_recursion
  , test_create_contract
  , test_context_restore_on_exit
  ) where

import Fmt (Buildable, (+|), (|+))
import Test.Tasty (TestTree, testGroup)

import Morley.Mavryk.Address
import Morley.Mavryk.Core
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Cleveland.Lorentz.Types
import Test.Util.Contracts

-- Tests from Mavryk
--
-- Taken from https://gitlab.com/mavryk-network/mavryk-protocol/-/blob/6eef585d8e897b94b330323987ee0e6228189791/tests_python/tests_alpha/test_contract.py#L681
----------------------------------------------------------------------------

amongMavrykContracts :: FilePath -> FilePath
amongMavrykContracts file = contractsDir </> "mavryk_examples" </> "opcodes" </> file

originateViewedContract
  :: MonadCleveland caps m => m (ContractHandle Natural Natural ())
originateViewedContract =
  importContract (amongMavrykContracts "view_toplevel_lib.mv")
    >>= \c -> originate
      "viewedContract"
      (3 :: Natural)
      c
      [mv|777u|]

test_runtime :: TestTree
test_runtime = testGroup "Basic interpreter logic"
  [ checkContract "view_op_id" ((0, 0) :: (Natural, Natural)) (10, 3)
  , checkContract "view_op_add" (42 :: Natural) 13
  , checkContract "view_fib" (0 :: Natural) 55
  , checkContract "view_mutual_recursion" (0 :: Natural) 20
  , checkContract "view_op_nonexistent_func" True False
  , checkContract "view_op_nonexistent_addr" True False
  , checkContract "view_op_toplevel_inconsistent_input_type" (5 :: Natural) 0
  , checkContract "view_op_toplevel_inconsistent_output_type" True False
  ]
  where
    checkContract
      :: (NiceStorage st, Eq st, Buildable st, AsRPC st ~ st)
      => String -> st -> st -> TestTree
    checkContract fileName initSt expected =
      testScenario fileName $ scenario $ clarifyErrors ("For '" +| fileName |+ "'") do
        viewedContract <- originateViewedContract
        viewCaller
          <- importContract @(Natural, Address) @_ @() (amongMavrykContracts $ fileName <> ".mv")
          >>= originate "viewCaller" initSt
        transfer viewCaller $ calling def (10, toAddress viewedContract)
        getStorage viewCaller @@== expected

test_step_constants :: TestTree
test_step_constants =
  testScenario "Step constants contract" $ scenario do
    alice <- newAddress auto
    viewedContract <- originateViewedContract
    viewCaller
      <- importContract @Address @(Maybe ((Mumav, Mumav), ((Address, Address), Address))) @()
         (amongMavrykContracts "view_op_test_step_contants.mv")
      >>= originate "viewCaller" Nothing
    withSender alice do
      transfer viewCaller $ calling def (toAddress viewedContract)
    let expectedSelfAddress = toAddress viewedContract
        expectedSender = toAddress viewCaller
        expectedSource = toAddress alice
    getStorage viewCaller @@== Just
      ( ( 0
        , 777
        )
      , ( ( expectedSelfAddress
          , expectedSender
          )
        , expectedSource
        )
      )

test_recursion :: TestTree
test_recursion =
  testScenario "Recursive call" $ scenario do
    contract
      <- importContract @() @() @() (amongMavrykContracts "view_rec.mv")
      >>= originate "contract" ()
    transfer contract
      & expectTransferFailure gasExhaustion

test_create_contract :: TestTree
test_create_contract =
  testScenario "Recursive call" $ scenario do
    contractCreator
      <- importContract @() @(Maybe Address) @()
         (amongMavrykContracts "create_contract_with_view.mv")
      >>= originate "contractCreator" Nothing
    transfer contractCreator
    contractAddr <- getStorage contractCreator >>= \case
      Nothing -> failure "Unexpected outcome"
      Just addr -> pure addr

    constViewObserver
      <- importContract @(Natural, Address) @Natural @()
         (amongMavrykContracts "view_op_constant.mv")
      >>= originate "constViewObserver" 2
    transfer constViewObserver $ calling def (10, contractAddr)

    getStorage constViewObserver @@== 10

-- In Mavryk they have a lot of tests covering the fact that environment must
-- be restored on view exist (the tests were added along with a bugfix),
-- but we don't need this, we use 'ReaderT' that makes the issue hard to produce.
test_context_restore_on_exit :: TestTree
test_context_restore_on_exit =
  testScenario "Context restores" $ scenario do
    viewedContract <- originateViewedContract
    selfAfterView
      <- importContract @Address @Address @()
         (amongMavrykContracts "self_after_view.mv")
      >>= \c -> originate
        "selfAfterView"
        (toAddress viewedContract)
        c
        [mv|1500u|]  -- transferred by the contract further

    transfer selfAfterView $ calling def (toAddress viewedContract)
    getStorage selfAfterView @@== toAddress selfAfterView
