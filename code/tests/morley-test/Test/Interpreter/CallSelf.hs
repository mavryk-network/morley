-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for the contract that calls self several times.

module Test.Interpreter.CallSelf
  ( test_self_caller
  ) where

import Hedgehog (forAll, property, withTests)
import Hedgehog.Gen qualified as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Unsafe qualified (fromIntegral)

import Morley.Michelson.Typed as T
import Morley.Michelson.Untyped qualified as U
import Test.Cleveland
import Test.Cleveland.Michelson.Import (embedContract)

import Test.Util.Contracts

type Parameter = 'TInt
type Storage = 'TNat

test_self_caller :: TestTree
test_self_caller =
  testProperty propertyDescription $
    withTests 10 $ property $ do
      callCount <- forAll $ Gen.enum minCalls maxCalls
      testScenarioProps $ clevelandTransferScenario (T.convertContract selfCaller) callCount
  where
    propertyDescription =
      "calls itself n times, sets storage to n OR fails due to gas limit"

    minCalls = 1
    maxCalls = 10

    selfCaller = $$(embedContract @Parameter @Storage (inContractsDir "call_self_several_times.mv"))

    clevelandTransferScenario :: U.Contract -> Integer -> Scenario m
    clevelandTransferScenario uSelfContract parameter = scenario do
      address <- originate "self-caller" (U.ValueInt 0) uSelfContract
      transfer address [mv|1u|] $ unsafeCalling def parameter
      let expectedStorage :: Natural = Unsafe.fromIntegral @Integer @Natural parameter
      getStorage @Natural address @@== expectedStorage
