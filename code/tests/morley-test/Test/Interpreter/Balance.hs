-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Interpreter.Balance
  ( test_balanceIncludesAmount
  , test_balanceIncludesAmountComplexCase
  ) where

import Hedgehog (Gen, forAll, property, withTests)
import Hedgehog.Gen qualified as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Morley.Michelson.Typed
import Morley.Michelson.Untyped qualified as U
import Morley.Mavryk.Core
import Test.Cleveland
import Test.Cleveland.Michelson (testTreesWithUntypedContract)

import Test.Util.Contracts

data Fixture =
  Fixture
    { fStartingBalance :: Mumav
    , fAmount :: Mumav
    } deriving stock (Show)

genFixture :: Gen Fixture
genFixture = do
  fStartingBalance <- Gen.enum 1000 5000
  fAmount <- Gen.enum 0 1000
  return Fixture{..}

test_balanceIncludesAmount :: IO [TestTree]
test_balanceIncludesAmount = do
  testTreesWithUntypedContract
    (inContractsDir "check_if_balance_includes_incoming_amount.mv") $
      \checker ->
        pure
          [ testProperty "BALANCE includes AMOUNT" $ withTests 50 $ property $ do
              fixture <- forAll genFixture
              testScenarioProps $ scenario $ clevelandBalanceTestScenario
                checker fixture
          ]

clevelandBalanceTestScenario :: Monad m => U.Contract -> Fixture -> ClevelandT m ()
clevelandBalanceTestScenario checker Fixture{..} = do
  let result = unsafeAddMumav fStartingBalance fAmount

  address <- originate
    "checkIfBalanceIncludeAmount"
    (untypeValue $ toVal ())
    checker
    fStartingBalance

  transfer address fAmount $ unsafeCalling def result
  getBalance address @@== result

test_balanceIncludesAmountComplexCase :: IO [TestTree]
test_balanceIncludesAmountComplexCase = do
  testTreesWithUntypedContract (inContractsDir "balance_test_case_a.mv") $ \contractA ->
    testTreesWithUntypedContract (inContractsDir "balance_test_case_b.mv") $ \contractB ->
      pure
        [ testScenario "BALANCE returns expected value in nested calls" $ scenario do
            addressA <- originate
              "balance_test_case_a"
              (untypeValue $ toVal @[Mumav] [])
              contractA
            addressB <- toAddress <$> originate
              "balance_test_case_b"
              (untypeValue $ toVal ())
              contractB

            transfer addressA [mv|100u|] $ unsafeCalling def addressB

            -- A sends 30 to B, then B sends 5 back to A. A records call to BALANCE at each entry.
            -- We expect that 5 mumav sent back are included in the second call to BALANCE.
            let expectedStorage = [75, 100]
            getStorage @[Mumav] addressA @@== expectedStorage
        ]
