-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Test.Interpreter.Reference
  ( test_InterpreterWithReferenceImplementation
  , test_Regression688
  ) where

import Data.Default (def)
import Data.Map qualified as Map
import Data.Singletons (Sing, demote, fromSing)
import Fmt (pretty)
import Hedgehog (MonadTest, PropertyT, annotate, evalIO, forAll, property, withTests, (===))
import System.IO.Memoize (once)
import Test.Tasty (TestTree, testGroup, withResource)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson.Typed (genValue)
import Morley.AsRPC (AsRPC, TAsRPC, rpcStorageScopeEvi, valueAsRPC)
import Morley.Client
  (AliasBehavior(..), HasMavrykClient, RunError(..), runMorleyClientM, runMorleyOnlyRpcM)
import Morley.Client.Action.Origination qualified as Reference
import Morley.Client.Logging (WithClientLog)
import Morley.Client.RPC (HasMavrykRpc, RunCodeErrors(..))
import Morley.Client.MavrykClient.Impl (getSecretKey)
import Morley.Client.Types
import Morley.Client.Util as Reference (RunContractParameters(..), runContract)
import Morley.Michelson.Interpret
  (InterpretError(..), MichelsonFailed(..), MichelsonFailureWithStack(..))
import Morley.Michelson.Runtime as Morley
import Morley.Michelson.Runtime.GState (genesisAddress)
import Morley.Michelson.Typed
  (Contract, Contract'(..), EpAddress(..), SingT(..), SomeContract(..), Value, Value'(..),
  dfsMapValue, dfsTraverseValue)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Arith (ArithError(..), MumavArithErrorType(..))
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Typed.T
import Morley.Michelson.Typed.Util (DfsSettings(..))
import Morley.Michelson.Untyped qualified as U
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Mavryk.Core (Timestamp(..), dummyChainId, getCurrentTime, zeroMumav)
import Morley.Mavryk.Crypto
import Morley.Util.Peano (Peano, pattern S, pattern Z)
import Test.Cleveland (NetworkEnv(..), mkMorleyOnlyRpcEnvNetwork)
import Test.Cleveland.Internal.Abstract (Moneybag(Moneybag))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Michelson.Import
import Test.Cleveland.Tasty (whenNetworkEnabled)
import Test.Cleveland.Util (failedTest)
import Test.Util.Contracts (getWellTypedMichelsonContracts)

-- To make `run_code` accept parameters with `contract t` we have to originate a
-- bunch of dummy contracts ahead of time. Otherwise, `run_code` looks up the
-- contract on chain and complains it's invalid (either because it doesn't exist
-- or its type is incorrect). The alternative is to exclude ~16 contracts from
-- testing, which sounds worse.
type DummyContracts = Map T.T (ContractAddress, Reference.OriginationData)

deriving stock instance Ord Peano
deriving stock instance Ord T.T

test_InterpreterWithReferenceImplementation :: IO TestTree
test_InterpreterWithReferenceImplementation = do
  files <- filter (`notElem` excludedContracts) <$> getWellTypedMichelsonContracts

  let noNeverParameter :: SomeContract -> Bool
      noNeverParameter (SomeContract (Contract{} :: Contract cp st)) =
        not $ isNeverType $ demote @cp

  filesAndContracts :: [(FilePath, SomeContract)] <-
    filter (noNeverParameter . snd) <$> forM files \file -> do
      someContract <- importSomeContract file
      pure (file, someContract)

  pure $
    whenNetworkEnabled $ \withEnv ->
      withResource (once $ createDummyContracts withEnv (snd <$> filesAndContracts)) (const pass) $
        \idcs -> testGroup "compare interpreter with reference implementation" $
          withFrozenCallStack $ testContract (join idcs) withEnv <$> filesAndContracts
        -- this 'join' probably needs some explanation. So 'withResource'
        -- eagerly acquires the resource and saves it to a 'TVar'. Hence, @idcs
        -- :: IO resource@. 'once' on the other hand makes the IO action lazy;
        -- it returns @IO (IO a)@. Hence, @idcs :: IO (IO a)@. The outer 'IO' is
        -- just reading the 'TVar', the inner actually performs the memoized
        -- deferred computation. The use of 'withResource' allows us to run
        -- 'once' eagerly before the test tree. We can't move @once $
        -- createDummyContracts ...@ to the outside IO context because it needs
        -- access to @withEnv@.
  where
    -- These constracts are currently excluded from the tests since
    -- our or reference implementations doesn't act in an expected way.
    --
    -- Ideally, we should remove the usage of this list one day :coolstory:
    excludedContracts :: [FilePath]
    excludedContracts =
      [ "../../contracts/voting_powers.mv"
      , "../../contracts/mavryk_examples/opcodes/voting_power.mv"
      -- ↑ We cannot predict the total voting power in a real chain.
      -- There is still a similar @voting_power.mv@ that touches only
      -- @VOTING_POWER@ instruction.
      , "../../contracts/mavryk_examples/opcodes/level.mv"
      -- ↑ We cannot predict the actual block level in a real chain.
      -- There is another @../../contracts/level.mv@ contract that checks that we can interpret
      -- the @LEVEL@ instruction, without checking its result.
      , "../../contracts/sapling_annot.mv"
      , "../../contracts/sapling_transaction.mv"
      , "../../contracts/mavryk_examples/opcodes/sapling_empty_state.mv"
      -- ↑ Sapling types and instructions are supported for typechecking only and not the actual
      -- implementation.
      , "../../contracts/mavryk_examples/mini_scenarios/large_str_id.mv"
      -- ↑ Exponential growth of storage, crashes on even small-ish (a few dozen) parameter values
      , "../../contracts/mavryk_examples/mini_scenarios/receive_tickets_in_big_map.mv"
      -- ↑ Uses big_map parameter which apparently needs to exist beforehand
      , "../../contracts/mavryk_examples/mini_scenarios/large_error.mv"
      -- ↑ Sporadically causes gas exhaustion on network
     ]

test_Regression688 :: IO TestTree
test_Regression688 = do
  let file = "../../contracts/mavryk_examples/mini_scenarios/xcat_dapp.mv"
      parameter = VOr @_ @('TOr 'TBytes 'TBytes) $ Left $
        VPair
          (VAddress $ EpAddress'
            { eaAddress = MkAddress $ ImplicitAddress $ Hash
                { hTag = HashKey KeyTypeEd25519
                , hBytes = "UK\224\155\204\188\151\163%\183I\194*H\252P\246\&4\192\205"
                }
            , eaEntrypoint = U.DefEpName}
          , VPair (VBytes "", VTimestamp $ Timestamp 1225497600)
          )
      storage = VPair (
        VBigMap @'TBytes @('TPair ('TPair 'TAddress 'TAddress) ('TPair 'TMumav 'TTimestamp))
          Nothing mempty,VUnit)

  contract <- importContract file

  pure $
    whenNetworkEnabled $ \withEnv ->
      testProperty "#688 regression test" $ withTests 1 $ property $
        withFrozenCallStack $
          compareWithReference mempty withEnv file contract parameter storage T.unsafeEpcCallRoot

testContract
  :: HasCallStack
  => IO DummyContracts
  -> (forall a. (NetworkEnv -> IO a) -> IO a)
  -> (FilePath, SomeContract) -> TestTree
testContract dcs withEnv (file, SomeContract (contract@Contract{..} :: Contract cp st)) =
  -- We run each contract 4 times to check that it behaves the same way on different
  -- inputs.
  testProperty ("compare result with morley interpreter for " <> file) $
    withTests 4 $ property $ case T.mkDefEntrypointCall cParamNotes of
      T.MkEntrypointCallRes _ (epc :: T.EntrypointCallT cp epArg) -> do
        parameter <- forAll $ genValue @epArg
        storage <- forAll $ genValue @st

        compareWithReference dcs withEnv file contract parameter storage epc

idContract :: (ParameterScope cp, StorageScope st) => T.Contract cp st
idContract = T.defaultContract $ T.CDR T.:# T.NIL T.:# T.PAIR

walkParameterContracts
  :: forall cp m. Monad m
  => (forall arg. Value ('T.TContract arg) -> m (Value ('T.TContract arg)))
  -> Value cp -> m (Value cp)
walkParameterContracts f = dfsTraverseValue def
  { dsValueStep = \case
      c@VContract{} -> f c
      x -> pure x
  }

walkParameterContractsSing
  :: forall (cp :: T.T) m. (Monad m, WellTyped cp)
  => (forall arg. ParameterScope arg => Sing ('T.TContract arg) -> m ())
  -> Sing cp
  -> m ()
walkParameterContractsSing f = \case
  STOption t -> walkParameterContractsSing f t
  STList t -> walkParameterContractsSing f t
  STSet{} -> pass -- set can't have contract inside
  STTicket t -> walkParameterContractsSing f t
  STPair a b -> do
    walkParameterContractsSing f a
    walkParameterContractsSing f b
  STOr a b -> do
    walkParameterContractsSing f a
    walkParameterContractsSing f b
  STMap _ v -> walkParameterContractsSing f v
  STBigMap _ v -> walkParameterContractsSing f v
  --
  c@STContract{} -> f c
  --
  STKey                  -> pass
  STSignature            -> pass
  STChainId              -> pass
  STUnit                 -> pass
  STOperation            -> pass
  STLambda{}             -> pass
  STInt                  -> pass
  STNat                  -> pass
  STString               -> pass
  STBytes                -> pass
  STMumav                -> pass
  STBool                 -> pass
  STKeyHash              -> pass
  STBls12381Fr           -> pass
  STBls12381G1           -> pass
  STBls12381G2           -> pass
  STTimestamp            -> pass
  STAddress              -> pass
  STChest                -> pass
  STChestKey             -> pass
  STNever                -> pass
  STSaplingState{}       -> pass
  STSaplingTransaction{} -> pass

createDummyContracts
  :: ((NetworkEnv -> IO DummyContracts) -> IO DummyContracts)
  -> [SomeContract]
  -> IO DummyContracts
createDummyContracts withEnv contracts = withEnv \env -> do
  Moneybag addr <- setupMoneybagAddress env
  moneybagSecretKey <- runMorleyClientM (neMorleyClientEnv env) $ do
    getSecretKey (awaAlias addr)
  -- using MorleyOnlyRpcM here for speed; we only care about getting this
  -- contracts on the network, not known to @mavkit-client@. So save some time
  -- (quite a bit!) on @mavkit-client@ invocations.
  runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [moneybagSecretKey]) $
    makeContracts addr requiredParams
  where
    requiredParams :: Map T.T Reference.OriginationData
    requiredParams = executingState mempty $ forM_ @_ @_ @() contracts $
      \(SomeContract (Contract{} :: T.Contract cp st)) ->
        sing @cp & walkParameterContractsSing \(STContract (arg :: Sing arg)) -> do
          let k = fromSing arg
          modify $ Map.insertWith (flip const) k $ Reference.OriginationData
            { odAliasBehavior = OverwriteDuplicateAlias
            , odName = ContractAlias $ pretty k
            , odBalance = 0
            , odContract = idContract @arg
            , odStorage = VUnit
            , odMbFee = Nothing
            , odDelegate = Nothing
            }
    makeContracts
      :: (HasMavrykRpc m, HasMavrykClient m, WithClientLog env m)
      => ImplicitAddressWithAlias
      -> Map T.T Reference.OriginationData
      -> m DummyContracts
    makeContracts addr ps = fromList . zipWith (flip $ fmap . (,)) (toPairs ps) . snd <$>
      Reference.originateContracts addr (toList ps)
    -- Originating one of these contracts takes about 1500 gas at the time of
    -- writing, and gas limit per block is 5200000, so unless we're originating
    -- thousands of contracts, this _should_ fit. But it's always an option to
    -- split it into chunks if it doesn't. -- @lierdakil

compareWithReference
  :: forall cp st epArg. (ParameterScope epArg, StorageScope st)
  => IO DummyContracts
  -> (forall a. (NetworkEnv -> IO a) -> IO a)
  -> FilePath -> Contract cp st -> Value epArg -> Value st -> T.EntrypointCallT cp epArg
  -> PropertyT IO ()
compareWithReference dummyCts withEnv file contract parameter' storage epc = do
  -- replace random `contract t` values with dummy ones which exist on the net
  (parameter, dummyCts' :: DummyContracts) <- evalIO . usingStateT mempty $
    parameter' & walkParameterContracts @epArg \case
      c@(VContract addr' (epc' :: T.SomeEntrypointCallT arg))
        | STUnit <- sing @arg
        , MkAddress ImplicitAddress{} <- addr'
        -> pure c -- allow implicit addresses for `contract unit`
        | otherwise -> do
            -- in case this looks mysterious, we're not evaluating 'dummyCts'
            -- until we need it here, but we later need to reuse it while
            -- setting up contract environment, hence we're saving it to state.
            dummyCts' <- lift dummyCts
            put dummyCts'
            case Map.lookup (demote @arg) dummyCts' of
              Just (addr, _) -> pure (VContract (MkAddress addr) epc')
              Nothing -> error $ "failed to find dummy contract for " <> pretty (demote @arg)

  let balance = 4000000000000

  resReference <- evalIO $ withEnv \NetworkEnv{..} ->
    try @_ @RunCodeErrors $ runMorleyClientM neMorleyClientEnv $
      Reference.runContract @cp @st RunContractParameters
        { rcpContract = contract
        , rcpParameter = T.untypeValue parameter
        , rcpStorage = T.untypeValue storage
        , rcpBalance = balance
        , rcpAmount = zeroMumav
        , rcpSender = Just genesisAddress
        , rcpSource = Just genesisAddress
        , rcpLevel = Nothing
        , rcpNow = Nothing
        }

  currentTimestamp <- evalIO getCurrentTime

  let
    -- Reference implementation sends 0,05 mv to implicit contract for this contract
    amount = if file == "../../contracts/mavryk_examples/opcodes/proxy.mv"
            then 50000 else minBound

    od2cs :: Reference.OriginationData -> ContractState
    od2cs Reference.OriginationData{..} = ContractState
      { csBalance = odBalance
      , csContract = odContract
      , csStorage = odStorage
      , csDelegate = Nothing
      }
    resMorley = snd <$> Morley.runCode (runCodeParameters contract storage epc parameter)
      { rcBalance = balance
      , rcAmount = amount
      , rcKnownContracts = fromList $ fmap od2cs <$> toList dummyCts'
      , rcNow = currentTimestamp
      , rcSelf = Just [ta|KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi|]
      }

  compareResults resReference resMorley parameter storage

compareResults
      :: forall cp st m.
         (HasCallStack, StorageScope st, MonadTest m)
      => Either RunCodeErrors (AsRPC (Value st))
      -> Either (InterpretError Void) (Value st) -> Value cp -> Value st -> m ()
compareResults (Left rpcErr) (Left interpreterErr) _ _ = compareErrors rpcErr interpreterErr
compareResults (Left err) (Right _) parameter storage = do
  failedTest . fromString $
    "Morley interpreter unexpectedly didn't fail.\n Passed parameter: " <> pretty parameter <>
    ".\n Passed storage: " <> pretty storage <> ".\n Reference implementation failed with: " <>
    pretty err
compareResults (Right _) (Left err) parameter storage = do
  failedTest . fromString $
    "Morley interpreter unexpectedly failed.\n " <> "Passed parameter: " <> pretty parameter <>
    ".\n Passed storage: " <> pretty storage <> ".\n Morley interpreter failed with: " <>
    pretty err
compareResults (Right st1) (Right st2) parameter storage =
  withDict (rpcStorageScopeEvi @st) $ do
    annotate $
      ("Both contracts succeeded, but new storages are different.\n Passed parameter: " <>
        pretty parameter <> ".\n Passed storage: " <> pretty storage <> ".\n" <>
        "Reference implementation returned storage: " <> pretty st1 <> ".\n" <>
        "Morley returned storage: " <> pretty st2
      )
    compareValues st1 st2

-- | Compare values with weaken equality requirements, e.g. we don't
-- check that @VAddress@, @VBytes@, @VChainId@ and @VTimestamp@ have same constructor arguments,
-- since they can be different even when both interpreters were successfully run.
--
-- We also don't check big_map contents or their IDs.
compareValues
  :: forall st m. (HasCallStack, MonadTest m, SingI st)
  => Value (TAsRPC st) -> Value st -> m ()
compareValues storageReference storageMorley =
  preprocessValue storageReference === preprocessValue (valueAsRPC storageMorley)
  where
    preprocessValue :: Value (TAsRPC st) -> Value (TAsRPC st)
    preprocessValue = dfsMapValue def{dsValueStep = pure . placeStubs} . scrubBigMapIDs (sing @st)

    placeStubs :: Value t -> Value t
    placeStubs = \case
      VAddress _ -> VAddress $ EpAddress genesisAddress U.DefEpName
      VBytes _ -> VBytes "kek"
      VChainId _ -> VChainId dummyChainId
      VTimestamp _ -> VTimestamp $ Timestamp 100
      v -> v

    -- Scrub all big_map IDs from a storage value.
    --
    -- The morley interpreter and the RPC might assign different IDs to each big_map,
    -- so we need to scrub all IDs before checking whether the two storages are equivalent.
    scrubBigMapIDs :: forall t. Sing t -> Value (TAsRPC t) -> Value (TAsRPC t)
    scrubBigMapIDs storageSing storage =
      case (storageSing, storage) of
        (STBigMap{}, VNat _) -> VNat 0
        (STOption vSing, VOption v) -> VOption $ scrubBigMapIDs vSing <$> v
        (STList vSing, VList v) -> VList $ scrubBigMapIDs vSing <$> v
        (STPair lSing rSing, VPair v) ->
          VPair $ bimap (scrubBigMapIDs lSing) (scrubBigMapIDs rSing) v
        (STOr lSing rSing, VOr v) -> VOr $ bimap (scrubBigMapIDs lSing) (scrubBigMapIDs rSing) v
        (STMap _ vSing, VMap v) -> VMap $ scrubBigMapIDs vSing <$> v
        _ -> storage

assertRpcErrs :: (HasCallStack, MonadTest m) => [RunError] -> (RunError -> Bool) -> String -> m ()
assertRpcErrs errs predicate msg =
  if any predicate errs then pass else do failedTest $ fromString msg

-- Note that error comparison can be extended when the new contracts will be added.
compareErrors :: (HasCallStack, MonadTest m) => RunCodeErrors -> InterpretError Void -> m ()
compareErrors rpcErr@(RunCodeErrors errs) InterpretError{ieFailure=mfwsFailed -> runtimeErr} =
  case runtimeErr of
    MichelsonFailedWith v ->
      assertRpcErrs errs
      (\case
          ScriptRejected {} -> True
          _ -> False
      ) $ "Morley interpreter failed with FAILWITH " <> pretty v <> ", \
          \however reference interpreter failed with:\n" <> displayException rpcErr
    MichelsonArithError arithErr -> case arithErr of
      ShiftArithError {}->
        assertRpcErrs errs
        (\case
            ScriptOverflow -> True
            _ -> False
        ) $ "Morley interpreter failed with shift overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
      MumavArithError AddOverflow _ _ ->
        assertRpcErrs errs
        (\case
            MumavAdditionOverflow {} -> True
            _ -> False
        ) $ "Morley interpreter failed with mumav addition overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
      MumavArithError MulOverflow _ _ ->
        assertRpcErrs errs
        (\case
            MumavMultiplicationOverflow {} -> True
            ScriptOverflow -> True
            _ -> False
        ) $ "Morley interpreter failed with mumav multiplication overflow, \
            \however reference interpreter failed with:\n" <> displayException rpcErr
    MichelsonGasExhaustion ->
      assertRpcErrs errs
      (\case
          GasExhaustedOperation -> True
          _ -> False
      ) $ "Morley interpreter failed due to gas exhaustion, \
          \however reference interpreter failed with:\n" <> displayException rpcErr
    _ -> do
      failedTest . fromString $ "Unexpected morley runtime failure:\n" <> pretty runtimeErr <>
          "\nReference interpreter failed with:\n" <> displayException rpcErr

isNeverType :: T -> Bool
isNeverType = \case
  TNever    -> True
  TTicket t -> isNeverType t
  TPair a b -> isNeverType a || isNeverType b
  TOr a b   -> isNeverType a && isNeverType b
  -- non-interesting cases:
  TKey                  -> False
  TSignature            -> False
  TChainId              -> False
  TUnit                 -> False
  TOption{}             -> False
  TList{}               -> False
  TSet{}                -> False
  TOperation            -> False
  TContract{}           -> False
  TLambda{}             -> False
  TMap{}                -> False
  TBigMap{}             -> False
  TInt                  -> False
  TNat                  -> False
  TString               -> False
  TBytes                -> False
  TMumav                -> False
  TBool                 -> False
  TKeyHash              -> False
  TBls12381Fr           -> False
  TBls12381G1           -> False
  TBls12381G2           -> False
  TTimestamp            -> False
  TAddress              -> False
  TChest                -> False
  TChestKey             -> False
  TSaplingState{}       -> False
  TSaplingTransaction{} -> False
