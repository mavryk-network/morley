-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests the `WithLoc` of nested loop instr

module Test.Interpreter.WithLoc
  ( unit_WithLoc_nested_loop
  ) where

import Control.Monad.Writer (tell)
import Data.Vinyl (Rec(..))
import Fmt (pretty)
import Test.Tasty.HUnit (Assertion)

import Morley.Michelson.ErrorPos
import Morley.Michelson.Interpret
import Morley.Michelson.Runtime.Dummy (dummyBigMapCounter, dummyContractEnv, dummyGlobalCounter)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Instr
import Test.Cleveland.Michelson (importContract)

import Test.Util.Contracts (contractsDir, (</>))
import Test.Util.HUnit (assertEqualBuild)

unit_WithLoc_nested_loop :: Assertion
unit_WithLoc_nested_loop = do
  (contract :: T.Contract 'T.TUnit 'T.TInt) <- importContract (contractsDir </> "nested_loop.mv")
  initialEl <- mkStkEl (T.VPair (T.VUnit, T.VInt 0))
  withLocAssertion
    (initialEl :& RNil)
    (MorleyLogs
      [ "8:7"
      , "10:14"
      , "10:14"
      , "10:14"
      , "8:7"
      , "10:14"
      , "10:14"
      , "8:7"
      ])
    contract

withLocAssertion
  :: HasCallStack
  => Rec (StkEl NoStkElMeta) (T.ContractInp cp st)
  -> MorleyLogs
  -> T.Contract cp st
  -> Assertion
withLocAssertion initialStack expectedLogs contract = do
  let initialState = initInterpreterState dummyGlobalCounter dummyBigMapCounter dummyContractEnv
  let logs = rslLogs $
        runEvalOp (runWithLog (T.unContractCode $ T.cCode contract) initialStack)
          dummyContractEnv initialState
  assertEqualBuild
    "Expect the logs to be the same, but they are not."
    logs expectedLogs

runWithLog :: EvalM m => InstrRunner NoStkElMeta m
runWithLog instr stack = do
  let logWithLoc ics = tell $ one $ pretty $ unErrorSrcPos ics
  case instr of
    WithLoc ics (LOOP _) -> logWithLoc ics
    WithLoc ics (LOOP_LEFT _) -> logWithLoc ics
    WithLoc ics (ITER _) -> logWithLoc ics
    _ -> pass
  runInstrImpl runWithLog instr stack
