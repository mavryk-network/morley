-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Interpreter.Pack
  ( test_PackDataAgainstReference
  ) where

import Fmt (Buildable(..), hexF, pretty)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (Assertion, testCase, (@?=))
import Text.Show (show)

import Morley.Client (runMorleyOnlyRpcM)
import Morley.Client qualified as Reference
import Morley.Michelson.Interpret.Pack (packValue')
import Morley.Michelson.Interpret.Unpack (unpackValue')
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.T
import Test.Cleveland (NetworkEnv(..), mkMorleyOnlyRpcEnvNetwork)
import Test.Cleveland.Michelson.Import
import Test.Cleveland.Tasty (whenNetworkEnabled)
import Test.Cleveland.Util
import Test.Util.Contracts (getWellTypedMichelsonContracts)

test_PackDataAgainstReference :: IO TestTree
test_PackDataAgainstReference = do
  files <- getWellTypedMichelsonContracts

  filesAndContracts :: [(FilePath, T.SomeContract)] <-
    forM files \file -> (file,) <$> importSomeContract file

  pure $
    whenNetworkEnabled $ \withEnv -> testGroup "PACK and UNPACK against reference" $
      [ testGroup "PACK" $
          filesAndContracts <&> \(file, contract) ->
            testCase file $ withFrozenCallStack $ compareWithReference withEnv contract
      , testGroup "UNPACK roundtrip" $
          filesAndContracts <&> \(file, contract) ->
            testCase file $ withFrozenCallStack $ compareWithReferenceUnpack withEnv contract
      ]

type TestValTy st = 'TLambda ('TPair ('TPair ('TOption 'TKeyHash) 'TMumav) st) 'TAddress

prepareValue :: T.Contract cp st -> T.Value (TestValTy st)
prepareValue contract@T.Contract{} = T.mkVLam $ T.RfNormal $
  T.UNPAIR `T.Seq` T.UNPAIR `T.Seq` T.CREATE_CONTRACT contract `T.Seq` T.DROP

runReference
  :: (forall a. (NetworkEnv -> IO a) -> IO a)
  -> T.Contract cp st -> IO (T.Value (TestValTy st), Text)
runReference withEnv contract@T.Contract{} = (value,) <$> withEnv \ne ->
  runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork ne []) $
    Reference.packData Reference.HeadId value T.starNotes
  where
    value = prepareValue contract

compareWithReference
  :: (forall a. (NetworkEnv -> IO a) -> IO a)
  -> T.SomeContract -> Assertion
compareWithReference withEnv (T.SomeContract contract@T.Contract{}) = do
  (value, resReference) <- runReference withEnv contract
  pretty (hexF $ packValue' value) @?= resReference

compareWithReferenceUnpack
  :: (forall a. (NetworkEnv -> IO a) -> IO a)
  -> T.SomeContract -> Assertion
compareWithReferenceUnpack withEnv (T.SomeContract contract@T.Contract{}) = do
  (value, resReference) <- runReference withEnv contract
  STB (Right value) @?= STB (either error unpackValue' $ fromHex resReference)

newtype ShowThroughBuild a = STB
  { unSTB :: a
  } deriving newtype (Eq, Ord)

instance Buildable a => Show (ShowThroughBuild a) where
  show = pretty . unSTB
