-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'transfer_n_create.mv' contract. See [#643]
module Test.Interpreter.TransferAndCreate
  ( test_transferAndCreate
  ) where

import Fmt ((+|), (|+))
import Test.Tasty (TestTree)

import Morley.Mavryk.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_transferAndCreate :: IO TestTree
test_transferAndCreate =
  pure $ testScenario "'transfer_n_create.mv' performs origination after transfer" $ scenario do
    contract <- importContract @() @Address @() (contractsDir  </> "transfer_n_create.mv")
    transferAndCreate <- originate "transferAndCreate" (toAddress constAddr) contract
    oldBalance <- getBalance constAddr
    transfer transferAndCreate [mv|10u|]
    newBalance <- getBalance constAddr
    newBalance - oldBalance @== 1
    newAddr <- getStorage @Address transferAndCreate
    -- Here we check that origination was performed
    case newAddr of
      MkAddress a@ContractAddress{} -> do
        () <- getStorage @() a
        pass
      _ -> do
        fail $ "newAddr: expected contract, but got " +| newAddr |+ ""

-- Address hardcoded in 'transfer_n_create.mv'.
constAddr :: ImplicitAddress
constAddr = [ta|mv1VRYAgy8jErk7wK85goZLhtWAVLficKksU|]
