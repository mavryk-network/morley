-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for the @PRINT@ extended command

module Test.Interpreter.Print
  ( test_print_simple
  , test_print_operations
  ) where

import Fmt ((+|), (|+))
import Test.Tasty (TestTree)

import Morley.Michelson.Interpret (MorleyLogs(..))
import Morley.Michelson.Text (MText)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Instr
import Morley.Mavryk.Address
import Test.Cleveland

test_print_simple :: [TestTree]
test_print_simple =
  [ testScenarioOnEmulator "PRINT prints naturals" $ scenarioEmulated do
      printer <- originate "printer" () $ TypedContract @Natural @_ @() printerContract
      logsInfo <- getMorleyLogs_ $
        transfer printer $ calling def 123
      collectLogs logsInfo @== MorleyLogs ["123"]
  , testScenarioOnEmulator "PRINT prints strings" $ scenarioEmulated do
      printer <- originate "printer" () $ TypedContract @MText @_ @() printerContract
      logsInfo <- getMorleyLogs_ $
        transfer printer $ calling def "hello"
      collectLogs logsInfo @== MorleyLogs ["\"hello\""]
  , testScenarioOnEmulator "PRINT prints right combs" $ scenarioEmulated do
      printer <- originate "printer" () $
        TypedContract @(Integer, (Integer, Integer)) @_ @() printerContract
      logsInfo <- getMorleyLogs_ $
        transfer printer $ calling def (1, (2, 3))
      collectLogs logsInfo @== MorleyLogs ["Pair 1 (Pair 2 3)"]
  , testScenarioOnEmulator "PRINT prints sequences" $ scenarioEmulated do
      printer <- originate "printer" () $
        TypedContract @[Integer] @_ @() printerContract
      logsInfo <- getMorleyLogs_ $
        transfer printer $ calling def [1..5]
      collectLogs logsInfo @== MorleyLogs ["{ 1; 2; 3; 4; 5 }"]
  ]

test_print_operations :: TestTree
test_print_operations =
    testScenarioOnEmulator "PRINT prints operations" $ scenarioEmulated do
      addr <- toAddress <$> newFreshAddress auto
      printer <- originate "printerOps" () $ TypedContract @Address @_ @() operationPrinter
      logsInfo <- getMorleyLogs_ $
        transfer printer $ calling def addr
      collectLogs logsInfo @==
        MorleyLogs [
          "{ Transfer 123 μꜩ tokens to Contract " +| addr |+
          " call Call <default>: × with parameter Unit\n}"
          ]

operationPrinter :: T.Contract 'T.TAddress 'T.TUnit
operationPrinter = T.Contract{..}
  where
    cParamNotes = T.starParamNotes
    cStoreNotes = T.starNotes

    cEntriesOrder = def

    stackRef = PrintComment . one . Right $ mkStackRef @0

    cViews = def
    cCode :: T.ContractCode 'T.TAddress 'T.TUnit
    cCode = T.mkContractCode $
      UNPAIR `Seq`
      CONTRACT T.DefEpName `Seq`
      IF_NONE (PUSH (T.VString "No contract") `Seq` FAILWITH) (
        PUSH (T.VMumav 123) `Seq`
        PUSH T.VUnit `Seq`
        TRANSFER_TOKENS `Seq`
        DIP NIL `Seq`
        CONS `Seq`
        Ext (PRINT stackRef) `Seq`
        DROP `Seq`
        NIL `Seq`
        PAIR
      )

printerContract :: (T.ContainsT 'T.PSNestedBigMaps p ~ 'False, T.ConstantScope p) => T.Contract p (T.ToT ())
printerContract = T.Contract{..}
  where
    cParamNotes = T.starParamNotes
    cStoreNotes = T.starNotes

    cEntriesOrder = def

    stackRef = PrintComment . one . Right $ mkStackRef @0

    cViews = def
    cCode = T.mkContractCode $
      UNPAIR `Seq`
      Ext (PRINT stackRef) `Seq`
      DROP `Seq`
      NIL `Seq`
      PAIR
