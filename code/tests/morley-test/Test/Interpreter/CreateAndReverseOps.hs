-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'create_2_n_reverse_ops.mv' contract.

-- Here we check that by storing 'GlobalCounter' in 'OriginationOpertion'
-- contract addresses aren't confused with each other even if we reverse the
-- list of operations manually in the contract.

module Test.Interpreter.CreateAndReverseOps
  ( test_createAndReverseOps
  ) where

import Fmt ((+|), (|+))
import Test.Tasty (TestTree)

import Morley.Michelson.Text (MText)
import Morley.Mavryk.Address
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Util.Contracts

test_createAndReverseOps :: IO TestTree
test_createAndReverseOps =
  pure $ testScenario "Resuling addresses match each other" $ scenario do
    contract <- importContract @() @(Address, Address) @()
      (contractsDir </> "create_2_n_reverse_ops.mv")
    createAndReverseOps <- originate "createAndReverseOps" (someAddr, someAddr) contract
    transfer createAndReverseOps
    (addrWithUnitStorage, addrWithStringStorage) <-
      getStorage @(Address, Address) createAndReverseOps
    case addrWithUnitStorage of
      MkAddress addr@ContractAddress{} -> do
        () <- getStorage @() addr
        pass
      _ -> do
        void $ fail $ "addrWithUnitStorage: Expected contract, but got " +| addrWithUnitStorage |+ ""
        pass
    case addrWithStringStorage of
      MkAddress addr@ContractAddress{} -> do
        stString <- getStorage @MText addr
        stString @== "kek"
      _ -> fail $ "addrWithStringStorage: Expected contract, but got " +| addrWithUnitStorage |+ ""

someAddr :: Address
someAddr = MkAddress [ta|mv1VRYAgy8jErk7wK85goZLhtWAVLficKksU|]
