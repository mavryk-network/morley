-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Util.Parser
  ( shouldParse
  ) where

import Fmt (build, indentF, pretty, unlinesF)
import Test.HUnit.Base (assertFailure)
import Text.Megaparsec (errorBundlePretty)

import Morley.Michelson.Parser (Parser, codeSrc)
import Morley.Michelson.Parser qualified as Parser

-- | Expect the given text to be successfully parsed.
shouldParse :: HasCallStack => Parser a -> Text -> IO a
shouldParse parser text =
  case Parser.parseNoEnv parser codeSrc text of
    Left err ->
      assertFailure . pretty $ unlinesF
        [ "Failed to parse:"
        , indentF 2 $ build text
        , "Error:"
        , indentF 2 $ build $ errorBundlePretty err
        ]
    Right res -> return res
infix 2 `shouldParse`
