-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for sr1 addresses
module Test.Mv4Address
  ( unit_keyFormat
  , test_checkSignature
  , test_cant_be_delegate
  , test_packSignature
  ) where

import Fmt (pretty, (+|), (|+))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (Assertion, (@?=))

import Data.Text qualified as T
import Morley.Michelson.Typed
import Morley.Mavryk.Address
import Morley.Mavryk.Crypto
import Morley.Mavryk.Crypto.BLS qualified as BLS
import Morley.Util.PeanoNatural
import Test.Cleveland
import Test.Cleveland.Util (fromHex)

unit_keyFormat :: Assertion
unit_keyFormat = do
  Right key <- pure $
    parseSecretKey "unencrypted:BLsk2RZ15dYu9hMggGcArYhzpku8mUToYBJnH2qWEryxxoQHvuJBC1"
  let pub = toPublic key
      addr = mkKeyAddress pub
      bytes = "\x12\x34\x56"
  sig <- sign key "\x12\x34\x56"
  formatPublicKey pub @?=
    "BLpk1yoPpFtFF3jGUSn2GrGzgHVcj1cm5o6HTMwiqSjiTNFSJskXFady9nrdhoZzrG6ybXiTSK5G"
  pretty addr @?= ("mv4VmAyHhsxkHhZQwMj1j1VryNSDj3kwnLRb" :: Text)
  formatSignature sig @?=
    "BLsig9xCrxURpiHA8nm1EGyLrtc3DtJ88ddhVKnooXGHTbDPEUP9mogNf1d4roAnSFXcXVR\
    \m3gDuEAbGWS1rZgfFw3sw7muV8zQM37tUXAtHFMiBTaDBYqBuc67gdxk6BCGb8qTqtDgQXf"
  checkSignature pub sig bytes @?= True

test_checkSignature :: TestTree
test_checkSignature = testScenario "bls signature can be checked" $ scenario do
  let sk = detSecretKey' KeyTypeBLS "foo"
      pk = toPublic sk
      bytes = "Hello, world!"
  sig <- runIO $ sign sk bytes
  handle <- originate "check_signature" False $ checkSigContract
  transfer handle $ calling def (pk, sig, bytes)
  getStorage handle @@== True

checkSigContract :: TypedContract (PublicKey, Signature, ByteString) Bool ()
checkSigContract = TypedContract $ defaultContract $
  CAR :# UNPAIRN (Succ Two) :# CHECK_SIGNATURE :# NIL :# PAIR

test_cant_be_delegate :: TestTree
test_cant_be_delegate = testScenario "bls address can't be delegate" $ scenario do
  sk <- SecretKeyBLS <$> runIO BLS.randomSecretKey
  addr <- importSecretKey sk auto
  transfer addr [mv|0.5|]
  attempt (registerDelegate addr) >>= \case
    Right{} -> failure "Expected registration to fail"
    Left (err :: SomeException) -> do
      let errMsg = displayException err
          check :: MonadCleveland caps m => Text -> m ()
          check msg
            | msg `T.isInfixOf` toText errMsg = pass
            | otherwise = failure $ "Expected " +| errMsg |+ " to contain " +| msg |+ ""
      ifEmulation
        (check "mv4 addresses can't be delegates")
        (check ".delegate.forbidden_mv4")

test_packSignature :: TestTree
test_packSignature = testScenario "bls signature can be packed" $ scenario do
  let sk = detSecretKey' KeyTypeBLS "foo"
      bytes = "Hello, world!"
  sig <- runIO $ sign sk bytes
  handle <- originate "pack_signature" "" $ packSigContract
  transfer handle $ calling def (bytes, sig)
  Right <$> getStorage handle @@== fromHex
    "0507070a0000000d48656c6c6f2c20776f726c64210a000000608a8328fc32b2\
    \82d2f15f8887b1ed9d8f0329f78ca1be98ddf0c27f9be49e8ce799c1a6934a95\
    \1371098f6318124a625a13bd0299a2f84e3102efbc866adbebad26196de7e47a\
    \03b13fb7c79a6b83f507962eab98e74c48178fae121080a487e8"

packSigContract :: TypedContract (ByteString, Signature) ByteString ()
packSigContract = TypedContract $ defaultContract $ CAR :# PACK :# NIL :# PAIR
