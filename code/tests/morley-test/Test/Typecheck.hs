-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Typecheck
  ( unit_Good_contracts
  , unit_Bad_contracts
  , test_srcPosition
  , unit_Unreachable_code
  , test_Roundtrip
  , test_StackRef
  , test_TcTypeError_display
  , hprop_ValueSeq_as_list
  , test_SELF
  , test_Value_contract
  , test_Nested_Sequences
  , test_timelock
  , unit_recursive_lambda_value
  ) where

import Data.Default (def)
import Data.Map qualified as M
import Data.Text.IO.Utf8 qualified as Utf8 (readFile)
import Data.Typeable (typeRep)
import Fmt (pretty, prettyText)
import Hedgehog (Gen, Property, evalNF, forAll, property, withTests, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Hspec (expectationFailure)
import Test.Hspec.Expectations (Expectation)
import Test.HUnit (Assertion, assertFailure, (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)

import Hedgehog.Gen.Michelson.Typed
import Hedgehog.Gen.Mavryk.Crypto.Timelock
import Morley.Michelson.ErrorPos (ErrorSrcPos(..), Pos(..), SrcPos(..), srcPos)
import Morley.Michelson.Parser (MichelsonSource(..), utypeQ)
import Morley.Michelson.Runtime (prepareContract)
import Morley.Michelson.Text (MText)
import Morley.Michelson.TypeCheck
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped (ParameterType(..), buildEpName, noAnn)
import Morley.Michelson.Untyped qualified as Un
import Morley.Mavryk.Address
import Morley.Mavryk.Core (ChainId, Mumav, Timestamp)
import Morley.Mavryk.Crypto (Bls12381Fr, Bls12381G1, Bls12381G2, KeyHash, PublicKey, Signature)
import Morley.Util.Interpolate (itu)
import Morley.Util.MismatchError
import Test.Cleveland.Michelson (failedTest, meanTimeUpperBoundPropNF, sec)
import Test.Cleveland.Michelson.Import (ContractReadError(..), readContract)

import Test.Util.Contracts (getIllTypedContracts, getWellTypedContracts, inContractsDir)

unit_Good_contracts :: Assertion
unit_Good_contracts
  = mapM_ (\f -> do checkFile True f def{ tcVerbose = True } (const pass)
                    checkFile True f def (const pass))
    =<< getWellTypedContracts

unit_Bad_contracts :: Assertion
unit_Bad_contracts
  = mapM_ (\f -> do checkFile False f def{ tcVerbose = True } (const pass)
                    checkFile False f def (const pass))
    =<< getIllTypedContracts

test_srcPosition :: [TestTree]
test_srcPosition =
  [ testCase "Verify instruction position in a typecheck error" $ do
      checkIllFile (inContractsDir "ill-typed/add_mismatch.mv") $ \case
          TcFailedOnInstr (Un.ADD _) _ (ErrorSrcPos (SrcPos (Pos 12) (Pos 9))) _ (Just NotEnoughItemsOnStack) -> True
          _ -> False

      checkIllFile (inContractsDir "ill-typed/compare_eq_fail.mv") $ \case
          TcFailedOnInstr (Un.COMPARE _) _ (ErrorSrcPos (SrcPos (Pos 7) (Pos 11))) _
                                              (Just (TypeEqError _)) -> True
          _ -> False
  ]

checkFile
  :: HasCallStack
  => Bool
  -> FilePath
  -> TypeCheckOptions
  -> (TcError -> Expectation)
  -> Expectation
checkFile wellTyped file options onError = do
  c <- prepareContract (Just file)
  case typeCheckingWith options $ typeCheckContract c of
    Left err
      | wellTyped ->
        expectationFailure $
        "Typechecker unexpectedly failed on \"" <> file <>
        "\": " <> displayException err
      | otherwise -> onError err
    Right _
      | not wellTyped ->
        assertFailure $
        "Typechecker unexpectedly considered \"" <> file <> "\" well-typed."
      | otherwise -> pass

checkIllFile :: FilePath -> (TcError -> Bool) -> Expectation
checkIllFile file check = checkFile False file def
  \e -> if check e then pass else unexpected file e
  where
    unexpected f e =
      expectationFailure $ "Unexpected typecheck error: " <> displayException e <> " in file: " <> f

unit_Unreachable_code :: Assertion
unit_Unreachable_code = do
  let file = inContractsDir "ill-typed/fail_before_nop.mv"
  let ics = ErrorSrcPos (srcPos 7 13)
  econtract <- readContract @'T.TUnit @'T.TUnit (MSFile file) <$> Utf8.readFile file
  econtract @?= Left (CRETypeCheck (MSFile file) $ TcUnreachableCode ics (one $ Un.WithSrcEx ics $ Un.SeqEx []))

test_Roundtrip :: [TestTree]
test_Roundtrip =
  [ testGroup "Value"
    [ roundtripValue @Integer $ genValueInt def
    , roundtripValue @Timestamp $ genValueTimestamp def
    , roundtripValue @PublicKey $ genValue @'T.TKey
    , roundtripValue @Signature $ genValue @'T.TSignature
    , roundtripValue @ChainId $ genValue @'T.TChainId
    , roundtripValue @(Maybe MText) $ genValue @('T.TOption 'T.TString)
    , roundtripValue @[Maybe Integer] $ genValueList def $ genValue @('T.TOption 'T.TInt)
    , roundtripValue @(Set Integer) $ genValue @('T.TSet 'T.TInt)
    , roundtripValue @(Integer, MText) $ genValuePair (genValueInt def) $ genValue @'T.TString
    , roundtripValue @(Integer, (MText, Integer)) $
        genValuePair (genValueInt def) $ genValuePair (genValue @'T.TString) (genValueInt def)
    , roundtripValue @(Either MText Integer) $ genValue @('T.TOr 'T.TString 'T.TInt)
    , roundtripValue @(Map MText Bool) $ genValue @('T.TMap 'T.TString 'T.TBool)
    , roundtripValue @Natural $ genValue @'T.TNat
    , roundtripValue @MText $ genValue @'T.TString
    , roundtripValue @ByteString $ genValue @'T.TBytes
    , roundtripValue @Mumav $ genValueMumav def
    , roundtripValue @Bool $ genValue @'T.TBool
    , roundtripValue @KeyHash genValueKeyHash
    , roundtripValue @Address $ genValue @'T.TAddress
    , roundtripValue @Bls12381Fr $ genValue @'T.TBls12381Fr
    , roundtripValue @Bls12381G1 $ genValue @'T.TBls12381G1
    , roundtripValue @Bls12381G2 $ genValue @'T.TBls12381G2
    ]
  ]
  where
  roundtripValue
    :: forall (a :: Type).
        ( Each [T.SingI, T.ForbidOp] '[T.ToT a]
        , Typeable a
        )
    => Gen (T.Value $ T.ToT a)
    -> TestTree
  roundtripValue gen = testGroup (show $ typeRep (Proxy @a))
    [ roundtripValue' @a T.untypeValue "Readable" gen
    , roundtripValue' @a T.untypeValueOptimized "Optimized" gen
    , roundtripValue' @a T.untypeValueHashable "Hashable" gen
    ]

  roundtripValue'
    :: forall (a :: Type).
       Each [T.SingI, T.ForbidOp] '[T.ToT a]
    => (T.Value $ T.ToT a -> Un.Value)
    -> String
    -> Gen (T.Value $ T.ToT a)
    -> TestTree
  roundtripValue' doUntype name genV =
    testProperty name $ property $ do
      val :: T.Value (T.ToT a) <- forAll $ genV
      let uval = doUntype val
      case typeCheckingWith def $ typeCheckValue uval of
        Right got -> got === val
        Left err -> failedTest $
                    "Type check unexpectedly failed: " <> pretty err

test_StackRef :: [TestTree]
test_StackRef =
  [ testProperty "Typecheck fails when ref is out of bounds" $ property $ do
      let instr = printStRef 2
          hst = stackEl ::& stackEl ::& SNil
      case
        typeCheckingWith def . runTypeCheckIsolated $
        typeCheckList [Un.WithSrcEx def $ Un.PrimEx instr] hst
        of
          Left err -> void $ evalNF err
          Right _ -> failedTest "Typecheck unexpectedly succeeded"
  , testProperty "Typecheck time is reasonably bounded" $
      let hst = stackEl ::& SNil
          run i =
            case
              typeCheckingWith def . runTypeCheckIsolated $
              typeCheckList [Un.WithSrcEx def $ Un.PrimEx (printStRef i)] hst
            of
              Left err -> err
              Right _ -> error "Typecheck unexpectedly succeeded"
      -- Making code processing performance scale with code size looks like a
      -- good property, so we'd like to avoid scenario when user tries to
      -- access 100500-th element of stack and typecheck hangs as a result
      in  meanTimeUpperBoundPropNF (sec 1) run 100000000000
  ]
  where
    printStRef i = Un.EXT . Un.UPRINT $ Un.PrintComment [Right (Un.StackRef i)]
    stackEl = (T.sing @'T.TUnit, T.Dict)

test_TcTypeError_display :: [TestTree]
test_TcTypeError_display =
  -- One may say that it's madness to write tests on 'Buildable' instances,
  -- but IMO (martoon) it's worth resulting duplication because tests allow
  -- avoiding silly errors like lost spaces and ensuring general sanity
  -- of used way to display content.
  [ testCase "TypeEqError" $
      prettyText (TypeEqError MkMismatchError{meActual=T.TUnit, meExpected=T.TKey})
      @?= [itu|
        Types not equal:
          Expected: key
          Actual:   unit
        |]

  , testCase "StackEqError" $
      prettyText (StackEqError $ MkMismatchError{meExpected=[T.TUnit, T.TBytes], meActual=[]})
      @?= [itu|
        Stacks not equal:
          Expected: [unit, bytes]
          Actual:   []
        |]

  , testCase "UnsupportedTypes" $
      prettyText (UnsupportedTypeForScope (T.TBigMap T.TInt T.TInt) T.BtHasBigMap)
      @?= "Type 'big_map int int' is unsupported here because it has 'big_map'"

  , testCase "InvalidValueType" $
      prettyText (InvalidValueType T.TUnit)
      @?= "Value type is never a valid: unit"
  ]

hprop_ValueSeq_as_list :: Property
hprop_ValueSeq_as_list = property $ do
  l <- forAll $ Gen.nonEmpty (Range.linear 0 100) (Gen.integral (Range.linearFrom 0 -1000 1000))
  let
    untypedValue = Un.ValueSeq $ Un.ValueInt <$> l
    typedValue = T.VList $ T.VInt <$> toList l
  typeCheckingWith def (typeCheckValue untypedValue) === Right typedValue

unit_recursive_lambda_value :: Assertion
unit_recursive_lambda_value = do
  let untypedValue = Un.ValueLamRec $ one $ Un.PrimEx $ Un.DIP [Un.PrimEx Un.DROP]
      typedValue = T.mkVLamRec @'T.TUnit @'T.TUnit $
        T.RfNormal $ T.DIP T.DROP
  typeCheckingWith def (typeCheckValue untypedValue) @?= Right typedValue

test_Nested_Sequences :: [TestTree]
test_Nested_Sequences =
  let
    runTC = typeCheckingWith def
    illTypedList = Un.ValueLambda $ Un.SeqEx [Un.SeqEx []] : [Un.SeqEx []]
    wellTypedList = Un.ValueLambda $ [Un.SeqEx [], Un.SeqEx []]
    wellTypedLambda = Un.ValueSeq $ Un.ValueSeq (Un.ValueNil :| []) :| [Un.ValueNil]
  in
    [ testCase "Nested seq with different levels of nesting can't be typechecked as list" $
        case runTC $ typeCheckValue @('T.TList ('T.TList 'T.TInt)) illTypedList of
          Left _      -> pass
          Right other -> assertFailure $ "Unexpected result: " <> pretty other
    , testCase "Nested seq with the same levels of nesting can be typechecked as list" $
        case runTC $ typeCheckValue @('T.TList ('T.TList 'T.TInt)) wellTypedList of
          Right _  -> pass
          Left err -> assertFailure $ "Unexpected error: " <> pretty err
    , testCase "Nested seq with different levels of nesting can be typechecked as lambda" $
        case runTC $ typeCheckValue @('T.TLambda 'T.TInt 'T.TInt) wellTypedLambda of
          Right _  -> pass
          Left err -> assertFailure $ "Unexpected error: " <> pretty err
    ]

test_SELF :: [TestTree]
test_SELF =
  [ testCase "Entrypoint not present" $
      checkFile False (inContractsDir "ill-typed/self-bad-entrypoint.mv") def $
      \case
        TcFailedOnInstr Un.SELF{} _ _ _ (Just EntrypointNotFound{}) -> pass
        other -> assertFailure $ "Unexpected error: " <> pretty other

  , testCase "Entrypoint type mismatch" $
      checkFile False (inContractsDir "ill-typed/self-entrypoint-type-mismatch.mv")
        def (const pass)

  , testCase "Entrypoint can be found" $
      checkFile True (inContractsDir "entrypoints/self1.mv") def
        (const pass)
  ]

test_Value_contract :: [TestTree]
test_Value_contract =
  [ testCase "No contract exists" $
      case typeCheckingWith def $
             typeVerifyParameter @('T.TContract 'T.TUnit) mempty addrUVal1
      of
        Left (TcFailedOnValue _ _ _ _ (Just (UnknownContract _))) -> pass
        res -> assertFailure $ "Unexpected result: " <> either pretty pretty res

  , testCase "Entrypoint does not exist" $
      case typeCheckingWith def $
             typeVerifyParameter @('T.TContract 'T.TKey) env1 addrUVal1
      of
        Left (TcFailedOnValue _ _ _ _ (Just (EntrypointNotFound _))) -> pass
        res -> assertFailure $ "Unexpected result: " <> either pretty pretty res

  , testCase "Correct contract value" $
      case typeCheckingWith def $
             typeVerifyParameter @('T.TContract 'T.TInt) env1 addrUVal2
      of
        Right _ -> pass
        res -> assertFailure $ "Unexpected result: " <> either pretty pretty res
  ]
  where
    addr1' = mkContractHashHack "123"
    addr1 = T.EpAddress (ContractAddress addr1') . unsafe $ buildEpName "a"
    addr2 = T.EpAddress (ContractAddress addr1') . unsafe $ buildEpName "q"
    addrUVal1 = Un.ValueString $ T.mformatEpAddress addr1
    addrUVal2 = Un.ValueString $ T.mformatEpAddress addr2

    env1 = M.fromList
      [ ( addr1',
          unsafe . mkSomeParamType
            $ ParameterType [utypeQ| or (nat %s) (int %q) |] noAnn
        )
      ]

test_timelock :: [TestTree]
test_timelock =
  [ testGroup "deprecation"
    [ testCase "chest type" $ do
        checkIllFile (inContractsDir "ill-typed/chest_type.mv") \case
          TcDeprecatedType "Timelock mechanism is affected by a vulnerability." T.TChest -> True
          _ -> False

    , testCase "chest_key type" $ do
        checkIllFile (inContractsDir "ill-typed/chest_key_type.mv") \case
          TcDeprecatedType "Timelock mechanism is affected by a vulnerability." T.TChestKey -> True
          _ -> False

    , testCase "chest and chest_key type together" $ do
        checkIllFile (inContractsDir "ill-typed/open_chest.mv") \case
          -- the error is thrown with the first offending type typechecker finds,
          -- which is in this case chest_key because it's the parameter.
          TcDeprecatedType "Timelock mechanism is affected by a vulnerability." T.TChestKey -> True
          _ -> False

    , testProperty "timelock values" $ withTests 10 $ property $ do
        -- generating chest and key is expensive, so we limit the number of tests
        (chest, key) <- forAll $ genChestAndKey
        let
          unChest = T.untypeValue $ T.toVal chest
          unKey = T.untypeValue $ T.toVal key
          errPos = ErrorSrcPos $ SrcPos (Pos 0) (Pos 0)
          expectedErr v t m = (TcFailedOnValue v t m errPos Nothing)
        typeCheckingWith def (typeCheckValue @'T.TChest unChest)
          === Left (expectedErr unChest T.TChest "chest type temporarily deprecated")
        typeCheckingWith def (typeCheckValue @'T.TChestKey unKey)
          === Left (expectedErr unKey T.TChestKey "chest_key type temporarily deprecated")
    ]
  , let go fp = checkFile True (inContractsDir fp) def{ tcStrict = False } (const pass)
    in testGroup "lax mode"
    [ testCase "with instructions" $ go "ill-typed/open_chest.mv"
    , testCase "chest type" $ go "ill-typed/chest_type.mv"
    , testCase "chest_key type" $ go "ill-typed/chest_key_type.mv"
    , testProperty "timelock values" $ withTests 10 $ property $ do
        -- generating chest and key is expensive, so we limit the number of tests
        (chest, key) <- forAll $ genChestAndKey
        let
          tyChest = T.toVal chest
          tyKey = T.toVal key
          unChest = T.untypeValue $ T.toVal chest
          unKey = T.untypeValue $ T.toVal key
          opts = def{tcStrict = False}
        typeCheckingWith opts (typeCheckValue @'T.TChest unChest)
          === Right tyChest
        typeCheckingWith opts (typeCheckValue @'T.TChestKey unKey)
          === Right tyKey
    ]
  ]
