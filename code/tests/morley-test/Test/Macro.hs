-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Macro
  ( unit_PAPAIR
  , unit_UNPAPAIR
  , unit_CADR
  , unit_SET_CADR
  , unit_MAP_CADR
  , unit_mapPairLeaves
  , unit_expand
  , unit_expandValue
  , test_carnAndCdrnExpandToGetN
  , test_reference_macros
  , test_reference_macros_client
  ) where

import Debug qualified

import Control.Concurrent.Async (forConcurrently)
import Data.Text qualified as T
import Hedgehog (forAll, property, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Gen.Michelson.Untyped qualified as Gen.U
import Hedgehog.Range qualified as Range
import System.Process (readProcessWithExitCode)
import Test.Hspec (Expectation, shouldBe)
import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)

import Morley.Michelson.ErrorPos (ErrorSrcPos(..), SrcPos, srcPos)
import Morley.Michelson.Macro
import Morley.Michelson.Parser (MichelsonSource(..), parseNoEnv, parsedOp)
import Morley.Michelson.Printer.Util
import Morley.Michelson.Untyped
  (ExpandedOp(..), InstrAbstract(..), Value, Value'(..), mkAnnotation, noAnn)

import Test.Cleveland.Tasty (whenNetworkEnabled)

checkMacroExpansionMatchesReference :: (Text, Text) -> TestTree
checkMacroExpansionMatchesReference (inp, expected) =
  testCase (toString inp <> " macro expansion against reference") do
    bimap displayException (printRenderDoc' . expand) (parseNoEnv parsedOp MSUnspecified inp)
      @?= bimap displayException printRenderDoc' (parseNoEnv parsedOp MSUnspecified expected)

printRenderDoc' :: RenderDoc a => a -> LText
printRenderDoc' = printRenderDoc True

test_reference_macros :: [TestTree]
test_reference_macros =
  checkMacroExpansionMatchesReference <$> referenceMacrosTests

test_reference_macros_client :: TestTree
test_reference_macros_client = whenNetworkEnabled \_ ->
  testProperty "Macro expansion reference against mavkit-client" $ property do
    input <- forAll Gen.U.genMacro
    let inputInstr = Mac input (srcPos 1 1)
    expected <- liftIO $ getReferenceMacro $ printRenderDoc' inputInstr
    printRenderDoc' inputInstr === printRenderDoc' expected

getReferenceMacro :: ToString a => a -> IO ParsedOp
getReferenceMacro input = do
    (_, res, _)  <- readProcessWithExitCode "mavkit-client"
      [ "--mode", "mockup"
      , "convert", "data", "{" <> toString input <> "}"
      , "from", "michelson", "to", "michelson"
      ]
      ""
    pure
      $ either (error . toText . displayException) id
      $ parseNoEnv parsedOp MSUnspecified
      $ T.drop 2 . T.dropEnd 2 . toText $ res

_updateReferenceMacros :: IO ()
_updateReferenceMacros = do
  ress <- forConcurrently inputs $ fmap printRenderDoc' . getReferenceMacro
  putTextLn $ "  [ " <> T.intercalate "\n  , " (fmt <$> zip inputs ress) <> "\n  ]"
  where
    inputs = fst <$> referenceMacrosTests
    minlen = maybe 0 maximum . nonEmpty $ length <$> inputs
    fmt (l, r) =
      T.justifyLeft (minlen + 3) ' ' (Debug.show l)
      <> " ==> "
      <> Debug.show r

{- | Updating these tests:

Open this in GHCi, e.g.

> stack repl code/tests/morley-test/Test/Macro.hs

or

> cabal repl code/tests/morley-test/Test/Macro.hs

Load this module (if needed)

> :load Test.Macro

Then run @_updateReferenceMacros@:

> _updateReferenceMacros

and copy-paste its output here.
-}
referenceMacrosTests :: [(Text, Text)]
referenceMacrosTests =
  [ "DIIP {}"                       ==> "DIP 2 {  }"
  , "CMPEQ"                         ==> "{ COMPARE; EQ }"
  , "CMPEQ @a"                      ==> "{ COMPARE; EQ @a }"
  , "CMPLT @a"                      ==> "{ COMPARE; LT @a }"
  , "CMPGT @a"                      ==> "{ COMPARE; GT @a }"
  , "CDAR"                          ==> "{ CDR; CAR }"
  , "CDDAR"                         ==> "{ CDR; CDR; CAR }"
  , "CDAR @a"                       ==> "{ CDR; CAR @a }"
  , "CDDAR @a"                      ==> "{ CDR; CDR; CAR @a }"
  , "SET_CDAR @a"                   ==> "{ DUP; DIP { CDR @%%; { CDR @%%; SWAP; PAIR % %@ } }; CAR @%%; PAIR %@ %@ @a }"
  , "SET_CDDAR @a"                  ==> "{ DUP; DIP { CDR @%%; { DUP; DIP { CDR @%%; { CDR @%%; SWAP; PAIR % %@ } }; CAR @%%; PAIR %@ %@ } }; CAR @%%; PAIR %@ %@ @a }"
  , "IFCMPEQ {} {}"                 ==> "{ COMPARE; EQ; IF {  } {  } }"
  , "IFEQ {} {}"                    ==> "{ EQ; IF {  } {  } }"
  , "IFLT {} {}"                    ==> "{ LT; IF {  } {  } }"
  , "FAIL"                          ==> "{ UNIT; FAILWITH }"
  , "PAPPAIIR :t1 @v1"              ==> "{ DIP { PAIR }; DIP { PAIR }; PAIR :t1 @v1 }"
  , "PAPAPPAPAIIR :t1 @v1"          ==> "{ DIP 3 { PAIR }; DIP 2 { PAIR }; DIP 2 { PAIR }; DIP { PAIR }; PAIR :t1 @v1 }"
  , "PAPAPPAPAIIR %f1 %f2 %f3 %f4"  ==> "{ DIP 3 { PAIR %f4 }; DIP 2 { PAIR %f3 }; DIP 2 { PAIR }; DIP { PAIR %f2 }; PAIR %f1 }"
  , "UNPAPPAIIR @v1 %f1"            ==> "{ UNPAIR; DIP { UNPAIR }; DIP { UNPAIR } }"
  , "CADDADADR @v"                  ==> "{ CAR; CDR; CDR; CAR; CDR; CAR; CDR @v }"
  , "CAR @v1 5"                     ==> "{ GET @v1 11 }"
  , "CDR @v1 5"                     ==> "{ GET @v1 10 }"
  , "DUUUUUUP"                      ==> "DUP 6"
  , "ASSERT"                        ==> "{ IF {  } { { UNIT; FAILWITH } } }"
  , "ASSERT_CMPEQ"                  ==> "{ { COMPARE; EQ }; IF {  } { { UNIT; FAILWITH } } }"
  , "ASSERT_NONE"                   ==> "{ IF_NONE {  } { { UNIT; FAILWITH } } }"
  , "ASSERT_SOME"                   ==> "{ IF_NONE { { UNIT; FAILWITH } } {  } }"
  , "ASSERT_LEFT"                   ==> "{ IF_LEFT {  } { { UNIT; FAILWITH } } }"
  , "ASSERT_RIGHT"                  ==> "{ IF_LEFT { { UNIT; FAILWITH } } {  } }"
  , "IF_SOME {} {}"                 ==> "{ IF_NONE {  } {  } }"
  , "IF_RIGHT {} {}"                ==> "{ IF_LEFT {  } {  } }"
  ]
  where (==>) = (,)

defPos :: SrcPos
defPos = srcPos 1 1

defICS :: ErrorSrcPos
defICS = ErrorSrcPos defPos

-- TODO [#719]: it seems to me that these duplicated "where" blocks should be
-- replaced with some reasonable mini-EDSL - at least to facilitate tests
-- writing - and that would be a rather big refactoring.
-- Dunno how to deal with this duplication otherwise.
{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

unit_PAPAIR :: Expectation
unit_PAPAIR = do
  expandPapair defICS pair n n `shouldBe` Left (PAIR n n n n)
  expandPapair defICS (P leaf pair) n n `shouldBe`
    Right [primEx $ DIP (expandMacro defICS $ PAPAIR pair n n), primEx $ PAIR n n n n]
  fmap expand [mac $ PAPAIR (P pair leaf) n n] `shouldBe`
    [WithSrcEx defICS $ SeqEx [WithSrcEx defICS $ primEx $ PAIR n n n n, primEx $ PAIR n n n n]]
  fmap expand [mac $ PAPAIR (P pair pair) n n] `shouldBe`
    [WithSrcEx defICS $ SeqEx [WithSrcEx defICS $ primEx (PAIR n n n n),
             primEx (DIP [primEx (PAIR n n n n)]),
             primEx (PAIR n n n n)]]
  where
    mac = flip Mac defPos
    primEx = PrimEx
    n = noAnn
    leaf = F n
    pair = P leaf leaf

unit_UNPAPAIR :: Expectation
unit_UNPAPAIR = do
  expandUnpapair defICS pair `shouldBe`
    [primEx $ UNPAIR n n n n]

  fmap expand [mac $ UNPAPAIR $ UP UF pair] `shouldBe`
    [WithSrcEx defICS $ SeqEx
      [ primEx (UNPAIR n n n n)
      , primEx (DIP [primEx (UNPAIR n n n n)])
      ]
    ]

  fmap expand [mac $ UNPAPAIR $ UP pair UF] `shouldBe`
    [WithSrcEx defICS $ SeqEx
      [ primEx (UNPAIR n n n n)
      , primEx (UNPAIR n n n n)
      ]
    ]

  fmap expand [mac $ UNPAPAIR $ UP pair pair] `shouldBe`
    [WithSrcEx defICS $ SeqEx
      [ primEx (UNPAIR n n n n)
      , primEx (DIP [primEx $ UNPAIR n n n n])
      , primEx (UNPAIR n n n n)
      ]
    ]
  where
    mac = flip Mac defPos
    primEx = PrimEx
    n = noAnn
    pair = UP UF UF

unit_CADR :: Expectation
unit_CADR = do
  expandCadr defICS ([A]) v f `shouldBe` [primEx $ CAR v f]
  expandCadr defICS ([D]) v f `shouldBe` [primEx $ CDR v f]
  expandCadr defICS (A:xs) v f `shouldBe` primEx (CAR n n) : expandMacro defICS (CADR xs v f)
  expandCadr defICS (D:xs) v f `shouldBe` primEx (CDR n n) : expandMacro defICS (CADR xs v f)
  where
    primEx = PrimEx
    v = "var"
    f = "field"
    n = noAnn
    xs = [A, D]

unit_SET_CADR :: Expectation
unit_SET_CADR = do
  expandSetCadr defICS [A] v f `shouldBe` primEx <$> [ CDR "%%" noAnn, SWAP, PAIR noAnn v f "@"]
  expandSetCadr defICS [D] v f `shouldBe` primEx <$> [ CAR "%%" noAnn, PAIR noAnn v "@" f]
  expandSetCadr defICS (A:xs) v f `shouldBe`
    primEx <$> [DUP noAnn, DIP [primEx carN, expand $ Mac (SET_CADR xs noAnn f) defPos], cdrN, SWAP, pairN]
  expandSetCadr defICS (D:xs) v f `shouldBe`
    primEx <$> [DUP noAnn, DIP [primEx cdrN, expand $ Mac (SET_CADR xs noAnn f) defPos], carN, pairN]
  where
    primEx = PrimEx
    v = "var"
    f = "field"
    xs = [A, D]
    carN = CAR "%%" noAnn
    cdrN = CDR "%%" noAnn
    pairN = PAIR noAnn v "@" "@"

unit_MAP_CADR :: Expectation
unit_MAP_CADR = do
  expandMapCadr defICS [A] v f ops `shouldBe`
    primEx <$> [DUP noAnn, cdrN, DIP [primEx $ CAR noAnn f, SeqEx ops'], SWAP, PAIR noAnn v f "@"]
  expandMapCadr defICS [D] v f ops `shouldBe`
    concat [primEx <$> [DUP noAnn, CDR noAnn f], [SeqEx ops'], primEx <$> [SWAP, carN, PAIR noAnn v "@" f]]
  expandMapCadr defICS (A:xs) v f ops `shouldBe`
    primEx <$> [DUP noAnn, DIP [primEx carN, expand $ Mac (MAP_CADR xs noAnn f ops) defPos], cdrN, SWAP, pairN]
  expandMapCadr defICS (D:xs) v f ops `shouldBe`
    primEx <$> [DUP noAnn, DIP [primEx cdrN, expand $ Mac (MAP_CADR xs noAnn f ops) defPos], carN, pairN]
  where
    primEx = PrimEx
    v = "var"
    f = "field"
    n = noAnn
    xs = [A, D]
    ops = PSSequence [Prim (DUP n) defPos]
    ops' = [WithSrcEx defICS $ PrimEx (DUP n)]
    carN = CAR "%%" noAnn
    cdrN = CDR "%%" noAnn
    pairN = PAIR noAnn v "@" "@"

unit_mapPairLeaves :: Expectation
unit_mapPairLeaves = do
  mapPairLeaves [f, f] pair `shouldBe` P (F f) (F f)
  mapPairLeaves annotations (P pair (F n)) `shouldBe`
    P (P (leaf "field1") (leaf "field2")) (leaf "field3")
  mapPairLeaves annotations (P pair pair) `shouldBe`
    P (P (leaf "field1") (leaf "field2")) (P (leaf "field3") (F n))
  where
    annotations = unsafe . mkAnnotation <$> ["field1", "field2", "field3"]
    n = noAnn
    f = "field"
    leaf f' = F (unsafe . mkAnnotation $ f')
    pair = P (F n) (F n)

unit_expand :: Expectation
unit_expand = do
  expand diip `shouldBe` expandedDiip
  expand (prim $ IF (diipSeq) (diipSeq)) `shouldBe` (primEx $ IF [expandedDiip] [expandedDiip])
  expand (Seq [diip, diip] defPos) `shouldBe` (WithSrcEx aIcs $ SeqEx $ [expandedDiip, expandedDiip])
  where
    aIcs = ErrorSrcPos defPos
    prim = flip Prim defPos
    primEx = WithSrcEx aIcs . PrimEx
    mac = flip Mac defPos
    diipSeq = PSSequence [diip]
    diip :: ParsedOp
    diip = mac (DIIP 2 $ PSSequence [prim SWAP])
    expandedDiip :: ExpandedOp
    expandedDiip = primEx (DIPN 2 [primEx SWAP])

unit_expandValue :: Expectation
unit_expandValue = do
  expandValue parsedPair `shouldBe` expandedPair
  expandValue parsedPapair `shouldBe` expandedPapair
  expandValue parsedLambdaWithMac `shouldBe` expandedLambdaWithMac
  where
    mac = flip Mac defPos
    primEx = PrimEx

    parsedPair :: ParsedValue
    parsedPair = ValuePair (ValueInt 5) (ValueInt 5)

    expandedPair :: Value
    expandedPair = ValuePair (ValueInt 5) (ValueInt 5)

    parsedPapair :: ParsedValue
    parsedPapair = ValuePair (ValuePair (ValueInt 5) (ValueInt 5)) (ValueInt 5)

    expandedPapair :: Value
    expandedPapair = ValuePair (ValuePair (ValueInt 5) (ValueInt 5)) (ValueInt 5)

    parsedLambdaWithMac :: ParsedValue
    parsedLambdaWithMac = ValueLambda $ PSSequence
      [mac (PAPAIR (P (F noAnn) (P (F noAnn) (F noAnn))) noAnn noAnn)]

    expandedLambdaWithMac :: Value
    expandedLambdaWithMac = ValueLambda . one $ WithSrcEx defICS $ SeqEx
      [ primEx $ DIP [primEx $ PAIR noAnn noAnn noAnn noAnn]
      , primEx $ PAIR noAnn noAnn noAnn noAnn
      ]

test_carnAndCdrnExpandToGetN :: [TestTree]
test_carnAndCdrnExpandToGetN =
  [ testProperty "CAR k to GET 2k+1" $ property do
    k <- forAll $ Gen.word $ Range.linear 0 100
    n <- forAll $ Gen.U.genAnnotation
    expand' (CARN n k) === primEx (GETN n $ 2 * k + 1)
  , testProperty "CDR k to GET 2k" $ property do
    k <- forAll $ Gen.word $ Range.linear 0 100
    n <- forAll $ Gen.U.genAnnotation
    expand' (CDRN n k) === primEx (GETN n $ 2 * k)
  ]
  where
    primEx = WithSrcEx defICS . SeqEx . one . PrimEx
    mac = flip Mac defPos
    expand' = expand . mac
