-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utility functions to read sample contracts (for testing).

module Test.Util.Contracts
  ( contractsDir
  , inContractsDir
  , (</>)

  , getIllTypedContracts
  , getWellTypedContracts
  , getUnparsableContracts
  , getWellTypedMichelsonContracts
  , getWellTypedMorleyContracts
  , getContractsWithReferences
  ) where

import Data.List (isSuffixOf)
import System.Directory (listDirectory)
import System.FilePath (addExtension, (</>))

-- | Directory with sample contracts.
contractsDir :: FilePath
contractsDir = "../../contracts/"

inContractsDir :: FilePath -> FilePath
inContractsDir = (contractsDir </>)

getIllTypedContracts :: IO [FilePath]
getIllTypedContracts = do
  illTyped <- concatMapM (\ext ->
                            concatMapM (getContractsWithExtension ext)
                            illTypedContractDirs
                         ) [".mv"]
  unparsable <- getUnparsableContracts
  return $ filter (not . (flip elem (unparsable <> unsupportedContracts))) illTyped

getWellTypedContracts :: IO [FilePath]
getWellTypedContracts = getWellTypedMichelsonContracts <> getWellTypedMorleyContracts

getUnparsableContracts :: IO [FilePath]
getUnparsableContracts = do
  unparsable <- concatMapM (flip getContractsWithExtension (contractsDir </> "unparsable"))
    [".mv"]
  return $ unparsable ++ unparsableContracts

getWellTypedMichelsonContracts :: IO [FilePath]
getWellTypedMichelsonContracts = do
  wellTyped <- concatMapM (getContractsWithExtension ".mv") wellTypedContractDirs
  return $ filter (not . flip elem unsupportedContracts) wellTyped

getWellTypedMorleyContracts :: IO [FilePath]
getWellTypedMorleyContracts = do
  wellTyped <- concatMapM (getContractsWithExtension ".mv") wellTypedContractDirs
  return $ filter (not . flip elem unsupportedContracts) wellTyped

getContractsWithExtension :: String -> FilePath -> IO [FilePath]
getContractsWithExtension ext dir = mapMaybe convertPath <$> listDirectory dir
  where
    convertPath :: FilePath -> Maybe FilePath
    convertPath fileName
      | (ext `isSuffixOf` fileName) =
        Just (dir </> fileName)
      | otherwise = Nothing

wellTypedContractDirs :: [FilePath]
wellTypedContractDirs
  = contractsDir
  : map (contractsDir </>)
    [ "annotation-mismatch"
    , "bytes-arith"
    , "bytes-conv"
    ]
  <> map ((contractsDir </> "mavryk_examples") </>)
    [ "attic"
    , "entrypoints"
    , "macros"
    , "mini_scenarios"
    , "non_regression"
    , "opcodes"
    ]

illTypedContractDirs :: [FilePath]
illTypedContractDirs =
  [ contractsDir </> "ill-typed"
  , contractsDir </> "mavryk_examples" </> "ill_typed"
  , contractsDir </> "mavryk_examples" </> "legacy"
  ]

unsupportedContracts :: [FilePath]
unsupportedContracts =
  [ contractsDir </> "mavryk_examples" </> "ill_typed" </> name
  | name <-
      [ "badly_indented.mv" -- we are a little more permissive about indentation than the reference.
      ]
  ] <>
  [ contractsDir </> "mavryk_examples" </> "mini_scenarios" </> name
  | name <-
      [ "999_constant.mv" -- we don't support constants
      , "constant_unit.mv"
      , "constant_entrypoints.mv"
      , "tx_rollup_deposit_013_014.mv" -- we don't support TORUs
      , "tx_rollup_deposit_015_015.mv"
      , "add_clear_tickets.mv" -- TICKET as used here is deprecated
      ]
  ]

unparsableContracts :: [FilePath]
unparsableContracts =
  [ contractsDir </> "mavryk_examples" </> "ill_typed" </> name
  | name <-
      [ "big_map_arity.mv"
      , "chain_id_arity.mv"
      , "create_contract_rootname.mv"
      , "missing_only_code_field.mv"
      , "missing_only_parameter_field.mv"
      , "missing_only_storage_field.mv"
      , "missing_parameter_and_storage_fields.mv"
      , "multiple_code_field.mv"
      , "multiple_parameter_field.mv"
      , "multiple_storage_and_code_fields.mv"
      , "multiple_storage_field.mv"
      , "sapling_build_empty_state_with_int_parameter.mv"
      , "view_op_invalid_arity.mv"
      , "view_toplevel_invalid_arity.mv"
      , "view_toplevel_duplicated_name.mv"
      ]
      ++
      [ "view_" <> kind <> "_bad_name_" <> name <> ".mv"
      | kind <- [ "op", "toplevel" ]
      , name <- [ "invalid_type", "invalid_char_set", "non_printable_char"
                , "too_long"
                ]
      ]
  ] ++
  [ contractsDir </> "mavryk_examples" </> "legacy" </> name
  | name <-
      [ "create_account.mv"
      , "originator.mv"
      , "steps_to_quota.mv"
      ]
  ]

getContractsWithReferences :: String -> FilePath -> String -> IO [(FilePath, FilePath)]
getContractsWithReferences ext fp refExt =
  fmap attachPrettyPath <$> getContractsWithExtension ext fp
  where
    attachPrettyPath :: FilePath -> (FilePath, FilePath)
    attachPrettyPath src = (src, addExtension src  refExt)
