-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main (main) where

import Data.Default (def)
import Data.Text.IO.Utf8 qualified as Utf8 (readFile)
import Gauge.Main (bench, bgroup, defaultMain, nf)
import Main.Utf8 (withUtf8)
import Text.Megaparsec (parse)

import Morley.Michelson.Interpret (interpret)
import Morley.Michelson.Parser as P
import Morley.Michelson.Runtime (prepareContract)
import Morley.Michelson.Runtime.Dummy
import Morley.Michelson.Text
import Morley.Michelson.TypeCheck as T
import Morley.Michelson.Typed as T
import Morley.Mavryk.Address
import Morley.Util.PeanoNatural
import Test.Cleveland.Michelson.Import (importContract)

testassert_square2 :: T.Contract ('T.TPair 'T.TInt 'T.TInt) 'T.TUnit
testassert_square2 = T.defaultContract $
  T.CAR :#
  T.DIP (T.UNIT) :#
  T.UNPAIR :#
  T.Ext (T.TEST_ASSERT
    (T.TestAssert "CheckSides"
      (T.PrintComment [Left "Sides are ", Right (T.StackRef Zero), Left " x ", Right (T.StackRef One)])
      ( T.DUP :#
        T.GT :#
        T.DIP (T.PUSH (T.VInt 101) :# T.COMPARE :# T.GT) :#
        T.DIPN Two (T.DUP :# T.GT :# T.DIP (T.PUSH (T.VInt 101) :# T.COMPARE :# T.GT)) :#
        T.AND :# T.AND :# T.AND
      )
    )) :#
  T.MUL :#
  T.Ext (T.PRINT $ T.PrintComment [Left "Area is ", Right $ T.StackRef Zero]) :#
  T.DROP :#
  T.NIL @'T.TOperation :#
  T.PAIR

main :: IO ()
main = withUtf8 $ do
  let
    basicFp = "../../contracts/basic1.mv"
    stringCallerFp = "../../contracts/string_caller.mv"
    callSelfFp = "../../contracts/call_self_several_times.mv"
    contractPaths = [basicFp, stringCallerFp, callSelfFp]
  contracts <- traverse (\x -> (x,) <$> Utf8.readFile x) contractPaths
  let makeParseBench (filename, code) =
        bench filename $ nf (parse P.program filename) code
  preparedContracts <- evaluateNF =<< traverse (\x -> (x,) <$> prepareContract (Just x)) contractPaths
  let
    makeTypeCheckBench (filename, contract) = bench filename $
      nf (T.typeCheckingWith def . T.typeCheckContract) contract

  basicC <- importContract basicFp
  stringCallerC <- importContract stringCallerFp
  callSelfC <- importContract callSelfFp
  let
    basicBench = bench basicFp
      (nf
        (interpret basicC T.epcPrimitive T.VUnit (T.VList [T.VInt 0]) dummyGlobalCounter dummyBigMapCounter)
        dummyContractEnv
      )

    dummyAddress = detGenKeyAddress "thegreatandpowerful"
    dummyString = unsafe . mkMText $ "TGAP"
    stringCallerBench = bench stringCallerFp
      (nf
        (interpret stringCallerC T.epcPrimitive  (T.toVal dummyString) (T.toVal $ MkAddress dummyAddress) dummyGlobalCounter dummyBigMapCounter)
        dummyContractEnv
      )
    callSelfBench = bench callSelfFp
      (nf
        (interpret callSelfC T.epcPrimitive (T.toVal (100 :: Integer)) (T.toVal (0 :: Natural)) dummyGlobalCounter dummyBigMapCounter)
        dummyContractEnv
      )

    sq2Bench = bench "testassert_square2"
      (nf
        (interpret testassert_square2 T.epcPrimitive (T.toVal (100 :: Integer, 200 :: Integer)) T.VUnit dummyGlobalCounter dummyBigMapCounter)
        dummyContractEnv
      )



  defaultMain
    [ bgroup "parsing" $ map makeParseBench contracts
    , bgroup "type-checking" $ map makeTypeCheckBench $ force $
        ("testassert_square2", T.convertContract testassert_square2) : preparedContracts
    , bgroup "interpreting" $ [basicBench, stringCallerBench, callSelfBench, sq2Bench]
    ]
