-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- Note: This extension is enabled because it allows better representation of tests
-- with @BinBase@. These tests require special representation behavior because 'show' works
-- incorrect with non-decimal base.
{-# LANGUAGE HexFloatLiterals #-}

-- | Tests for custom arithmetic datatypes, added to Lorentz
module Test.Lorentz.CustomArith
  ( test_FixedArith
  , test_NFixedArith
  , test_RationalArith
  , test_NRationalArith
  , test_Conversions
  , test_Fixed_DecBase_properties
  , test_Fixed_BinBase_properties
  , test_NFixed_DecBase_properties
  , test_NFixed_BinBase_properties
  , test_LSR_LSL
  ) where

import Data.Bits (shiftL, shiftR)
import Data.Fixed (HasResolution)
import Data.Ratio (denominator, numerator, (%))
import Data.Type.Equality ((:~:)(..))
import GHC.TypeNats (type (+))
import Hedgehog (MonadTest, forAll, property, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Prelude hiding (Rational, div, drop, reduce)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (assertBool, testCase, (@?=))
import Unsafe.Coerce (unsafeCoerce)

import Lorentz
import Morley.Michelson.Interpret (MichelsonFailed(..), MichelsonFailureWithStack(..))
import Morley.Michelson.Text
import Morley.Michelson.Typed

test_FixedArith :: TestTree
test_FixedArith =
  testGroup "Arithmetic operations on Fixed values"
    [ testGroup "Arithmetics"
      [ testGroup "Decimal base"
        [ testCase "Fixed add" do
            add -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (2.2 :: NFixed (DecBase 3))
        , testCase "Fixed sub" do
            sub -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (0 :: Fixed (DecBase 3))
        , testCase "Fixed mul" do
            mul -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (1.21 :: NFixed (DecBase 6))
        , testCase "Fixed ediv 1" do
            ediv -$ (123.456 :: Fixed (DecBase 3)) ::: (100 :: Integer)
              @?= Just (1, 23.456 :: (NFixed (DecBase 3)))
        , testCase "Fixed ediv 2" do
            ediv -$ (2 :: Fixed (DecBase 3)) ::: (100 :: Integer)
              @?= Just (0, 2 :: (NFixed (DecBase 3)))
        ]
      , testGroup "Binary base"
        [ testCase "Fixed add" do
            add -$ (1.1 :: Fixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (2.2 :: Fixed (BinBase 1))
        , testCase "Fixed sub" do
            sub -$ (1.1 :: Fixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (0 :: Fixed (BinBase 1))
        , testCase "Fixed mul" do
            mul -$ (1.1 :: Fixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (1.21 :: Fixed (BinBase 2))
        ]
      ]
    , testGroup "Rounding"
        [ testGroup "Rounding with decimal base"
            [ testCase "Round up Fixed with decimal base 1" do
                round_ -$ (1.23456 :: (Fixed (DecBase 6)))
                  @?= (1.235 :: Fixed (DecBase 3))
            , testCase "Round up Fixed with decimal base 2" do
                round_ -$ (1.23446 :: (Fixed (DecBase 6)))
                  @?= (1.234 :: Fixed (DecBase 3))
            , testCase "Round up Fixed with decimal base 3" do
                round_ -$ (0.177 :: (Fixed (DecBase 3)))
                  @?= (0.18 :: Fixed (DecBase 2))
            , testCase "Round up Fixed with decimal base 4" do
                round_ -$ (0.173 :: (Fixed (DecBase 3)))
                  @?= (0.17 :: Fixed (DecBase 2))
            , testCase "Round up Fixed with decimal base 5" do
                round_ -$ (1.230 :: (Fixed (DecBase 3)))
                  @?= (1.23 :: Fixed (DecBase 2))
            , testCase "Ceil 1" do
                ceil_ -$ (0.177 :: (Fixed (DecBase 3)))
                  @?= (0.18 :: Fixed (DecBase 2))
            , testCase "Ceil 2" do
                ceil_ -$ (0.173 :: (Fixed (DecBase 3)))
                  @?= (0.18 :: Fixed (DecBase 2))
            , testCase "Ceil 3" do
                ceil_ -$ (1.230 :: (Fixed (DecBase 3)))
                  @?= (1.23 :: Fixed (DecBase 2))
            , testCase "Floor 1" do
                floor_ -$ (0.177 :: (Fixed (DecBase 3)))
                  @?= (0.17 :: Fixed (DecBase 2))
            , testCase "Floor 2" do
                floor_ -$ (0.173 :: (Fixed (DecBase 3)))
                  @?= (0.17 :: Fixed (DecBase 2))
            , testCase "Floor 3" do
                floor_ -$ (1.230 :: (Fixed (DecBase 3)))
                  @?= (1.23 :: Fixed (DecBase 2))
            , testCase "Round down" do
                round_ -$ (1.234 :: Fixed (DecBase 3))
                  @?= (1.23400 :: (Fixed (DecBase 6)))
            ]
        , testGroup "Rounding with Binary Base"
            [ testCase "Round up Fixed with binary base 1" do
                round_ -$ (0x1.23456 :: (Fixed (BinBase 6)))
                  @?= (0x1.235 :: Fixed (BinBase 3))
            , testCase "Round up Fixed with binary base 2" do
                round_ -$ (0x1.23446 :: (Fixed (BinBase 6)))
                  @?= (0x1.234 :: Fixed (BinBase 3))
            , testCase "Round up Fixed with binary base 3" do
                round_ -$ (0x0.177 :: (Fixed (BinBase 3)))
                  @?= (0x0.18 :: Fixed (BinBase 2))
            , testCase "Round up Fixed with binary base 4" do
                round_ -$ (0x0.173 :: (Fixed (BinBase 3)))
                  @?= (0x0.17 :: Fixed (BinBase 2))
            , testCase "Round up Fixed with binary base 5" do
                round_ -$ (0x1.230 :: (Fixed (BinBase 3)))
                  @?= (0x1.23 :: Fixed (BinBase 2))
            , testCase "Ceil 1" do
                ceil_ -$ (0x0.177 :: (Fixed (BinBase 3)))
                  @?= (0x0.18 :: Fixed (BinBase 2))
            , testCase "Ceil 2" do
                ceil_ -$ (0x0.173 :: (Fixed (BinBase 3)))
                  @?= (0x0.18 :: Fixed (BinBase 2))
            , testCase "Ceil 3" do
                ceil_ -$ (0x1.230 :: (Fixed (BinBase 12)))
                  @?= (0x1.23 :: Fixed (BinBase 8

                  ))
            , testCase "Floor 1" do
                floor_ -$ (0x0.177 :: (Fixed (BinBase 3)))
                  @?= (0x0.17 :: Fixed (BinBase 2))
            , testCase "Floor 2" do
                floor_ -$ (0x0.173 :: (Fixed (BinBase 3)))
                  @?= (0x0.17 :: Fixed (BinBase 2))
            , testCase "Floor 3" do
                floor_ -$ (0x1.230 :: (Fixed (BinBase 3)))
                  @?= (0x1.23 :: Fixed (BinBase 2))
            ]
        , testGroup "Casts"
          [ testCase "Cast to Integer" do
              fromFixed @(Fixed (DecBase 6)) -$ (1.23456 :: (Fixed (DecBase 6)))
                @?= 1
          , testCase "Cast Integer to Fixed" do
              toFixed @(Fixed (DecBase 6)) -$ (123456 :: Integer)
                @?= (123456 :: (Fixed (DecBase 6)))
          , testCase "Cast to Integer (Binary base)" do
              fromFixed @(Fixed (BinBase 6)) -$ (1.23456 :: (Fixed (BinBase 6)))
                @?= 1
          , testCase "Cast Integer to Fixed (Binary base)" do
              toFixed @(Fixed (BinBase 6)) -$ (123456 :: Integer)
                @?= (123456.00 :: (Fixed (BinBase 6)))
          ]
        ]
    , testGroup "Division"
        [ testCase "Divide big Fixed(Dec) to small Fixed(Dec)" do
            div @(Maybe (Fixed (DecBase 8)))
              -$ (625.123 :: Fixed (DecBase 3)) ::: (123.23 :: Fixed (DecBase 2))
              @?= Just (5.07281506 :: Fixed (DecBase 8))
        , testCase "Divide small Fixed(Dec) to big Fixed(Dec)" do
            div @(Maybe (Fixed (DecBase 8)))
              -$ (123.23 :: Fixed (DecBase 3)) ::: (625.123 :: Fixed (DecBase 2))
              @?= Just (0.19713015 :: Fixed (DecBase 8))
        , testCase "Divide big Fixed(Bin) to small Fixed(Bin)" do
            div @(Maybe (Fixed (BinBase 8)))
              -$ (625.123 :: Fixed (BinBase 3)) ::: (123.23 :: Fixed (BinBase 2))
              @?= (Just (5.079 :: Fixed (BinBase 8)))
        , testCase "Divide small Fixed(Bin) to big Fixed(Bin)" do
            div @(Maybe (Fixed (BinBase 8)))
              -$ (123.23 :: Fixed (BinBase 3)) ::: (625.123 :: Fixed (BinBase 2))
              @?= (Just (0.19713015 :: Fixed (BinBase 8)))
        , testCase "Divide by zero" do
            div @(Maybe (Fixed (BinBase 8)))
              -$ (123.23 :: Fixed (BinBase 3)) ::: (0 :: Fixed (BinBase 2))
              @?= (Nothing)

        , testCase "Divide Natural by Fixed(Dec)" do
            div -$ (123 :: Natural) ::: (0.23 :: Fixed (DecBase 3))
              @?= Just (534.782608 :: Fixed (DecBase 6))
        , testCase "Divide Natural by Fixed(Bin)" do
            div -$ (123 :: Natural) ::: (0.25 :: Fixed (BinBase 3))
              @?= Just (492 :: Fixed (BinBase 6))
        , testCase "Divide Integer by Fixed(Dec)" do
            div -$ ((-123) :: Integer) ::: (0.23 :: Fixed (DecBase 3))
              @?= Just (negate (534.782609 :: Fixed (DecBase 6)))
        , testCase "Divide Integer by Fixed(Bin)" do
            div -$ ((-123) :: Integer) ::: (0.25 :: Fixed (BinBase 3))
              @?= Just (negate (492 :: Fixed (BinBase 6)))

        , testCase "Divide two Naturals to get Fixed(Dec)" do
            div -$ (2 :: Natural) ::: (3 :: Natural)
              @?= Just (0.666666 :: Fixed (DecBase 6))
        , testCase "Divide two Integers to get Fixed(Dec)" do
            div -$ ((-2) :: Integer) ::: ((-3) :: Integer)
              @?= Just (0.666667 :: Fixed (DecBase 6))
        , testCase "Divide Integer/Natural to get Fixed(Dec)" do
            div -$ ((-2) :: Integer) ::: (3 :: Natural)
              @?= Just (negate (0.666667 :: Fixed (DecBase 6)))
        , testCase "Divide Natural/Integer to get Fixed(Dec)" do
            div -$ (2 :: Natural) ::: ((-3) :: Integer)
              @?= Just (negate (0.666666 :: Fixed (DecBase 6)))

        , testCase "Divide two Naturals to get Fixed(Bin)" do
            div -$ (2 :: Natural) ::: (3 :: Natural)
              @?= Just (0.66 :: Fixed (BinBase 6))
        , testCase "Divide two Integers to get Fixed(Bin)" do
            div -$ ((-2) :: Integer) ::: ((-3) :: Integer)
              @?= Just (0.68 :: Fixed (BinBase 6))
        , testCase "Divide Integer/Natural to get Fixed(Bin)" do
            div -$ ((-2) :: Integer) ::: (3 :: Natural)
              @?= Just (negate (0.68 :: Fixed (BinBase 6)))
        , testCase "Divide Natural/Integer to get Fixed(Bin)" do
            div -$ (2 :: Natural) ::: ((-3) :: Integer)
              @?= Just (negate (0.66 :: Fixed (BinBase 6)))
        ]
    ]

test_NFixedArith :: TestTree
test_NFixedArith =
  testGroup "Arithmetic operations on NFixed values"
    [ testGroup "NFixed to Fixed"
        [ testCase "Cast NFixed to Fixed 1" do
            castNFixedToFixed -$ (1.23456 :: NFixed (DecBase 6))
              @?= (1.23456 :: Fixed (DecBase 6))
        , testCase "Cast NFixed to Fixed 2" do
            castNFixedToFixed -$ (0 :: NFixed (DecBase 6))
              @?= (0 :: Fixed (DecBase 6))
        , testCase "Cast NFixed to Fixed 3" do
            castNFixedToFixed -$ (0.12 :: NFixed (DecBase 2))
              @?= (0.12 :: Fixed (DecBase 2))
        ]
    , testGroup "Fixed to NFixed"
        [ testCase "Cast Fixed to NFixed 1" do
            castFixedToNFixed -$ (1.23456 :: Fixed (DecBase 6))
              @?= Just (1.23456 :: NFixed (DecBase 6))
        , testCase "Cast Fixed to NFixed 2" do
            castFixedToNFixed -$ (0 :: Fixed (DecBase 6))
              @?= Just (0 :: NFixed (DecBase 6))
        , testCase "Cast Fixed to NFixed 3" do
            castFixedToNFixed -$ (0.12 :: Fixed (DecBase 2))
              @?= Just (0.12 :: NFixed (DecBase 2))
        ]
    , testGroup "Decimal base"
        [ testCase "NFixed add" do
            add -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (2.2 :: NFixed (DecBase 3))
        , testCase "NFixed sub" do
            sub -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (0 :: Fixed (DecBase 3))
        , testCase "NFixed mul" do
            mul -$ (1.1 :: NFixed (DecBase 3)) ::: (1.1 :: NFixed (DecBase 3))
              @?= (1.21 :: NFixed (DecBase 6))
        , testCase "NFixed ediv 1" do
            ediv -$ (123.456 :: NFixed (DecBase 3)) ::: (100 :: Integer)
              @?= Just (1, 23.456 :: (NFixed (DecBase 3)))
        , testCase "NFixed ediv 2" do
            ediv -$ (2 :: NFixed (DecBase 3)) ::: (100 :: Integer)
              @?= Just (0, 2 :: (NFixed (DecBase 3)))
        ]
    , testGroup "Binary base"
        [ testCase "NFixed add" do
            add -$ (1.1 :: NFixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (2.2 :: NFixed (BinBase 1))
        , testCase "NFixed sub" do
            sub -$ (1.1 :: NFixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (0 :: Fixed (BinBase 1))
        , testCase "NFixed mul" do
            mul -$ (1.1 :: NFixed (BinBase 1)) ::: (1.1 :: NFixed (BinBase 1))
              @?= (1.21 :: NFixed (BinBase 2))
        , testCase "NFixed lsl" do
            lsl -$ (1 :: NFixed (BinBase 3)) ::: (0 :: Natural) @?= 1
            lsl -$ (1 :: NFixed (BinBase 3)) ::: (1 :: Natural) @?= 2
            lsl -$ (1 :: NFixed (BinBase 3)) ::: (2 :: Natural) @?= 4
        , testCase "NFixed lsr" do
            lsr -$ (1 :: NFixed (BinBase 3)) ::: (0 :: Natural) @?= 1
            lsr -$ (1 :: NFixed (BinBase 3)) ::: (1 :: Natural) @?= 0.5
            lsr -$ (1 :: NFixed (BinBase 3)) ::: (2 :: Natural) @?= 0.25
        ]
    , testGroup "Rounding"
        [ testGroup "Rounding with decimal base"
            [ testCase "Round up NFixed with decimal base 1" do
                round_ -$ (1.23456 :: (NFixed (DecBase 6)))
                  @?= (1.235 :: NFixed (DecBase 3))
            , testCase "Round up NFixed with decimal base 2" do
                round_ -$ (1.23446 :: (NFixed (DecBase 6)))
                  @?= (1.234 :: NFixed (DecBase 3))
            , testCase "Round up NFixed with decimal base 3" do
                round_ -$ (0.177 :: (NFixed (DecBase 3)))
                  @?= (0.18 :: NFixed (DecBase 2))
            , testCase "Round up NFixed with decimal base 4" do
                round_ -$ (0.173 :: (NFixed (DecBase 3)))
                  @?= (0.17 :: NFixed (DecBase 2))
            , testCase "Round up NFixed with decimal base 5" do
                round_ -$ (1.230 :: (NFixed (DecBase 3)))
                  @?= (1.23 :: NFixed (DecBase 2))
            , testCase "Ceil 1" do
                ceil_ -$ (0.177 :: (NFixed (DecBase 3)))
                  @?= (0.18 :: NFixed (DecBase 2))
            , testCase "Ceil 2" do
                ceil_ -$ (0.173 :: (NFixed (DecBase 3)))
                  @?= (0.18 :: NFixed (DecBase 2))
            , testCase "Ceil 3" do
                ceil_ -$ (1.230 :: (NFixed (DecBase 3)))
                  @?= (1.23 :: NFixed (DecBase 2))
            , testCase "Floor 1" do
                floor_ -$ (0.177 :: (NFixed (DecBase 3)))
                  @?= (0.17 :: NFixed (DecBase 2))
            , testCase "Floor 2" do
                floor_ -$ (0.173 :: (NFixed (DecBase 3)))
                  @?= (0.17 :: NFixed (DecBase 2))
            , testCase "Floor 3" do
                floor_ -$ (1.230 :: (NFixed (DecBase 3)))
                  @?= (1.23 :: NFixed (DecBase 2))
            , testCase "Round down" do
                round_ -$ (1.234 :: NFixed (DecBase 3))
                  @?= (1.23400 :: (NFixed (DecBase 6)))
            ]
        , testGroup "Rounding with Binary Base"
            [ testCase "Round NFixed with binary base" do
                round_ -$ (0x1.23456 :: (NFixed (BinBase 6)))
                  @?= (0x1.235 :: NFixed (BinBase 3))
            , testCase "Ceil 1" do
                ceil_ -$ (0x0.177 :: (NFixed (BinBase 3)))
                  @?= (0x0.18 :: NFixed (BinBase 2))
            , testCase "Ceil 2" do
                ceil_ -$ (0x0.173 :: (NFixed (BinBase 3)))
                  @?= (0x0.18 :: NFixed (BinBase 2))
            , testCase "Ceil 3" do
                ceil_ -$ (0x1.230 :: (NFixed (BinBase 12)))
                  @?= (0x1.23 :: NFixed (BinBase 8))
            , testCase "Floor 1" do
                floor_ -$ (0x0.177 :: (NFixed (BinBase 3)))
                  @?= (0x0.17 :: NFixed (BinBase 2))
            , testCase "Floor 2" do
                floor_ -$ (0x0.173 :: (NFixed (BinBase 3)))
                  @?= (0x0.17 :: NFixed (BinBase 2))
            , testCase "Floor 3" do
                floor_ -$ (0x1.230 :: (NFixed (BinBase 3)))
                  @?= (0x1.23 :: NFixed (BinBase 2))
            ]
        , testGroup "Casts"
          [ testCase "Cast to Integer" do
              fromFixed @(NFixed (DecBase 6)) -$ (1.23456 :: (NFixed (DecBase 6)))
                @?= 1
          , testCase "Cast Natural to NFixed" do
              toFixed @(NFixed (DecBase 6)) -$ (123456 :: Natural)
                @?= (123456 :: (NFixed (DecBase 6)))
          , testCase "Cast to Integer (Binary base)" do
              fromFixed @(NFixed (BinBase 6)) -$ (1.23456 :: (NFixed (BinBase 6)))
                @?= 1
          , testCase "Cast Natural to NFixed (Binary base)" do
              toFixed @(NFixed (BinBase 6)) -$ (123456 :: Natural)
                @?= (123456 :: (NFixed (BinBase 6)))
          ]
        ]
    , testGroup "Division"
        [ testCase "Divide big NFixed(Dec) to small NFixed(Dec,2)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (625.123 :: NFixed (DecBase 3)) ::: (123.23 :: NFixed (DecBase 2))
              @?= Just (5.07281506 :: NFixed (DecBase 8))
        , testCase "Divide small NFixed(Dec) to big NFixed(Dec,2)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (123.23 :: NFixed (DecBase 3)) ::: (625.123 :: NFixed (DecBase 2))
              @?= Just (0.19713015 :: NFixed (DecBase 8))
        , testCase "Divide big NFixed(Bin) to small NFixed(Bin,2)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (625.123 :: NFixed (BinBase 3)) ::: (123.23 :: NFixed (BinBase 2))
              @?= (Just (5.079 :: NFixed (BinBase 8)))
        , testCase "Divide small NFixed(Bin) to big NFixed(Bin,2)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (123.23 :: NFixed (BinBase 3)) ::: (625.123 :: NFixed (BinBase 2))
              @?= (Just (0.19713015 :: NFixed (BinBase 8)))

        , testCase "Divide big NFixed(Dec) to small NFixed(Dec,3)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (625.123 :: NFixed (DecBase 3)) ::: (123.23 :: NFixed (DecBase 3))
              @?= Just (5.07281506 :: NFixed (DecBase 8))
        , testCase "Divide small NFixed(Dec) to big NFixed(Dec,3)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (123.23 :: NFixed (DecBase 3)) ::: (625.123 :: NFixed (DecBase 3))
              @?= Just (0.19712920 :: NFixed (DecBase 8))
        , testCase "Divide big NFixed(Bin) to small NFixed(Bin,3)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (625.123 :: NFixed (BinBase 3)) ::: (123.23 :: NFixed (BinBase 3))
              @?= (Just (5.075 :: NFixed (BinBase 8)))
        , testCase "Divide small NFixed(Bin) to big NFixed(Bin,3)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (123.23 :: NFixed (BinBase 3)) ::: (625.123 :: NFixed (BinBase 3))
              @?= (Just (0.19713015 :: NFixed (BinBase 8)))

        , testCase "Divide big NFixed(Dec) to small NFixed(Dec,3)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (625.123 :: NFixed (DecBase 3)) ::: (123.23 :: NFixed (DecBase 4))
              @?= Just (5.07281506 :: NFixed (DecBase 8))
        , testCase "Divide small NFixed(Dec) to big NFixed(Dec,3)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (123.23 :: NFixed (DecBase 3)) ::: (625.123 :: NFixed (DecBase 4))
              @?= Just (0.19712920 :: NFixed (DecBase 8))
        , testCase "Divide big NFixed(Bin) to small NFixed(Bin,3)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (625.123 :: NFixed (BinBase 3)) ::: (123.23 :: NFixed (BinBase 4))
              @?= (Just (5.071 :: NFixed (BinBase 8)))
        , testCase "Divide small NFixed(Bin) to big NFixed(Bin,3)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (123.23 :: NFixed (BinBase 3)) ::: (625.123 :: NFixed (BinBase 4))
              @?= (Just (0.19713015 :: NFixed (BinBase 8)))

        , testCase "Divide big NFixed(Dec) to small NFixed(Dec,10)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (625.123 :: NFixed (DecBase 3)) ::: (123.23 :: NFixed (DecBase 10))
              @?= Just (5.07281506 :: NFixed (DecBase 8))
        , testCase "Divide small NFixed(Dec) to big NFixed(Dec,10)" do
            div @(Maybe (NFixed (DecBase 8)))
              -$ (123.23 :: NFixed (DecBase 3)) ::: (625.123 :: NFixed (DecBase 10))
              @?= Just (0.19712920 :: NFixed (DecBase 8))
        , testCase "Divide big NFixed(Bin) to small NFixed(Bin,10)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (625.123 :: NFixed (BinBase 3)) ::: (123.23 :: NFixed (BinBase 10))
              @?= (Just (5.071 :: NFixed (BinBase 8)))
        , testCase "Divide small NFixed(Bin) to big NFixed(Bin,10)" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (123.23 :: NFixed (BinBase 3)) ::: (625.123 :: NFixed (BinBase 10))
              @?= (Just (0.19713015 :: NFixed (BinBase 8)))

        , testCase "Divide by zero" do
            div @(Maybe (NFixed (BinBase 8)))
              -$ (123.23 :: NFixed (BinBase 3)) ::: (0 :: NFixed (BinBase 2))
              @?= (Nothing)
        , testCase "Divide Natural by NFixed(Dec)" do
            div -$ (123 :: Natural) ::: (0.23 :: NFixed (DecBase 3))
              @?= Just (534.782608 :: NFixed (DecBase 6))
        , testCase "Divide Natural by NFixed(Bin)" do
            div -$ (123 :: Natural) ::: (0.25 :: NFixed (BinBase 3))
              @?= Just (492 :: NFixed (BinBase 6))
        , testCase "Divide two Naturals to get NFixed(Dec)" do
            div -$ (2 :: Natural) ::: (3 :: Natural)
              @?= Just (0.666666 :: NFixed (DecBase 6))
        , testCase "Divide two Naturals to get NFixed(Bin)" do
            div -$ (2 :: Natural) ::: (3 :: Natural)
              @?= Just (0.66 :: NFixed (BinBase 6))
        ]
    ]

test_RationalArith :: TestTree
test_RationalArith =
  testGroup "Arithmetic operations on Rational values"
    [ testGroup "Constructors"
        [ testCase "Constructing Rationals using operator" do
            (push 2 |%!| push 3) -$ ZSNil
              @?= (unsafe (mkRational (2, 3)))
        , testCase "Constructing Rationals using operator fails with exceptions" do
            let res = mfwsFailed $ fromLeft (error "Expcted Failure here") ((push 2 |%!| push 0) -$? ZSNil)
            assertBool "Expected failure with different error" $
              res == MichelsonFailedWith (VPair (VString (UnsafeMText {unMText = "Zero_denominator"}),VPair (VInt 2,VNat 0)))
        ]
    , testGroup "Basic operations between Rational and non-Rational values"
        [ testCase "Add Rational Integer" do
            add -$ (1 %! 2) ::: (1 :: Integer)
              @?= (3 %! 2)
        , testCase "Add Integer Rational" do
            add -$ (1 :: Integer) ::: (1 %! 2)
              @?= (3 %! 2)
        , testCase "Sub Rational Integer" do
            sub -$ (1 %! 2) ::: (1 :: Integer)
              @?= (-1 %! 2)
        , testCase "Sub Integer Rational" do
            sub -$ (1 :: Integer) ::: (1 %! 2)
              @?= (1 %! 2)
        , testCase "Mul Rational Integer" do
            mul -$ (1 %! 2) ::: (2 :: Integer)
              @?= (2 %! 2)
        , testCase "Mul Integer Rational" do
            mul -$ (3 :: Integer) ::: (1 %! 2)
              @?= (3 %! 2)
        , testCase "Div Rational Integer" do
            div -$ (1 %! 2) ::: (2 :: Integer)
              @?= (1 %! 4)
        , testCase "Div Rational Natural" do
            div -$ (1 %! 2) ::: (2 :: Natural)
              @?= (1 %! 4)
        , testCase "Div Integer Rational" do
            div -$ (3 :: Integer) ::: (1 %! 2)
              @?= (6 %! 1)
        ]
    , testGroup "Basic operations between Rationals"
        [ testCase "Add 1" do
            add -$ (1 %! 2) ::: (2 %! 3)
              @?= (7 %! 6)
        , testCase "Add 2" do
            add -$ (1 %! 2) ::: (-1 %! 2)
              @?= (0 %! 4)
        , testCase "Add 3" do
            add -$ (1 %! 4) ::: (1 %! 4)
              @?= (8 %! 16)
        , testCase "Sub 1" do
            sub -$ (1 %! 2) ::: (2 %! 3)
              @?= (-1 %! 6)
        , testCase "Sub 2" do
            sub -$ (1 %! 2) ::: (-1 %! 2)
              @?= (4 %! 4)
        , testCase "Sub 3" do
            sub -$ (1 %! 4) ::: (1 %! 4)
              @?= (0 %! 16)
        , testCase "Mul 1" do
            mul -$ (1 %! 2) ::: (2 %! 3)
              @?= (2 %! 6)
        , testCase "Mul 2" do
            mul -$ (1 %! 2) ::: (-1 %! 2)
              @?= (-1 %! 4)
        , testCase "Mul 3" do
            mul -$ (1 %! 4) ::: (1 %! 4)
              @?= (1 %! 16)
        , testCase "Div 1" do
            div -$ (1 %! 2) ::: (2 %! 3)
              @?= (3 %! 4)
        , testCase "Div 2" do
            div -$ (1 %! 2) ::: (-1 %! 2)
              @?= (-2 %! 2)
        , testCase "Div 3" do
            div -$ (1 %! 4) ::: (1 %! 4)
              @?= (4 %! 4)
        , testCase "Triple Mul" do
            tripleMul -$ (1 %! 2) ::: (2 %! 3) ::: (3 %! 5)
              @?= (6 %! 30)
        ]
    , let
          numDen x = (Data.Ratio.numerator x, Data.Ratio.denominator x)
          safeDiv :: (Integral a, Integral b) => b -> a -> Maybe (b, Natural)
          safeDiv _ 0 = Nothing
          safeDiv n d
            = Just $ second fromIntegralOverflowing
            $ numDen $ n % (fromIntegralOverflowing d)
          absMax :: Num a => a
          absMax = 999
      in testGroup "Rational division between integrals"
        [ testProperty "Integer/Natural" $ property do
            n :: Integer <- forAll $ Gen.integral (Range.linearFrom 0 (-absMax) absMax)
            d :: Natural <- forAll $ Gen.integral (Range.linearFrom 0 0 absMax)
            (div -$ n ::: d) === (uncurry (%!) <$> safeDiv n d)
        , testProperty "Natural/Natural" $ property do
            n :: Natural <- forAll $ Gen.integral (Range.linearFrom 0 0 absMax)
            d :: Natural <- forAll $ Gen.integral (Range.linearFrom 0 0 absMax)
            (div -$ n ::: d) === (uncurry (%%!) <$> safeDiv n d)
        , testProperty "Natural/Integer" $ property do
            n :: Natural <- forAll $ Gen.integral (Range.linearFrom 0 0 absMax)
            d :: Integer <- forAll $ Gen.integral (Range.linearFrom 0 (-absMax) absMax)
            (div -$ n ::: d) === (uncurry (%!) <$> safeDiv (fromIntegral n) d)
        , testProperty "Integer/Integer" $ property do
            n :: Integer <- forAll $ Gen.integral (Range.linearFrom 0 (-absMax) absMax)
            d :: Integer <- forAll $ Gen.integral (Range.linearFrom 0 (-absMax) absMax)
            (div -$ n ::: d) === (uncurry (%!) <$> safeDiv n d)
        ]
    , testGroup "Lorentz rounding"
        [ testCase "Round 1" do
            round_ -$ (7 %! 6)
              @?= (1 :: Integer)
        , testCase "Round 2" do
            round_ -$ (11 %! 6)
              @?= (2 :: Integer)
        , testCase "Round 3" do
            round_ -$ (1 %! 6)
              @?= (0 :: Integer)
        , testCase "Round 4" do
            round_ -$ (-1 %! 6)
              @?= (0 :: Integer)
        , testCase "Ceil 1" do
            ceil_ -$ ( 7 %! 6)
              @?= (2 :: Integer)
        , testCase "Ceil 2" do
            ceil_ -$ (11 %! 6)
              @?= (2 :: Integer)
        , testCase "Ceil 3" do
            ceil_ -$ (1 %! 6)
              @?= (1 :: Integer)
        , testCase "Ceil 4" do
            ceil_ -$ (-1 %! 6)
              @?= (0 :: Integer)
        , testCase "Ceil 5" do
            ceil_ -$ (6 %! 1)
              @?= (6 :: Integer)
        , testCase "Floor 1" do
            floor_ -$ (7 %! 6)
              @?= (1 :: Integer)
        , testCase "Floor 2" do
            floor_ -$ (11 %! 6)
              @?= (1 :: Integer)
        , testCase "Floor 3" do
            floor_ -$ (1 %! 6)
              @?= (0 :: Integer)
        , testCase "Floor 4" do
            floor_ -$ (-1 %! 6)
              @?= (-1 :: Integer)
        ]
    , testGroup "GCD and Normalization"
        [ testCase "GCD 15 5" do
            gcdEuclid -$ (15 ::: 5)
              @?= (5)
        , testCase "GCD 3 18" do
            gcdEuclid -$ (12 ::: 18)
              @?= (6)
        , testCase "GCD 17 2" do
            gcdEuclid -$ (17 ::: 2)
              @?= (1)
        , testCase "reduce 2 2" do
            reduce -$ (2 %! 2)
              @?= (1 %! 1)
        , testCase "reduce 3 5" do
            reduce -$ (3 %! 5)
              @?= (3 %! 5)
        , testCase "reduce 12 18" do
            reduce -$ (12 %! 18)
              @?= (2 %! 3)
        , testCase "reduce extended 2 2" do
            euclidExtendedNormalization -$ (2 %! 2)
              @?= (1 %! 1)
        , testCase "reduce extended 3 5" do
            euclidExtendedNormalization -$ (3 %! 5)
              @?= (3 %! 5)
        , testCase "reduce extended 12 18" do
            euclidExtendedNormalization -$ (12 %! 18)
              @?= (2 %! 3)
        ]
    ]

test_NRationalArith :: TestTree
test_NRationalArith =
  testGroup "Arithmetic operations on NRational values"
    [ testGroup "Basic operations between NRational and non-Rational values"
        [ testCase "Add Rational Integer" do
            add -$ (1 %%! 2) ::: (1 :: Integer)
              @?= (3 %! 2)
        , testCase "Add Integer Rational" do
            add -$ (1 :: Integer) ::: (1 %%! 2)
              @?= (3 %! 2)
        , testCase "Sub Rational Integer" do
            sub -$ (1 %%! 2) ::: (1 :: Integer)
              @?= (-1 %! 2)
        , testCase "Sub Integer Rational" do
            sub -$ (1 :: Integer) ::: (1 %%! 2)
              @?= (1 %! 2)
        , testCase "Mul Rational Integer" do
            mul -$ (1 %%! 2) ::: (2 :: Integer)
              @?= (2 %! 2)
        , testCase "Mul Integer Rational" do
            mul -$ (3 :: Integer) ::: (1 %%! 2)
              @?= (3 %! 2)
        , testCase "Div Rational Integer" do
            div -$ (1 %%! 2) ::: (2 :: Integer)
              @?= (1 %! 4)
        , testCase "Div Rational Natural" do
            div -$ (1 %%! 2) ::: (2 :: Natural)
              @?= (1 %%! 4)
        , testCase "Div Integer Rational" do
            div -$ (3 :: Integer) ::: (1 %%! 2)
              @?= (6 %! 1)
        ]
    , testGroup "Basic operations between Rationals"
        [ testCase "Add 1" do
            add -$ (1 %%! 2) ::: (2 %%! 3)
              @?= (7 %%! 6)
        , testCase "Add 2" do
            add -$ (1 %%! 2) ::: (-1 %! 2)
              @?= (0 %! 4)
        , testCase "Add 3" do
            add -$ (1 %%! 4) ::: (1 %%! 4)
              @?= (8 %%! 16)
        , testCase "Sub 1" do
            sub -$ (1 %%! 2) ::: (2 %%! 3)
              @?= (-1 %! 6)
        , testCase "Sub 2" do
            sub -$ (1 %%! 2) ::: (-1 %! 2)
              @?= (4 %! 4)
        , testCase "Sub 3" do
            sub -$ (1 %%! 4) ::: (1 %%! 4)
              @?= (0 %! 16)
        , testCase "Mul 1" do
            mul -$ (1 %%! 2) ::: (2 %%! 3)
              @?= (2 %%! 6)
        , testCase "Mul 2" do
            mul -$ (1 %%! 2) ::: (-1 %! 2)
              @?= (-1 %! 4)
        , testCase "Mul 3" do
            mul -$ (1 %%! 4) ::: (1 %%! 4)
              @?= (1 %%! 16)
        , testCase "Div 1" do
            div -$ (1 %%! 2) ::: (2 %%! 3)
              @?= (3 %%! 4)
        , testCase "Div 2" do
            div -$ (1 %%! 2) ::: (-1 %! 2)
              @?= (-2 %! 2)
        , testCase "Div 3" do
            div -$ (1 %%! 4) ::: (1 %%! 4)
              @?= (4 %%! 4)
        , testCase "Triple Mul" do
            tripleMul -$ (1 %%! 2) ::: (2 %%! 3) ::: (3 %%! 5)
              @?= (6 %%! 30)
        ]
    , testGroup "Lorentz rounding"
        [ testCase "Round 1" do
            round_ -$ (7 %%! 6)
              @?= (1 :: Natural)
        , testCase "Round 2" do
            round_ -$ (11 %%! 6)
              @?= (2 :: Natural)
        , testCase "Round 3" do
            round_ -$ (1 %%! 6)
              @?= (0 :: Natural)
        , testCase "Ceil 1" do
            ceil_ -$ ( 7 %%! 6)
              @?= (2 :: Natural)
        , testCase "Ceil 2" do
            ceil_ -$ (11 %%! 6)
              @?= (2 :: Natural)
        , testCase "Ceil 3" do
            ceil_ -$ (1 %%! 6)
              @?= (1 :: Natural)
        , testCase "Ceil 5" do
            ceil_ -$ (6 %%! 1)
              @?= (6 :: Natural)
        , testCase "Floor 1" do
            floor_ -$ (7 %%! 6)
              @?= (1 :: Natural)
        , testCase "Floor 2" do
            floor_ -$ (11 %%! 6)
              @?= (1 :: Natural)
        , testCase "Floor 3" do
            floor_ -$ (1 %%! 6)
              @?= (0 :: Natural)
        ]
    , testGroup "GCD and Normalization"
        [ testCase "GCD 15 5" do
            gcdEuclid -$ (15 ::: 5)
              @?= (5)
        , testCase "GCD 3 18" do
            gcdEuclid -$ (12 ::: 18)
              @?= (6)
        , testCase "GCD 17 2" do
            gcdEuclid -$ (17 ::: 2)
              @?= (1)
        , testCase "reduce 2 2" do
            reduce -$ (2 %%! 2)
              @?= (1 %%! 1)
        , testCase "reduce 3 5" do
            reduce -$ (3 %%! 5)
              @?= (3 %%! 5)
        , testCase "reduce 12 18" do
            reduce -$ (12 %%! 18)
              @?= (2 %%! 3)
        ]
    ]

test_Conversions :: TestTree
test_Conversions =
  testGroup "Custom arith types conversion test"
    [ testCase "Fixed to Rational 1" do
        convertFixedToRational -$ (1.23 :: Fixed (DecBase 2))
          @?= (123 %! 100)
    , testCase "Fixed to Rational 2" do
        convertFixedToRational -$ (-1.23 :: Fixed (DecBase 2))
          @?= (-123 %! 100)
    , testCase "Fixed to Rational 3" do
        convertFixedToRational -$ (0 :: Fixed (DecBase 2))
          @?= (0 %! 100)
    , testCase "Rational to Fixed 1" do
        convertRationalToFixed -$ (5 %! 10)
          @?= (0.5 :: Fixed (DecBase 2))
    , testCase "Rational to Fixed 2" do
        convertRationalToFixed -$ (2 %! 3)
          @?= (0.66668 :: Fixed (DecBase 5))
    , testCase "Rational to Fixed 3" do
        convertRationalToFixed -$ (17 %! 19)
          @?= (0.89486 :: Fixed (DecBase 5))
    , testCase "Safe Fixed to NRational (Success)" do
        convertFixedToNRational -$ (1.23 :: Fixed (DecBase 2))
          @?= Just (123 %%! 100)
    , testCase "Safe Fixed to NRational (Failure)" do
        convertFixedToNRational -$ (-1.23 :: Fixed (DecBase 2))
          @?= Nothing
    , testCase "Unsafe Fixed to NRational" do
        unsafeConvertFixedToNRational -$ (1.23 :: Fixed (DecBase 2))
          @?= (123 %%! 100)
    , testCase "NRational to Fixed 1" do
        convertNRationalToFixed -$ (5 %%! 10)
          @?= (0.5 :: Fixed (DecBase 2))
    , testCase "NRational to Fixed 2" do
        convertNRationalToFixed -$ (2 %%! 3)
          @?= (0.66668 :: Fixed (DecBase 5))
    , testCase "NRational to Fixed 3" do
        convertNRationalToFixed -$ (17 %%! 19)
          @?= (0.89486 :: Fixed (DecBase 5))
    ]

test_Fixed_DecBase_properties :: [TestTree]
test_Fixed_DecBase_properties = fixed_poly_properties @DecBase

test_Fixed_BinBase_properties :: [TestTree]
test_Fixed_BinBase_properties = fixed_poly_properties @BinBase

fixed_poly_properties
  :: forall base.
    ( LorentzFixedBase base
    , forall t. KnownNat t => HasResolution (base t)
    )
  => [TestTree]
fixed_poly_properties =
  [ testProperty "Negation" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          (neg -$ x) === (-x)
  , testProperty "Addition" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkFixed a) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y@(MkFixed b) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          (add -$ x ::: y) === MkFixed (a + b)
  , testProperty "Subtraction" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkFixed a) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y@(MkFixed b) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          (sub -$ x ::: y) === MkFixed (a - b)
  , testProperty "Multiplication" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2, someNatVal (t1 + t2)) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2), SomeNat (_ :: Proxy t3)) -> do
          x@(MkFixed a) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y@(MkFixed b) :: Fixed (base t2) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          case unsafeCoerce Refl :: t1 + t2 :~: t3 of
            Refl -> (mul -$ x ::: y) === MkFixed (a * b)
  , testProperty "Fixed/Fixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t3 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2, someNatVal t3) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2), SomeNat (_ :: Proxy t3)) -> do
          x :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y :: Fixed (base t2) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          checkDivision @base @t3 x y
  , testProperty "Integer/Fixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2)) -> do
          x :: Integer <-
            forAll $ Gen.integral $ Range.linearFrom 0 (-100_000) 100_000
          y :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          checkDivision @base @t2 x y
  , testProperty "Natural/Fixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2)) -> do
          x :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
          y :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          checkDivision @base @t2 x y
  , testProperty "Fixed/Integer" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1) of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkFixed a) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y :: Integer <-
            forAll $ Gen.integral $ Range.linearFrom 0 (-100_000) 100_000
          let mult = getBase @base ^ t1
              ediv'
                | x > 0 = quotRem
                -- this is the weirdness of mavryk' ediv...
                | y < 0 = join bimap negate ... divMod . negate
                | otherwise = divMod
          (ediv -$ x ::: y) === case y of
            0 -> Nothing
            _ -> Just (second (MkNFixed . fromIntegralOverflowing) $ ediv' a (y*mult))
  , testProperty "Fixed/Natural" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1) of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkFixed a) :: Fixed (base t1) <-
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 (-100_000) 100_000
          y :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
          let mult = getBase @base ^ t1
              ediv'
                | x > 0 = quotRem
                -- this is the weirdness of mavryk' ediv...
                | y < 0 = join bimap negate ... divMod . negate
                | otherwise = divMod
          (ediv -$ x ::: y) === case y of
            0 -> Nothing
            _ -> Just (second (MkNFixed . fromIntegralOverflowing) $ ediv' a (fromIntegral $ y*mult))
  ]

test_NFixed_DecBase_properties :: [TestTree]
test_NFixed_DecBase_properties = nfixed_poly_properties @DecBase

test_NFixed_BinBase_properties :: [TestTree]
test_NFixed_BinBase_properties = nfixed_poly_properties @BinBase

nfixed_poly_properties
  :: forall base.
    ( LorentzFixedBase base
    , forall t. KnownNat t => HasResolution (base t)
    )
  => [TestTree]
nfixed_poly_properties =
  [ testProperty "Addition" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y@(MkNFixed b) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          (add -$ x ::: y) === MkNFixed (a + b)
  , testProperty "Subtraction" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y@(MkNFixed b) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          (sub -$ x ::: y) === MkFixed (fromIntegral a - fromIntegral b)
  , testProperty "Multiplication" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2, someNatVal (t1 + t2)) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2), SomeNat (_ :: Proxy t3)) -> do
          x@(MkNFixed a) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y@(MkNFixed b) :: NFixed (base t2) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          case unsafeCoerce Refl :: t1 + t2 :~: t3 of
            Refl -> (mul -$ x ::: y) === MkNFixed (a * b)
  , testProperty "NFixed/NFixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t3 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2, someNatVal t3) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2), SomeNat (_ :: Proxy t3)) -> do
          x :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y :: NFixed (base t2) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          checkDivision @base @t3 x y
  , testProperty "Integer/NFixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2)) -> do
          x :: Integer <-
            forAll $ Gen.integral $ Range.linearFrom 0 (-100_000) 100_000
          y :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          checkDivision @base @t2 x y
  , testProperty "Natural/NFixed" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      t2 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1, someNatVal t2) of
        (SomeNat (_ :: Proxy t1), SomeNat (_ :: Proxy t2)) -> do
          x :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
          y :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          checkDivision @base @t2 x y
  , testProperty "NFixed/Integer" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1) of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y :: Integer <-
            forAll $ Gen.integral $ Range.linearFrom 0 (-100_000) 100_000
          let mult = getBase @base ^ t1
          (ediv -$ x ::: y) === case y of
            0 -> Nothing
            _ -> Just $ second (MkNFixed . fromIntegralOverflowing) $
              quotRem (fromIntegral a) (y*mult)
  , testProperty "NFixed/Natural" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case (someNatVal t1) of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (base t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
          let mult = getBase @base ^ t1
          (ediv -$ x ::: y) === case y of
            0 -> Nothing
            _ -> Just $ second MkNFixed $ quotRem a (y*mult)
  ]

checkDivision
  :: forall base t f x y m.
    ( LorentzFixedBase base
    , MonadTest m
    , Each '[IsoValue, Real] '[x, y]
    , ArithOpHs Div x y (Maybe (f (base t)))
    , KnownNat t
    , Each '[IsoValue, Eq, Show, Fractional] '[(f (base t))]
    )
  => x -> y -> m ()
checkDivision x y = do
  let x', y' :: Ratio Integer
      x' = toRational x
      y' = toRational y
      res = x' / y'
      resRounded = toRational (round' (res * mult) :: Integer) / mult
      mult = getBase @base ^ natVal (Proxy @t)
      round'
        | y < 0 = ceiling
        | otherwise = floor
  (div -$ x ::: y) === case y of
    0 -> Nothing
    _ -> Just (fromRational resRounded :: f (base t))

test_LSR_LSL :: [TestTree]
test_LSR_LSL =
  [ testProperty "LSR" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (BinBase t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100
          (lsr -$ x ::: y) ===
            (MkNFixed (shiftR a (fromIntegralOverflowing y)) :: NFixed (BinBase t1))
  , testProperty "LSR" $ property do
      t1 <- forAll $ Gen.integral $ Range.linearFrom 0 0 10
      case someNatVal t1 of
        SomeNat (_ :: Proxy t1) -> do
          x@(MkNFixed a) :: NFixed (BinBase t1) <- fmap fromRational $
            forAll $ Gen.realFrac_ $ Range.constantFrom 0 0 100_000
          y :: Natural <-
            forAll $ Gen.integral $ Range.linearFrom 0 0 100
          (lsl -$ x ::: y) ===
            (MkNFixed (shiftL a (fromIntegralOverflowing y)) :: NFixed (BinBase t1))
  ]
