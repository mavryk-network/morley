-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

-- | Tests for Lorentz on-chain views.

module Test.Lorentz.Views
  ( test_Views
  ) where

import Lorentz
import Prelude hiding (drop, some, swap, take, view)

import Data.Coerce (coerce)
import Test.Tasty (TestTree, testGroup)

import Morley.Mavryk.Address (ta)
import Morley.Util.Type
import Morley.Util.TypeTuple
import Test.Cleveland
import Test.Cleveland.Lorentz.Requester

data Views1
type instance RevealViews Views1 =
  [ "plusSt" ?:: Natural >-> Natural
  , "fail" ?:: MText >-> Never
  , "stPow" ?:: Natural >-> Natural
  ] ++
  RevealViews PlusView

data PlusView deriving stock (Generic)
type instance RevealViews PlusView =
 '[ "plus" ?:: (Integer, Integer) >-> Integer
  ]
instance ViewsDescriptorHasDoc PlusView

viewedContract1 :: Contract Never Natural Views1
viewedContract1 = compileLorentzContract $
  defaultContractData
    (car # never)
  & setViews
    ( mkView @"plusSt" (unpair # add)
    , mkView @"fail" (car # failWith)
    , mkView @"stPow"
        ( car #
          push @Integer 1 # rsub # isNat # ifSome
            ( -- transfer the same view recursively
              viewE @"plusSt"
                ! #address do selfAddress # asAddressOf_ viewedContract1
                ! #arg do push 0
              |*|
              -- transfer another view
              viewE @"stPow"
                ! #address do selfAddress # asAddressOf_ viewedContract1
                ! #arg take
            )
            ( push 1 )
        )
    , mkView @"plus" (car # unpair # add)
    )

transferTokensInLambda :: Contract Never () ViewsTTIV
transferTokensInLambda = compileLorentzContract $
  ContractData (mkContractCode $ car # never)
    ( recFromTuple $ mkView @"transferTokensLambda" Lorentz.do
        drop
        lambda Lorentz.do
          push [mv|100u|]
          unit
          transferTokens
        -- NB: this is a hole in the mavryk type system, below code
        -- tests whether our constraints match the network.
        dup
        pushContractRef (unit # failWith) $
          callingDefAddress $ toTAddress @() @() [ta|mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ|]
        exec @(ContractRef ()) @Operation
        drop
    )
    intactCompilationOptions

data Views2
type instance RevealViews Views2 =
 '[ "plus10"
      -- TODO [#716]: here we should use not TAddress but something else that
      -- 1. does not carry the parameter type
      -- 2. can narrow the list of views when converted from e.g. ContractHandle
      --    (because we are fine with any contract that contains more views, not only
      --     'PlusView')
      --
      -- For now, we have to cast 'TAddress'.
      ?:: (Integer, TAddress () PlusView)
      >-> Integer
  ]

data ViewsTTIV
type instance RevealViews ViewsTTIV =
 '[ "transferTokensLambda"
      ?:: ()
      >-> Lambda (ContractRef ()) Operation
  ]

viewedContract2 :: Contract Never () Views2
viewedContract2 = compileLorentzContract $
  defaultContractData
    (car # never)
  & setViews
    ( mkView @"plus10" (car # unpair # push 10 # pair # view @"plus")
    )

data AccessAddrView
type instance RevealViews AccessAddrView =
 '[ "originatedContract" ?:: () >-> TAddress Never PlusView
  ]

-- | On transfer originates a contract with 'PlusView' view and save its address in storage.
-- The view in the originated contract will sum up two numbers and add extra 100.
view3OriginatorContract :: Contract () (Maybe (TAddress Never PlusView)) AccessAddrView
view3OriginatorContract = compileLorentzContract $
  defaultContractData
    ( drop #
      pairE
        ( createContractE
          ! #storage (push 100)
          ! #delegate none
          ! #balance (push zeroMumav)
          ! #contract newContract
          |:| nil
        , some
        )
    )
  & setViews
    ( mkView @"originatedContract" (cdr # assertSome [mt|Nothing originated|])
    )
  where
    newContract = compileLorentzContract @Never @Natural @PlusView $
      defaultContractData (car # never)
      & setViews
        ( mkView @"plus" (unpair # unpair # add # add)
        )

-- | On transfer, calls the view in the contract originated by 'view3OriginatorContract'.
view3Caller :: Contract () (TAddress () AccessAddrView, Maybe Integer) ()
view3Caller = defaultContract $
  cdr #
  car #
  stackType @'[TAddress () AccessAddrView] #
  viewE @"plus"
    ! #arg do push (10, 1)
    ! #address do
        viewE @"originatedContract"
            ! #arg unit
            ! #address dup
    #
  stackType @[Integer, TAddress () AccessAddrView] #
  swap #
  pairE (take, some) #
  nil # pair

-- | Calls 'view3OriginatorContract' and 'view3Caller' sequentially.
view3CreatorAndCaller :: Contract (ContractRef (), ContractRef ()) () ()
view3CreatorAndCaller = defaultContract $
  car #
  unpair #
  transferTokensE
    ! #arg unit
    ! #amount (push zeroMumav)
    ! #contract take
  |:|
  transferTokensE
    ! #arg unit
    ! #amount (push zeroMumav)
    ! #contract take
  |:| nil
  |@| unit

test_Views :: TestTree
test_Views = testGroup "Views"
  -- NB: the primary way to call views should be via callView,
  -- but might as well ensure that te requester contract works as intended, too.
  [ testGroup "Via requester contract"

    [ testScenario "Simple case with requester contract" $ scenario do
          viewed <- originate "viewed" 1 viewedContract1
          requester <- originate "requester" [] (contractRequester @"plusSt" viewed)
          forM_ [5, 10] $ transfer requester . calling def
          getStorage requester @@== [11, 6]

    , testScenario "Failing view" $ scenario do
        viewed <- originate "viewed" 0 viewedContract1
        requester <- originate "requester" [] (contractRequester @"fail" viewed)
        transfer requester (calling def [mt|nyan|])
          & expectFailedWith [mt|nyan|]

    , testScenario "Recursive view" $ scenario do
        viewed <- originate "viewed" 2 viewedContract1
        requester <- originate "requester" [] (contractRequester @"stPow" viewed)
        forM_ [5, 0] $ transfer requester . calling def
        getStorage requester @@== [1, 32]

    , testScenario "Calling view in another contract" $ scenario do
        viewedHelper <- originate "viewed" 0 viewedContract1
        viewed <- originate "viewed" () viewedContract2
        requester <- originate "requester" [] (contractRequester @"plus10" viewed)
        -- TODO [#716]: coerce shouldn't be necessary
        transfer requester $ calling def (20, coerce $ toTAddress viewedHelper)
        getStorage requester @@== [30]
    ]

  , testGroup "Directly called"

    [ testScenario "Simple case" $ scenario do
        viewed <- originate "viewed" 1 viewedContract1
        forM [5, 10] (callView viewed #plusSt) @@== [6, 11]

    , testScenario "Failing view" $ scenario do
        viewed <- originate "viewed" 0 viewedContract1
        callView viewed #fail [mt|nyan|]
          & expectFailedWith [mt|nyan|]

    , testScenario "Recursive view" $ scenario do
        viewed <- originate "viewed" 2 viewedContract1
        forM [5, 0] (callView viewed #stPow) @@== [32, 1]

    , testScenario "Calling view in another contract" $ scenario do
        viewedHelper <- originate "viewed" 0 viewedContract1
        viewed <- originate "viewed" () viewedContract2
        -- TODO [#716]: coerce shouldn't be necessary
        callView viewed #plus10 (20, coerce $ toTAddress viewedHelper) @@== 30
    ]

  , testScenario "Calling a just originated view" $ scenario do
      -- It might be that a view is originated and called within the same
      -- global operation, we want to be sure this works
      originator <- originate "originator" Nothing view3OriginatorContract
      caller <- originate "caller" (toTAddress originator, Nothing) view3Caller
      allCaller <- originate "allCaller" () view3CreatorAndCaller

      transfer allCaller $ calling def (toContractRef originator, toContractRef caller)
      snd <$> getStorage caller @@== Just 111

  , testScenario "transferTokensInLambda typechecks" $ scenario do
      void $ originate "transferTokensInLambda" () transferTokensInLambda

  ]
