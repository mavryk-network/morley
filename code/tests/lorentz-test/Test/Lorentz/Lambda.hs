-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

-- | Tests on basic lambda functionality
module Test.Lorentz.Lambda
  ( test_Lambda
  ) where

import Lorentz
import Prelude ()

import Test.Tasty (TestTree, testGroup)

import Test.Cleveland

lambdaContract :: Contract (Lambda () [Operation]) () ()
lambdaContract = defaultContract Lorentz.do
  car
  unit
  exec
  unit
  swap
  pair

test_Lambda :: TestTree
test_Lambda = testGroup "Lambda"
  [ testScenario "Simple case with requester contract" $ scenario do
      addr <- newAddress auto
      handle <- originate "simpleLambda" () lambdaContract (1 :: Mumav)
      transfer handle $ calling def $ mkLambda Lorentz.do
        dip Lorentz.do
          pushContractRef (unit # failWith) $ callingDefAddress $ TAddress @() @() (toAddress addr)
          push 1
        transferTokens
        dip nil
        cons
  ]
