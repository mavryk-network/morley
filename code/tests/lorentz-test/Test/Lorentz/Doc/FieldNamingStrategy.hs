-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Doc.FieldNamingStrategy
  ( unit_stripFieldPrefix
  , unit_snake
  , unit_pascal
  , unit_stripFieldPrefixVia
  , unit_sumType
  ) where

import Lorentz

import Fmt (prettyText)
import Test.Tasty.HUnit (Assertion, (@?=))

import Morley.Michelson.Parser
import Morley.Michelson.Typed.Haskell.Doc
import Morley.Util.Interpolate

data TypeWithFields = TypeWithFields
  { twfField1 :: Integer
  , twfField2 :: Bool
  , twfField3 :: Natural
  , twfFA2Config :: ()
  , twfUSPosition :: ()
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

[typeDoc| TypeWithFields "A type with fields" stripFieldPrefix |]

unit_stripFieldPrefix :: Assertion
unit_stripFieldPrefix = do
  prettyText . buildADTRep (\_ _ -> "nil") . snd <$> typeDocHaskellRep (Proxy @TypeWithFields) []
    @?= Just ("\n" <> [itu|
      * ***field1*** :nil
      * ***field2*** :nil
      * ***field3*** :nil
      * ***fa2Config*** :nil
      * ***usPosition*** :nil
    |])
  getAnnotation @TypeWithFields NotFollowEntrypoint
    @?= [notes|
      pair (pair (int %field1) (bool %field2)) (nat %field3) (unit %fa2Config) (unit %usPosition)|]

data TypeWithFields2 = TypeWithFields2
  { twf2Field1 :: Integer
  , twf2Field2 :: Bool
  , twf2Field3 :: Natural
  , twf2FA2Config :: ()
  , twf2USPosition :: ()
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc TypeWithFields2 where
  typeDocMdDescription = "TWF2"

instance TypeHasFieldNamingStrategy TypeWithFields2 where
  typeFieldNamingStrategy = toSnake . dropPrefix

unit_snake :: Assertion
unit_snake = do
  prettyText . buildADTRep (\_ _ -> "nil") . snd <$> typeDocHaskellRep (Proxy @TypeWithFields2) []
    @?= Just ("\n" <> [itu|
      * ***field1*** :nil
      * ***field2*** :nil
      * ***field3*** :nil
      * ***fa2_config*** :nil
      * ***us_position*** :nil
    |])
  getAnnotation @TypeWithFields2 NotFollowEntrypoint
    @?= [notes|
      pair (pair (int %field1) (bool %field2)) (nat %field3) (unit %fa2_config) (unit %us_position)
      |]

data TypeWithFields3 = TypeWithFields3
  { twf3Field1 :: Integer
  , twf3Field2 :: Bool
  , twf3Field3 :: Natural
  , twf3FA2Config :: ()
  , twf3USPosition :: ()
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

[typeDoc| TypeWithFields3 "A type with fields" dropPascal |]

dropPascal :: Text -> Text
dropPascal = toPascal . dropPrefix

unit_pascal :: Assertion
unit_pascal = do
  prettyText . buildADTRep (\_ _ -> "nil") . snd <$> typeDocHaskellRep (Proxy @TypeWithFields3) []
    @?= Just ("\n" <> [itu|
      * ***Field1*** :nil
      * ***Field2*** :nil
      * ***Field3*** :nil
      * ***FA2Config*** :nil
      * ***USPosition*** :nil
    |])
  getAnnotation @TypeWithFields3 NotFollowEntrypoint
    @?= [notes|
      pair (pair (int %Field1) (bool %Field2)) (nat %Field3) (unit %FA2Config) (unit %USPosition)
      |]


data TypeWithFieldsVia = TypeWithFieldsVia
  { twfvField1 :: Integer
  , twfvField2 :: Bool
  , twfvField3 :: Natural
  , twfvFA2Config :: ()
  , twfvUSPosition :: ()
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FieldCamelCase

[typeDoc| TypeWithFieldsVia "A type with fields using via deriving" |]

unit_stripFieldPrefixVia :: Assertion
unit_stripFieldPrefixVia = do
  prettyText . buildADTRep (\_ _ -> "nil") . snd <$> typeDocHaskellRep (Proxy @TypeWithFields) []
    @?= Just ("\n" <> [itu|
      * ***field1*** :nil
      * ***field2*** :nil
      * ***field3*** :nil
      * ***fa2Config*** :nil
      * ***usPosition*** :nil
    |])
  getAnnotation @TypeWithFields NotFollowEntrypoint
    @?= [notes|
      pair (pair (int %field1) (bool %field2)) (nat %field3) (unit %fa2Config) (unit %usPosition)|]

data SumType
  = SumInt Integer
  | SumBool Bool
  | SumNat Natural
  | SumUnit1 ()
  | SumUnit2 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

[typeDoc| SumType "A sum type" constStrategy |]

constStrategy :: Text -> Text
constStrategy = const "foobar"

-- | Sum type shouldn't be affected by field renaming strategy
unit_sumType :: Assertion
unit_sumType = do
  prettyText . buildADTRep (\_ _ -> "nil") . snd <$> typeDocHaskellRep (Proxy @SumType) []
    @?= Just ((<> "\n") [itu|
      *one of* \&
      + **SumInt**nil
      + **SumBool**nil
      + **SumNat**nil
      + **SumUnit1**nil
      + **SumUnit2**nil
      |])
  getAnnotation @SumType NotFollowEntrypoint
    @?= [notes|
      or
        (or (int %sumInt) (bool %sumBool))
        (or
          (nat %sumNat)
          or (unit %sumUnit1) (unit %sumUnit2)
        )
      |]
