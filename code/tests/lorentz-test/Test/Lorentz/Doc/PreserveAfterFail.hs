-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Testing 'EpdDelegate' entrypoint documentation flattening with
-- 'FlattenedEntrypointsKind'
module Test.Lorentz.Doc.PreserveAfterFail
  ( unit_docs_preserved_after_failwith
  ) where

import Lorentz

import Test.Tasty.HUnit (Assertion, (@?=))

unit_docs_preserved_after_failwith :: Assertion
unit_docs_preserved_after_failwith = do
  let docs = toStrict $ buildMarkdownDoc $ finalizedAsIs contr
      -- NB: toStrict is required here due to some weirdness wrt how lazy Text is matched
  docs @?= "Hello, World!\n\nfoobar\n"

contr :: Contract () () ()
contr = defaultContract
  $ failWith
  # doc (DDescription "Hello, World!")
  # doc (DDescription "foobar")
