-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for bytes conversion
module Test.Lorentz.BytesConv
  ( test_Bytes_arith
  ) where

import Fmt (Buildable(..), hexF, pretty, (+|), (|+))
import Test.Tasty (TestTree)

import Lorentz ((#))
import Lorentz qualified as L
import Morley.Michelson.Typed (toVal, untypeValue)
import Test.Cleveland

data BytesOp
  = IntToBytes Integer ByteString
  | NatToBytes Natural ByteString
  | BytesToInt ByteString Integer
  | BytesToNat ByteString Natural

instance Buildable BytesOp where
  build = \case
    IntToBytes i o -> "IntToBytes " +| i |+ " = " +| hexF' o
    NatToBytes i o -> "NatToBytes " +| i |+ " = " +| hexF' o
    BytesToInt i o -> "BytesToInt " +| hexF' i |+ " = " +| o |+ ""
    BytesToNat i o -> "BytesToNat " +| hexF' i |+ " = " +| o |+ ""
    where
      hexF' = ("0x" <>) . hexF

-- examples from https://gitlab.com/mavryk-network/mavryk-protocol/-/merge_requests/6681
test_Bytes_arith :: [TestTree]
test_Bytes_arith = genTest <$>
  [ NatToBytes 0x00 ""
  , NatToBytes 0x01 "\x01"
  , NatToBytes 0xff "\xff"
  , NatToBytes 0x100 "\x01\x00"

  , BytesToNat  "" 0
  , BytesToNat  "\x00"         0x00
  , BytesToNat  "\x01"         0x01
  , BytesToNat  "\x00\x01"     0x01
  , BytesToNat  "\xff"         0xff
  , BytesToNat  "\x00\x00\xff" 0xff
  , BytesToNat  "\x01\x00"     0x0100

  , IntToBytes 0x00    ""
  , IntToBytes 0x01    "\x01"
  , IntToBytes 0x7f    "\x7f"
  , IntToBytes (-0x80) "\x80"
  , IntToBytes (-0x8000) "\x80\x00" -- constructed by analogy
  , IntToBytes (-0x8001) "\xff\x7f\xff" -- constructed by analogy
  , IntToBytes 0x80    "\x00\x80" -- not "\x80"
  , IntToBytes (-0x81) "\xff\x7f" -- not "\x7f"
  , IntToBytes 0x8000  "\x00\x80\x00" -- not "\x80\x00"

  , BytesToInt ""         0x00
  , BytesToInt "\x01"     0x01
  , BytesToInt "\x00\x01" 0x01
  , BytesToInt "\x7f"     0x7f
  , BytesToInt "\x00\x7f" 0x7f
  , BytesToInt "\x80"     (-0x80) -- not 0x80
  , BytesToInt "\xff\x80" (-0x80)
  , BytesToInt "\xff\x7f" (-0x81) -- NB: example was broken in source
  , BytesToInt "\x80\x00" (-0x8000)
  , BytesToInt "\xff\x7f\xff" (-0x8001) -- constructed by analogy
  , BytesToInt "\x7f\xff" 0x7fff -- constructed by analogy
  ]

genTest :: BytesOp -> TestTree
genTest op = testScenario (pretty op) $ scenario do
  mkRC op

mkRC :: MonadCleveland caps m => BytesOp -> m ()
mkRC = \case
  IntToBytes i o ->
    runCode RunCode
      { rcContract = L.defaultContract @Integer @ByteString $ L.car # L.bytes # L.nil # L.pair
      , rcStorage = untypeValue $ toVal ("" :: ByteString)
      , rcParameter = untypeValue $ toVal i
      , ..
      } @@== o
  NatToBytes i o ->
    runCode RunCode
      { rcContract = L.defaultContract @Natural @ByteString $ L.car # L.bytes # L.nil # L.pair
      , rcStorage = untypeValue $ toVal ("" :: ByteString)
      , rcParameter = untypeValue $ toVal i
      , ..
      } @@== o
  BytesToInt i o ->
    runCode RunCode
      { rcContract = L.defaultContract @ByteString @Integer $ L.car # L.int # L.nil # L.pair
      , rcStorage = untypeValue $ toVal (0 :: Integer)
      , rcParameter = untypeValue $ toVal i
      , ..
      } @@== o
  BytesToNat i o ->
    runCode RunCode
      { rcContract = L.defaultContract @ByteString @Natural $ L.car # L.nat # L.nil # L.pair
      , rcStorage = untypeValue $ toVal (0 :: Natural)
      , rcParameter = untypeValue $ toVal i
      , ..
      } @@== o
  where
    rcAmount = 0
    rcBalance = 0
    rcSource = Nothing
    rcNow = Nothing
    rcLevel = Nothing
