-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for Lorentz packing/unpacking

module Test.Lorentz.Pack
  ( test_lambda_roundtrip
  ) where

import Debug qualified (show)
import Lorentz
import Prelude hiding (drop, swap)

import Fmt (pretty)
import Test.HUnit (Assertion, assertFailure, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Michelson.Typed.Annotation (Anns(..), starNotes)
import Morley.Michelson.Typed.Instr (Instr(..))
import Morley.Michelson.Typed.Util (DfsSettings(..), SomeAnns(..), dfsFoldInstr, instrAnns)
import Morley.Michelson.Untyped.Annotation (pattern UnsafeAnnotation)

test_lambda_roundtrip :: [TestTree]
test_lambda_roundtrip =
  [ testCase "Packing and then unpacking a Lambda does not add empty annotations" $
      lambdaRoundtripWithoutNotes lam
  ]
  where
    lam :: Lambda () ()
    lam = mkLambda $ push @Natural 5 # drop

-- | Checks that packing and unpacking a lambda made of instructions without
-- Annotations will produce the same lambda, still without annotations.
lambdaRoundtripWithoutNotes
  :: forall i o. NiceUnpackedValue (Lambda i o)
  => Lambda i o
  -> Assertion
lambdaRoundtripWithoutNotes l = case lUnpackValueRaw @(Lambda i o) $ lPackValueRaw l of
  Left err -> assertFailure $ "Unpacking error: " <> pretty err
  Right ul -> case dfsFoldInstr dfsSettings instrNotes $ iAnyCode (unWrappedLambda ul) of
    [] -> ul @?= l
    notes -> assertFailure $ "Lambda has annotations: " <> pretty notes
  where
    unWrappedLambda = \case
      WrappedLambda i -> i
      RecLambda (i :: '[i, Lambda i o] :-> '[o]) -> dip (I $ LAMBDA_REC $ unLorentzInstr i) # i
    dfsSettings = def { dsGoToValues = True}

    instrNotes :: Instr inp out -> [Text]
    instrNotes i = maybe [] getAnns $ instrAnns i

    getAnns :: SomeAnns -> [Text]
    getAnns = \case
      SomeAnns (ann `AnnsCons` rest)
        | UnsafeAnnotation "" <- ann -> getAnns (SomeAnns rest)
        | otherwise -> Debug.show ann : getAnns (SomeAnns rest)
      SomeAnns (ann `AnnsTyCons` rest)
        | ann == starNotes -> getAnns (SomeAnns rest)
        | otherwise -> Debug.show ann : getAnns (SomeAnns rest)
      SomeAnns AnnsNil -> []
      SomeUncheckedAnns xs -> Debug.show <$> toList xs
