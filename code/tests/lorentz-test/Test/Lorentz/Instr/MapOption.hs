-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'map' instruction over 'Maybe' (a.k.a. @option@)
module Test.Lorentz.Instr.MapOption
  ( test_mapOption
  ) where

import Lorentz

import Hedgehog (forAll, property)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Prelude hiding (drop, map)
import Test.Tasty.Hedgehog (testProperty)

import Test.Tasty (TestTree)

import Test.Cleveland

exampleMapContract :: Contract (Maybe Integer) Integer ()
exampleMapContract = defaultContract $
  unpair #
  map (add # push @Natural 0) #
  drop #
  nil #
  pair

test_mapOption :: [TestTree]
test_mapOption =
  [ testScenario "MAP over option int" $ scenario do
      handle <- originate "map example contract" 123 exampleMapContract
      transfer handle $ calling def Nothing
      getStorage handle @@== 123
      transfer handle $ calling def (Just 321)
      getStorage handle @@== 444
      transfer handle $ calling def Nothing
      getStorage handle @@== 444
  , testProperty "MAP over option int randomized test" $ property do
      let intGen = Gen.integral (Range.linear -10000 10000)
      st <- forAll intGen
      arg <- forAll $ Gen.maybe intGen
      testScenarioProps $ scenario do
        handle <- originate "map example contract" st exampleMapContract
        transfer handle $ calling def arg
        getStorage handle @@== (fromMaybe 0 arg + st)
  ]
