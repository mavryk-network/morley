-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Testing 'EpdRecursive' entrypoint documentation flattening with
-- 'entryCaseFlattened'
module Test.Lorentz.Entrypoints.Doc.RecursiveFlatten
  ( unit_RecursiveFlatten
  ) where

import Lorentz hiding (not)
import Prelude hiding (drop, fail)

import Test.Tasty.HUnit (Assertion)

import Test.Lorentz.Entrypoints.Doc.Utils

unit_RecursiveFlatten :: Assertion
unit_RecursiveFlatten = do
  let docs = toStrict $ buildMarkdownDoc $ finalizedAsIs contr
      -- NB: toStrict is required here due to some weirdness wrt how lazy Text is matched
  mapM_ (hasEP docs)
    [ "set_admin"
    , "accept_admin"
    , "propose"
    , "vote"
    , "plain1"
    , "plain2"
    , "plain3"
    ]
  mapM_ (hasNot docs)
    [ "Call the contract (default entrypoint)"
    , "`adminEP`"
    , "`voteEP`"
    , "`partiallyRecursiveEP`"
    , "`rec1`"
    ]
  countEPSections docs 1

contr :: Contract Parameter () ()
contr = defaultContract
  $ car
  # (entryCaseFlattened @Parameter
      ( #cAdminEP /-> adminEP
      , #cVoteEP /-> voteEP
      ))
  # push ()
  # nil
  # pair
  where
    adminEP =
      entryCaseFlattened @AdminEP
        ( #cSet_admin /-> drop
        , #cAccept_admin /-> drop
        , #cPartiallyRecursiveEP /-> partiallyRecursiveEP
        )
    voteEP =
      entryCaseSimple @VoteEP
        ( #cPropose /-> drop
        , #cVote /-> drop
        )
    partiallyRecursiveEP =
       entryCaseFlattened @PartiallyRecursiveEP
        ( #cRec1 /-> rec1
        , #cPlain1 /-> drop )
    rec1 =
      entryCaseSimple @Plain23
        ( #cPlain2 /-> drop
        , #cPlain3 /-> drop )

data AdminEP
  = Set_admin Address
  | Accept_admin ()
  | PartiallyRecursiveEP PartiallyRecursiveEP
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

data PartiallyRecursiveEP
  = Rec1 Plain23
  | Plain1 ()
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

data Plain23 = Plain2 () | Plain3 ()
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

data VoteEP
  = Propose ("proposal_uri" :! MText, "choices" :! [MText])
  | Vote ("proposal_uri" :! MText, "choice_index" :! Natural)
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

data Parameter
  = AdminEP AdminEP
  | VoteEP VoteEP
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

[entrypointDoc| Parameter recursive |]
[entrypointDoc| PartiallyRecursiveEP recursive |]
[entrypointDoc| AdminEP recursive |]
[entrypointDoc| Plain23 plain |]
[entrypointDoc| VoteEP plain |]
