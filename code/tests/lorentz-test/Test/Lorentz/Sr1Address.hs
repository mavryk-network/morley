-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}

-- | Tests for sr1 addresses
module Test.Lorentz.Sr1Address
  ( test_sr1keyhash
  ) where

import Lorentz
import Prelude (pass)

import Test.Tasty (TestTree)

import Morley.Mavryk.Address
import Test.Cleveland

test_sr1keyhash :: TestTree
test_sr1keyhash = testScenario "sr1 address is passed properly" $ scenario do
  let caddr = toTAddress [ta|sr1RYurGZtN8KNSpkMcCt9CgWeUaNkzsAfXf|]
  handle <- originate "sr1_test" "\x00" sr1Contract
  -- Until we support smart rollups, the following call will fail, but we
  -- still want to typecheck it.
  if False
    then transfer handle $ calling def caddr
    else pass

sr1Contract :: Contract (TAddress ByteString ()) ByteString ()
sr1Contract = compileLorentzContract $ defaultContractData Lorentz.do
  unpair
  dip dup
  contract @ByteString
  assertSome @MText "Bad contract address"
  push [mv|0|]
  dig @2
  transferTokens
  nil; swap
  cons
  pair
