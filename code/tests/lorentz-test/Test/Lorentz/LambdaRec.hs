-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

-- | Tests on recursive lambda functionality
module Test.Lorentz.LambdaRec
  ( test_LambdaRec
  ) where

import Lorentz
import Prelude (Num)

import Test.Tasty (TestTree, testGroup)

import Morley.Michelson.Typed.Arith
import Test.Cleveland

newtype Counter = Counter Integer
  deriving newtype (Num, IsoValue, HasAnnotation, HasRPCRepr)
newtype Nth = Nth Integer
  deriving newtype (Num, IsoValue, HasAnnotation, HasRPCRepr)
newtype Nth1 = Nth1 Integer
  deriving newtype (Num, IsoValue, HasAnnotation, HasRPCRepr)

instance UnaryArithOpHs Eq' Counter where
  type UnaryArithResHs Eq' Counter = Bool

instance ArithOpHs Add Nth Nth1 Nth
instance ArithOpHs Sub Counter Natural Counter

instance Nth `CanCastTo` Nth1

lambdaContract :: Contract (WrappedLambda '[Counter, Nth, Nth1] '[Nth, Nth1], Counter) (Nth, Nth1) ()
lambdaContract = defaultContract Lorentz.do
  unpair
  dip unpair
  unpair
  execute
  pair
  nil
  pair

test_LambdaRec :: TestTree
test_LambdaRec = testGroup "LambdaRec"
  [ testScenario "Simple recursive fibonacci" $ scenario do
      handle <- originate "recursiveLambda" (1, 1) lambdaContract
      let param :: WrappedLambda '[Counter, Nth, Nth1] '[Nth, Nth1]
          param = mkLambdaRec Lorentz.do
            dup
            ifEq0
              Lorentz.do
                drop
                dipN @2 drop
              Lorentz.do
                dip Lorentz.do
                  dup
                  dip add
                  checkedCoerce_ @Nth @Nth1
                  swap
                  push @Natural 1
                sub
                dig @3
                execute
      transfer handle $ calling def (param, 4)
      getStorage handle @@== (8, 5)
      transfer handle $ calling def (param, 2)
      getStorage handle @@== (21, 13)
  ]
