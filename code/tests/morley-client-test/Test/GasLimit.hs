-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for gas limit calculation implementation in 'morley-client'.
module Test.GasLimit
  ( test_GasLimitCalculation
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Client (runMorleyClientM, runMorleyOnlyRpcM)
import Morley.Client.Action.Batched
import Morley.Client.Action.Common (TD(..), TransactionData(..))
import Morley.Client.MavrykClient.Impl (getSecretKey)
import Morley.Client.Types (awaAlias)
import Morley.Michelson.Typed (IsoValue(..))
import Morley.Michelson.Untyped (pattern DefEpName)
import Morley.Mavryk.Address
import Morley.Mavryk.Core (mv)
import Morley.Mavryk.Crypto
import Morley.Mavryk.Crypto.Ed25519 (randomSecretKey)

import Test.Cleveland (mkMorleyOnlyRpcEnvNetwork, neMorleyClientEnv)
import Test.Cleveland.Internal.Abstract (Moneybag(..), NetworkEnv)
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty (whenNetworkEnabled)

test_GasLimitCalculation :: [TestTree]
test_GasLimitCalculation =
  [ whenNetworkEnabled $ \withEnv ->
      testCase "Attempt a batch with 100 transfers" $ withEnv $
        sendNTransfers 100
  , whenNetworkEnabled $ \withEnv ->
      testCase "Attempt a batch with 9 transfers (#933 regression test)" $ withEnv $
        sendNTransfers 9
  ]

sendNTransfers :: Int -> NetworkEnv -> IO ()
sendNTransfers n env = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  moneybagSecretKey <- runMorleyClientM (neMorleyClientEnv env) $ do
    getSecretKey $ awaAlias moneybagAddr

  destSecretKey <- SecretKeyEd25519 <$> randomSecretKey
  let dest = mkKeyAddress . toPublic $ destSecretKey

  -- NB: using MorleyOnlyRpcM because it's _a lot_ faster; MorleyClientM
  -- spends an unholy amount of time looking up aliases.
  void $ runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [destSecretKey, moneybagSecretKey]) do
    runOperationsBatch moneybagAddr $ replicateM n $ do
      runTransactionM $ TransactionData TD
        { tdReceiver = Constrained dest
        , tdAmount = [mv|100u|]
        , tdEpName = DefEpName
        , tdParam = toVal ()
        , tdMbFee = Nothing
        }
