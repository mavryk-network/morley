-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.AliasCache
  ( test_genKey
  , test_genFreshKey
  , test_importKey
  , test_importKeyError
  , test_rememberContract
  , test_importKey_no_cache
  , test_genKey_no_cache
  , test_genFreshKey_no_cache
  , test_rememberContract_no_cache
  ) where

import Crypto.Random (getRandomBytes)
import Fmt (hexF, pretty)
import Test.Tasty (TestName, TestTree)
import Test.Tasty.HUnit (assertFailure, testCase, (@?), (@?=))

import Morley.Client
import Morley.Client.MavrykClient.Impl (importKey, lookupAliasCache)
import Morley.Client.MavrykClient.Types (tceAliasMapL)
import Morley.Client.Types.AliasesAndAddresses
import Morley.Michelson.Typed (SingI)
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Mavryk.Crypto (detSecretKey)
import Morley.Util.Constrained
import Test.Cleveland (NetworkEnv(..))
import Test.Cleveland.Tasty (whenNetworkEnabled)

-- | To avoid issues with tests interleaving, create an isolated cache per test
-- case, with a given initial value.
testCacheIsolated'
  :: Maybe [Constrained NullConstraint AddressWithAlias]
  -> TestName
  -> MorleyClientM ()
  -> TestTree
testCacheIsolated' initCache name cont = whenNetworkEnabled \withEnv ->
  testCase name $ withEnv \env -> do
    mvar <- newMVar $ mkAliasesAndAddresses <$> initCache
    let env' = neMorleyClientEnv env & mceMavrykClientL . tceAliasMapL .~ mvar
    runMorleyClientM env' cont

-- | Test with isolated empty, but existing cache.
testCacheIsolated :: TestName -> MorleyClientM () -> TestTree
testCacheIsolated = testCacheIsolated' $ Just []

(@==) :: (MonadIO m, Eq a, Show a) => m a -> a -> m ()
l @== r = do
  res <- l
  liftIO $ res @?= r

newAlias :: (SingI kind, L1AddressKind kind, MonadIO m) => m (Alias kind)
newAlias = do
  bs <- liftIO $ (getRandomBytes 16 :: IO ByteString)
  pure $ mkAlias $ pretty $ "this_alias_shouldnt_exist_yet_" <> hexF bs

test_genKey :: TestTree
test_genKey =
  testCacheIsolated "genKey updates cache and doesn't generate new key on subsequent calls" do
    alias <- newAlias

    awa <- genKey alias
    lookupAliasCache alias @== Just (awaAddress awa)

    awa' <- genKey alias
    lookupAliasCache alias @== Just (awaAddress awa')

test_genFreshKey :: TestTree
test_genFreshKey =
  testCacheIsolated "genFreshKey generates new keys on each call" do
    alias <- newAlias

    awa <- genFreshKey alias
    lookupAliasCache alias @== Just (awaAddress awa)

    awa' <- genFreshKey alias
    lookupAliasCache alias @== Just (awaAddress awa')

    liftIO $ awa /= awa' @? "Should generate different addresses"

test_importKey :: TestTree
test_importKey =
  testCacheIsolated "importKey updates cache" do
    alias <- newAlias
    secret <- liftIO $ detSecretKey <$> getRandomBytes 16
    secret2 <- liftIO $ detSecretKey <$> getRandomBytes 16

    awa <- importKey False alias secret
    lookupAliasCache alias @== Just (awaAddress awa)

    awa' <- importKey True alias secret2
    lookupAliasCache alias @== Just (awaAddress awa')

test_importKeyError :: TestTree
test_importKeyError =
  testCacheIsolated "importKey doesn't update cache on duplicate alias" do
    alias <- newAlias
    secret <- liftIO $ detSecretKey <$> getRandomBytes 16
    secret2 <- liftIO $ detSecretKey <$> getRandomBytes 16

    awa <- importKey False alias secret
    lookupAliasCache alias @== Just (awaAddress awa)

    res <- try $ importKey False alias secret2
    liftIO $ case res of
      Left DuplicateAlias{} -> pass
      Left e -> assertFailure $ "Expected DuplicateAlias error, but got " <> displayException e
      Right{} -> assertFailure "Expected exception, but importKey succeeded"
    lookupAliasCache alias @== Just (awaAddress awa)

test_rememberContract :: TestTree
test_rememberContract =
  testCacheIsolated "rememberContract updates cache when appropriate" do
    alias <- newAlias
    addr <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16

    void $ rememberContract DontSaveAlias addr alias
    lookupAliasCache alias @== Nothing

    void $ rememberContract ForbidDuplicateAlias addr alias
    lookupAliasCache alias @== Just addr

    addr2 <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16

    res <- try $ rememberContract ForbidDuplicateAlias addr2 alias
    liftIO case res of
      Left DuplicateAlias{} -> pass
      Left e -> assertFailure $ "Expected DuplicateAlias error, but got " <> displayException e
      Right{} -> assertFailure "Expected exception, but rememberContract succeeded"
    lookupAliasCache alias @== Just addr

    void $ rememberContract KeepDuplicateAlias addr2 alias
    lookupAliasCache alias @== Just addr

    void $ rememberContract OverwriteDuplicateAlias addr2 alias
    lookupAliasCache alias @== Just addr2

test_importKey_no_cache :: TestTree
test_importKey_no_cache =
  testCacheIsolated' Nothing "importKey doesn't update null cache" do
    alias <- newAlias
    secret <- liftIO $ detSecretKey <$> getRandomBytes 16

    void $ importKey True alias secret
    lookupAliasCache alias @== Nothing

test_rememberContract_no_cache :: TestTree
test_rememberContract_no_cache =
  testCacheIsolated' Nothing "rememberContract doesn't update null cache" do
    alias <- newAlias
    addr <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16

    void $ rememberContract DontSaveAlias addr alias
    lookupAliasCache alias @== Nothing

    void $ rememberContract ForbidDuplicateAlias addr alias
    lookupAliasCache alias @== Nothing

    addr2 <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16

    void $ rememberContract KeepDuplicateAlias addr2 alias
    lookupAliasCache alias @== Nothing

    void $ rememberContract OverwriteDuplicateAlias addr2 alias
    lookupAliasCache alias @== Nothing

test_genKey_no_cache :: TestTree
test_genKey_no_cache =
  testCacheIsolated' Nothing "genKey updates null cache" do
    alias <- newAlias
    awa <- genKey alias
    lookupAliasCache alias @== Just (awaAddress awa)

test_genFreshKey_no_cache :: TestTree
test_genFreshKey_no_cache =
  testCacheIsolated' Nothing "genFreshKey updates null cache" do
    alias <- newAlias
    awa <- genFreshKey alias
    lookupAliasCache alias @== Just (awaAddress awa)
