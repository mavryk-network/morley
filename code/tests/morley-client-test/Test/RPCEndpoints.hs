-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on various @mavkit-node@ RPC endpoints.
module Test.RPCEndpoints
  ( test_getBlockOperations
  , test_getScriptSize
  ) where

import Control.Concurrent.Async (forConcurrently_)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (Assertion, testCase)

import Morley.Client
import Morley.Michelson.Typed
import Morley.Mavryk.Core (zeroMumav)
import Test.Cleveland (NetworkEnv(neMorleyClientEnv))
import Test.Cleveland.Internal.Abstract (Moneybag(..))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Lorentz (toL1Address)
import Test.Cleveland.Michelson.Import (importContract)
import Test.Cleveland.Tasty (whenNetworkEnabled)

import Test.Util.Contracts (inContractsDir)

-- | Ensure running 'getBlockOperations' on blocks with various
-- transaction types does not fail.
test_getBlockOperations :: TestTree
test_getBlockOperations = whenNetworkEnabled $ \withEnv ->
  testCase "getBlockOperations works" $ withEnv $ \env -> do
    contract <- importContract @(ToT ()) @(ToT [Integer]) $ inContractsDir "basic1.mv"
    dest <- runMorleyClientM (neMorleyClientEnv env) $
      genFreshKey "temporary-alias"
    let operations =
          [ OpOriginate $ OriginationData
              { odAliasBehavior = OverwriteDuplicateAlias
              , odName = "basic"
              , odContract = contract
              , odBalance = zeroMumav
              , odStorage = toVal @[Integer] []
              , odMbFee = Nothing
              , odDelegate = Nothing
              }
          , OpTransfer $ TransactionData $ TD
              { tdReceiver = toL1Address dest
              , tdAmount = 1
              , tdEpName = DefEpName
              , tdParam = toVal ()
              , tdMbFee = Nothing
              }
          ]

    concurrentEndpointCalls env operations

-- | Calls 'getBlockOperations' endpoint for the last 5 blocks concurrently.
concurrentEndpointCalls :: NetworkEnv -> [OperationInfo ClientInput] -> Assertion
concurrentEndpointCalls env ops = do
  let mce = neMorleyClientEnv env
  Moneybag moneybag <- setupMoneybagAddress env
  void $ runMorleyClientM mce $ runOperations moneybag ops
  forConcurrently_ [0..4] $ \depth ->
    runMorleyClientM mce $ getBlockOperations (AtDepthId depth)

-- | Ensure 'getScriptSize' interface does not change with Mavkit updates.
test_getScriptSize :: TestTree
test_getScriptSize = whenNetworkEnabled \withEnv ->
  testCase "getScriptSize work" $ withEnv \env -> do
    ct <- importContract @(ToT ()) @(ToT [Integer]) $ inContractsDir "basic1.mv"
    let st = VList [VInt 5]
    void $ runMorleyClientM (neMorleyClientEnv env) $
      computeContractSize ct st
