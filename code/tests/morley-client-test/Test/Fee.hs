-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for fee calculation implementation in 'morley-client'.
module Test.Fee
  ( test_FeeCalculation
  ) where

import Data.List.NonEmpty qualified as NE
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (Assertion, testCase, (@?=))

import Morley.Client
import Morley.Client.Action.Common (computeStorageLimit)
import Morley.Client.MavrykClient.Impl
  (calcOriginationFee, calcRevealFee, calcTransferFee, importKey)
import Morley.Client.MavrykClient.Types (CalcOriginationFeeData(..), CalcTransferFeeData(..))
import Morley.Micheline.Json (MavrykMumav(..))
import Morley.Michelson.Runtime.GState (genesisAddress)
import Morley.Michelson.Typed (IsoValue(..))
import Morley.Michelson.Untyped (pattern DefEpName)
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias (AddressOrAlias(..))
import Morley.Mavryk.Core (mv, zeroMumav)
import Morley.Mavryk.Crypto
import Morley.Mavryk.Crypto.Ed25519 qualified as Ed25519
import Test.Cleveland (NetworkEnv(neMorleyClientEnv), mkMorleyOnlyRpcEnvNetwork)
import Test.Cleveland.Internal.Abstract (Moneybag(..))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Michelson.Import (importContract)
import Test.Cleveland.Tasty (whenNetworkEnabled)

import Test.Util.Contracts (contractsDir, (</>))

test_FeeCalculation :: IO TestTree
test_FeeCalculation = pure $ whenNetworkEnabled $ \withEnv ->
  testGroup "Compare morley-client fee calculation with `mavkit-client`"
    [ testCase "single transfer in a batch has the same fee" $ withEnv \env -> do
      compareTransferFeeCalculation env $ one $ trivialTransfer
    , testCase "multiple transfers in a batch have the same fee" $ withEnv \env -> do
      compareTransferFeeCalculation env $ NE.fromList $ replicate 5 trivialTransfer
    , testCase "origination has the same fee" $ withEnv \env -> do
      soBigContract <- importContract @(ToT ()) @(ToT ()) $ contractsDir </> "so_big.mv"
      compareOriginationFeeCalculation env $ OriginationData
        { odAliasBehavior = OverwriteDuplicateAlias
        , odName = "so_big"
        , odBalance = zeroMumav
        , odContract = soBigContract
        , odStorage = toVal ()
        , odMbFee = Nothing
        , odDelegate = Nothing
        }
    , testCase "reveal has the same fee" $ withEnv \env -> do
      compareRevealFeeCalculation env
    , testCase "call to non-implicit contract has the same fee" $ withEnv \env -> do
      compareContractCallFeeCalculation env
    ]
  where
    trivialTransfer = TransactionData $ TD
      { tdReceiver = Constrained genesisAddress
      , tdAmount = 100
      , tdEpName = DefEpName
      , tdParam = toVal ()
      , tdMbFee = Nothing
      }

compareTransferFeeCalculation
  :: NetworkEnv -> NonEmpty TransactionData -> Assertion
compareTransferFeeCalculation env transferBatch = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  runMorleyClientM (neMorleyClientEnv env) $ revealKeyUnlessRevealed moneybagAddr
  (appliedResults, feesMorleyClient) <- fmap (unzip . toList) $ runMorleyClientM (neMorleyClientEnv env) $
    dryRunOperationsNonEmpty moneybagAddr (map OpTransfer transferBatch)
  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feesMavrykClient <- runMorleyClientM (neMorleyClientEnv env) $ calcTransferFee
    (AddressResolved $ awaAddress moneybagAddr) Nothing (computeStorageLimit appliedResults pp)
    (map transactionDataToCalcTransferFeeData $ toList transferBatch)
  feesMorleyClient @?= feesMavrykClient
  where
    transactionDataToCalcTransferFeeData :: TransactionData -> CalcTransferFeeData
    transactionDataToCalcTransferFeeData
      (TransactionData TD{tdReceiver = Constrained tdReceiver, ..})
      = CalcTransferFeeData
          { ctfdTo = AddressResolved $ tdReceiver
          , ctfdParam = tdParam
          , ctfdEp = tdEpName
          , ctfdAmount = MavrykMumav tdAmount
          }

compareOriginationFeeCalculation
  :: NetworkEnv -> OriginationData -> Assertion
compareOriginationFeeCalculation env od@OriginationData{..} = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  runMorleyClientM (neMorleyClientEnv env) $ revealKeyUnlessRevealed moneybagAddr
  (appliedResults, feesMorleyClient) <- fmap (unzip . toList) $ runMorleyClientM (neMorleyClientEnv env) $
    dryRunOperationsNonEmpty moneybagAddr (one $ OpOriginate od)
  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feeMavrykClient <- runMorleyClientM (neMorleyClientEnv env) $ calcOriginationFee
    CalcOriginationFeeData
      { cofdFrom = AddressResolved $ awaAddress moneybagAddr
      , cofdBalance = MavrykMumav odBalance
      , cofdMbFromPassword = Nothing
      , cofdContract = odContract
      , cofdStorage = odStorage
      , cofdBurnCap = computeStorageLimit appliedResults pp
      }
  feesMorleyClient @?= [feeMavrykClient]

compareRevealFeeCalculation
  :: NetworkEnv -> Assertion
compareRevealFeeCalculation env = do
  sk <- SecretKeyEd25519 <$> liftIO Ed25519.randomSecretKey
  let pub = toPublic sk
  let addr = mkKeyAddress pub

  awa <- runMorleyClientM (neMorleyClientEnv env) do
    awa <- importKey True "rpc-revealed-key" sk
    Moneybag moneybag <- liftIO $ setupMoneybagAddress env
    void $ transfer moneybag addr [mv|1 milli|] DefEpName (toVal ()) Nothing
    return awa

  (appliedResults, feesRPC) :| [] <- runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) do
    let rd = RevealData{ rdPublicKey = pub, rdMbFee = Nothing }
    dryRunOperationsNonEmpty awa (one $ OpReveal rd)

  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feesMavrykClient <- runMorleyClientM (neMorleyClientEnv env) $ calcRevealFee
    (awaAlias awa) Nothing (computeStorageLimit [appliedResults] pp)

  feesRPC @?= feesMavrykClient

compareContractCallFeeCalculation
  :: NetworkEnv -> Assertion
compareContractCallFeeCalculation env = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  contractAddr <- runMorleyClientM (neMorleyClientEnv env) $ do
    add1Contract <- liftIO $ importContract @(ToT Integer) @(ToT Integer) $

      contractsDir </> "mavryk_examples" </> "attic" </> "add1.mv"
    snd <$> originateContract OverwriteDuplicateAlias "add1" moneybagAddr
      zeroMumav add1Contract (toVal @Integer 1) Nothing Nothing
  compareTransferFeeCalculation env $ one $ TransactionData $ TD
      { tdReceiver = Constrained contractAddr
      , tdAmount = 0
      , tdEpName = DefEpName
      , tdParam = toVal @Integer 1
      , tdMbFee = Nothing
      }
