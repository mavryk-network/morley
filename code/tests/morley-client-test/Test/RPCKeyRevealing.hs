-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on key revealing performed via RPC.
module Test.RPCKeyRevealing
  ( test_rpcKeyRevealing
  ) where

import Crypto.Random (getRandomBytes)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client.Action qualified as WithMavrykClient
import Morley.Client.Action.Reveal
import Morley.Client.Full qualified as WithMavrykClient
import Morley.Client.OnlyRPC
import Morley.Client.RPC.Getters as WithMavrykClient
import Morley.Client.MavrykClient.Impl qualified as WithMavrykClientImpl
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Mavryk.Address
import Morley.Mavryk.Core
import Morley.Mavryk.Crypto
import Test.Cleveland
import Test.Cleveland.Internal.Abstract (Moneybag(Moneybag))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty

test_rpcKeyRevealing :: TestTree
test_rpcKeyRevealing =
  whenNetworkEnabled $ \withEnv ->
  testGroup "Key revealing via RPC"
  [ testCase "Can reveal a new key" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
      let addr = mkKeyAddress pub

      awa <- WithMavrykClient.runMorleyClientM (neMorleyClientEnv env) $ do
        awa <- WithMavrykClientImpl.importKey True "rpc-revealed-key" sk
        void $ WithMavrykClient.transfer moneybag addr [mv|1 milli|] U.DefEpName T.VUnit Nothing
        pure awa

      mManager <- runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) $ do
        void $ revealKey awa
        getManagerKey addr

      mManager @?= Just pub

  , testCase "revealKeyUnlessRevealed works" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
      let addr = mkKeyAddress pub

      awa <- WithMavrykClient.runMorleyClientM (neMorleyClientEnv env) $ do
        awa <- WithMavrykClientImpl.importKey True "rpc-revealed-key" sk
        void $ WithMavrykClient.transfer moneybag addr [mv|1 milli|] U.DefEpName T.VUnit Nothing
        pure awa

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) $ do
        void $ revealKey awa
        void $ revealKeyUnlessRevealed awa
  ]
