-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE FunctionalDependencies #-}

module Test.AddressOrAlias.Common
  ( setup
  , sr1Address
  , someOption
  , contractOption
  , implicitOption
  , mkShouldSucceed
  , mkParserShouldFail
  , mkResolveBothShouldThrow
  , mkResolveShouldThrow
  , pAlias
  , pAddress
  ) where

import Crypto.Random (getRandomBytes)
import Fmt (Buildable)
import Options.Applicative qualified as Opt
import Test.Tasty.HUnit (assertFailure, (@?=))

import Morley.CLI (addressOrAliasOption, someAddressOrAliasOption)
import Morley.Client
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Util.Interpolate
import Morley.Util.Named
import Test.Cleveland ()

sr1Address :: SmartRollupAddress
sr1Address = [ta|sr1RYurGZtN8KNSpkMcCt9CgWeUaNkzsAfXf|]

mkParser
  :: HasCallStack
  => Opt.Parser a
  -> String
  -> Either String a
mkParser optionParser input =
  case Opt.execParserPure (Opt.prefs mempty) (Opt.info optionParser mempty) ["--aoa", input] of
    Opt.Success aoa -> Right aoa
    Opt.Failure failure ->
      -- Mimic the behavior of `Opt.handleParseResult`, but without exiting.
      let (errorMsg, _exit) = Opt.renderFailure failure "program-name"
      in  Left errorMsg
    Opt.CompletionInvoked _ -> error "unexpected CompletionInvoked"

tryDisplay :: MonadCatch m => m a -> m (Either String a)
tryDisplay action =
  first displayException <$> tryAny action

someOption :: Opt.Parser SomeAddressOrAlias
someOption =
  someAddressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

contractOption :: Opt.Parser ContractAddressOrAlias
contractOption =
  addressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

implicitOption :: Opt.Parser ImplicitAddressOrAlias
implicitOption =
  addressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

data GettableAlias
data GettableAddress

pAlias :: Proxy GettableAlias
pAlias = Proxy

pAddress :: Proxy GettableAddress
pAddress = Proxy

class Gettable ty addressOrAlias res | ty addressOrAlias -> res where
  getMaybe :: addressOrAlias -> MorleyClientM (Maybe res)
  getThrow :: addressOrAlias -> MorleyClientM res

instance (Resolve addressOrAlias, res ~ ResolvedAddress addressOrAlias)
  => Gettable GettableAddress addressOrAlias res where
  getMaybe = resolveAddressMaybe
  getThrow = resolveAddress

instance (Resolve addressOrAlias, res ~ ResolvedAlias addressOrAlias)
  => Gettable GettableAlias addressOrAlias res where
  getMaybe = getAliasMaybe
  getThrow = getAlias

mkShouldSucceed
  :: (HasCallStack, Gettable ty addressOrAlias res, Eq res, Show res)
  => Proxy ty
  -> Opt.Parser addressOrAlias
  -> String
  -> res
  -> MorleyClientM ()
mkShouldSucceed (_ :: Proxy ty) optionParser input expectedAddr = do
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getMaybe @ty aoa
      liftIO $ result1 @?= Right (Just expectedAddr)

      result2 <- tryDisplay $ getThrow @ty aoa
      liftIO $ result2 @?= Right expectedAddr

-- | Expect the address/alias parser to fail.
mkParserShouldFail
  :: (HasCallStack, Buildable addressOrAlias)
  => Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkParserShouldFail optionParser input expectedErrorMsg =
  liftIO $
    case mkParser optionParser input of
      Right aoa -> assertFailure [itu|Expected parser to fail, but it succeeded with: #{aoa}|]
      Left err -> err @?= expectedErrorMsg

-- | Expect `resolveAddress` to throw, and `resolveAddressMaybe` to return `Nothing`.
mkResolveShouldThrow
  :: (HasCallStack, Gettable ty addressOrAlias res, Eq res, Show res)
  => Proxy ty
  -> Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkResolveShouldThrow (_ :: Proxy ty) optionParser input expectedErrorMsg =
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getMaybe @ty aoa
      liftIO $ result1 @?= Right Nothing

      result2 <- tryDisplay $ getThrow @ty aoa
      liftIO $ result2 @?= Left expectedErrorMsg

-- | Expect both `resolveAddress` and `resolveAddressMaybe` to throw.
mkResolveBothShouldThrow
  :: (HasCallStack, Gettable ty addressOrAlias res, Eq res, Show res)
  => Proxy ty
  -> Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkResolveBothShouldThrow (_ :: Proxy ty) optionParser input expectedErrorMsg =
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getMaybe @ty aoa
      liftIO $ result1 @?= Left expectedErrorMsg

      result2 <- tryDisplay $ getThrow @ty aoa
      liftIO $ result2 @?= Left expectedErrorMsg

-- | Creates 3 aliases:
--
-- * `implicit-alias`, associated with an implicit address.
-- * `contract-alias`, associated with an contract address.
-- * `ambiguous-alias`, associated with both an implicit address and a contract address.
setup :: MorleyClientM (ImplicitAddress, ContractAddress, ImplicitAddress, ContractAddress)
setup = do
  implicitAddr <- genKey "implicit-alias"
  contractAddr <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16
  rememberContract
    OverwriteDuplicateAlias
    contractAddr
    "contract-alias"

  contractAmbiguousAddr <- liftIO $ ContractAddress . mkContractHashHack <$> getRandomBytes 16
  implicitAmbiguousAddr <- genKey "ambiguous-alias"
  rememberContract
    OverwriteDuplicateAlias
    contractAmbiguousAddr
    "ambiguous-alias"

  pure (awaAddress implicitAddr, contractAddr, awaAddress implicitAmbiguousAddr, contractAmbiguousAddr)
