-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.AddressOrAlias.ResolveAlias
  ( test_parse_and_resolve_ContractAddressOrAlias
  , test_parse_and_resolve_ImplicitAddressOrAlias
  , test_parse_and_resolve_SomeAddressOrAlias
  ) where

import Fmt (pretty)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Client
import Morley.Mavryk.Address.Alias
import Morley.Util.Interpolate
import Test.Cleveland (NetworkEnv(..))
import Test.Cleveland.Tasty (whenNetworkEnabled)

import Test.AddressOrAlias.Common

contractAlias, contractAmbiguousAlias :: ContractAlias
implicitAlias, implicitAmbiguousAlias :: ImplicitAlias
contractAlias = "contract-alias"
implicitAlias = "implicit-alias"
implicitAmbiguousAlias = "ambiguous-alias"
contractAmbiguousAlias = "ambiguous-alias"

test_parse_and_resolve_ContractAddressOrAlias :: TestTree
test_parse_and_resolve_ContractAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve ContractAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, _, _) <- setup

          -- Parse and resolve addresses
          pretty contractAddr `shouldSucceed` contractAlias
          pretty implicitAddr `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: '#{implicitAddr}'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          pretty sr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected smart rollup address: #{sr1Address}

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "contract-alias" `shouldSucceed` contractAlias

          --   * with prefix
          "implicit:implicit-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:implicit-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:contract-alias" `shouldSucceed` contractAlias

          --   * with the wrong prefix
          "contract:implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "implicit:contract-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:contract-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `shouldSucceed` contractAmbiguousAlias

          --   * with prefix
          "implicit:ambiguous-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:ambiguous-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:ambiguous-alias" `shouldSucceed` contractAmbiguousAlias

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'contract:invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:invalid-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'contract:invalid-alias'.|]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye'.|]
  where
    shouldSucceed :: HasCallStack => String -> ContractAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed pAlias contractOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail contractOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow pAlias contractOption

test_parse_and_resolve_ImplicitAddressOrAlias :: TestTree
test_parse_and_resolve_ImplicitAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve ImplicitAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, _, _) <- setup

          -- Parse and resolve addresses
          pretty implicitAddr `shouldSucceed` implicitAlias
          pretty contractAddr `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: '#{contractAddr}'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]
          pretty sr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected smart rollup address: #{sr1Address}

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `shouldSucceed` implicitAlias
          "contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          --   * with prefix
          "implicit:implicit-alias" `shouldSucceed` implicitAlias
          "contract:contract-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:contract-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          --   * with the wrong prefix
          "contract:implicit-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:implicit-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]
          "implicit:contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `shouldSucceed` implicitAmbiguousAlias

          --   * with prefix
          "implicit:ambiguous-alias" `shouldSucceed` implicitAmbiguousAlias
          "contract:ambiguous-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:ambiguous-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'implicit:invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'implicit:invalid-alias'.|]
          "contract:invalid-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:invalid-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ'.|]
  where
    shouldSucceed :: HasCallStack => String -> ImplicitAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed pAlias implicitOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail implicitOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow pAlias implicitOption

test_parse_and_resolve_SomeAddressOrAlias :: TestTree
test_parse_and_resolve_SomeAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve SomeAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, implicitAmbiguousAddr, contractAmbiguousAddr) <- setup

          -- Parse and resolve addresses
          pretty implicitAddr `shouldSucceed` SomeAlias implicitAlias
          pretty contractAddr `shouldSucceed` SomeAlias contractAlias
          pretty sr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected smart rollup address: #{sr1Address}

            Usage: program-name --aoa CONTRACT OR IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `shouldSucceed` SomeAlias implicitAlias
          "contract-alias" `shouldSucceed` SomeAlias contractAlias

          --   * with prefix
          "implicit:implicit-alias" `shouldSucceed` SomeAlias implicitAlias
          "contract:contract-alias" `shouldSucceed` SomeAlias contractAlias

          --   * with the wrong prefix
          "contract:implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "implicit:contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `resolveBothShouldThrow`
            [itu|
            The alias 'ambiguous-alias' is assigned to:
              * a contract address: #{contractAmbiguousAddr}
              * an implicit address: #{implicitAmbiguousAddr}
            Use 'contract:ambiguous-alias' or 'implicit:ambiguous-alias' to disambiguate.
            |]

          --   * with prefix
          "implicit:ambiguous-alias" `shouldSucceed` SomeAlias implicitAmbiguousAlias
          "contract:ambiguous-alias" `shouldSucceed` SomeAlias contractAmbiguousAlias

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'implicit:invalid-alias'.|]
          "contract:invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'contract:invalid-alias'.|]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ'.|]
          "KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye'.|]
  where
    shouldSucceed :: HasCallStack => String -> SomeAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed pAlias someOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail someOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow pAlias someOption

    resolveBothShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveBothShouldThrow = mkResolveBothShouldThrow pAlias someOption
