-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests register delegate performed via RPC.
module Test.RPCDelegation
  ( test_rpcRegisterDelegate
  ) where

import Crypto.Random (getRandomBytes)
import Fmt (pretty)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client (getDelegate, runMorleyOnlyRpcM)
import Morley.Client.Action as WithMorleyClient
import Morley.Client.Action.Batched
import Morley.Client.Full qualified as WithMavrykClient
import Morley.Client.MavrykClient.Impl qualified as WithMavrykClientImpl
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Mavryk.Address
import Morley.Mavryk.Core
import Morley.Mavryk.Crypto
import Test.Cleveland hiding (getDelegate)
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Client
import Test.Cleveland.Tasty

test_rpcRegisterDelegate :: TestTree
test_rpcRegisterDelegate = whenNetworkEnabled $ \withEnv ->
  testGroup "Delegations via RPC" $ runTests withEnv <$>
    -- BLS keys can't be delegates
    filter (/= KeyTypeBLS) [minBound..]

runTests :: ((NetworkEnv -> IO ()) -> IO ()) -> KeyType -> TestTree
runTests withEnv keyType = testGroup (pretty keyType) $
  [ testCase "Can (self) register as delegate" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey' keyType <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
          addr = mkKeyAddress pub
          awa = AddressWithAlias addr "rpc-delegate-key"

      -- NB: running everything in one only-rpc group is about twice as fast as
      -- splitting into multiple interleaved client and only-rpc groups, so we
      -- use this hack.
      skMoneybag <- WithMavrykClient.runMorleyClientM (neMorleyClientEnv env) $ do
        WithMavrykClientImpl.getSecretKey (awaAlias moneybag)

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk, skMoneybag]) $ do
        void $ WithMorleyClient.transfer moneybag addr [mv|1 milli|] U.DefEpName T.VUnit Nothing
        void $ WithMorleyClient.registerDelegateOp awa
        delegate <- getDelegate (toL1Address addr)
        liftIO $ ImplicitAddress <$> delegate @?= Just addr

  , testCase "Can set/unset a delegate" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env

      skAlice <- detSecretKey' keyType <$> liftIO (getRandomBytes 16)
      let pubAlice = toPublic skAlice
          addrAlice@(ImplicitAddress keyHashAlice) = mkKeyAddress pubAlice
          awaAlice = AddressWithAlias addrAlice "alice-key"

      skBob <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pubBob = toPublic skBob
          addrBob = mkKeyAddress pubBob
          awaBob = AddressWithAlias addrBob "bob-key"

      skMoneybag <- WithMavrykClient.runMorleyClientM (neMorleyClientEnv env) $ do
        WithMavrykClientImpl.getSecretKey (awaAlias moneybag)

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [skAlice, skBob, skMoneybag]) $ do
        runOperationsBatch moneybag do
          runTransactionM $ TransactionData $ TD (toL1Address addrAlice)
            [mv|1 milli|] U.DefEpName T.VUnit Nothing
          runTransactionM $ TransactionData $ TD (toL1Address addrBob)
            [mv|5 milli|] U.DefEpName T.VUnit Nothing
          pure ()
        void $ WithMorleyClient.registerDelegateOp awaAlice
        void $ WithMorleyClient.setDelegateOp awaBob (Just keyHashAlice)
        bobsDelegate1 <- getDelegate (toL1Address addrBob)
        liftIO $ ImplicitAddress <$> bobsDelegate1 @?= Just addrAlice
        void $ WithMorleyClient.setDelegateOp awaBob Nothing
        bobsDelegate2 <- getDelegate (toL1Address addrBob)
        liftIO $ ImplicitAddress <$> bobsDelegate2 @?= Nothing
  ]
