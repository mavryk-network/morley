-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.WastefulOrigination (test_originateContract) where

import Debug qualified
import Lorentz qualified as L

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure, testCase)

import Morley.Client
import Morley.Mavryk.Address.Alias (Alias(..))
import Test.Cleveland.Internal.Abstract (Moneybag(..), neMorleyClientEnv)
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty (whenNetworkEnabled)

test_originateContract :: IO TestTree
test_originateContract = pure $ whenNetworkEnabled $ \withEnv ->
  testCase "originateContract" $ withEnv \env -> do
    Moneybag moneybagAddr <- setupMoneybagAddress env
    runMorleyClientM (neMorleyClientEnv env) do
      void $ lOriginateContract OverwriteDuplicateAlias
        (ContractAlias "some-contract") moneybagAddr L.zeroMumav
        ctr () Nothing Nothing -- this ensures the alias in question already exists
      void $ lOriginateContract ForbidDuplicateAlias
        (ContractAlias "some-contract") moneybagAddr L.zeroMumav
        ctr () Nothing Nothing -- this should fail before originating the contract
    assertFailure "We should have failed due to the duplicate alias."
    `catch` \case
      DuplicateAlias "some-contract" -> pass
      x -> assertFailure $ "Expected DuplicateAlias \"some-contract\", but got " <> Debug.show x
    where
      ctr :: L.Contract () () ()
      ctr = L.defaultContract $ L.drop L.# L.unit L.# L.nil @L.Operation L.# L.pair
