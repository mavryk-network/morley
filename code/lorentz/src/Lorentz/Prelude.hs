-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Commonly used parts of regular Prelude.
module Lorentz.Prelude
  ( ($)
  , (.)
  , (&)
  , type ($)
  , Eq
  , Ord
  , Bounded (..)
  , Semigroup (..)
  , Monoid (..)
  , Generic
  , Text
  , Either (..)
  , Maybe (..)
  , Proxy (..)
  , fromString
  , undefined
  , error
  , (!)
  , type (~)
  ) where

import Named ((!))
