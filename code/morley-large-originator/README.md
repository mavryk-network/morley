> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Mavryk mainnet (June 24th, 2023).

# morley-large-originator

This package contains a simple tool to originate contracts that are too large
to fit into the origination limit, by using a multi-step process.

IMPORTANT: this tool is still experimental, use it at your own risk.

## Installation

You can build and install `morley-large-originator` using [stack](https://haskellstack.org/):
```
stack install morley-large-originator
```

or you can download the binary from the latest
[`master` CI pipeline artifacts](https://gitlab.com/morley-framework/morley/-/pipelines?scope=branches&ref=master)
and put it somewhere in your `PATH`.

## Usage

You can consult the help page by calling
```
morley-large-originator --help
```
which will also explain to you what commands are available and how to get more
`help` about them specifically.

The tool currently has the ability to:
1. run all the steps for you using [morley-client](../morley-client)
2. save all the steps to file, letting you pick your favourite mavryk client

In both cases it will need the code and storage of the contract to originate as
well as some info about the origination parameters (e.g. balance and `from` address).

## How it works

The tool is based on a variation of the workaround described in
[this discussion](https://gitlab.com/mavryk-network/mavryk-protocol/-/issues/1053#note_481537115).

In short, while there is a limit on the size of an origination operation, there
is no limit in the size of a contract originated by another with the
`CREATE_CONTRACT` instruction.

So, to leverage this fact, we create and deploy an "originator" contract and a
lambda to be executed in it such that:
- the "originator" is very small and starts with a tiny `storage`
- the lambda is `PACK`ed and split into multiple chunks that can be `transfer`ed,
  each without exceeding operation limits
- the "originator" can receive a chunk at a time and store it to rebuild the lambda
- once the lambda is fully re-constructed, the "originator" can `UNPACK` it and
  run it, creating the large contract that we wanted

There is a single major difference compared to the details described in the
discussion linked above, which is that not only there is no way to `PUSH` the
large contract's storage in the lambda (which is just not allowed), but there is
also no way to build the values fully with instructions because `EMPTY_BIG_MAP`s
can't be used here due to a [known bug](https://gitlab.com/mavryk-network/mavryk-protocol/-/issues/1154).

What this means is that the "originator" code depends both on the large contract's
code and initial storage, because it may have to contain some `big_map` values.
