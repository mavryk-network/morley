-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import GHC.IO.Encoding (setFileSystemEncoding)
import Options.Applicative qualified as Opt
import Options.Applicative.Help.Pretty (Doc, line)
import System.IO (utf8)

import Morley.Client.Parser
import Morley.Client.Util
import Morley.Util.Main (wrapMain)
import Morley.Util.Named

main :: IO ()
main = wrapMain $ do
  -- grepcake: the following line is needed to parse CL arguments (argv) in
  -- utf-8. It might be necessary to add the similar line to other
  -- executables. However, I've filed the issue for `with-utf8`
  -- (https://github.com/serokell/haskell-with-utf8/issues/8). If it gets fixed
  -- in upstream, this line should be safe to remove. In that case, FIXME.
  setFileSystemEncoding utf8

  disableAlphanetWarning
  join $ Opt.execParser $ parserInfo
    ! #usage usageDoc
    ! #description "Morley Client: RPC client for interaction with mavryk node"
    ! #header "Morley Client"
    ! #parser clientParser

usageDoc :: Doc
usageDoc = mconcat
  [ "You can use help for specific COMMAND", line
  , "EXAMPLE:", line
  , "morley-client originate --help", line
  , line
  , "Documentation for morley tools can be found at the following links:", line
  , "  https://gitlab.com/morley-framework/morley/blob/master/README.md", line
  , "  https://gitlab.com/morley-framework/morley/tree/master/docs", line
  , line
  , "Sample contracts for running can be found at the following link:", line
  , "  https://gitlab.com/morley-framework/morley/tree/master/contracts", line
  , line
  , "USAGE EXAMPLE:", line
  , "morley-client -E florence.testnet.tezos.serokell.team:8732 originate \\", line
  , "  --from mv1UC6DvFYUynK2ktDqa7gCbzaPRRtaaVXeA \\", line
  , "  --contract ../contracts/mavryk_examples/attic/add1.mv --initial-balance 1 --initial-storage 0", line
  , line
  , "  This command will originate contract with code stored in add1.mv", line
  , "  on real network with initial balance 1 and initial storage set to 0", line
  , "  and return info about operation: operation hash and originated contract address", line
  , line
  , "morley-client -E florence.testnet.tezos.serokell.team:8732 transfer \\", line
  , "  --from mv1UC6DvFYUynK2ktDqa7gCbzaPRRtaaVXeA \\", line
  , "  --to KT1USbmjj6P2oJ54UM6HxBZgpoPtdiRSVABW --amount 1 --parameter 0", line
  , line
  , "  This command will perform tranfer to contract with address on real network", line
  , "  KT1USbmjj6P2oJ54UM6HxBZgpoPtdiRSVABW with amount 1 and parameter 0", line
  , "  as a result it will return operation hash"
  ]
