-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Morley client that connects with real Mavryk network through RPC and
-- the @mavkit-client@ binary. For more information please refer to README.
module Morley.Client
  ( -- * Command line parser
    parserInfo
  , clientConfigParser

  -- * Full client monad and environment
  , MorleyClientM
  , MorleyClientConfig (..)
  , MorleyClientEnv (..)
  , runMorleyClientM
  , mkMorleyClientEnv
  -- ** Lens
  , mceMavrykClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL

  -- * Only-RPC client monad and environment
  , MorleyOnlyRpcM (..)
  , MorleyOnlyRpcEnv (..)
  , mkMorleyOnlyRpcEnv
  , runMorleyOnlyRpcM

  -- * High-level actions
  , module Morley.Client.Action

  -- * RPC
  , BlockId (..)
  , HasMavrykRpc (..)
  , OperationInfo (..)
  , OperationInfoDescriptor (..)
  , RPCInput
  , getContract
  , getImplicitContractCounter
  , getContractStorage
  , getBigMapValue
  , getHeadBlock
  , getCounter
  , getProtocolParameters
  , runOperation
  , preApplyOperations
  , forgeOperation
  , getContractScript
  , getContractBigMap
  , getBalance
  , getDelegate
  , runCode
  , getManagerKey
  , getTicketBalance
  , getAllTicketBalances
  , GetTicketBalance (..)
  , GetAllTicketBalancesResponse (..)

  -- ** Errors
  , ClientRpcError (..)
  , ClientRpcErrorWithStack (..)
  , UnexpectedErrors (..)
  , IncorrectRpcResponse (..)
  , RunError (..)
  -- ** Getters
  , ValueDecodeFailure (..)
  , ValueNotFound (..)
  , readAllBigMapValues
  , readAllBigMapValuesMaybe
  , readContractBigMapValue
  , readBigMapValueMaybe
  , readBigMapValue

  -- * @mavkit-client@
  , HasMavrykClient (..)
  , Resolve(ResolvedAddress, ResolvedAlias)
  , ResolveError(..)
  , getAlias
  , getAliasMaybe
  , resolveAddress
  , resolveAddressMaybe
  , resolveAddressWithAlias
  , resolveAddressWithAliasMaybe
  , MavrykClientError (..)
  , AliasBehavior (..)

  -- * Util
  , disableAlphanetWarning
  , AddressWithAlias(..)
  , ImplicitAddressWithAlias
  , ContractAddressWithAlias

  -- * Reexports
  , Opt.ParserInfo -- Needed for tests
  ) where

import Options.Applicative qualified as Opt

import Morley.Client.Action
import Morley.Client.Full
import Morley.Client.OnlyRPC
import Morley.Client.Parser
import Morley.Client.RPC
import Morley.Client.MavrykClient
import Morley.Client.Types
import Morley.Client.Util
