-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Client.Types
  ( ToJSONObject
  , OperationInfoDescriptor (..)
  , OperationInfo (..)
  , AddressWithAlias(..)
  , ImplicitAddressWithAlias
  , ContractAddressWithAlias
  , _OpTransfer
  , _OpOriginate
  , _OpReveal
  , _OpDelegation
  , _OpTransferTicket
  ) where

import Control.Lens (makePrisms)
import Data.Aeson (ToJSON(..))
import Fmt (Buildable(..), (+|), (|+))

import Lorentz (ToAddress(..), ToTAddress(..))
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Mavryk.Address.Kinds

-- | Designates types whose 'ToJSON' instance produces only 'Data.Aeson.Object's.
class ToJSON a => ToJSONObject a

class OperationInfoDescriptor (i :: Type) where
  type family TransferInfo i :: Type
  type family TransferTicketInfo i :: Type
  type family OriginationInfo i :: Type
  type family RevealInfo i :: Type
  type family DelegationInfo i :: Type

data OperationInfo i
  = OpTransfer (TransferInfo i)
  | OpTransferTicket (TransferTicketInfo i)
  | OpOriginate (OriginationInfo i)
  | OpReveal (RevealInfo i)
  | OpDelegation (DelegationInfo i)

-- Requiring 'ToJSONObject' in superclass as those different types of operation
-- must be distinguishable and that is usually done by a special field
instance
  Each '[ToJSONObject]
    [TransferInfo i, TransferTicketInfo i, OriginationInfo i, RevealInfo i, DelegationInfo i] =>
  ToJSON (OperationInfo i) where
  toJSON = \case
    OpTransfer op -> toJSON op
    OpTransferTicket op -> toJSON op
    OpOriginate op -> toJSON op
    OpReveal op -> toJSON op
    OpDelegation op -> toJSON op
instance ToJSON (OperationInfo i) => ToJSONObject (OperationInfo i)

makePrisms ''OperationInfo

{- | A kinded address together with the corresponding alias. Due to the quirks
of @mavkit-client@ we usually need both in @morley-client@, hence the need for
this type. Historically, we asked @mavkit-client@ for the missing part, but that
resulted in a lot of inefficiencies.

Note that the 'Ord' instance is rather arbitrary, and doesn't necessarily
correspond to anything in Michelson. It's added for convenience, e.g. so that
'AddressWithAlias' is usable as a 'Map' key.
-}
data AddressWithAlias kind = AddressWithAlias
  { awaAddress :: KindedAddress kind
  , awaAlias :: Alias kind
  }
  deriving stock (Show, Eq, Ord)

instance ToAddress (AddressWithAlias kind) where
  toAddress = toAddress . awaAddress

instance ToTAddress cp vd (KindedAddress kind) => ToTAddress cp vd (AddressWithAlias kind) where
  toTAddress = toTAddress . awaAddress

instance Buildable (AddressWithAlias kind) where
  build (AddressWithAlias addr alias) = addr |+ " (" +| alias |+ ")"

type ImplicitAddressWithAlias = AddressWithAlias 'AddressKindImplicit
type ContractAddressWithAlias = AddressWithAlias 'AddressKindContract
