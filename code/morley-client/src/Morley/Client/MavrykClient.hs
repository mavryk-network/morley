-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Interface to @mavkit-client@ (and its implementation).

module Morley.Client.MavrykClient
  ( module Morley.Client.MavrykClient.Class
  , module Morley.Client.MavrykClient.Types
  , MavrykClientError (..)
  , AliasBehavior (..)
  , Resolve(ResolvedAddress, ResolvedAlias)
  , ResolveError(..)
  , resolveAddress
  , resolveAddressMaybe
  , resolveAddressWithAlias
  , resolveAddressWithAliasMaybe
  , getAlias
  , getAliasMaybe
  ) where

import Morley.Client.MavrykClient.Class
import Morley.Client.MavrykClient.Impl
import Morley.Client.MavrykClient.Types
