-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Client.Action.SizeCalculation
  ( computeUntypedContractSize
  , computeContractSize
  ) where

import Morley.Client.RPC
import Morley.Micheline.Class (ToExpression(..))
import Morley.Michelson.Typed (Contract, Value)
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Untyped qualified as U

computeUntypedContractSize :: HasMavrykRpc m => U.Contract -> U.Value -> m Natural
computeUntypedContractSize ct st = do
  pps <- getProtocolParameters
  let csProgram = toExpression ct
      csStorage = toExpression st
      csGas     = ppHardGasLimitPerOperation pps
      csLegacy  = False
  ScriptSize size <- getScriptSize CalcSize{..}
  return size

computeContractSize
  :: forall m cp st.
     ( HasMavrykRpc m
     , StorageScope st
     )
  => Contract cp st
  -> Value st
  -> m Natural
computeContractSize ct st = do
  pps <- getProtocolParameters
  let csProgram = toExpression ct
      csStorage = toExpression st
      csGas     = ppHardGasLimitPerOperation pps
      csLegacy  = False
  ScriptSize size <- getScriptSize CalcSize{..}
  return size
