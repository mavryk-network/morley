-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Container for address-to-alias and vice versa translation.
module Morley.Client.Types.AliasesAndAddresses
  ( AliasesAndAddresses
  , lookupAddr
  , lookupAlias
  , mkAliasesAndAddresses
  , insertAliasAndAddress
  , emptyAliasesAndAddresses
  ) where

import Data.Bimap (Bimap)
import Data.Bimap qualified as Bimap
import Data.Constraint ((\\))

import Morley.Client.Types
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Util.Constrained
import Morley.Util.Sing (castSing)

newtype AliasesAndAddresses = AliasesAndAddresses
  { unAliasesAndAddresses :: Bimap SomeAlias Address }
  -- Invariant: address kind matches between address and alias.

lookupAlias :: KindedAddress kind -> AliasesAndAddresses -> Maybe (Alias kind)
lookupAlias addr as = do
  Constrained alias <- Bimap.lookupR (Constrained addr) (unAliasesAndAddresses as)
  castSing alias \\ aliasKindSanity alias \\ addressKindSanity addr

lookupAddr :: Alias kind -> AliasesAndAddresses -> Maybe (KindedAddress kind)
lookupAddr alias as = do
  Constrained addr <- Bimap.lookup (Constrained alias) (unAliasesAndAddresses as)
  castSing addr \\ aliasKindSanity alias \\ addressKindSanity addr

mkAliasesAndAddresses :: [Constrained NullConstraint AddressWithAlias] -> AliasesAndAddresses
mkAliasesAndAddresses = AliasesAndAddresses
  . Bimap.fromList
  . map (\(Constrained AddressWithAlias{..}) -> (SomeAlias awaAlias, Constrained awaAddress))

insertAliasAndAddress
  :: Alias kind
  -> KindedAddress kind
  -> AliasesAndAddresses
  -> AliasesAndAddresses
insertAliasAndAddress alias addr
  = AliasesAndAddresses
  . Bimap.insert (Constrained alias) (Constrained addr)
  . unAliasesAndAddresses

emptyAliasesAndAddresses :: AliasesAndAddresses
emptyAliasesAndAddresses = AliasesAndAddresses Bimap.empty
