-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Full-featured Morley client, backed by @mavkit-client@.
module Morley.Client.MavrykClient.Types.MorleyClientM
  ( MorleyClientEnv(..)
  , MorleyClientConfig (..)
  , MorleyClientM
  , runMorleyClientM
  , mkMorleyClientEnv
  , mkLogAction
  -- * Lens
  , mceMavrykClientL
  , mceLogActionL
  , mceSecretKeyL
  , mceClientEnvL
  , mccEndpointUrlL
  , mccMavrykClientPathL
  , mccMbMavrykClientDataDirL
  , mccVerbosityL
  , mccSecretKeyL
  ) where

import Colog (HasLog(..), Message)
import Network.HTTP.Types (Status(..))
import Servant.Client (ClientEnv)
import Servant.Client.Core (Request, Response, RunClient(..))
import System.Environment (lookupEnv)
import UnliftIO (MonadUnliftIO)

import Morley.Client.App
import Morley.Client.Init
import Morley.Client.Logging (ClientLogAction)
import Morley.Client.RPC.Class
import Morley.Client.RPC.HttpClient
import Morley.Client.MavrykClient.Config
import Morley.Client.MavrykClient.Types
import Morley.Mavryk.Crypto.Ed25519 qualified as Ed25519
import Morley.Util.Lens (makeLensesWith, postfixLFields)

-- | Runtime environment for morley client.
data MorleyClientEnv = MorleyClientEnv
  { mceMavrykClient :: MavrykClientEnv
  -- ^ Environment for @mavkit-client@.
  , mceLogAction :: ClientLogAction MorleyClientM
  -- ^ Action used to log messages.
  , mceSecretKey :: Maybe Ed25519.SecretKey
  -- ^ Pass if you want to sign operations manually or leave it
  -- to @mavkit-client@.
  , mceClientEnv :: ClientEnv
  -- ^ Environment necessary to make HTTP calls.
  }

newtype MorleyClientM a = MorleyClientM
  { unMorleyClientM :: ReaderT MorleyClientEnv IO a }
  deriving newtype
    ( Functor, Applicative, Monad, MonadReader MorleyClientEnv
    , MonadIO, MonadThrow, MonadCatch, MonadMask, MonadUnliftIO
    )

-- | Run 'MorleyClientM' action within given t'MorleyClientEnv'. Retry action
-- in case of invalid counter error.
runMorleyClientM :: MorleyClientEnv -> MorleyClientM a -> IO a
runMorleyClientM env client = runReaderT (unMorleyClientM client) env

makeLensesWith postfixLFields ''MorleyClientEnv

instance HasMavrykClientEnv MorleyClientEnv where
  mavrykClientEnvL = mceMavrykClientL

instance HasLog MorleyClientEnv Message MorleyClientM where
  getLogAction = mceLogAction
  setLogAction action mce = mce { mceLogAction = action }

instance RunClient MorleyClientM where
  runRequestAcceptStatus :: Maybe [Status] -> Request -> MorleyClientM Response
  runRequestAcceptStatus statuses req = do
    env <- mceClientEnv <$> ask
    runRequestAcceptStatusImpl env statuses req
  throwClientError = throwClientErrorImpl

instance HasMavrykRpc MorleyClientM where
  getBlockHash = getBlockHashImpl
  getCounterAtBlock = getCounterImpl
  getBlockHeader = getBlockHeaderImpl
  getBlockConstants = getBlockConstantsImpl
  getBlockOperations = getBlockOperationsImpl
  getScriptSizeAtBlock = getScriptSizeAtBlockImpl
  getBlockOperationHashes = getBlockOperationHashesImpl
  getProtocolParametersAtBlock = getProtocolParametersImpl
  runOperationAtBlock = runOperationImpl
  preApplyOperationsAtBlock = preApplyOperationsImpl
  forgeOperationAtBlock = forgeOperationImpl
  injectOperation = injectOperationImpl
  getContractScriptAtBlock = getContractScriptImpl
  getContractStorageAtBlock = getContractStorageAtBlockImpl
  getContractBigMapAtBlock = getContractBigMapImpl
  getBigMapValueAtBlock = getBigMapValueAtBlockImpl
  getBigMapValuesAtBlock = getBigMapValuesAtBlockImpl
  getBalanceAtBlock = getBalanceImpl
  getDelegateAtBlock = getDelegateImpl
  runCodeAtBlock = runCodeImpl
  getChainId = getChainIdImpl
  getManagerKeyAtBlock = getManagerKeyImpl
  waitForOperation = (asks mceClientEnv >>=) . waitForOperationImpl
  getTicketBalanceAtBlock = getTicketBalanceAtBlockImpl
  getAllTicketBalancesAtBlock = getAllTicketBalancesAtBlockImpl
  packData = packDataImpl

-- | Construct 'MorleyClientEnv'.
--
-- * @mavkit-client@ path is taken from 'MorleyClientConfig', but can be
-- overridden using @MORLEY_MAVRYK_CLIENT@ environment variable.
-- * Node data is taken from @mavkit-client@ config and can be overridden
-- by 'MorleyClientConfig'.
-- * The rest is taken from 'MorleyClientConfig' as is.
mkMorleyClientEnv :: MorleyClientConfig -> IO MorleyClientEnv
mkMorleyClientEnv MorleyClientConfig{..} = do
  envMavrykClientPath <- lookupEnv "MORLEY_MAVRYK_CLIENT"
  let mavrykClientPath = fromMaybe mccMavrykClientPath envMavrykClientPath
  MavrykClientConfig {..} <- getMavrykClientConfig mavrykClientPath mccMbMavrykClientDataDir
  tceAliasMap <- newMVar Nothing
  let
    endpointUrl = fromMaybe tcEndpointUrl mccEndpointUrl
    mavrykClientEnv = MavrykClientEnv
      { tceEndpointUrl = endpointUrl
      , tceMavrykClientPath = mavrykClientPath
      , tceMbMavrykClientDataDir = mccMbMavrykClientDataDir
      , tceAliasMap
      }

  clientEnv <- newClientEnv endpointUrl
  pure MorleyClientEnv
    { mceMavrykClient = mavrykClientEnv
    , mceLogAction = mkLogAction mccVerbosity
    , mceSecretKey = mccSecretKey
    , mceClientEnv = clientEnv
    }
