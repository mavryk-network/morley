-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | @mavkit-client@ config.
module Morley.Client.MavrykClient.Config
  ( MavrykClientConfig (..)

  -- * @mavkit-client@ api
  , getMavrykClientConfig
  ) where

import Data.Aeson (FromJSON(..), eitherDecodeStrict, withObject, (.:))
import Servant.Client (BaseUrl(..))
import System.Exit (ExitCode(..))

import Morley.Client.MavrykClient.Helpers
import Morley.Client.MavrykClient.Types.Errors

-- | Configuration maintained by @mavkit-client@, see its @config@ subcommands
-- (e. g. @mavkit-client config show@).
-- Only the field we are interested in is present here.
newtype MavrykClientConfig = MavrykClientConfig { tcEndpointUrl :: BaseUrl }
  deriving stock Show

-- | For reading @mavkit-client@ config.
instance FromJSON MavrykClientConfig where
  parseJSON = withObject "node info" $ \o -> MavrykClientConfig <$> o .: "endpoint"

-- | Read @mavkit-client@ configuration.
getMavrykClientConfig :: FilePath -> Maybe FilePath -> IO MavrykClientConfig
getMavrykClientConfig client mbDataDir = do
  t <- readProcessWithExitCode' client
    (maybe [] (\dir -> ["-d", dir]) mbDataDir ++  ["config", "show"]) ""
  case t of
    (ExitSuccess, toText -> output, _) -> case eitherDecodeStrict . encodeUtf8 . toText $ output of
        Right config -> pure config
        Left err -> throwM $ ConfigParseError err
    (ExitFailure errCode, toText -> output, toText -> errOutput) ->
      throwM $ UnexpectedClientFailure errCode output errOutput
