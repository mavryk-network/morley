-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Interface to the @mavkit-client@ executable expressed in Haskell types.
module Morley.Client.MavrykClient.Impl
  ( MavrykClientError (..)

  -- * @mavkit-client@ api
  , signBytes
  , rememberContract
  , importKey
  , genKey
  , genFreshKey
  , revealKey
  , revealKeyUnlessRevealed
  , ResolveError(..)
  , Resolve(..)
  , resolveAddress
  , resolveAddressMaybe
  , getAlias
  , getAliasMaybe
  , getPublicKey
  , getSecretKey
  , getMavrykClientConfig
  , calcTransferFee
  , calcOriginationFee
  , calcRevealFee
  , getKeyPassword
  , registerDelegate
  , getAliasesAndAddresses
  , resolveAddressWithAlias
  , resolveAddressWithAliasMaybe

    -- * For tests
  , lookupAliasCache
  ) where

import Data.Aeson (encode)
import Data.ByteArray (ScrubbedBytes)
import Data.ByteString.Lazy.Char8 qualified as C (unpack)
import Data.Constraint ((\\))
import Data.Text qualified as T
import Fmt (pretty, (+|), (|+))
import Text.Printf (printf)
import UnliftIO.IO (hGetEcho, hSetEcho)

import Lorentz.Value
import Morley.Client.App
import Morley.Client.Logging
import Morley.Client.RPC.Getters (getManagerKey)
import Morley.Client.MavrykClient.Class qualified as Class
import Morley.Client.MavrykClient.Config
import Morley.Client.MavrykClient.Helpers
import Morley.Client.MavrykClient.Parser
import Morley.Client.MavrykClient.Resolve
import Morley.Client.MavrykClient.Types
import Morley.Client.MavrykClient.Types.Errors
import Morley.Client.MavrykClient.Types.MorleyClientM
import Morley.Client.Types
import Morley.Client.Types.AliasesAndAddresses
import Morley.Client.Util (readScrubbedBytes)
import Morley.Micheline
import Morley.Michelson.Typed.Scope
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Mavryk.Crypto
import Morley.Mavryk.Crypto.Ed25519 qualified as Ed25519
import Morley.Util.Constrained
import Morley.Util.Peano
import Morley.Util.SizedList.Types

-- Note: if we try to sign with an unknown alias, @mavkit-client@ will
-- report a fatal error (assert failure) to stdout. It's bad. It's
-- reported in two issues: https://gitlab.com/mavryk-network/mavryk-protocol/-/issues/653
-- and https://gitlab.com/mavryk-network/mavryk-protocol/-/issues/813.
-- I (@gromak) currently think it's better to wait for it to be resolved upstream.
-- Currently we will throw 'MavrykClientUnexpectedOutputFormat' error.
-- | Sign an arbtrary bytestring using @mavkit-client@.
-- Secret key of the address corresponding to give 'AddressOrAlias' must be known.
signBytes
  :: ImplicitAlias
  -> Maybe ScrubbedBytes
  -> ByteString
  -> MorleyClientM Signature
signBytes signerAlias mbPassword opHash = do
  logDebug $ "Signing for " <> pretty signerAlias
  output <- callMavrykClientStrict
    ["sign", "bytes", toCmdArg opHash, "for", toCmdArg signerAlias] MockupMode mbPassword
  liftIO case T.stripPrefix "Signature: " output of
    Nothing ->
      -- There is additional noise in the stdout in case key is password protected
      case T.stripPrefix "Enter password for encrypted key: Signature: " output of
        Nothing -> throwM $ MavrykClientUnexpectedSignatureOutput output
        Just signatureTxt -> txtToSignature signatureTxt
    Just signatureTxt -> txtToSignature signatureTxt
  where
    txtToSignature :: MonadCatch m => Text -> m Signature
    txtToSignature signatureTxt = either (throwM . MavrykClientCryptoParseError signatureTxt) pure $
      parseSignature . T.strip $ signatureTxt

-- | Generate a new secret key and save it with given alias.
-- If an address with given alias already exists, it will be returned
-- and no state will be changed.
genKey :: ImplicitAlias -> MorleyClientM ImplicitAddress
genKey name = do
  lookupAliasCache name >>= \case
    Just addr -> pure addr
    Nothing -> do
      let
        isAlreadyExistsError :: Text -> Bool
        -- We can do a bit better here using more complex parsing if necessary.
        isAlreadyExistsError = T.isInfixOf "already exists."

        errHandler _ errOut = pure (isAlreadyExistsError errOut)

      _ <-
        callMavrykClient errHandler
        ["gen", "keys", toCmdArg name] MockupMode Nothing
      invalidateAliasCache
      resolveAddress name

-- | Generate a new secret key and save it with given alias.
-- If an address with given alias already exists, it will be removed
-- and replaced with a fresh one.
genFreshKey :: ImplicitAlias -> MorleyClientM ImplicitAddress
genFreshKey name = do
  let
    isNoAliasError :: Text -> Bool
    -- We can do a bit better here using more complex parsing if necessary.
    isNoAliasError = T.isInfixOf "no public key hash alias named"

    errHandler _ errOutput = pure (isNoAliasError errOutput)

  _ <-
    callMavrykClient errHandler
    ["forget", "address", toCmdArg name, "--force"] MockupMode Nothing
  callMavrykClientStrict ["gen", "keys", toCmdArg name] MockupMode Nothing
  invalidateAliasCache
  resolveAddress name

-- | Reveal public key corresponding to the given alias.
-- Fails if it's already revealed.
revealKey :: ImplicitAlias -> Maybe ScrubbedBytes -> MorleyClientM ()
revealKey alias mbPassword = do
  logDebug $ "Revealing key for " +| alias |+ ""
  let
    alreadyRevealed = T.isInfixOf "previously revealed"
    revealedImplicitAccount = T.isInfixOf "only implicit accounts can be revealed"
    emptyImplicitContract = T.isInfixOf "Empty implicit contract"
    errHandler _ errOut =
      False <$ do
        when (alreadyRevealed errOut) (throwM (AlreadyRevealed alias))
        when (revealedImplicitAccount errOut) (throwM (CantRevealContract alias))
        when (emptyImplicitContract errOut) (throwM (EmptyImplicitContract alias))

  _ <-
    callMavrykClient errHandler
    ["reveal", "key", "for", toCmdArg alias] ClientMode mbPassword

  logDebug $ "Successfully revealed key for " +| alias |+ ""

-- | Reveal key for implicit address if necessary.
revealKeyUnlessRevealed :: ImplicitAddressWithAlias -> Maybe ScrubbedBytes -> MorleyClientM ()
revealKeyUnlessRevealed (AddressWithAlias addr alias) mbPassword = do
  mbManagerKey <- getManagerKey addr
  case mbManagerKey of
    Nothing -> revealKey alias mbPassword
    Just _  -> logDebug $ alias |+ " alias has already revealed key"

-- | Register alias as delegate
registerDelegate :: ImplicitAlias -> Maybe ScrubbedBytes -> MorleyClientM ()
registerDelegate alias mbPassword = do
  logDebug $ "Registering " +| alias |+ " as delegate"
  let
    emptyImplicitContract = T.isInfixOf "Empty implicit contract"
    errHandler _ errOut =
      False <$ do
        when (emptyImplicitContract errOut) (throwM (EmptyImplicitContract alias))

  _ <-
    callMavrykClient errHandler
    ["register", "key", toCmdArg alias, "as", "delegate"] ClientMode mbPassword

  logDebug $ "Successfully registered " +| alias |+ " as delegate"

-- | Call @mavkit-client@ to list known addresses or contracts
callListKnown :: String -> MorleyClientM Text
callListKnown objects =
  callMavrykClientStrict ["list", "known", objects] MockupMode Nothing

-- | Return 'PublicKey' corresponding to given 'AddressOrAlias'.
getPublicKey
  :: ImplicitAlias
  -> MorleyClientM PublicKey
getPublicKey alias = do
  logDebug $ "Getting " +| alias |+ " public key"
  output <- callMavrykClientStrict ["show", "address", toCmdArg alias] MockupMode Nothing
  liftIO case lines output of
    _ : [rawPK] -> do
      pkText <- maybe
        (throwM $ MavrykClientUnexpectedOutputFormat rawPK) pure
        (T.stripPrefix "Public Key: " rawPK)
      either (throwM . MavrykClientCryptoParseError pkText) pure $
        parsePublicKey pkText
    _ -> throwM $ MavrykClientUnexpectedOutputFormat output

-- | Return 'SecretKey' corresponding to given 'AddressOrAlias'.
getSecretKey
  :: ImplicitAlias
  -> MorleyClientM SecretKey
getSecretKey alias = do
  logDebug $ "Getting " +| alias |+ " secret key"
  output <- callMavrykClientStrict ["show", "address", toCmdArg alias, "--show-secret"] MockupMode Nothing
  liftIO case lines output of
    _ : _ : [rawSK] -> do
      skText <- maybe
        (throwM $ MavrykClientUnexpectedOutputFormat rawSK) pure
        (T.stripPrefix "Secret Key: " rawSK)
      either (throwM . MavrykClientCryptoParseError skText) pure $
        parseSecretKey skText
    _ -> throwM $ MavrykClientUnexpectedOutputFormat output

-- | Save a contract with given address and alias.
-- If @replaceExisting@ is @False@ and a contract with given alias
-- already exists, this function does nothing.
rememberContract
  :: AliasBehavior
  -> ContractAddress
  -> ContractAlias
  -> MorleyClientM ()
rememberContract aliasBehavior address alias = do
  case aliasBehavior of
    DontSaveAlias ->
      logInfo $ "Not saving " +| address |+ " as " +| alias |+ " as requested"
    OverwriteDuplicateAlias -> do
      void $ callMavrykClientStrict (args <> ["--force"]) MockupMode Nothing
      tryUpdateAliasCache alias address
    _ -> do
      lookupAliasCache alias >>= \case
        Nothing -> do
          let errHandler _ errOut
                | isAlreadyExistsError errOut = case aliasBehavior of
                    KeepDuplicateAlias -> pure True
                    ForbidDuplicateAlias -> throwM $ DuplicateAlias $ unAlias alias
                | otherwise = pure False
          void $ callMavrykClient errHandler args MockupMode Nothing
          tryUpdateAliasCache alias address
        Just{} -> case aliasBehavior of
          KeepDuplicateAlias -> pass
          ForbidDuplicateAlias -> throwM $ DuplicateAlias $ unAlias alias
  where
    args = ["remember", "contract", toCmdArg alias, pretty address]
    isAlreadyExistsError = T.isInfixOf "already exists"

importKey
  :: Bool
  -> ImplicitAlias
  -> SecretKey
  -> MorleyClientM ImplicitAddressWithAlias
importKey replaceExisting name key = do
  let isAlreadyExistsError = T.isInfixOf "already exists"
      errHandler _ errOut
        | isAlreadyExistsError errOut = throwM $ DuplicateAlias $ unAlias name
        | otherwise = pure False
      args = ["import", "secret", "key", toCmdArg name, toCmdArg key]
  void $ callMavrykClient errHandler
    (if replaceExisting then args <> ["--force"] else args)
    MockupMode Nothing
  let addr = mkKeyAddress (toPublic key)
  tryUpdateAliasCache name addr
  pure $ AddressWithAlias addr name

-- | Calc baker fee for transfer using @mavkit-client@.
calcTransferFee
  :: AddressOrAlias kind
  -> Maybe ScrubbedBytes
  -> MavrykInt64
  -> [CalcTransferFeeData]
  -> MorleyClientM [MavrykMumav]
calcTransferFee from mbPassword burnCap transferFeeDatas = do
  output <- callMavrykClientStrict
    [ "multiple", "transfers", "from", pretty from, "using"
    , C.unpack $ encode transferFeeDatas, "--burn-cap", showBurnCap burnCap, "--dry-run"
    ] ClientMode mbPassword
  withSomePeano (length transferFeeDatas) $
    \(_ :: Proxy n) -> toList <$> feeOutputParser @n output

-- | Calc baker fee for origination using @mavkit-client@.
calcOriginationFee :: UntypedValScope st => CalcOriginationFeeData cp st -> MorleyClientM MavrykMumav
calcOriginationFee CalcOriginationFeeData{..} = do
  output <- callMavrykClientStrict
    [ "originate", "contract", "-", "transferring"
    , showTez cofdBalance
    , "from", pretty cofdFrom, "running"
    , toCmdArg cofdContract, "--init"
    , toCmdArg cofdStorage, "--burn-cap"
    , showBurnCap cofdBurnCap, "--dry-run"
    ] ClientMode cofdMbFromPassword
  fees <- feeOutputParser @1 output
  case fees of
    singleFee :< Nil -> return singleFee

-- | Calc baker fee for revealing using @mavkit-client@.
--
-- Note that @mavkit-client@ does not support passing an address here,
-- at least at the moment of writing.
calcRevealFee :: ImplicitAlias -> Maybe ScrubbedBytes -> MavrykInt64 -> MorleyClientM MavrykMumav
calcRevealFee alias mbPassword burnCap = do
  output <- callMavrykClientStrict
    [ "reveal", "key", "for", toCmdArg alias
    , "--burn-cap", showBurnCap burnCap
    , "--dry-run"
    ] ClientMode mbPassword
  fees <- feeOutputParser @1 output
  case fees of
    singleFee :< Nil -> return singleFee

feeOutputParser :: forall n. (SingIPeano n) => Text -> MorleyClientM (SizedList n MavrykMumav)
feeOutputParser output =
  case parseBakerFeeFromOutput @n output of
    Right fee -> return fee
    Left err -> throwM $ MavrykClientParseFeeError output $ pretty err

showBurnCap :: MavrykInt64 -> String
showBurnCap x = printf "%.6f" $ (fromIntegralToRealFrac @MavrykInt64 @Float x) / 1000

showTez :: MavrykMumav -> String
showTez = toCmdArg . unMavrykMumav

-- | Get password for secret key associated with given address
-- in case this key is password-protected
getKeyPassword :: ImplicitAlias -> MorleyClientM (Maybe ScrubbedBytes)
getKeyPassword alias = do
  output <- callMavrykClientStrict [ "show", "address", pretty alias, "-S"] MockupMode Nothing
  encryptionType <-
    case parseSecretKeyEncryption output of
      Right t -> return t
      Left err -> throwM $ MavrykClientParseEncryptionTypeError output $ pretty err
  case encryptionType of
    EncryptedKey -> do
      putTextLn $ "Please enter password for '" <> pretty alias <> "':"
      Just <$> withoutEcho readScrubbedBytes
    _ -> pure Nothing

  where
    -- Hide entered password
    withoutEcho :: (MonadIO m, MonadMask m) => m a -> m a
    withoutEcho action = do
      old <- hGetEcho stdin
      bracket_ (hSetEcho stdin False) (hSetEcho stdin old) action

{- | Calls @mavkit-client list known contracts@ and returns
a list of @(alias, address)@ pairs.

Note that an alias can be ambiguous: it can refer to __both__ a contract and an implicit account.
When an alias "abc" is ambiguous, the list will contain two entries:

> ("abc", "KT1...")
> ("key:abc", "mv1...")
-}
getAliasesAndAddresses :: MorleyClientM AliasesAndAddresses
getAliasesAndAddresses = do
  env <- ask
  let mv = env ^. mceMavrykClientL . tceAliasMapL
  updateMVar' mv $ get >>= \case
    Nothing -> do
      res <- lift $ parseOutput =<< runMorleyClientM env (callListKnown "contracts")
      put $ Just res
      pure res
    Just a -> pure a
  where
    parseOutput :: Text -> IO AliasesAndAddresses
    parseOutput out = mkAliasesAndAddresses <$> mapM parseLine (lines out)

    -- Note: each line has the format "<alias>: <address>"
    parseLine :: Text -> IO (Constrained NullConstraint AddressWithAlias)
    parseLine ln = do
      let (aliasText', addressText) = first (T.dropEnd 2) $ T.breakOnEnd ": " ln
          -- This alias _might_ belong to both an implicit account and a contract,
          -- in which case it might be prefixed with "key".
          -- If so, we have to strip the prefix.
          aliasText = fromMaybe aliasText' $ T.stripPrefix "key:" aliasText'
      Constrained awaAddress <- parseL1Address addressText
      let awaAlias = mkAlias aliasText \\ addressKindSanity awaAddress
      pure . Constrained $ AddressWithAlias{..}

    parseL1Address :: Text -> IO L1Address
    parseL1Address addrText
      = either (throwM . MavrykClientParseAddressError addrText) pure
      . parseConstrainedAddress $ addrText

invalidateAliasCache :: MorleyClientM ()
invalidateAliasCache = do
  mv <- view $ mavrykClientEnvL . tceAliasMapL
  updateMVar' mv $ put Nothing

tryUpdateAliasCache :: Alias kind -> KindedAddress kind -> MorleyClientM ()
tryUpdateAliasCache alias addr = do
  mv <- view $ mavrykClientEnvL . tceAliasMapL
  updateMVar' mv $ modify' $ fmap $ insertAliasAndAddress alias addr

lookupAliasCache :: Alias kind -> MorleyClientM (Maybe (KindedAddress kind))
lookupAliasCache alias = do
  mv <- view $ mavrykClientEnvL . tceAliasMapL
  (lookupAddr alias =<<) <$> readMVar mv

instance Class.HasMavrykClient MorleyClientM where
  signBytes (AddressWithAlias _ senderAlias) mbPassword opHash = retryOnceOnTimeout $ do
    env <- ask
    case mceSecretKey env of
      Just sk -> pure . SignatureEd25519 $ Ed25519.sign sk opHash
      Nothing -> signBytes senderAlias mbPassword opHash
  rememberContract = failOnTimeout ... rememberContract
  getAliasesAndAddresses = retryOnceOnTimeout ... getAliasesAndAddresses
  genKey alias = flip AddressWithAlias alias <$> genKey alias
  genFreshKey alias = flip AddressWithAlias alias <$> genFreshKey alias
  getKeyPassword (AddressWithAlias _ alias) = retryOnceOnTimeout $ getKeyPassword alias
  getPublicKey (AddressWithAlias _ alias) = getPublicKey alias
