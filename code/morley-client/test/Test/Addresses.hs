-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Addresses
  ( contractAddress1
  , contractAddress2
  , contractAddress3
  ) where

import Morley.Mavryk.Address

contractAddress1, contractAddress2, contractAddress3 :: ContractAddress
contractAddress1 = ContractAddress $ mkContractHashHack "contractAddress1"
contractAddress2 = ContractAddress $ mkContractHashHack "contractAddress2"
contractAddress3 = ContractAddress $ mkContractHashHack "contractAddress3"
