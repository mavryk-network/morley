-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Test.ErrorJSONParser
  ( test_RunErrorJSONParser
  , test_InternalErrorJSONParser
  ) where

import Data.Aeson (eitherDecodeStrict)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client.RPC.Types
import Morley.Mavryk.Address (ta)
import Morley.Util.Interpolate (i, itu)

deriving stock instance Eq RunError
deriving stock instance Eq InternalError

test_RunErrorJSONParser :: TestTree
test_RunErrorJSONParser = testCase "RunError JSON parser is strict in error names" do
  eitherDecodeStrict @RunError [i|{"id": "some.prefix.delegate.already_active"}|]
    @?= Right DelegateAlreadyActive
  eitherDecodeStrict @RunError [i|{"id": "some.prefix_delegate.already_active"}|]
    @?= Left "Error in $: unknown id: some.prefix_delegate.already_active"

test_InternalErrorJSONParser :: TestTree
test_InternalErrorJSONParser = testCase "InternalError JSON parser is strict in error names" do
  eitherDecodeStrict @InternalError [itu|
      { "id": "some.prefix.unrevealed_key"
      , "contract": "mv1GcRgT8RwriXQHotHSaEHcwFGsV3mHPc3z"
      }
      |]
    @?= Right (UnrevealedKey [ta|mv1GcRgT8RwriXQHotHSaEHcwFGsV3mHPc3z|])
  eitherDecodeStrict @InternalError [itu|
      { "id": "some.prefix_unrevealed_key"
      , "contract": "mv1GcRgT8RwriXQHotHSaEHcwFGsV3mHPc3z"
      }
      |]
    @?= Left "Error in $: unknown id: some.prefix_unrevealed_key"
