-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Parser
  ( test_bakerFeeParser
  , test_secretKeyEncriptionParser
  ) where

import Test.Hspec.Expectations (shouldBe, shouldSatisfy)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Client.MavrykClient.Parser
import Morley.Client.MavrykClient.Types (SecretKeyEncryption(..))
import Morley.Micheline
import Morley.Mavryk.Core
import Morley.Util.SizedList qualified as SL

test_bakerFeeParser :: TestTree
test_bakerFeeParser = testGroup "`mavkit-client` baker fee parser"
  [ testCase "resulted fee is multiplied by 1e6" $
      parseBakerFeeFromOutput @1 "Fee to the baker: ꜩ0.056008\n"
        `shouldBe` Right (one $ MavrykMumav [mv|56008u|])
  , testCase "multiples fees can be parsed" $
      parseBakerFeeFromOutput @2 "Fee to the baker: ꜩ0.056008\n Fee to the baker: ꜩ0.056008\n"
        `shouldBe`  Right (SL.replicateT @2 $ MavrykMumav [mv|56008u|])
  , testCase "no valid baker fee in the output" $
      parseBakerFeeFromOutput @1 "Notfee to the baker: ꜩ0.056008\n"
        `shouldSatisfy` isLeft
  ]

test_secretKeyEncriptionParser :: TestTree
test_secretKeyEncriptionParser = testGroup "`mavkit-client` baker fee parser"
  [ testCase "unencrypted type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: unencrypted:edsk4TybjbpfhcQ81R5FnxkoZy14ZyXRUzfbXrmPgqKRpcGPJanAgY" `shouldBe`
    Right UnencryptedKey
  , testCase "encrypted type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: encrypted:edesk1a9cTRUMXf6L5cZXMqojELRRfTEoLXc4JzT9B3PqnhWweJnuWgcSk93NBHXYGFxf1uKHFPihBgVzJNwUVHR"
    `shouldBe`
    Right EncryptedKey
  , testCase "ledger type can be parsed" $
    parseSecretKeyEncryption
    "Secret Key: ledger://live-fossa-eager-walrus/bip25519/0h/0h" `shouldBe`
    Right LedgerKey
  , testCase "nonsense cannot be parsed 1" $
    parseSecretKeyEncryption
    "Secret Key: kek://live-fossa-eager-walrus/bip25519/0h/0h" `shouldSatisfy`
    isLeft
  , testCase "nonsense cannot be parsed 2" $
    parseSecretKeyEncryption
    "Not a secret Key: ledger://live-fossa-eager-walrus/bip25519/0h/0h" `shouldSatisfy`
    isLeft
  ]
