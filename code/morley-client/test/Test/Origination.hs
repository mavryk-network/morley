-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Origination
  ( test_lRunTransactionsUnit
  ) where

import Test.Hspec.Expectations (shouldThrow)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz qualified as L
import Morley.Client
import Morley.Mavryk.Core
import Test.Addresses
import Test.Util
import TestM

fakeState
  :: FakeState
fakeState = defaultFakeState
  { fsContracts = fromList $ one $ (contractAddress2, dumbContractState)
  , fsImplicits = fromList $ one $ genesisState @1
  }

dumbLorentzContract :: L.Contract Integer () ()
dumbLorentzContract = L.defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Mock test transaction sending"
  [ testCase "Successful origination" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $
      lOriginateContract OverwriteDuplicateAlias "dummy" addr1 [mv|100500u|]
      dumbLorentzContract () Nothing Nothing
  , testCase "Originator doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lOriginateContract OverwriteDuplicateAlias "dummy" addr3 [mv|100500u|]
      dumbLorentzContract () Nothing Nothing
  ]
  where
    addr1 = addrAndAliasFromGenesisState @1
    addr3 = addrAndAliasFromGenesisState @3

    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Origination unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        UnknownAccount{} -> True
        _ -> False
