-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Test.Errors
  ( test_handleOperationResult
  ) where

import Debug qualified

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure, testCase, (@?=))

import Morley.Client.Action.Common
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Mavryk.Core
import Morley.Util.Named

deriving stock instance Eq RunError

test_handleOperationResult :: TestTree
test_handleOperationResult = testCase "getAppliedResults #709 regression test" do
  let res = RunOperationResult
        { rrOperationContents =
            OperationContent (OtherOpResp "fake")
              (RunMetadata
                { rmOperationResult = OperationFailed []
                , rmInternalOperationResults =
                    [ InternalOperation {ioData = OtherOpResp "fake", ioResult = OperationFailed []}
                    , InternalOperation {ioData = OtherOpResp "fake", ioResult = OperationFailed []}
                    ]
                })
              :|
              [ OperationContent (OtherOpResp "fake")
                (RunMetadata
                  { rmOperationResult = OperationFailed
                      [ CantPayStorageFee
                      , BalanceTooLow (#balance :! [mv|149.38m|]) (#required :! [mv|355m|])
                      ]
                  , rmInternalOperationResults =
                      [ InternalOperation {ioData = OtherOpResp "fake", ioResult = OperationFailed []}
                      , InternalOperation {ioData = OtherOpResp "fake", ioResult = OperationFailed []}
                      ]
                  })
              , OperationContent (OtherOpResp "fake")
                  RunMetadata { rmOperationResult = OperationFailed []
                              , rmInternalOperationResults = []}
              ]
        }
  handleOperationResult @(Either SomeException) res 3 & \case
    Right _ -> assertFailure "Expected result to fail, but it succeeded."
    Left err -> case fromException err of
      Just (crewsError -> UnexpectedRunErrors e) ->
        e @?=
          [ CantPayStorageFee
          , BalanceTooLow (#balance :! [mv|149.38m|]) (#required :! [mv|355m|])
          ]
      _ -> assertFailure $ "Expected failure to be UnexpectedRunErrors, but it was " <> Debug.show err
