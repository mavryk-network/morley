-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Transaction
  ( test_lRunTransactionsUnit
  ) where

import Test.Hspec.Expectations (shouldThrow)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Client.Action.Transaction
import Morley.Michelson.Runtime.GState (genesisAddress3)
import Morley.Michelson.Untyped.Entrypoints
import Morley.Mavryk.Core (mv)
import Test.Addresses
import Test.Util
import TestM

fakeState
  :: FakeState
fakeState = defaultFakeState
  { fsContracts = fromList $ one (contractAddress2, dumbContractState)
  , fsImplicits = fromList $ one $ genesisState @1
  }

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Fake test transaction sending"
  [ testCase "Successful transaction" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer addr1 contractAddress2
        [mv|10u|] DefEpName () Nothing
  , testCase "Sender doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer addr3 contractAddress2
        [mv|10u|] DefEpName () Nothing
  , testCase "Destination doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer addr1 genesisAddress3
        [mv|10u|] DefEpName () Nothing
  ]
  where
    addr1 = addrAndAliasFromGenesisState @1
    addr3 = addrAndAliasFromGenesisState @3
    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Transaction sending unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        UnknownAccount{} -> True
        _ -> False
