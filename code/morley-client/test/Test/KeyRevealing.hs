-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.KeyRevealing
  ( test_keyRevealing
  ) where

import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz qualified as L
import Morley.Client
import Morley.Michelson.Runtime.GState (genesisAddress1)
import Morley.Michelson.Typed
import Morley.Mavryk.Core (mv)
import Test.Util
import TestM

fakeState :: FakeState
fakeState = defaultFakeState
  { fsImplicits = one $ second revealKeyState $ genesisState @1
  }

test_keyRevealing :: TestTree
test_keyRevealing = testGroup "Fake test key revealing"
  [ testCase "Manager key for new address is revealed only once for transfer" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $ do
      senderAddress <- genKey "sender"
      dummyTransfer addr1 $ awaAddress senderAddress
      mbManagerKey <- getManagerKey $ awaAddress senderAddress

      when (isJust mbManagerKey) $
        fail "Manager key was expected not to be revealed, but it's revealed."

      dummyTransfer senderAddress genesisAddress1
      mbManagerKey' <- getManagerKey $ awaAddress senderAddress

      when (isNothing mbManagerKey') $
        fail "Manager key was expected to be revealed, but it's not revealed."

      local
        (const noRevealHandlers)
        (dummyTransfer senderAddress genesisAddress1)

  , testCase "Manager key for new address is revealed only once for origination" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $ do
      originatorAddress <- genKey "originator"
      dummyTransfer addr1 $ awaAddress originatorAddress

      mbManagerKey <- getManagerKey $ awaAddress originatorAddress

      when (isJust mbManagerKey) $
        fail "Manager key was expected not to be revealed, but it's revealed."

      originateDummy originatorAddress

      mbManagerKey' <- getManagerKey $ awaAddress originatorAddress
      when (isNothing mbManagerKey') $
        fail "Manager key was expected to be revealed, but it's not revealed."

      local
        (const noRevealHandlers)
        (originateDummy originatorAddress)
  ]
  where
    addr1 = addrAndAliasFromGenesisState @1
    dummyTransfer from to =
      void $ transfer from to [mv|10u|] DefEpName (toVal ()) Nothing

    originateDummy addr =
      lOriginateContract OverwriteDuplicateAlias "dummy" addr [mv|10u|]
      dumbLorentzContract () Nothing Nothing

-- | Handlers which don't allow to reveal key.
noRevealHandlers :: (Monad m) => TestHandlers m
noRevealHandlers = TestHandlers $ chainOperationHandlers
  { hGetPublicKey = \_ -> throwM $ UnexpectedClientCall "getPublicKey" }

dumbLorentzContract :: L.Contract Integer () ()
dumbLorentzContract = L.defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

handleSuccess :: Either SomeException a -> Assertion
handleSuccess (Left err) = assertFailure $ displayException err
handleSuccess (Right _)  = pass
