<!-- Unreleased: append new entries here -->


0.4.0
=====
* [!1366](https://gitlab.com/morley-framework/morley/-/merge_requests/1366)
  Split instance `Resolve (AddressOrAlias kind)` into two
  + Now, to call `resolveAddress` on an alias, wrapping it in `AddressAlias` is
    optional
  + Ditto for `resolveAlias`, address and `AddressResolved`
  + `AmbiguousAlias` error is slightly generalized
  + `AmbiguousAlias` moved from `MavrykClientError` to `ResolveError`
  + `resolveAddressEither` thus doesn't throw anymore, returning `Left
    AmbiguousAlias` instead.
* [!1364](https://gitlab.com/morley-framework/morley/-/merge_requests/1364)
  Update to LTS-21.4 (GHC 9.4.5)

0.3.2
=====
* [!1350](https://gitlab.com/morley-framework/morley/-/merge_requests/1350)
  Add caching of address/alias mappings to morley-client
* [!1357](https://gitlab.com/morley-framework/morley/-/merge_requests/1357)
  Better tests for `PACK`
  + Add `pack_data` RPC endpoint support
* [!1361](https://gitlab.com/morley-framework/morley/-/merge_requests/1361)
  Add an `Ord` instance for `AddressWithAlias`
* [!1345](https://gitlab.com/morley-framework/morley/-/merge_requests/1345)
  Add RPC error response logs
* [!1342](https://gitlab.com/morley-framework/morley/-/merge_requests/1342)
  Replace fmt with prettyprinter
  + Improve formatting for operations and errors
* [!1323](https://gitlab.com/morley-framework/morley/-/merge_requests/1323)
  Trace called contracts on error
  + Reduce duplication of RPC types and parse more responses
  + Trace internal operations in morley-client
* [!1335](https://gitlab.com/morley-framework/morley/-/merge_requests/1335)
  Include morley CLI commands with morley-client
* [!1340](https://gitlab.com/morley-framework/morley/-/merge_requests/1340)
  Remove deprecated types and functions
  + `revealKeyOp`
  + `revealKeyUnlessRevealedOp`

0.3.1
=====
* [!1331](https://gitlab.com/morley-framework/morley/-/merge_requests/1331)
  Support implicit account tickets
  + New `transfer-ticket` CLI command;
  + New operation type, `OpTransferTicket`;
  + Add `ToTAddress` instance for `AddressWithAlias`;
* [!1330](https://gitlab.com/morley-framework/morley/-/merge_requests/1330)
  Support ticket balance queries
* [!1328](https://gitlab.com/morley-framework/morley/-/merge_requests/1328)
  Kill support for TORUs, minimal sr1 address support, mv4 address support
* [!1329](https://gitlab.com/morley-framework/morley/-/merge_requests/1329)
  Fix gas limit computation
  + Fixes a bug where in some cases batched transfer could exceed hard gas
    limit.
* [!1313](https://gitlab.com/morley-framework/morley/-/merge_requests/1313)
  Add `contractStateResolver` getter to build Morley interpreter's
  `ContractState` from chain.
* [!1315](https://gitlab.com/morley-framework/morley/-/merge_requests/1315)
  Avoid redundant calls to mavkit-client address store
  + Introduce `AddressWithAlias` and ways to resolve it (i.e.
    `resolveAddressWithAlias`/`-Either`/`-Maybe`)
  + Make RPC revelation and delegation more efficient, deprecate less efficient
   `revealKeyOp` and `revealKeyUnlessRevealedOp`
  + Use `AddressWithAlias` in critical code paths and key generators
  + Add batched delegation
* [!1304](https://gitlab.com/morley-framework/morley/-/merge_requests/1304)
  Relax lOriginateLargeContract constraints
  + Now requires `NiceParameter` like other origination functions instead of
    `NiceParameterFull`
* [!1286](https://gitlab.com/morley-framework/morley/-/merge_requests/1286)
  Use RPC revealing in `runOperationsNonEmptyHelper`
  + Replace `revealKey` with `getPublicKey` in `HasMavrykClient`.
  + Move `Morley.Client.Action.Common.revealKeyUnlessRevealed` to
    `MavrykClient.Impl`.
  + Implement key revealing in `runOperationsNonEmptyHelper` via prepending the
    reveal operation to the batch.
  + Rename `Morley.Client.Action.Reveal.revealKey*` to `*Op`, since those inject
    raw revelation **op**erations.
  + Introduce utility versions of `Morley.Client.Action.Reveal.revealKey*` that
    get the key using `getPublicKey`.

0.3.0
=====
* [!1281](https://gitlab.com/morley-framework/morley/-/merge_requests/1281)
  Use the new binaries names, `mavkit-client` and `mavkit-node`
* [!1270](https://gitlab.com/morley-framework/morley/-/merge_requests/1270)
  Remove key prefix in reveal operation
* [!1285](https://gitlab.com/morley-framework/morley/-/merge_requests/1285)
  Make revealKey API less confusing
  + Removed redundant `sender` parameter from functions in
    `Morley.Client.Action.Reveal`. They now accept a public key and optionally
    fee instead, since the sender is uniquely determined by the public key.
* [!1287](https://gitlab.com/morley-framework/morley/-/merge_requests/1287)
  Get rid of RpcNoOperationsRun error
  + Introduce `runTransactionsNonEmpty` to `Morley.Client.Action.Transaction`.
  + Remove `RpcNoOperationsRun` constructor from `IncorrectRpcResponse` as it
    can't in fact be thrown.
* [!1266](https://gitlab.com/morley-framework/morley/-/merge_requests/1266)
  Overload `getAlias`
  + Overloaded `getAlias` and `getAliasMaybe`
    to work with `SomeAddressOrAlias`.
  + `getAlias` and `getAliasMaybe` now check that the alias actually exists.
  + Replaced the `findAddress` and `getAliasEither` methods from
    the `HasMavrykClient` typeclass with `getAliasesAndAddresses`.
* [!1259](https://gitlab.com/morley-framework/morley/-/merge_requests/1259)
  More options for handling duplicate alias in morley-client
  + Added new datatype `Morley.Client.AliasBehavior`.
  + Various origination functions and datatypes now use this datatype instead of
    a boolean flag.
  + `DuplicateAlias` error is now part of `MavrykClientError`.
* [!1258](https://gitlab.com/morley-framework/morley/-/merge_requests/1258)
  Support implicit contract delegates and setting delegates during origination
  + Fix a small potential bug in RPC error code decoders.
  + Parse `deleagate.already_active` error
  + Support getting delegates for implicit addresses
  + Support setting contract delegate during origination
* [!1260](https://gitlab.com/morley-framework/morley/-/merge_requests/1260)
  Add `SomeAddressOrAlias`
  + Overloaded `resolveAddress` and `resolveAddressMaybe` to work with
    `SomeAddressOrAlias`.
  + Replaced the `transfer --to-implicit` and `--to-contract` options with a
    single `--to` option.
  + Replaced the `resolveAddressMaybe` method from the `HasMavrykClient`
    typeclass with `findAddress`.
* [!1226](https://gitlab.com/morley-framework/morley/-/merge_requests/1226)
  Implement delegation operation via RPC
* [!1257](https://gitlab.com/morley-framework/morley/-/merge_requests/1257)
  Export `Morley.Client.Action.Common.runErrorsToClientError` utility for
  converting `[RunError]` to `ClientRpcError`
* [!1244](https://gitlab.com/morley-framework/morley/-/merge_requests/1244)
  Smarter initial gas limit estimation
* [!1249](https://gitlab.com/morley-framework/morley/-/merge_requests/1249)
  Add getAliasMaybe and getAliasEither to morley-client
* [!1227](https://gitlab.com/morley-framework/morley/-/merge_requests/1227)
  Add timestamp to morley-client logs
  + Omit source locations from logs when verbosity is `<=2`.
* [!1242](https://gitlab.com/morley-framework/morley/-/merge_requests/1242)
  Use `Constrained` utility existential
* [!1247](https://gitlab.com/morley-framework/morley/-/merge_requests/1247)
  Refactor MorleyClientEnv
  + Removed `MorleyClientEnv'`, constructor now lives in `MorleyClientEnv`
  + Removed `Morley.Client.Env` module
  + `MorleyClientEnv`, `mkMorleyClientEnv` and `MorleyClientEnv` lenses moved to `Morley.Client.Full`
* [!1237](https://gitlab.com/morley-framework/morley/-/merge_requests/1237)
  Handle unregistered_delegate error in morley-client
* [!1215](https://gitlab.com/morley-framework/morley/-/merge_requests/1215)
  Output contract events in morley-client binary

0.2.1
=====
* [!1199](https://gitlab.com/morley-framework/morley/-/merge_requests/1199)
  Remove HasMavrykClient getMavrykClientConfig, importKey, get[Public/Secret]Key
* [!1212](https://gitlab.com/morley-framework/morley/-/merge_requests/1212)
  Get contract events
  + Operation result handlers now also return internal operation data.
  + Injecting the operation fetches contract events from internal operation
    data (received from preapply).
  + `OpTransfer` now has a list of contract events as the result.
* [!1210](https://gitlab.com/morley-framework/morley/-/merge_requests/1210)
  Use `consumed_milligas` instead of `consumed_gas`, as the latter is removed in Kathmandu protocol.
* [!1201](https://gitlab.com/morley-framework/morley/-/merge_requests/1201)
  Enforce reveal operation to use key aliases when doing `mavryk-client reveal`
* [!1177](https://gitlab.com/morley-framework/morley/-/merge_requests/1177)
  Distinguish implicit/contract aliases and addresses on the type level
* [!1183](https://gitlab.com/morley-framework/morley/-/merge_requests/1183)
  Avoid origination when an alias already exists

0.2.0
=====
* [!1161](https://gitlab.com/morley-framework/morley/-/merge_requests/1161)
  Remove support for `AliasHint`
* [!1164](https://gitlab.com/morley-framework/morley/-/merge_requests/1164)
  Add `now` and `level` params to the `/run_code`
* [!1150](https://gitlab.com/morley-framework/morley/-/merge_requests/1150)
  Implement wait-for-operation via RPC
  + Block injection is now checked for errors and injection is retried.
* [!1159](https://gitlab.com/morley-framework/morley/-/merge_requests/1159)
  Add script size calculation to morley-client
* [!1088](https://gitlab.com/morley-framework/morley/-/merge_requests/1088)
  Move data types related to address aliases to `morley`.
* [!1155](https://gitlab.com/morley-framework/morley/-/merge_requests/1155)
  Update operations limits estimation to match the v13.0 `mavryk-client` implementation.
* [!1147](https://gitlab.com/morley-framework/morley/-/merge_requests/1147)
  Fix error handling in morley-client
  + `handleOperationResult` is now exported from `Morley.Client.Action.Common`
* [!1114](https://gitlab.com/morley-framework/morley/-/merge_requests/1114)
  Update to ghc-9.0.2
* [!1108](https://gitlab.com/morley-framework/morley/-/merge_requests/1108)
  Remove support for the deprecated morley extensions
* [!1133](https://gitlab.com/morley-framework/morley/-/merge_requests/1133)
  Add missing fields to `TransactionOpResp` constructor.
* [!1127](https://gitlab.com/morley-framework/morley/-/merge_requests/1127)
  Add method to get secret key from mavryk-client
* [!1140](https://gitlab.com/morley-framework/morley/-/merge_requests/1140)
  Derive `newtype` `Eq`, `Ord`, `Show`, `Buildable` instances for `BlockHash`

0.1.2
=====
* [!1017](https://gitlab.com/morley-framework/morley/-/merge_requests/1017)
  Resolve some TODOs and link TODOs without issue id to the corresponding gitlab tickets.
* [!1082](https://gitlab.com/morley-framework/morley/-/merge_requests/1082)
  Fix/drop/comment noncanonical Show instances

0.1.1
=====
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Morley language extensions now require `--deprecated-morley-extensions` flag to parse.
* [!1077](https://gitlab.com/morley-framework/morley/-/merge_requests/1077)
  Ithaca changes: Use `head~2` block in the `branch` field of RPC operations.
* [!1034](https://gitlab.com/morley-framework/morley/-/merge_requests/1034)
  Add key revealing that uses only RPC.
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Use Morley's fixed-size lists
  + Use `SizedList` for `feeOutputParser`
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
  + `runContract` now supports parameter/storage values in their RPC
    representation (i.e. with bigmap IDs).
* [!1070](https://gitlab.com/morley-framework/morley/-/merge_requests/1070)
  Simplify cleveland's internals & public api
  + Removed `runContractSimple`, added `runContractParameters` and lenses.
* [!1060](https://gitlab.com/morley-framework/morley/-/merge_requests/1060)
  Move `AsRPC` type family to `morley`
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferentially.
  + Add `Buildable` instances to that effect for `FeeParserException`, `SecretKeyEncryptionParserException`.
  + Use `displayException` instead of `show` where appropriate.

0.1.0
=====
Initial release.
A client to interact with the Mavryk blockchain, by use of the `mavryk-node` RPC
and/or of the `mavryk-client` binary.
