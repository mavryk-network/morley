-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Michelson view.
module Morley.Michelson.Untyped.View
  ( ViewName (ViewName, ..)
  , mkViewName
  , BadViewNameError (..)
  , isValidViewNameChar
  , viewNameMaxLength
  , renderViewName
  , viewNameToMText

  , View' (..)
  , ViewsSet (..)
  , mkViewsSet
  , VS.ViewsSetError (..)

  -- * Manipulation
  , emptyViewsSet
  , addViewToSet
  , lookupView
  , viewsSetNames
  ) where

import Data.Aeson (FromJSON, ToJSON)
import Data.Aeson.TH (deriveJSON)
import Data.Coerce (coerce)
import Data.Data (Data)
import Data.Default (Default)

import Morley.Michelson.Internal.ViewName
import Morley.Michelson.Internal.ViewsSet qualified as VS
import Morley.Michelson.Untyped.Type
import Morley.Util.Aeson

-- | Untyped view in a contract.
data View' op = View
  { viewName :: ViewName
    -- ^ View name
  , viewArgument :: Ty
    -- ^ View argument type
  , viewReturn :: Ty
    -- ^ View return type
  , viewCode :: op
    -- ^ View code
  } deriving stock (Eq, Show, Functor, Data, Generic)

instance NFData op => NFData (View' op)

deriveJSON morleyAesonOptions ''View'

newtype ViewsSet instr = ViewsSet { unViewsSet :: Map ViewName (View' instr) }
  deriving newtype (FromJSON, ToJSON, Default, NFData, Container)
  deriving stock (Eq, Show, Data, Functor)

mkViewsSet :: [View' instr] -> Either VS.ViewsSetError (ViewsSet instr)
mkViewsSet = coerce ... VS.mkViewsSet viewName

emptyViewsSet :: forall instr. ViewsSet instr
emptyViewsSet = coerce $ VS.emptyViewsSet @(View' instr)

addViewToSet :: View' instr -> ViewsSet instr -> Either VS.ViewsSetError (ViewsSet instr)
addViewToSet x = coerce $ VS.addViewToSet viewName x

lookupView :: forall instr. ViewName -> ViewsSet instr -> Maybe (View' instr)
lookupView = coerce . VS.lookupView @(View' instr)

viewsSetNames :: forall instr. ViewsSet instr -> Set ViewName
viewsSetNames = VS.viewsSetNames @(View' instr) . coerce
