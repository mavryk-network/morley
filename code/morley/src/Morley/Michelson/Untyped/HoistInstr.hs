-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.Untyped.HoistInstr
  ( HoistInstr(..)
  ) where

-- | Helper class used to change underlying sequence functor in instructions.
class HoistInstr n where
  hoistInstr :: (f a -> g a) -> n f a -> n g a
