-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Michelson.Printer
  ( RenderDoc(..)
  , printUntypedContract
  , printTypedContractCode
  , printTypedContract
  , printSomeContract
  , printTypedValue
  , printUntypedValue
  ) where

import Data.Singletons (SingI)
import Data.Text.Lazy qualified as TL

import Morley.Michelson.Printer.Util
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U

-- | Convert an untyped contract into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printUntypedContract :: (RenderDoc op) => Bool -> U.Contract' op -> TL.Text
printUntypedContract = printRenderDoc

-- | Convert a typed contract into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printTypedContractCode :: (SingI p, SingI s) => Bool -> T.ContractCode p s -> TL.Text
printTypedContractCode forceSingleLine =
  printUntypedContract forceSingleLine . T.convertContractCode

-- | Convert typed contract into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printTypedContract :: Bool -> T.Contract p s -> TL.Text
printTypedContract forceSingleLine fc@T.Contract{} =
  printUntypedContract forceSingleLine $ T.convertContract fc

-- | Convert typed value into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printTypedValue
  :: forall t. (T.UntypedValScope t)
  => Bool -> T.Value t -> TL.Text
printTypedValue forceSingleLine =
  printUntypedValue forceSingleLine . T.untypeValue

-- | Convert untyped value into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printUntypedValue :: (Foldable f, RenderDoc op) => Bool -> U.Value' f op -> TL.Text
printUntypedValue = printRenderDoc

-- | Convert 'T.SomeContract' into a textual representation which
-- will be accepted by the OCaml reference client: @mavkit-client@.
printSomeContract :: Bool -> T.SomeContract -> TL.Text
printSomeContract forceSingleLine (T.SomeContract fc) =
  printTypedContract forceSingleLine fc
