-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | General utilities used by interpreter.
--
-- This is not supposed to import any Michelson modules.
module Morley.Michelson.Interpret.Utils
  ( encodeZarithNumber
  ) where

import Control.Exception (assert)
import Data.Bits qualified as Bits
import Unsafe qualified (fromIntegral)

-- | Encode a number as mavryk does this.
--
-- In the Mavryk reference implementation this encoding is called @zarith@.
encodeZarithNumber :: Integer -> NonEmpty Word8
encodeZarithNumber = doEncode True
  where
    {- Numbers, when packed by mavryk, are represented as follows:

    byte 0:         1              _         ______   ||  lowest digits
            has continuation  is negative   payload   ||
                                                      ||
    byte 1:         1                       _______   ||
    ...             1                       _______   ||
    byte n:         0                       _______   ||
            has continuation                payload   \/  highest digits
    -}
    doEncode :: Bool -> Integer -> NonEmpty Word8
    doEncode isFirst a
      | a >= byteWeight =
          let (hi, lo) = a `divMod` byteWeight
              byte = Bits.setBit (Unsafe.fromIntegral @Integer @Word8 lo) 7
          in byte :| toList (doEncode False hi)
      | a >= 0 =
          one (Unsafe.fromIntegral @Integer @Word8 a)
      | otherwise = assert isFirst $
          let h :| t = doEncode True (-a)
          in Bits.setBit h 6 :| t
      where
        byteWeight = if isFirst then 64 else 128
