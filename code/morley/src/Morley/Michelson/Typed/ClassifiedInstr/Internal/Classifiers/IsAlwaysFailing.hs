-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Actual decision implementation for 'FailureType'.
module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.IsAlwaysFailing
  ( module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.IsAlwaysFailing
  ) where

import Morley.Michelson.Typed.ClassifiedInstr.Internal.InstrEnum
import Morley.Michelson.Typed.ClassifiedInstr.Internal.Types

-- | Decide whether an instruction is always failing. Only 'NEVER' and
-- 'FAILWITH' are considered always failing.
isAlwaysFailing :: InstrEnum -> FailureType
isAlwaysFailing = \case
  NEVER -> AlwaysFailing
  FAILWITH -> AlwaysFailing
  --------------------
  WithLoc -> FailingNormal
  Meta -> FailingNormal
  DocGroup -> FailingNormal
  Nop -> FailingNormal
  Ext -> FailingNormal
  Seq -> FailingNormal
  Nested -> FailingNormal
  IF_NONE -> FailingNormal
  IF_LEFT -> FailingNormal
  IF_CONS -> FailingNormal
  AnnMAP -> FailingNormal
  ITER -> FailingNormal
  IF -> FailingNormal
  LOOP -> FailingNormal
  LOOP_LEFT -> FailingNormal
  DIP  -> FailingNormal
  DIPN  -> FailingNormal
  DROP -> FailingNormal
  DROPN -> FailingNormal
  SWAP -> FailingNormal
  DIG -> FailingNormal
  DUG -> FailingNormal
  UNPAIRN -> FailingNormal
  AnnCAR -> FailingNormal
  AnnCDR -> FailingNormal
  AnnDUP -> FailingNormal
  AnnDUPN -> FailingNormal
  AnnPUSH -> FailingNormal
  AnnSOME -> FailingNormal
  AnnNONE -> FailingNormal
  AnnUNIT -> FailingNormal
  AnnPAIR -> FailingNormal
  AnnUNPAIR -> FailingNormal
  AnnPAIRN -> FailingNormal
  AnnLEFT -> FailingNormal
  AnnRIGHT -> FailingNormal
  AnnNIL -> FailingNormal
  AnnCONS -> FailingNormal
  AnnSIZE -> FailingNormal
  AnnEMPTY_SET -> FailingNormal
  AnnEMPTY_MAP -> FailingNormal
  AnnEMPTY_BIG_MAP -> FailingNormal
  AnnMEM -> FailingNormal
  AnnGET -> FailingNormal
  AnnGETN -> FailingNormal
  AnnUPDATE -> FailingNormal
  AnnUPDATEN -> FailingNormal
  AnnGET_AND_UPDATE -> FailingNormal
  AnnLAMBDA -> FailingNormal
  AnnLAMBDA_REC -> FailingNormal
  AnnEXEC -> FailingNormal
  AnnAPPLY -> FailingNormal
  AnnCAST -> FailingNormal
  AnnRENAME -> FailingNormal
  AnnPACK -> FailingNormal
  AnnUNPACK -> FailingNormal
  AnnCONCAT -> FailingNormal
  AnnCONCAT' -> FailingNormal
  AnnSLICE -> FailingNormal
  AnnISNAT -> FailingNormal
  AnnADD -> FailingNormal
  AnnSUB -> FailingNormal
  AnnSUB_MUMAV -> FailingNormal
  AnnMUL -> FailingNormal
  AnnEDIV -> FailingNormal
  AnnABS -> FailingNormal
  AnnNEG -> FailingNormal
  AnnLSL -> FailingNormal
  AnnLSR -> FailingNormal
  AnnOR -> FailingNormal
  AnnAND -> FailingNormal
  AnnXOR -> FailingNormal
  AnnNOT -> FailingNormal
  AnnCOMPARE -> FailingNormal
  AnnEQ -> FailingNormal
  AnnNEQ -> FailingNormal
  AnnLT -> FailingNormal
  AnnGT -> FailingNormal
  AnnLE -> FailingNormal
  AnnGE -> FailingNormal
  AnnINT -> FailingNormal
  AnnNAT -> FailingNormal
  AnnBYTES -> FailingNormal
  AnnVIEW -> FailingNormal
  AnnSELF -> FailingNormal
  AnnCONTRACT -> FailingNormal
  AnnTRANSFER_TOKENS -> FailingNormal
  AnnSET_DELEGATE -> FailingNormal
  AnnCREATE_CONTRACT -> FailingNormal
  AnnIMPLICIT_ACCOUNT -> FailingNormal
  AnnNOW -> FailingNormal
  AnnAMOUNT -> FailingNormal
  AnnBALANCE -> FailingNormal
  AnnVOTING_POWER -> FailingNormal
  AnnTOTAL_VOTING_POWER -> FailingNormal
  AnnCHECK_SIGNATURE -> FailingNormal
  AnnSHA256 -> FailingNormal
  AnnSHA512 -> FailingNormal
  AnnBLAKE2B -> FailingNormal
  AnnSHA3 -> FailingNormal
  AnnKECCAK -> FailingNormal
  AnnHASH_KEY -> FailingNormal
  AnnPAIRING_CHECK -> FailingNormal
  AnnSOURCE -> FailingNormal
  AnnSENDER -> FailingNormal
  AnnADDRESS -> FailingNormal
  AnnCHAIN_ID -> FailingNormal
  AnnLEVEL -> FailingNormal
  AnnSELF_ADDRESS -> FailingNormal
  AnnTICKET -> FailingNormal
  AnnTICKET_DEPRECATED -> FailingNormal
  AnnREAD_TICKET -> FailingNormal
  AnnSPLIT_TICKET -> FailingNormal
  AnnJOIN_TICKETS -> FailingNormal
  AnnOPEN_CHEST -> FailingNormal
  AnnSAPLING_EMPTY_STATE -> FailingNormal
  AnnSAPLING_VERIFY_UPDATE -> FailingNormal
  AnnMIN_BLOCK_TIME -> FailingNormal
  AnnEMIT -> FailingNormal
