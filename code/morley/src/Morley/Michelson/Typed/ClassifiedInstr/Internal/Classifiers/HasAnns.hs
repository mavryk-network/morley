-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Actual decision implementation for 'HasAnns'.
module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.HasAnns
  ( module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.HasAnns
  ) where

import Morley.Michelson.Typed.ClassifiedInstr.Internal.InstrEnum
import Morley.Michelson.Typed.ClassifiedInstr.Internal.Types

-- | Decide whether instruction has annotations or not. All annotated
-- instructions should have @Ann@ prefix.
hasAnns :: InstrEnum -> HasAnns
hasAnns = \case
  WithLoc -> DoesNotHaveAnns
  Meta -> DoesNotHaveAnns
  DocGroup -> DoesNotHaveAnns
  Nop -> DoesNotHaveAnns
  Ext -> DoesNotHaveAnns
  Seq -> DoesNotHaveAnns
  Nested -> DoesNotHaveAnns
  IF_NONE -> DoesNotHaveAnns
  IF_LEFT -> DoesNotHaveAnns
  IF_CONS -> DoesNotHaveAnns
  ITER -> DoesNotHaveAnns
  IF -> DoesNotHaveAnns
  LOOP -> DoesNotHaveAnns
  LOOP_LEFT -> DoesNotHaveAnns
  DIP  -> DoesNotHaveAnns
  DIPN  -> DoesNotHaveAnns
  FAILWITH  -> DoesNotHaveAnns
  NEVER  -> DoesNotHaveAnns
  DROP -> DoesNotHaveAnns
  DROPN -> DoesNotHaveAnns
  SWAP -> DoesNotHaveAnns
  DIG -> DoesNotHaveAnns
  DUG -> DoesNotHaveAnns
  UNPAIRN -> DoesNotHaveAnns
  ------------------------
  AnnMAP -> DoesHaveStandardAnns
  AnnCAR -> DoesHaveStandardAnns
  AnnCDR -> DoesHaveStandardAnns
  AnnDUP -> DoesHaveStandardAnns
  AnnDUPN -> DoesHaveStandardAnns
  AnnPUSH -> DoesHaveStandardAnns
  AnnSOME -> DoesHaveStandardAnns
  AnnNONE -> DoesHaveStandardAnns
  AnnUNIT -> DoesHaveStandardAnns
  AnnPAIR -> DoesHaveStandardAnns
  AnnUNPAIR -> DoesHaveStandardAnns
  AnnPAIRN -> DoesHaveStandardAnns
  AnnLEFT -> DoesHaveStandardAnns
  AnnRIGHT -> DoesHaveStandardAnns
  AnnNIL -> DoesHaveStandardAnns
  AnnCONS -> DoesHaveStandardAnns
  AnnSIZE -> DoesHaveStandardAnns
  AnnEMPTY_SET -> DoesHaveStandardAnns
  AnnEMPTY_MAP -> DoesHaveStandardAnns
  AnnEMPTY_BIG_MAP -> DoesHaveStandardAnns
  AnnMEM -> DoesHaveStandardAnns
  AnnGET -> DoesHaveStandardAnns
  AnnGETN -> DoesHaveStandardAnns
  AnnUPDATE -> DoesHaveStandardAnns
  AnnUPDATEN -> DoesHaveStandardAnns
  AnnGET_AND_UPDATE -> DoesHaveStandardAnns
  AnnLAMBDA -> DoesHaveStandardAnns
  AnnLAMBDA_REC -> DoesHaveStandardAnns
  AnnEXEC -> DoesHaveStandardAnns
  AnnAPPLY -> DoesHaveStandardAnns
  AnnCAST -> DoesHaveStandardAnns
  AnnRENAME -> DoesHaveStandardAnns
  AnnPACK -> DoesHaveStandardAnns
  AnnUNPACK -> DoesHaveStandardAnns
  AnnCONCAT -> DoesHaveStandardAnns
  AnnCONCAT' -> DoesHaveStandardAnns
  AnnSLICE -> DoesHaveStandardAnns
  AnnISNAT -> DoesHaveStandardAnns
  AnnADD -> DoesHaveStandardAnns
  AnnSUB -> DoesHaveStandardAnns
  AnnSUB_MUMAV -> DoesHaveStandardAnns
  AnnMUL -> DoesHaveStandardAnns
  AnnEDIV -> DoesHaveStandardAnns
  AnnABS -> DoesHaveStandardAnns
  AnnNEG -> DoesHaveStandardAnns
  AnnLSL -> DoesHaveStandardAnns
  AnnLSR -> DoesHaveStandardAnns
  AnnOR -> DoesHaveStandardAnns
  AnnAND -> DoesHaveStandardAnns
  AnnXOR -> DoesHaveStandardAnns
  AnnNOT -> DoesHaveStandardAnns
  AnnCOMPARE -> DoesHaveStandardAnns
  AnnEQ -> DoesHaveStandardAnns
  AnnNEQ -> DoesHaveStandardAnns
  AnnLT -> DoesHaveStandardAnns
  AnnGT -> DoesHaveStandardAnns
  AnnLE -> DoesHaveStandardAnns
  AnnGE -> DoesHaveStandardAnns
  AnnINT -> DoesHaveStandardAnns
  AnnNAT -> DoesHaveStandardAnns
  AnnBYTES -> DoesHaveStandardAnns
  AnnVIEW -> DoesHaveStandardAnns
  AnnSELF -> DoesHaveStandardAnns
  AnnCONTRACT -> DoesHaveStandardAnns
  AnnTRANSFER_TOKENS -> DoesHaveStandardAnns
  AnnSET_DELEGATE -> DoesHaveStandardAnns
  AnnCREATE_CONTRACT -> DoesHaveStandardAnns
  AnnIMPLICIT_ACCOUNT -> DoesHaveStandardAnns
  AnnNOW -> DoesHaveStandardAnns
  AnnAMOUNT -> DoesHaveStandardAnns
  AnnBALANCE -> DoesHaveStandardAnns
  AnnVOTING_POWER -> DoesHaveStandardAnns
  AnnTOTAL_VOTING_POWER -> DoesHaveStandardAnns
  AnnCHECK_SIGNATURE -> DoesHaveStandardAnns
  AnnSHA256 -> DoesHaveStandardAnns
  AnnSHA512 -> DoesHaveStandardAnns
  AnnBLAKE2B -> DoesHaveStandardAnns
  AnnSHA3 -> DoesHaveStandardAnns
  AnnKECCAK -> DoesHaveStandardAnns
  AnnHASH_KEY -> DoesHaveStandardAnns
  AnnPAIRING_CHECK -> DoesHaveStandardAnns
  AnnSOURCE -> DoesHaveStandardAnns
  AnnSENDER -> DoesHaveStandardAnns
  AnnADDRESS -> DoesHaveStandardAnns
  AnnCHAIN_ID -> DoesHaveStandardAnns
  AnnLEVEL -> DoesHaveStandardAnns
  AnnSELF_ADDRESS -> DoesHaveStandardAnns
  AnnTICKET -> DoesHaveStandardAnns
  AnnTICKET_DEPRECATED -> DoesHaveStandardAnns
  AnnREAD_TICKET -> DoesHaveStandardAnns
  AnnSPLIT_TICKET -> DoesHaveStandardAnns
  AnnJOIN_TICKETS -> DoesHaveStandardAnns
  AnnOPEN_CHEST -> DoesHaveStandardAnns
  AnnSAPLING_EMPTY_STATE -> DoesHaveStandardAnns
  AnnSAPLING_VERIFY_UPDATE -> DoesHaveStandardAnns
  ------------------------
  AnnMIN_BLOCK_TIME -> DoesHaveNonStandardAnns
  AnnEMIT -> DoesHaveNonStandardAnns
