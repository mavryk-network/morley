-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Cryptographic primitives related to hashing.

module Morley.Mavryk.Crypto.Hash
  ( blake2b
  , blake2b160
  , keccak
  , sha256
  , sha3
  , sha512
  ) where

import Crypto.Hash (Blake2b_160, Blake2b_256, Digest, Keccak_256, SHA256, SHA3_256, SHA512, hash)
import Data.ByteArray qualified as ByteArray

-- | Compute a cryptographic hash of a bytestring using the
-- Blake2b_256 cryptographic hash function. It's used by the BLAKE2B
-- instruction in Michelson.
blake2b :: ByteString -> ByteString
blake2b = fromDigest @Blake2b_256 . hash

-- | Compute a cryptographic hash of a bytestring using the
-- Blake2b_160 cryptographic hash function.
blake2b160 :: ByteString -> ByteString
blake2b160 = fromDigest @Blake2b_160 . hash

-- | Compute a cryptographic hash of a bytestring using the
-- Sha256 cryptographic hash function.
sha256 :: ByteString -> ByteString
sha256 = fromDigest @SHA256 . hash

-- | Compute a cryptographic hash of a bytestring using the
-- Sha512 cryptographic hash function.
sha512 :: ByteString -> ByteString
sha512 = fromDigest @SHA512 . hash

-- | Compute a cryptographic hash of a bytestring using the Sha3_256
-- cryptographic hash function. It is used by the SHA3 Michelson instruction.
sha3 :: ByteString -> ByteString
sha3 = fromDigest @SHA3_256 . hash

-- | Compute a cryptographic hash of a bytestring using the Keccak_256
-- cryptographic hash function. It is used by the KECCAK Michelson instruction.
keccak :: ByteString -> ByteString
keccak = fromDigest @Keccak_256 . hash

fromDigest :: forall a. Digest a -> ByteString
fromDigest = ByteArray.convert
