-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- NB: 'genSingletonsType' creates some unused synonyms and GHC complains. To
-- minimize the impact of this option, this is in a separate module.
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

-- | Address kinds.
module Morley.Mavryk.Address.Kinds
  ( AddressKind(..)
  , SingAddressKind(..)
  , forEachAddressKind
  ) where

import Fmt (Buildable(..))
import Language.Haskell.TH (ExpQ)

import Morley.Util.Sing

-- | Address "kind"
data AddressKind
  = AddressKindImplicit
  -- ^ an implicit address, @mv1@-@mv3@
  | AddressKindContract
  -- ^ a contract address, @KT1@
  | AddressKindSmartRollup
  -- ^ a smart rollup address, @sr1@
  deriving stock (Eq, Bounded, Enum, Ord, Show, Generic)

instance NFData AddressKind

instance Buildable AddressKind where
  build = \case
    AddressKindImplicit -> "implicit"
    AddressKindContract -> "contract"
    AddressKindSmartRollup -> "smart rollup"

genSingletonsType ''AddressKind

forEachAddressKind :: ExpQ -> ExpQ
forEachAddressKind r = [e|
  \case
    SAddressKindImplicit -> $r
    SAddressKindContract -> $r
    SAddressKindSmartRollup -> $r
  |]
