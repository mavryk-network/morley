-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module re-exports types from "Morley.Util.SizedList"
--
-- Since "Morley.Util.SizedList" is intended to be imported qualified, this module provides
-- a convenient way to import only types, which are unlinkely to be ambiguous.
module Morley.Util.SizedList.Types
  ( -- * Base types
    SizedList
  , SizedList'(..)
  , SomeSizedList(..)

  -- * Utility type synonyms
  , SingIPeano
  , IsoNatPeano
  ) where

import Morley.Util.SizedList
