-- SPDX-FileCopyrightText: 2018 obsidian.systems
-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-obsidian-systems

-- | Module that defines helper types and functions that are related
-- to Micheline.
module Morley.Micheline.Json
  ( StringEncode (..)
  , MavrykBigNum
  , MavrykInt64
  , MavrykMumav (..)
  , MavrykNat
  ) where

import Data.Aeson (FromJSON, ToJSON, parseJSON, toEncoding, toJSON)
import Data.Aeson.Encoding qualified as AE
import Data.Aeson.Types qualified as Aeson
import Data.Bits (Bits)
import Data.Typeable (typeRep)
import Fmt (Buildable(..))
import Text.Show qualified as T

import Morley.Mavryk.Core (Mumav(..), mkMumav)
import Unsafe qualified (unsafeM)

printAsString :: (PrettyShow a, Show a) => a -> Aeson.Value
printAsString a = Aeson.String $ show a

parseAsString :: forall a. (Read a, Typeable a) => Aeson.Value -> Aeson.Parser a
parseAsString = Aeson.withText (T.show $ typeRep (Proxy :: Proxy a)) $ \txt ->
  maybe (fail "Failed to parse string") pure $ readMaybe (toString txt)

parseStringEncodedIntegral :: (Read a, Typeable a) => Aeson.Value -> Aeson.Parser (StringEncode a)
parseStringEncodedIntegral x = StringEncode <$> parseAsString x

newtype StringEncode a = StringEncode { unStringEncode :: a }
  deriving stock (Generic, Eq, Ord, Bounded, Read, Show)
  deriving newtype (Enum, Num, Integral, Bits, Real, NFData, Hashable)

type instance IntBaseType (StringEncode a) = IntBaseType a

type MavrykBigNum = StringEncode Integer

instance FromJSON MavrykBigNum where
  parseJSON = parseStringEncodedIntegral

instance ToJSON MavrykBigNum where
  toJSON (StringEncode x) = Aeson.String $ show x
  toEncoding (StringEncode x) = AE.integerText x

type MavrykInt64 = StringEncode Int64

instance FromJSON MavrykInt64 where
  parseJSON = parseStringEncodedIntegral

instance Buildable MavrykInt64 where
  build = show . unStringEncode

instance ToJSON MavrykInt64 where
  toJSON (StringEncode x) = Aeson.String $ show x
  toEncoding (StringEncode x) = AE.int64Text x

newtype MavrykMumav = MavrykMumav { unMavrykMumav :: Mumav }
  deriving stock (Show, Eq, Ord)

instance ToJSON MavrykMumav where
  toJSON = printAsString . unMumav . unMavrykMumav

instance FromJSON MavrykMumav where
  parseJSON v = do
    i     <- parseAsString @Int64 v
    mumav <- Unsafe.unsafeM $ mkMumav i
    pure $ MavrykMumav mumav

type MavrykNat = StringEncode Natural

instance Buildable MavrykNat where
  build = show . unStringEncode

instance ToJSON MavrykNat where
  toJSON (StringEncode x) = Aeson.String $ show x
  toEncoding (StringEncode x) = AE.integerText $ fromIntegral x

instance FromJSON MavrykNat where
  parseJSON = parseStringEncodedIntegral
