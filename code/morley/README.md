> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Mavryk mainnet (June 24th, 2023).

# Morley: Developer tools for the Michelson Language

[![Hackage](https://img.shields.io/hackage/v/morley.svg)](https://hackage.haskell.org/package/morley)

Morley aims to make writing smart contracts in Michelson pleasant and effective.
It contains 2 major things:

1. An [executable](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-executable) that lets you perform various operations on Michelson smart contracts.
2. A [library](https://gitlab.com/morley-framework/morley/-/tree/master/code/morley#morley-library) with core Mavryk and Michelson data types, and functions such as Michelson typechecker and interpreter.

## Morley executable

### Dependencies

To install using stack or cabal, you need to install [libsodium](https://doc.libsodium.org) first.

If installing into a non-standard location, after its installation is complete, add
the directory containing `libsodium.so` to your `LD_LIBRARY_PATH` environment
variable. Otherwise, compilation with stack or cabal will fail.

### How to install

There are three ways to get Morley executable:
- [Docker](https://docs.docker.com/) based (preferable).
  * Get [script](https://gitlab.com/morley-framework/morley/-/blob/master/scripts/morley.sh)
 (e. g. using `curl https://gitlab.com/morley-framework/morley/raw/master/scripts/morley.sh > morley.sh`)
  and run it `./morley.sh <args>`. This script will pull a docker image that contains the latest version of Morley executable from the `production` branch and run it with the given arguments.
    You'll need `coreutils` to be installed in order to use `morley.sh`.
  * Usage example:
    + `./morley.sh` to see help message
    + `./morley.sh run --contract add1.mv --storage 1 --parameter 1 --amount 1`
- [Stack](https://docs.haskellstack.org/en/stable/README/) based.
  * Clone this git repository and run `stack build` command,
    after that you can do `stack exec -- morley <args>` to run morley executable built from the source code.
  * Usage example:
    + `stack exec -- morley --help` to see help message
    + `stack exec -- morley originate --contract contracts/mavryk_examples/attic/add1.mv --storage 1 --verbose`
- [Cabal](https://www.haskell.org/cabal/) based.
  * Clone this git repository, go to this directory and run `cabal new-update && cabal new-build` command,
    after that you can do `cabal new-run -- morley <args>` to run morley executable built from the source code.
  * Usage example:
    + `cabal new-run -- morley --help` to see help message
    + `cabal new-run -- morley originate --contract contracts/mavryk_examples/attic/add1.mv --storage 1 --verbose`

### Usage

Morley executable does not interact with Mavryk node and Mavryk network.
It works in _emulated environment_ which is stored in a simple JSON file on disk.
The following commands depend on environment and may modify it:
- `emulate originate` a contract.
- `emulate transfer` tokens to a given address (and call a smart contract if it's the destination).
- `emulate run` a contract. A given contract is being originated first, and then the transaction is being sent to it.

The following commands don't depend on environment:
- `optimize` a contract by replacing certain sequences of instructions with equivalent but more efficient ones.
- `typecheck` a contract.
- `repl` starts a REPL where you can type Michelson instructions and interpret them.
- `analyze` a contract and print some statistics about it.
- `print` a contract in vanilla Michelson format.
It can be useful in some cases:
  + You have a contract with inconsistent/ugly formatting and want to format it in uniform style.
  + You want to print a contract on a single line.
- `parse` a contract and return its representation in Haskell types.

NOTE: `$TERM` and `$TERMINFO` environment variables need to be set for an enhanced terminal experience when using utilities like
`repl`

You can get more info about these commands by running `morley <command> --help`.
Run `morley --help` to get the list of all commands.

## Morley library

Morley-the library is available on [Hackage](https://hackage.haskell.org/package/morley). To use `morley` as library, simply include it in the `package.yaml`.

It is recommended to also
add `morley` extra-deps to your `stack.yaml`. The extra-deps can be found [here](../../stack.yaml#L25-62).

The library consists of the following parts:

- The `Morley.Mavryk.*` hierarchy ([`Morley.Mavryk.Address`](http://hackage.haskell.org/package/morley/docs/Morley-Mavryk-Address.html), [`Morley.Mavryk.Core`](http://hackage.haskell.org/package/morley/docs/Morley-Mavryk-Core.html), [`Morley.Mavryk.Crypto`](http://hackage.haskell.org/package/morley/docs/Morley-Mavryk-Crypto.html)) is designed to implement cryptographic primitives, string and byte formats, and any other functionality specific to the Mavryk protocol which is required for testing/execution of Michelson contracts but is used not only by Michelson.
- [`Morley.Michelson.Untyped`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Untyped.html) and [`Morley.Michelson.Typed`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Typed.html) hierarchies define Haskell data types that assemble a Michelson contract. See [michelsonTypes.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/michelsonTypes.md).
- [`Morley.Michelson.TypeCheck`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-TypeCheck.html): A typechecker that validates Michelson contracts according to the Michelson's typing rules. Essentially, it performs conversion from untyped representation to the typed one. See [morleyTypechecker.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyTypechecker.md).
- [`Morley.Michelson.Interpret`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Interpret.html): An interpreter for Michelson contracts which doesn't perform any side effects. See [morleyInterpreter.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyInterpreter.md).
- [`Morley.Michelson.Macro`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Macro.html) Types for macros, syntactic sugar, and other extensions that are described in the next chapter.
- [`Morley.Michelson.Parser`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Parser.html) A parser to turn a `.mv` into a Haskell ADT.
- [`Morley.Michelson.Runtime`](http://hackage.haskell.org/package/morley/docs/Morley-Michelson-Runtime.html): A high-level interface to Morley functionality, see [morleyRuntime.md](https://gitlab.com/morley-framework/morley/-/blob/master/code/morley/docs/morleyRuntime.md).
- The `Morley.Micheline.*` hierarchy ([`Morley.Micheline.Binary`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Binary.html), [`Morley.Micheline.Class`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Class.html),
[`Morley.Micheline.Expression`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Expression.html), [`Morley.Micheline.Json`](http://hackage.haskell.org/package/morley/docs/Morley-Micheline-Json.html))
contains the representation of Micheline `Expression`s, conversion to/from Michelson values,
as well as encoding/decoding to/from JSON and binary format.
- The `Morley.Util.*` hierarchy defines various `Morley`-related utilities that are used all over the project.
