-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Data.Version (showVersion)
import Options.Applicative
  (execParser, footerDoc, fullDesc, header, help, helper, info, infoOption, long, progDesc)
import Options.Applicative.Help.Pretty (Doc, line)
import Paths_morley (version)

import Morley.App.CLI
import Morley.Util.Main

main :: IO ()
main = wrapMain $ join $ execParser programInfo
  where
    programInfo = info (helper <*> versionOption <*> argParser) $
      mconcat
      [ fullDesc
      , progDesc "Morley: Haskell implementation of Michelson typechecker and interpreter"
      , header "Morley tools"
      , footerDoc $ usageDoc
      ]

    versionOption = infoOption ("morley-" <> showVersion version)
      (long "version" <> help "Show version.")

usageDoc :: Maybe Doc
usageDoc = Just $ mconcat
   [ "You can use help for specific COMMAND", line
   , "EXAMPLE:", line
   , "  morley emulate run --help", line
   , line
   , "Documentation for morley tools can be found at the following links:", line
   , "  https://gitlab.com/morley-framework/morley/blob/master/README.md", line
   , "  https://gitlab.com/morley-framework/morley/tree/master/docs", line
   , line
   , "Sample contracts for running can be found at the following link:", line
   , "  https://gitlab.com/morley-framework/morley/tree/master/contracts", line
   , line
   , "USAGE EXAMPLE:", line
   , "  morley parse --contract add1.mv", line
   , line
   , "  This command will parse contract stored in add1.mv", line
   , "  and return its representation in haskell types", line
   , line
   , "  morley emulate originate --contract add1.mv --storage 1 --verbose", line
   , line
   , "  This command will originate contract with code stored in add1.mv", line
   , "  with initial storage value set to 1 and return info about", line
   , "  originated contract: its balance, storage and contract code"]
