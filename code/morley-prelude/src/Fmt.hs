-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE NoImplicitPrelude #-}

-- | Re-exporting Wadler-Leijen formatter utilities plus 'Buildable'.
module Fmt
  ( module Exports
  ) where

import Universum (Text)

import Fmt.Buildable as Exports
import Fmt.Operators as Exports
import Fmt.Utils as Exports

{-# ANN module ("HLint: ignore" :: Text) #-}
