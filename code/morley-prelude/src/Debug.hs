-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE NoImplicitPrelude #-}

-- | This module reexports an unconstrained version of 'show', which can be used
-- to print non-human-readable types. Generally, you don't want this. The cases where
-- this makes sense are:
--
-- * You want to print specifically a Haskell representation for debugging purposes
-- * There is no meaningful human-readable representation
-- * You're implementing a @Show@ instance
--
-- It's perfectly okay to use @Debug.show@ in these cases. When the output is intended to be seen
-- by the end user, however, you should use @Fmt.pretty@ instead.
--
-- This module should be imported qualified:
--
-- > import qualified Debug (show)
module Debug
  ( show
  ) where

import Universum (show)
