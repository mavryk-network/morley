#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package tasty-hedgehog
  --package hedgehog
  --package cleveland
  --package morley
  --package morley-prelude
  --package o-clock
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QuasiQuotes, OverloadedStrings, TypeApplications #-}

module StringCallerSpec
  ( test_string_caller
  ) where

import Hedgehog (forAll, property, withTests)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import Time (sec)

import Hedgehog.Gen.Michelson (genMText)
import Hedgehog.Range.Defaults (def)
import Lorentz.Address (toAddress)
import Morley.Michelson.Text (MText)
import Morley.Michelson.Typed
import Morley.Mavryk.Address (Address, ta)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain $
  testGroup "StringCallerSpec" test_string_caller

test_string_caller :: [TestTree]
test_string_caller =
  [ testScenario (prefix <> "a constant" <> suffix) $ myScenario constStr
  , testProperty (prefix <> "an arbitrary value" <> suffix) $
      -- the test is somewhat redundant, so we limit it to two runs here
      withTests 2 $ property $ do
        str <- forAll $ genMText def
        testScenarioProps $ myScenario str
  ]
  where
  constStr = "caller"
  prefix =
    "stringCaller calls failOrStoreAndTransfer and updates its storage with "
  suffix =
    " and properly updates balances. But fails if failOrStoreAndTransfer's\
    \ balance is ≥ 1300 and NOW is > 500"

myScenario :: Monad m => MText -> Scenario m
myScenario str = scenario $ do
  let
    initFailOrStoreBalance = 900
    initStringCallerBalance = 500

  failOrStoreAndTransfer
    <- importContract @MText @MText @() "contracts/fail_or_store_and_transfer.mv"
  stringCaller <- importContract @MText @Address @() "contracts/string_caller.mv"

  -- Originate both contracts
  failOrStoreAndTransferHandle <- originate OriginateData
    { odName = "failOrStoreAndTransfer"
    , odStorage = "hello"
    , odBalance = initFailOrStoreBalance
    , odContract = failOrStoreAndTransfer
    }
  stringCallerHandle <- originate OriginateData
    { odContract = stringCaller
    , odName = "stringCaller"
    , odStorage = toAddress failOrStoreAndTransferHandle
    , odBalance = initStringCallerBalance
    }

  -- initial timestamp is 100 on the emulated environment, so
  -- stringCaller will work

  -- Transfer 100 mumav to stringCaller, it should transfer 300 mumav
  -- to failOrStoreAndTransfer
  let transferToStringCaller =
        transfer TransferData
          { tdTo = stringCallerHandle
          , tdParameter = str
          , tdAmount = 100
          , tdEntrypoint = DefEpName
          }
  transferToStringCaller

  -- Check balances and storage of 'failOrStoreAndTransfer'
  let
    -- `string_caller.mv` transfers 300 mumav.
    -- 'fail_or_store_and_transfer.mv' transfers 5 mumav.
    -- Also 100 mumav are transferred from the "moneybag" address.
    expectedStringCallerBalance = initStringCallerBalance - 300 + 100
    expectedFailOrStoreBalance  = initFailOrStoreBalance  + 300 - 5
    expectedConstAddrBalance    = 5

  getStorage failOrStoreAndTransferHandle @@== str
  getBalance failOrStoreAndTransferHandle @@== expectedFailOrStoreBalance
  getBalance stringCallerHandle           @@== expectedStringCallerBalance
  getBalance [ta|mv1TxMEnmav51G1Hwcib1rBnBeniDMgG8nkJ|]
    @@== expectedConstAddrBalance

  -- Now let's transfer 100 mumav to stringCaller again.
  -- This time execution should fail, because failOrStoreAndTransfer should fail
  -- because its balance is greater than 1300.
  expectTransferFailure
    (addressIs failOrStoreAndTransferHandle)
    transferToStringCaller

  -- Now let's advance time by 500 seconds and watch stringCaller fail
  advanceTime $ sec 500
  expectTransferFailure
    (addressIs stringCallerHandle)
    transferToStringCaller
