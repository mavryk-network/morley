#!/usr/bin/env stack
{- stack
  script
  --resolver snapshot.yaml
  --package base-noprelude
  --package tasty
  --package tasty-hedgehog
  --package hedgehog
  --package morley
  --package cleveland
  --package morley-prelude
  --ghc-options "-hide-package base"
-}
-- Note that stack shebang and its arguments list cannot be separated with a newline
-- because otherwise arguments won't be used.

-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedStrings, TypeApplications #-}

module CompareSpec
  ( hprop_compare
  ) where

import Hedgehog (Gen, Property, forAll, property)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Mavryk.Core (genMumav)
import Hedgehog.Range.Defaults (def)
import Morley.Mavryk.Core (Mumav)
import Test.Cleveland
import Test.Cleveland.Tasty

main :: IO ()
main = clevelandMain $
  testProperty "compare" hprop_compare

genParameter :: Gen (Mumav, Mumav)
genParameter = (,) <$> genMumav def <*> genMumav def

hprop_compare :: Property
hprop_compare = property $ do
    input <- forAll genParameter
    testScenarioProps $ scenario $ do
      contract <- importContract @(Mumav, Mumav) @[Bool] @() "contracts/compare.mv"
      handle <- originateSimple "compare" [] contract
      call handle CallDefault input
      getStorage handle @@== mkExpected input

mkExpected :: (Mumav, Mumav) -> [Bool]
mkExpected (a, b) = [a == b, a > b, a < b, a >= b, a <= b]
