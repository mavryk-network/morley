-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Mavryk.Core
  ( genChainId
  , genMumav
  , genTimestamp
  ) where

import Hedgehog (MonadGen, Range)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Mavryk.Core
  (ChainId(..), Mumav(..), Timestamp, mkMumav, timestampFromSeconds, timestampToSeconds)

import Hedgehog.Range.Defaults ()

genChainId :: MonadGen m => m ChainId
genChainId = UnsafeChainId <$> Gen.bytes (Range.singleton 4)

-- | Generates an arbitrary `Mumav` value constrained to the given range.
genMumav :: MonadGen m => Range Mumav -> m Mumav
genMumav range = unsafe . mkMumav <$> Gen.word64 (fromIntegral . unMumav <$> range)

genTimestamp :: MonadGen m => Range Timestamp -> m Timestamp
genTimestamp range =
  timestampFromSeconds <$> Gen.integral (timestampToSeconds <$> range)
