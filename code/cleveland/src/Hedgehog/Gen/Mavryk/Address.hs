-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Address in Mavryk.

module Hedgehog.Gen.Mavryk.Address
  ( genAddress
  , genContractAddress
  , genKeyAddress
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Hedgehog.Gen.Mavryk.Crypto (genKeyHash)
import Morley.Mavryk.Address
import Morley.Mavryk.Crypto

genAddress :: MonadGen m => m Address
genAddress = Gen.choice [MkAddress <$> genKeyAddress, MkAddress <$> genContractAddress]

genKeyAddress :: MonadGen m => m ImplicitAddress
genKeyAddress = ImplicitAddress <$> genKeyHash

genContractAddress :: MonadGen m => m ContractAddress
genContractAddress = ContractAddress . Hash HashContract <$> Gen.bytes (Range.singleton 20)
