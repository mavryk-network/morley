-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Mavryk.Crypto.BLS12381
  ( genBls12381Fr
  , genBls12381G1
  , genBls12381G2
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen

import Morley.Mavryk.Crypto.BLS12381
import Test.Cleveland.Util

genBls12381Fr :: MonadGen m => m Bls12381Fr
genBls12381Fr = Gen.enumBounded

genBls12381G1 :: MonadGen m => m Bls12381G1
genBls12381G1 = genRandom generate

genBls12381G2 :: MonadGen m => m Bls12381G2
genBls12381G2 = genRandom generate
