-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Mavryk.Crypto.BLS
  ( genPublicKey
  , genSecretKey
  , genSignature
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Mavryk.Crypto.BLS (PublicKey, SecretKey, Signature, detSecretKey, sign, toPublic)

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = toPublic <$> genSecretKey

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = detSecretKey <$> Gen.bytes (Range.singleton 32)

genSignature :: MonadGen m => m Signature
genSignature = sign <$> genSecretKey <*> Gen.utf8 (Range.linear 0 100) Gen.unicode
