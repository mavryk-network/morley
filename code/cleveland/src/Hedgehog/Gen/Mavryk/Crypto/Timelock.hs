-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Mavryk.Crypto.Timelock
  ( genChestAndKey
  , genChestAndKeyWithParams
  , genTLTime
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Mavryk.Crypto.Timelock

genChestAndKey :: MonadGen m => m (Chest, ChestKey)
genChestAndKey = fst <$> genChestAndKeyWithParams Nothing

genTLTime :: MonadGen m => m TLTime
genTLTime = toTLTime <$> Gen.word16 (Range.linear 1000 10000)

genChestAndKeyWithParams
  :: MonadGen m
  => Maybe TLTime
  -> m ((Chest, ChestKey), (ByteString, TLTime))
genChestAndKeyWithParams mbtime = do
  seed <- Gen.enumBounded
  payload <- Gen.bytes (Range.linear 1 1024)
  time <- maybe genTLTime pure mbtime
  let (chest, key) = createChestAndChestKeyFromSeed seed payload time
  pure ((chest, key), (payload, time))
