-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Mavryk.Crypto.P256
  ( genPublicKey
  , genSecretKey
  , genSignature
  ) where

import Crypto.Random (drgNewSeed, seedFromInteger, withDRG)
import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Mavryk.Crypto.P256
  (PublicKey, SecretKey, Signature, detSecretKey, detSecretKeyDo, sign, toPublic)

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = toPublic <$> genSecretKey

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = detSecretKey <$> Gen.bytes (Range.singleton 32)

genSignature :: MonadGen m => m Signature
genSignature = do
  seed <- drgNewSeed . seedFromInteger <$> Gen.integral (Range.linearFrom 0 -1000 1000)
  byteToSign <- Gen.word8 Range.linearBounded
  return $ fst $ withDRG seed $ do
    sk <- detSecretKeyDo
    sign sk (one byteToSign)
