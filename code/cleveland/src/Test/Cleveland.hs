-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Michelson contracts testing on a real Mavryk network.
--
-- See the [documentation](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md)
-- for usage instructions.
module Test.Cleveland
  ( Alias
  , ContractAlias
  , ImplicitAlias
  , KindedAddress
  , ContractAddress
  , ImplicitAddress
  , AddressWithAlias (..)
  , ImplicitAddressWithAlias
  , ContractAddressWithAlias
  , ContractHandle (..)
  , TransferData (..)
  , TypedContract (..)
  , Scenario
  , PureM
  , ClientM

  -- * Validation
  , TransferFailure

  -- * Real network implementation based on @mavkit-client@ and RPC.
  , testScenario
  , testScenarioOnNetwork
  , scenarioNetwork
  , MonadScenario(..)
  , getMorleyClientEnv
  , getOnlyRpcEnv

  -- * Emulation tests
  , testScenarioOnEmulator
  , scenarioEmulated
  , withInitialNow
  , withInitialLevel
  , withMinBlockTime
  , withChainId
  , collectLogs
  , logsForAddress

  -- * Cleveland actions
  , MonadCleveland
  , MonadOps
  , MonadEmulated
  , ClevelandT
  , EmulatedT
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , refillable
  , refillables
  , newAddress
  , newAddresses
  , newFreshAddress
  , importSecretKey
  , enumAliases
  , signBytes
  , signBinary
  , originate
  , Large (..)
  , transfer
  , transferTicket
  , unsafeTransferTicket
  , WithContractEvents (..)
  , ContractEvent (..)
  , calling
  , Default(def)
  , unsafeCalling
  , importUntypedContract
  , importContract
  , noViews
  , comment
  , getBalance
  , getDelegate
  , registerDelegate
  , setDelegate
  , getMorleyLogs
  , getMorleyLogs_
  , getStorage
  , getFullStorage
  , getSomeStorage
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , getMinBlockTime
  , RunCode(..)
  , runCode
  , branchout
  , offshoot
  , setVotingPowers
  , whenEmulation
  , whenNetwork
  , ifEmulation
  , inBatch
  , getTicketBalance
  , getAllTicketBalances
  , SomeTicket(..)

  -- * Address conversions
  , ToAddress(..)
  , ToL1Address(..)
  , ToL1TAddress(..)
  , ToContractAddress(..)
  , ToImplicitAddress(..)

  -- * Primitives re-exports
  , EntrypointRef (..)
  , VotingPowers
  , Mumav
  , mv
  , mkVotingPowers
  , mkVotingPowersFromMap

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith
  , evalJust
  , evalRight
  , Showing(..)

  -- * Calling views
  , callView
  , unsafeCallView

    -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectCustomErrorAnyArg
  , expectNumericError
  , clarifyErrors

  -- ** Exception predicates
  , TransferFailurePredicate
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , gasExhaustion
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  -- | Convert the many error formats available in @morley@ and @lorentz@ to t'Morley.Michelson.Typed.Existential.SomeConstant'.
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError

  -- * Helpers
  , auto
  , pattern DefEpName
  , ep
  , (?-)
  , (:!)
  , NamedF (..)

  -- * Integration with @hedgehog@
  , testScenarioProps

  -- * Config (reexports)
  , MavrykClientEnv(..)
  , MorleyClientEnv(..)
  , NetworkEnv (..)
  , MorleyLogs
  -- ** Config utilities
  , mkMorleyOnlyRpcEnvNetwork

  -- * AsRPC
  , AsRPC.HasRPCRepr(AsRPC)
  , AsRPC.deriveRPC

  -- * Utilities
  , mapEach
  , forEach
  ) where

import Data.Default (Default(def))

import Lorentz (EntrypointRef(..), Mumav, noViews, pattern DefEpName, mv)
import Morley.AsRPC qualified as AsRPC
import Morley.Client (MorleyClientEnv(..))
import Morley.Client.MavrykClient.Types (MavrykClientEnv(..))
import Morley.Client.Types
  (AddressWithAlias(..), ContractAddressWithAlias, ImplicitAddressWithAlias)
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Runtime (VotingPowers, mkVotingPowers, mkVotingPowersFromMap)
import Morley.Mavryk.Address
import Morley.Mavryk.Address.Alias
import Morley.Util.Named
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Actions
import Test.Cleveland.Internal.Client (ClientM, mkMorleyOnlyRpcEnvNetwork)
import Test.Cleveland.Internal.Hedgehog
import Test.Cleveland.Internal.Pure (PureM)
import Test.Cleveland.Internal.Scenario
import Test.Cleveland.Lorentz.Types
import Test.Cleveland.Tasty.Internal
import Test.Cleveland.Util
