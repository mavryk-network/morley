-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Internally used typeclass for operations-related actions
module Test.Cleveland.Internal.Actions.MonadOps
  ( module Test.Cleveland.Internal.Actions.MonadOps
  ) where

import Test.Cleveland.Internal.Actions.Originate
import Test.Cleveland.Internal.Actions.Transfer

-- | Synonym typeclass for monads where network operations can occur.
--
-- This has instances for @MonadCleveland@ and @ClevelandOpsBatch@ contexts.
--
-- Practically, if you want to use 'transfer' or 'originate' in a monad, add a
-- 'MonadOps' constraint on it, f. ex.:
--
-- > callEp1 :: MonadOps m => ContractHandle MyParam () () -> Integer -> m ()
-- > callEp1 ch = transfer ch . calling #entrypoint1
class (MonadTransfer m, MonadOriginate m) => MonadOps m
instance (MonadTransfer m, MonadOriginate m) => MonadOps m

-- Note: 'MonadOps' is a typeclass and not a constraint synonym because GHC (as
-- of 9.0) is being silly and complaining about impredicative polymorphism when
-- defining a constraint synonym with quantified constraints.
