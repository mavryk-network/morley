-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module allows the use of "Test.Cleveland" tests in @tasty@.
--
-- These tests can be run on:
--
-- * the "Morley.Michelson.Runtime" emulator using
--   'testScenarioOnEmulator'/@scenarioEmulated@
-- * both on the emulator and a real Mavryk network using
--   'testScenario'/@scenario@.
--
-- 'whenNetworkEnabled' can be used to write tests that need
-- to run a Mavryk network, but are not necessarily written using "Test.Cleveland".
--
-- If a @TestTree@ contains many tests scheduled to run on a real Mavryk network,
-- those tests will be run sequentially.
--
-- Example:
--
-- > import Test.Cleveland
-- > import Test.Cleveland.Tasty
-- >
-- > main :: IO ()
-- > main = clevelandMain test
-- >
-- > test :: TestTree
-- > test = testScenario "storage is 1" $ scenario do
-- >   addr <- originate OriginateData {..}
-- >   getStorage addr @@== 1
--
-- A cleveland/tasty test suite can be run in one of three modes, by setting either the @--cleveland-mode@
-- command line option or the @TASTY_CLEVELAND_MODE@ environment variable to:
--
-- * @all@ - runs all tests: non-cleveland tests, cleveland emulator tests and cleveland network tests.
-- * @disable-network@ - skips cleveland network tests.
-- * @only-network@ - runs only cleveland network tests and skips all other tests.
--
-- In a CI environment (i.e. if the @CI@ environment variable is set to @true@ (case-insensitive) or @1@),
-- the default mode is @all@.
--
-- Otherwise, the default mode is @disable-network@.
module Test.Cleveland.Tasty
  (
  -- * Main
    clevelandMain
  , clevelandMainWithIngredients
  , clevelandIngredients
  , loadTastyEnv

  -- * Test cases
  , testScenario
  , testScenarioOnEmulator
  , testScenarioOnNetwork

  , whenNetworkEnabled

  -- * Reading/setting options
  , modifyNetworkEnv
  ) where

import Test.Cleveland.Tasty.Internal
