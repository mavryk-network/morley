-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.Emulated
  ( test_Emulated
  ) where

import Test.Tasty (TestTree, testGroup)

import Morley.Mavryk.Address
import Test.Cleveland

import TestSuite.Util (shouldFailWithMessage)

test_Emulated :: [TestTree]
test_Emulated =
  [ testGroup "branchout"
    [ testScenarioOnEmulator "passes if all branches pass" $ scenarioEmulated $
        branchout
          [ "a" ?- pass
          , "b" ?- pass
          ]

    , testScenarioOnEmulator "fails if any branch fails" $ scenarioEmulated do
        addr <- newAddress auto
        let branchA = "a" ?- transfer addr [mv|0u|]
        let branchB = "b" ?- pass
        branchout [branchA, branchB] & expectTransferFailure emptyTransaction
        branchout [branchB, branchA] & expectTransferFailure emptyTransaction

    , testScenarioOnEmulator "a branch's effects do not leak into another branch" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- do
              transfer test [mv|2u|]
              getBalance test @@== initialBalance + 2
          , "b" ?- do
              transfer test [mv|3u|]
              getBalance test @@== initialBalance + 3
          ]

    , testScenarioOnEmulator "branchout's effects are discarded" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        branchout
          [ "a" ?- transfer test [mv|3u|] ]
        getBalance test @@== initialBalance

    , testScenarioOnEmulator "adds branch name to error" $ scenarioEmulated do
        branchout
          [ "<branch name>" ?- void $
              signBytes "" (AddressWithAlias [ta|mv1PURPrAE6NfZpVKUqsSek1YLNnZYmvtKkW|] "unknown-addr")
          ]
          & shouldFailWithMessage "<branch name>"
    ]
  , testGroup "offshoot"
    [ testScenarioOnEmulator "fails if inner scenario fails" $ scenarioEmulated do
        addr <- newAddress auto
        offshoot "a" (transfer addr [mv|0u|])
          & expectTransferFailure emptyTransaction

    , testScenarioOnEmulator "offshoot's effects are discarded" $ scenarioEmulated do
        test <- newAddress auto
        initialBalance <- getBalance test
        offshoot "a" $ transfer test [mv|1u|]
        getBalance test @@== initialBalance
    ]
  ]
