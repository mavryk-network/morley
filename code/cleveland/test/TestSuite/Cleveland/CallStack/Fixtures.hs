-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.CallStack.Fixtures
  ( dummyPropWithPureError
  , dummyPropWithNestedPureError
  ) where

import Hedgehog (Property, property)
import Morley.Mavryk.Core
import Test.Cleveland

dummyPropWithPureError :: Property
dummyPropWithPureError = property $ testScenarioProps $ scenario do
    error "Pure error" @== (1 :: Int)

dummyPropWithNestedPureError :: Property
dummyPropWithNestedPureError = property $ testScenarioProps $ scenario do
    (10 - 11 :: Mumav) @== 0
