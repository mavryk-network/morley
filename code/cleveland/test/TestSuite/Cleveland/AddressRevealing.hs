-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.AddressRevealing
  ( test_AddressRevealing
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland

test_AddressRevealing :: TestTree
test_AddressRevealing =
  testScenario "New address key is revealed" $ scenario do
    test <- newAddress auto
    test2 <- newFreshAddress auto

    transfer test [mv|1000u|]
    comment "new key address is revealed"
    withSender test $ transfer test2 [mv|1u|]
