-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Calls from empty implicit addresses are disallowed
module TestSuite.Cleveland.EmptyImplicitAddress
  ( test_EmptyImplicitAddressCalls
  , test_EmptyImplicitAddressTransfers
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland

import TestSuite.Util

test_EmptyImplicitAddressCalls :: TestTree
test_EmptyImplicitAddressCalls =
  testScenario "Calling a contract with empty implicit address fails" $ scenario do
    addr <- newFreshAddress auto
    contract <- originate "contract" () $ idContract @() @()
    transfer contract (calling def ())
      & withSender addr
      & shouldFailWithMessage "Empty implicit contract"

test_EmptyImplicitAddressTransfers :: TestTree
test_EmptyImplicitAddressTransfers =
  testScenario "Transferring from empty implicit address fails" $ scenario do
    addr <- newFreshAddress auto
    recvr <- newAddress auto
    transfer recvr [mv|0|]
      & withSender addr
      & shouldFailWithMessage "Empty implicit contract"
