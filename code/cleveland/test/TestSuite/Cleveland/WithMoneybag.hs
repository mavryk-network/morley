-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.WithMoneybag
  ( test_withMoneybag
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland

test_withMoneybag :: TestTree
test_withMoneybag =
  testScenario "withMoneybag overrides the funder of new accounts" $ scenario do
    newMoneybag <- newFreshAddress "newMoneybag"
    transfer newMoneybag [mv|4|]

    -- At the end of this test, we want to check that `newAddress` transfers funds from "newMoneybag" to "testAccount".
    -- However, this will only happen if:
    --   1. the "testAccount" alias is not yet known to @mavkit-client@
    --   2. or, it is known, but it has less than 0.5 XTZ.
    -- We use `newFreshAddress` here to ensure "testAccount" is a known alias with 0 XTZ.
    _ <- newFreshAddress "testAccount"

    -- Check that "testAccount" was funded with "newMoneybag"'s tez.
    b0 <- getBalance newMoneybag
    withMoneybag newMoneybag $ newAddress "testAccount"
    b1 <- getBalance newMoneybag
    checkCompares b1 (<) b0
