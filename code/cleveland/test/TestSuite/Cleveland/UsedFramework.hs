-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.UsedFramework
  ( test_usedFramework
  , test_whenEmulation
  ) where

import Test.Tasty (TestTree, testGroup)

import Test.Cleveland

test_usedFramework :: TestTree
test_usedFramework = testGroup "getRunMode"
  [ testScenarioOnNetwork "On chain" $ scenario do
      whenEmulation $ failure "Returned wrong mode"

  , testScenarioOnEmulator "Cleveland on emulator" $ scenario do
      whenNetwork $ failure "Returned wrong mode"

  , testScenarioOnEmulator "Emulated on emulator" $ scenarioEmulated do
      whenNetwork $ failure "Returned wrong mode"
  ]

test_whenEmulation :: TestTree
test_whenEmulation = testGroup "whenEmulation"
  [ testScenarioOnEmulator "Allows using emulation-only actions within" $ scenario do
      whenEmulation $ setVotingPowers (mkVotingPowers [])

  , testScenarioOnNetwork "Is skipped during on-chain run" $ scenario do
      whenEmulation $ failure "Sad"
  ]
