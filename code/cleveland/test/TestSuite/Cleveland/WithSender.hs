-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.WithSender
  ( test_Money_are_spent_from_sender
  , test_Sender_in_contract_call_is_updated
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland
import TestSuite.Util (idContract, saveSender)

test_Money_are_spent_from_sender :: TestTree
test_Money_are_spent_from_sender =
  -- Running this on emulator won't work because there
  -- originations and transfers cost nothing
  testScenarioOnNetwork "Money is spent from the sender's address" $ scenario do
    user <- newAddress auto

    balance1 <- getBalance user
    withSender user $ originate "contract" () $ idContract @() @()
    balance2 <- getBalance user
    checkCompares balance2 (<) balance1

    balance3 <- getBalance user
    withSender user $ inBatch $ originate "contract" () $ idContract @() @()
    balance4 <- getBalance user
    checkCompares balance4 (<) balance3

test_Sender_in_contract_call_is_updated :: TestTree
test_Sender_in_contract_call_is_updated =
  testScenario "Transfer is made from the sender's address" $ scenario do
    user <- newAddress auto

    contract <- originate "contract" [] saveSender

    withSender user do
      transfer contract
      transfer contract [mv|0u|]
      transfer contract
      inBatch $ transfer contract

    getStorage contract @@== toAddress <$> replicate 4 user
