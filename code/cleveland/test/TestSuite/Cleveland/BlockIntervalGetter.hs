-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.BlockIntervalGetter
  ( test_BlockIntervalGetter
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland

test_BlockIntervalGetter :: TestTree
test_BlockIntervalGetter =
  testScenario "Approximate block interval can be obtained" $ scenario do
    comment "Getting approximate block interval"
    blockInterval <- getApproximateBlockInterval
    comment "Wait for obtained interval amount of time"
    advanceTime blockInterval
