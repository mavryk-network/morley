-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on importing functionality.
module TestSuite.Cleveland.Import
  ( test_Import
  , test_Embed
  , test_ImportInt
  , test_EmbedInt
  , test_ImportAddress
  , test_EmbedAddress
  , test_ImportMumav
  , test_EmbedMumav
  ) where

import Test.Tasty (TestTree)

import Lorentz.Value (Address)
import Morley.Mavryk.Address (ta)
import Test.Cleveland
import Test.Cleveland.Lorentz (embedContract, embedValue, importValue)

test_Import :: [TestTree]
test_Import =
  [ testScenario "Can embed a contract" $ scenario do
      contract <- originate "basic1" []
        $$(embedContract @() @[Integer] @() "../../contracts/basic1.mv")

      transfer contract
  ]

test_Embed :: [TestTree]
test_Embed =
  [ testScenario "Can embed a contract" $ scenario do
      contractCode <- importContract @() @[Integer] @() "../../contracts/basic1.mv"
      contract <- originate "basic1" [] contractCode

      transfer contract
  ]

test_ImportInt :: [TestTree]
test_ImportInt =
  [ testScenarioOnEmulator "Can import an int value" $ scenario do
      value <- runIO $ importValue @Integer "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedInt :: [TestTree]
test_EmbedInt =
  [ testScenarioOnEmulator "Can embed an int value" $ scenario do
      let value = $$(embedValue @Integer "./test/fixtures/intValue.txt")

      value @== 42
  ]

test_ImportAddress :: [TestTree]
test_ImportAddress =
  [ testScenarioOnEmulator "Can import an address value" $ scenario do
      value <- runIO $ importValue @Address "./test/fixtures/addrValue.txt"

      value @== toAddress [ta|mv1QiG7R8QULdSWqXiFAvn3nPeBorVedDLap|]
  ]

test_EmbedAddress :: [TestTree]
test_EmbedAddress =
  [ testScenarioOnEmulator "Can embed an address value" $ scenario do
      let value = $$(embedValue @Address "./test/fixtures/addrValue.txt")

      value @== toAddress [ta|mv1QiG7R8QULdSWqXiFAvn3nPeBorVedDLap|]
  ]

test_ImportMumav :: [TestTree]
test_ImportMumav =
  [ testScenarioOnEmulator "Can import a mumav value" $ scenario do
      value <- runIO $ importValue @Mumav "./test/fixtures/intValue.txt"

      value @== 42
  ]

test_EmbedMumav :: [TestTree]
test_EmbedMumav =
  [ testScenarioOnEmulator "Can embed a mumav value" $ scenario do
      let value = $$(embedValue @Mumav "./test/fixtures/intValue.txt")

      value @== 42
  ]
