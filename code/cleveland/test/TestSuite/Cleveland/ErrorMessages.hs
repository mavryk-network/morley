-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.ErrorMessages
  ( test_error_message_shows_contract_alias
  ) where

import Lorentz as L
import Test.Cleveland
import Test.Tasty (TestTree)
import TestSuite.Util (shouldFailWithMessage)

test_error_message_shows_contract_alias :: TestTree
test_error_message_shows_contract_alias =
  testScenario "Error message shows contract alias" $ scenario do
    ch <- originate "my-contract-alias" () $ L.defaultContract @() @() $ L.car # L.failWith
    shouldFailWithMessage "(my-contract-alias) failed with: Unit" do
      transfer ch
