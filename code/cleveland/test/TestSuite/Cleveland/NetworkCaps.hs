-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.NetworkCaps
  ( test_network_caps
  ) where

import Test.Tasty (TestTree)

import Morley.Client qualified as Client
import Test.Cleveland

test_network_caps :: TestTree
test_network_caps = testScenarioOnNetwork "Network scenario can get NetworkEnv" $ scenarioNetwork do
  env <- getMorleyClientEnv
  chainID <- getChainId
  chainID' <- runIO $ Client.runMorleyClientM env Client.getChainId
  chainID @== chainID'
