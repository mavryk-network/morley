-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for the initial state configuration of emulated scenarios

module TestSuite.Cleveland.InitialState
  ( test_init
  ) where

import Hedgehog (property, withTests)
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Morley.Mavryk.Core (ChainId(..), Timestamp(..))
import Test.Cleveland

test_init :: IO [TestTree]
test_init = do
  let expectedNow = Timestamp 1000_000
  let expectedLevel = 2255
  pure
    [ testProperty "Sets initial configuration and check it works as expected in emulator" $
        withTests 10 $ property $ testScenarioProps $
          withInitialLevel expectedLevel $
          withInitialNow expectedNow $ scenario $ do
            (getNow @@== expectedNow)
            (getLevel @@== expectedLevel)

    , testScenarioOnEmulator "Sets initial configuration and check the envirionment is as expected in emulated unit tests"
        $ withInitialNow expectedNow
        $ withInitialLevel expectedLevel
        $ scenario $ do
            getLevel @@== expectedLevel
            getNow @@== expectedNow

    , testScenarioOnEmulator "Sets initial configuration and check the envirionment is as expected in emulated unit tests"
        $ withInitialNow expectedNow
        $ withInitialLevel expectedLevel
        $ scenarioEmulated $ do
            getLevel @@== expectedLevel
            getNow @@== expectedNow

    , testScenarioOnEmulator "Sets MINIMAL_BLOCK_DELAY"
        $ withMinBlockTime 123
        $ scenarioEmulated $ getMinBlockTime @@== 123

    , testScenarioOnEmulator "Sets CHAIN_ID"
        $ withChainId (UnsafeChainId "\01\02\03\04")
        $ scenarioEmulated $ getChainId @@== UnsafeChainId "\01\02\03\04"
    ]
