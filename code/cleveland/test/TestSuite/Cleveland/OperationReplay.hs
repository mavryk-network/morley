-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.OperationReplay
  ( test_replayOrigination_fails
  , test_replayTransfer_fails
  ) where

import Test.Tasty (TestTree)

import Morley.Mavryk.Address (Address)
import Test.Cleveland
import TestSuite.Util (idContract, shouldFailWithMessage)
import TestSuite.Util.Contracts

test_replayTransfer_fails :: TestTree
test_replayTransfer_fails =
  testScenarioOnEmulator "Transfer operation replay is prohibited" $ scenarioEmulated do
    dummyAddress <- toAddress . chAddress <$> originate "dummy" () (idContract @() @())
    contract <- importContract @Address @() @() (contractsDir </> "replay_transfer.mv")
    replayTransfer <- originate "replayTransfer" () contract
    transfer replayTransfer (calling def dummyAddress)
      & shouldFailWithMessage "Operation replay attempt"

test_replayOrigination_fails :: TestTree
test_replayOrigination_fails =
  testScenarioOnEmulator "Origination operation replay is prohibited" $ scenarioEmulated do
    contract <- importContract @() @() @() (contractsDir </> "replay_origination.mv")
    replayOrigination <- originate "replayOrigination" () contract
    transfer replayOrigination
      & shouldFailWithMessage "Operation replay attempt"
