-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Test for the calculation of an address from its public key. See [#354]
module TestSuite.Cleveland.PublicKeyToAddress
  ( test_publicKeyToAddress
  ) where

import Hedgehog (forAll, property, withTests)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Unsafe qualified (fromJust)

import Lorentz as L hiding (comment)
import Test.Cleveland
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract (SpecificOrDefaultAlias(..))

publicKeyToAddress :: Contract PublicKey (Maybe Address) ()
publicKeyToAddress = defaultContract $
  L.car #
  L.hashKey #
  L.implicitAccount #
  L.address #
  L.some #
  L.nil #
  L.pair

test_publicKeyToAddress :: TestTree
test_publicKeyToAddress =
  testProperty "The address is calculated correctly from the given public key" $
    withTests 200 $ property $ do
      alias <- fromString <$> (forAll $ Gen.string (Range.linear 0 100) Gen.unicode)
      testScenarioProps $ scenario do
        c <- originate "publicKeyToAddress" Nothing publicKeyToAddress

        addr1 <- newAddress $ SpecificAlias alias
        addr1pk <- getPublicKey addr1

        transfer c $ calling def addr1pk
        addr2 <- Unsafe.fromJust <$> getStorage @(Maybe Address) c

        toAddress addr1 @== addr2
