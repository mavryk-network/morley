-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.ContractAllocation
  ( test_ContractAllocation
  ) where

import Test.Tasty (TestTree)

import Morley.Mavryk.Address
import Morley.Util.Named
import Test.Cleveland

import TestSuite.Cleveland.Lorentz.Contracts.ContractAllocator

test_ContractAllocation :: TestTree
test_ContractAllocation =
  testScenario "Contracts can be allocated and called" $ scenario do
    addresses :: [Address] <- replicateM 10 $ toAddress <$> newFreshAddress auto
    let storage = #storage :! (take 570 $ cycle addresses)
    allocatorAddr <- originate "allocator" storage allocatorContract [mv|25micro|]

    transfer allocatorAddr $ calling def (take 25 $ cycle addresses)
